jQuery(document).ready(function($){
	var faqListURL = "./faq-list-api/";
	
	var faqPanel = $(".accordion");
	
	var system = $(".system-val").val();
	
	$.ajax({
		async: false,
		url : faqListURL+system,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(data) {
			faqPanel.html('');
			
			$.each(data, function(){
				faqPanel.append(prepareFAQ(this.question, this.answer));// inside list there are question and answer property
			});
		},
		error : function(err) {
			console.log(err)
		}
	});
	
	$(".accordion > li:first-child").children("div").show();
	
	$(".accordion > li").click(function() {
		$(this).siblings("li").children("div").slideUp(300);
		if (false == $(this).children("div").is(":visible")) {
			$(this).children("div").slideUp(300)
		}
		$(this).children("div").slideToggle(300)
	});
	
	$(".accordion > li:eq(0)").show();
});

function prepareFAQ(question, answer){
	var div = $("<li> <h2>"+ question +"?</h2> <div class=content> <p >"+ answer +" </p> </div> </li>")
 
	return div;
}