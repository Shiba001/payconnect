<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
	var logListURL = "./log-files";
	var downloadURL = "./log-download";
	var tbody = $("#log-body");
	
	$.ajax({
		method: 'POST',
		url : logListURL,
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(data) {
			$.each(data, function(){
				var URL = downloadURL+"?fileName="+this.fileName;
				
				var tr = $("<tr><td><a href = '"+ URL +"'>" + this.fileName + "</a></td><td>"+ this.dateModified +"</td></tr>");
				
				tbody.append(tr);
				
				
				console.log(this.fileName + ' ' + this.dateModified );
			});
		},
		error : function(err) {
			console.log(err);
		}
	});
});
</script>
<title>Payconnect Log Files</title>
</head>
<body>
	<div class="log-table" style="margin: 10% 20% 0; width: 100%;" >
		<table id="log" border="1" width="50%" style="text-align : center;">
			<thead style="background-color: gray; color : white;">
				<tr>
					<th>File Name</th>
					<th>Last Modified</th>
				</tr>
			</thead>
			<tbody id="log-body">
			</tbody>
		</table>
	</div>
</body>
</html>