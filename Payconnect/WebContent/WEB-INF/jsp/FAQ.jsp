<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta charset=utf-8>
<title>FAQs - PayConnect - Mobile App</title>
<link href=resources/css/style.css rel=stylesheet />
<script type=text/javascript src=resources/js/jquery-latest.js></script>
<script type=text/javascript src=resources/js/faq.js></script>
</head>
<body>
	<div class=wrapper>
		<section class=genInfo> <header>
		<h1>Frequently Asked Questions</h1>
		</header>
		<div class=container>
			<ul class=accordion>
				
			</ul>
		</div>
		</section>
	</div>
	<input type="hidden" class="system-val" id="system" name="system" value="${system}">
</body>
</html>