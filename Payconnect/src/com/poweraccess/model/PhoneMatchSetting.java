package com.poweraccess.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*@Entity
@Table(name = "PHONE_NUMBER_MANAGEMENT")*/
public class PhoneMatchSetting implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int featureID;
	private String phonenumbersame;
	private String phonenumberdifferent;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	public int getFeatureID() {
		return featureID;
	}

	public void setFeatureID(int featureID) {
		this.featureID = featureID;
	}
	
	@Column(name = "PHONENUMBERSAME")
	public String getPhonenumbersame() {
		return phonenumbersame;
	}

	public void setPhonenumbersame(String phonenumbersame) {
		this.phonenumbersame = phonenumbersame;
	}

	@Column(name = "PHONENUMBERDIFFERENT")
	public String getPhonenumberdifferent() {
		return phonenumberdifferent;
	}

	public void setPhonenumberdifferent(String phonenumberdifferent) {
		this.phonenumberdifferent = phonenumberdifferent;
	}

}
