package com.poweraccess.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;



/*@Entity
@Table(name="VERSION_MANAGEMENT")*/
public class VersionManagement implements Serializable{
	private static final long serialVersionUID = -723583058586873479L;
	
	private int versionID;
	private double version;
	private String  upgrade;
	private Date entryDate;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="VERSION_ID")
	public int getVersionID() {
		return versionID;
	}
	public void setVersionID(int versionID) {
		this.versionID = versionID;
	}
	
	@Column(name="VERSION")
	@NumberFormat(style = Style.NUMBER)
	public double getVersion() {
		return version;
	}
	public void setVersion(double version) {
		this.version = version;
	}
	
	@Column(name="UPGRADE")
	public String getUpgrade() {
		return upgrade;
	}
	public void setUpgrade(String upgrade) {
		this.upgrade = upgrade;
	}
	
	//@Temporal(TemporalType.DATE)
	@Column(name="ENTRY_DATE")
	public Date getEntryDate() {
		return entryDate;
	}
	
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	
	
}
