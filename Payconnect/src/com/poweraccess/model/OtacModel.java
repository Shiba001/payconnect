package com.poweraccess.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;

import com.axisbankcmspaypro.entity.BaseEntity;

import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "OTAC")
public class OtacModel implements Serializable {

	private static final long serialVersionUID = -723583058586873479L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "USER_ID")
	private String userID;

	@Column(name = "OTAC")
	private String otac;

	@Column(name = "REGISTERED_PHONE")
	private String registeredPhone;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "create_date", columnDefinition = "TIMESTAMP DEFAULT SYSDATE", updatable = true, insertable = true)
	private Timestamp createDate;

	@Column(name = "isMatch")
	private String isMatch;

	@Column(name = "sms_receive_date", columnDefinition = "TIMESTAMP", updatable = true, insertable = true)
	private Timestamp smsReceiveDate;

	@Column(name = "enc_str")
	private String encStr;

	@Column(name = "verify_date", columnDefinition = "TIMESTAMP", updatable = true, insertable = true)
	private Timestamp verifyDate;

	@Column(name = "mobile_no")
	private String mobileNo;

	@Column(name = "dec_str")
	private String decStr;

	@Column(name = "device_id")
	private String deviceId;

	@Column(name = "isValidated")
	private String isValidated;

	// add new field system
	@Column(name = "system")
	private String system;

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setOtac(String otac) {
		this.otac = otac;
	}

	public void setRegisteredPhone(String registeredPhone) {
		this.registeredPhone = registeredPhone;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public void setIsMatch(String isMatch) {
		this.isMatch = isMatch;
	}

	public void setSmsReceiveDate(Timestamp smsReceiveDate) {
		this.smsReceiveDate = smsReceiveDate;
	}

	public void setEncStr(String encStr) {
		this.encStr = encStr;
	}

	public void setVerifyDate(Timestamp verifyDate) {
		this.verifyDate = verifyDate;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public void setDecStr(String decStr) {
		this.decStr = decStr;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setIsValidated(String isValidated) {
		this.isValidated = isValidated;
	}

	public String getUserID() {
		return userID;
	}

	public String getOtac() {
		return otac;
	}

	public String getRegisteredPhone() {
		return registeredPhone;
	}

	public String getStatus() {
		return status;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public String getIsMatch() {
		return isMatch;
	}

	public Timestamp getSmsReceiveDate() {
		return smsReceiveDate;
	}

	public String getEncStr() {
		return encStr;
	}

	public Timestamp getVerifyDate() {
		return verifyDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public String getDecStr() {
		return decStr;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public String getIsValidated() {
		return isValidated;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OtacModel other = (OtacModel) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OtacModel [id=" + id + "]";
	}
	
	
	
	

}
