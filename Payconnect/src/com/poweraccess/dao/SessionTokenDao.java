package com.poweraccess.dao;


import com.poweraccess.model.SessionToken;

public interface SessionTokenDao {
	
	public void addSessionToken(SessionToken sessionToken);
	
	//public void updateSessionToken(SessionToken sessionToken);
	
	public void deleteSessionToken(SessionToken sessionToken);
	
	public SessionToken getSessionToken(String userID);
	
	public SessionToken getUserByToken(SessionToken sessionToken);
	
	public void insertSessionToken(SessionToken sessionToken);
}
