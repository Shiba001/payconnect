package com.poweraccess.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;



import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.common.CriteriaValue;
import com.enumeration.Operator;

public abstract class GenericDAO<E, PK extends Serializable> implements
		Serializable {

	private static final long serialVersionUID = -7617241776030190675L;

	private Class<E> entityClass;

	@Autowired
	@Qualifier(value="sessionFactory")
	protected SessionFactory sessionFactory;


	public GenericDAO(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	public void create(E entity) {
		sessionFactory.getCurrentSession().persist(entity);
	}

	public void update(E entity) {
		sessionFactory.getCurrentSession().update(entity);
	}

	public void delete(E entity) {

		sessionFactory.getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<E> findAll() {
		Criteria cr = sessionFactory.getCurrentSession().createCriteria(
				entityClass);
		return cr.list();
	}

	@SuppressWarnings("unchecked")
	public E find(PK id) {
		return (E) sessionFactory.getCurrentSession().get(entityClass, id);
	}

	/**
	 * 
	 * @param criterias
	 * @param startIndex
	 * @param maxIndex
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<E> search(Map<String, CriteriaValue> criterias,
			Integer startIndex, Integer maxIndex) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				entityClass);
		Junction conditionGroup = Restrictions.conjunction();

		if (criterias != null && !criterias.isEmpty()) {
			// Set<String> keys = criterias.keySet();

			for (String key : criterias.keySet()) {
				CriteriaValue criteriaValue = criterias.get(key);

				if (criteriaValue != null
						&& criteriaValue.getOperator() != null
						&& criteriaValue.getValuObject() != null) {
					if (criteriaValue.getOperator() == Operator.eq) {
						conditionGroup.add(Restrictions.eq(key,
								criteriaValue.getValuObject()));
					} else if (criteriaValue.getOperator() == Operator.gt) {
						conditionGroup.add(Restrictions.gt(key,
								criteriaValue.getValuObject()));
					} else if (criteriaValue.getOperator() == Operator.le) {
						conditionGroup.add(Restrictions.le(key,
								criteriaValue.getValuObject()));
					}
				}
			}
		}
		criteria.add(conditionGroup);

		if (startIndex != null && maxIndex != null && startIndex > 0
				&& maxIndex > 0 && startIndex <= maxIndex) {
			criteria.setFirstResult(startIndex);
			criteria.setMaxResults(maxIndex);
		}

		return criteria.list();
	}

	public Class<E> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}
	
	

	
	
	
	


}