package com.poweraccess.dao;

import java.util.List;

import com.poweraccess.model.OtacModel;
import com.poweraccess.model.PhoneMatchSetting;

public interface OtacDao {
	
	public void saveOtac(OtacModel otac);
	public void updateOtac(OtacModel otac);
	public OtacModel checkOtacExist(String userId);
	public OtacModel checkOtac(String otac, String userID);
	public OtacModel getLastOtac(String userId);
	public List getLastOtacList(String userId, String systemValue); 
	
	public List getLastOtacListForMatchReceipent(String userId); 
	
	
	public PhoneMatchSetting getPhoneConfig();
	
	public void insertOtac(OtacModel otac);
}
