package com.poweraccess.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.poweraccess.model.SessionToken;

@Repository("sessionTokenDao")
public class SessionTokenDaoImpl implements SessionTokenDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	

	public void addSessionToken(SessionToken objSessonToken){
		sessionFactory.getCurrentSession().saveOrUpdate(objSessonToken);
		
	}
	
	/*public void updateSessionToken( SessionToken objSessionToken){
		
		//sessionFactory.getCurrentSession().update( objSessionToken);
		sessionFactory.getCurrentSession().createQuery("UPDATE SessionToken set sessionToken= '"+objSessionToken.getSessionToken()+"' WHERE userID = '"+objSessionToken.getUserID() +"' ").executeUpdate();
	}
	*/
	public void deleteSessionToken(SessionToken objSessionToken){
		sessionFactory.getCurrentSession().createQuery("DELETE FROM SessionToken WHERE userID = '"+ objSessionToken.getUserID()+"' ").executeUpdate();
	}
	
	public SessionToken getSessionToken(String userID){
		
		return (SessionToken) sessionFactory.getCurrentSession().createQuery("FROM SessionToken WHERE userID = '" + userID + "'").uniqueResult();
		
	}
	
	public SessionToken getUserByToken(SessionToken sessionToken){
		return (SessionToken) sessionFactory.getCurrentSession().createQuery("FROM SessionToken WHERE sessionToken = '" + sessionToken.getSessionToken() + "' and userID ='" + sessionToken.getUserID() + "' ").uniqueResult();
	}
	
	@Override
	public void insertSessionToken(SessionToken sessionToken) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().persist(sessionToken);
		
	}
}
