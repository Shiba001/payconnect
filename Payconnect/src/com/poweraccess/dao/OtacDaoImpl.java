package com.poweraccess.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.poweraccess.model.OtacModel;
import com.poweraccess.model.PhoneMatchSetting;
import com.poweraccess.model.SessionToken;


@Repository("otacDao")
public class OtacDaoImpl implements OtacDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional 
	public void saveOtac(OtacModel otac){
		
		sessionFactory.getCurrentSession().save(otac);
	}
	
	@Transactional 
	public void updateOtac(OtacModel otac){
		
		sessionFactory.getCurrentSession().update(otac);//update(otac);
	}
	
	public OtacModel checkOtacExist(String userId) {
		
		return (OtacModel) sessionFactory.getCurrentSession().createQuery("FROM OtacModel WHERE userID = '" + userId + "'").uniqueResult();
	}
	
	public OtacModel checkOtac(String otac, String userID) {
		
		return (OtacModel) sessionFactory.getCurrentSession().createQuery("FROM OtacModel WHERE otac = '" + otac + "' and userID = '"+userID+"'").uniqueResult();
	}
	
//	public OtacModel getLastOtac(String userId) {
//		
//		return (OtacModel) sessionFactory.getCurrentSession().createQuery("FROM OtacModel WHERE userID = '" + userId + "' AND ROWNUM = 1 ORDER BY createDate DESC").uniqueResult();
//		//return (OtacModel) sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM (SELECT * FROM axis.otac WHERE user_id = '" + userId + "' ORDER BY create_date DESC) WHERE rownum =1").uniqueResult();
//	}
	
	public List getLastOtacList(String userId, String systemValue) {
		
		return (List) sessionFactory.getCurrentSession().createQuery("FROM OtacModel WHERE system='"+systemValue +"' AND userID = '" + userId + "'  ORDER BY createDate DESC").list();
		//return (OtacModel) sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM (SELECT * FROM axis.otac WHERE user_id = '" + userId + "' ORDER BY create_date DESC) WHERE rownum =1").uniqueResult();
	}
	
	
	
	public List getLastOtacListForMatchReceipent(String userId) {
		
		return (List) sessionFactory.getCurrentSession().createQuery("FROM OtacModel WHERE  userID = '" + userId + "'  ORDER BY createDate DESC").list();
		//return (OtacModel) sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM (SELECT * FROM axis.otac WHERE user_id = '" + userId + "' ORDER BY create_date DESC) WHERE rownum =1").uniqueResult();
	}
	
	public PhoneMatchSetting getPhoneConfig() {
		
		return (PhoneMatchSetting) sessionFactory.getCurrentSession().createQuery("FROM PhoneMatchSetting WHERE ROWNUM = 1").uniqueResult();
	}

	
	/**
	 * This method is to create otac details to db
	 * @author Supratim Sarkar
	 * @param OtacModel otac
	 */
	@Override
	public void insertOtac(OtacModel otac) {
		sessionFactory.getCurrentSession().persist(otac);
	}
	
	/**
	 * This method is to get latest otac details from db
	 * @author Supratim Sarkar
	 * @param userId
	 */
	public OtacModel getLastOtac(String userId) {
		
		String hql= "FROM OtacModel otac WHERE otac.userID=:userID and AND ROWNUM = 1 ORDER BY otac.createDate DESC";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("userID", userId);
		
		return (OtacModel) query.uniqueResult();
		
	}
}
