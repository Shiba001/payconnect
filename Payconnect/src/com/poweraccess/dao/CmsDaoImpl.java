package com.poweraccess.dao;


import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.poweraccess.model.VersionManagement;

@Repository("cmsDao")
public class CmsDaoImpl implements CmsDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	@Transactional 
	public void saveVersion(VersionManagement version){
		sessionFactory.getCurrentSession().saveOrUpdate(version);
	}
	
	@Transactional 
	@SuppressWarnings("unchecked")
	public List<VersionManagement> getVersions(){
		return (List<VersionManagement>) sessionFactory.getCurrentSession().createCriteria(VersionManagement.class).addOrder(Order.desc("versionID")).list();
		
	}
	
	@Transactional 
	public VersionManagement getVersion(VersionManagement objVersion){
		
		return (VersionManagement) sessionFactory.getCurrentSession().createQuery("FROM VersionManagement WHERE version = " + objVersion.getVersion() +" ").uniqueResult();
	}
	
	@Transactional 
	public VersionManagement getLatestVersion(){
		
		double dblVersion = (Double)sessionFactory.getCurrentSession().createQuery("select max(version) FROM VersionManagement ").uniqueResult();
		return (VersionManagement) sessionFactory.getCurrentSession().createQuery("FROM VersionManagement WHERE version = '" + dblVersion + "'").uniqueResult();
		//return (VersionManagement) sessionFactory.getCurrentSession().createQuery("FROM VersionManagement").uniqueResult();
	}
	
	

	

}
