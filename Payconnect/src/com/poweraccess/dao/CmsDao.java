package com.poweraccess.dao;

import java.util.List;



import com.poweraccess.model.VersionManagement;

public interface CmsDao {
	
	
	public void saveVersion(VersionManagement version);
	public List<VersionManagement> getVersions();
	public VersionManagement getVersion(VersionManagement objVersion);
	public VersionManagement getLatestVersion();
	
	
}
