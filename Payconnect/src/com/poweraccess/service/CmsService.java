package com.poweraccess.service;

import java.util.List;



import com.poweraccess.model.VersionManagement;

public interface CmsService {
    	
	public void addVersion(VersionManagement version);
	public List<VersionManagement> versionList();
	public VersionManagement getVersion(VersionManagement objVersion);
	public VersionManagement getLatestVersion();
	
	
}
