package com.poweraccess.service;

import com.poweraccess.model.SessionToken;

public interface SessionTokenService {
	
	public void addSessionToken(SessionToken sessionToken);
	
	//public void updateSessionToken(SessionToken sessionToken);
	
	public void deleteToken(SessionToken objSessionToken);
	
	public SessionToken getSessionToken(String userID);
	
	public SessionToken getUserByToken(SessionToken sessionToken);
	
	public void insertSessionToken(String userID,String sessionToken);
}
