package com.poweraccess.service;

import java.util.List;

import com.poweraccess.model.OtacModel;
import com.poweraccess.model.PhoneMatchSetting;

public interface OtacService {
	public void addOtac(OtacModel otac);

	public void updateOtac(OtacModel otac);

	public OtacModel getOtacByUser(String userId);

	public OtacModel getOtac(String otac, String userId);

	public OtacModel getLastOtac(String userId);

	public List getLastOtacList(String userId, String systemValue);

	public List getLastOtacListForMatchReceipent(String userId);

	public PhoneMatchSetting getPhoneConfig();

	public void insertOtac(String otac, String userID, String registeredPhone,
			String status, String isMatch, String mobileNo, String decStr,
			String deviceId, String isValidated, String system);
}
