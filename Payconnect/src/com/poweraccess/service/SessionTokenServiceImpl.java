package com.poweraccess.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.poweraccess.dao.SessionTokenDao;
import com.poweraccess.model.SessionToken;

@Service("sessionTokenService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SessionTokenServiceImpl implements SessionTokenService{
	@Autowired
	private SessionTokenDao sessionTokenDao;
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void addSessionToken(SessionToken sessionToken){
		sessionTokenDao.addSessionToken(sessionToken);
	}
	
	/*@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void updateSessionToken(SessionToken objSessionToken){
		sessionTokenDao.updateSessionToken(objSessionToken);
	}
	*/
	public void deleteToken(SessionToken objSessionToken){
		
		sessionTokenDao.deleteSessionToken(objSessionToken);
	}
	
	public SessionToken getSessionToken(String userID){
		return	sessionTokenDao.getSessionToken(userID);
	}
	
	public SessionToken getUserByToken(SessionToken sessionToken){
		return sessionTokenDao.getUserByToken(sessionToken);
	}

	@Override
	public void insertSessionToken(String userID,String sessionToken) {
		// TODO Auto-generated method stub
		
		SessionToken token = new SessionToken();
		token.setUserID(userID);
		token.setSessionToken(sessionToken);
		sessionTokenDao.insertSessionToken(token);
		
	}
}
