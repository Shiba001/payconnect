package com.poweraccess.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.poweraccess.dao.CmsDao;
import com.poweraccess.model.VersionManagement;

@Service("cmsService")

@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)

public class CmsServiceImpl implements CmsService{
	@Autowired
	private CmsDao cmsDao;
	
	
	public void addVersion(VersionManagement objVersion){
		cmsDao.saveVersion(objVersion);
	}
	
	public List<VersionManagement> versionList(){
		return cmsDao.getVersions();
	}
	
	public VersionManagement getVersion(VersionManagement objVersion){
		return cmsDao.getVersion(objVersion);
	}
	
	public VersionManagement getLatestVersion(){
		return cmsDao.getLatestVersion();
	}
	
	
	
}
