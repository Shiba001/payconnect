package com.poweraccess.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.poweraccess.dao.OtacDao;
import com.poweraccess.model.OtacModel;
import com.poweraccess.model.PhoneMatchSetting;

@Service("otacService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class OtacServiceImpl implements OtacService {

	@Autowired
	private OtacDao otacDao;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void addOtac(OtacModel otac) {

		otacDao.saveOtac(otac);
	}

	public void updateOtac(OtacModel otac) {

		otacDao.updateOtac(otac);
	}

	public OtacModel getOtacByUser(String userId) {
		return otacDao.checkOtacExist(userId);
	}

	public OtacModel getOtac(String otac, String userId) {
		return otacDao.checkOtac(otac, userId);
	}

	public OtacModel getLastOtac(String userId) {
		return otacDao.getLastOtac(userId);
	}

	public List getLastOtacList(String userId, String systemValue) {
		return (List) otacDao.getLastOtacList(userId, systemValue);
	}

	public List getLastOtacListForMatchReceipent(String userId) {
		return (List) otacDao.getLastOtacListForMatchReceipent(userId);
	}

	public PhoneMatchSetting getPhoneConfig() {
		return otacDao.getPhoneConfig();
	}

	@Override
	public void insertOtac(String otac, String userID, String registeredPhone,
			String status, String isMatch, String mobileNo, String decStr,
			String deviceId, String isValidated, String system) {
		// TODO Auto-generated method stub

		
		OtacModel otacModel = new OtacModel();
		otacModel.setOtac(otac);
		otacModel.setUserID(userID);
		otacModel.setRegisteredPhone(registeredPhone);
		otacModel.setStatus(status);
		otacModel.setIsMatch(isMatch);
		otacModel.setMobileNo(mobileNo);
		otacModel.setDecStr(decStr);
		otacModel.setDeviceId(deviceId);
		otacModel.setIsValidated(isValidated);
		otacModel.setSystem(system);

		otacDao.insertOtac(otacModel);

	}

}
