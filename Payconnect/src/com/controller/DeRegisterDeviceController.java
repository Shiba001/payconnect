package com.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.axisbankcmspaypro.business.AppDeRegisterDeviceBusiness;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.DeRegisterDevice;

@Controller
public class DeRegisterDeviceController {
	
private static final Logger logger = Logger.getLogger(DeRegisterDeviceController.class);
	
	
	@Autowired
	public MessageUtil messageUtil;
	
	@Autowired
	private AppDeRegisterDeviceBusiness appDeRegisterDeviceBusiness;
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/deregisterdevice", method = RequestMethod.POST)
	public @ResponseBody JSONObject deRegisterDevice(HttpServletRequest request) throws Exception{
		if(logger.isInfoEnabled()){
			logger.info("deRegisterDevice start");
			logger.info("/api/deregisterdevice API");
		}
		
		HashMap<String, String> headerMap = (new Common()).getHeadersInfo(request);
		
	
		String userId = Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("user_id"));
		
		String sessionToken = Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("session_token"));
	
		String system = Common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
		
		String deviceID = Common.decode(Common.PRIVATE_KEY,request.getParameter("deviceid"));
		
		JSONObject jsonObject = new JSONObject();
		
		try {
			DeRegisterDevice deRegisterDevice = new DeRegisterDevice();
			deRegisterDevice.setSystem(system);
			deRegisterDevice.setUserId(userId);
			deRegisterDevice.setSessionToken(sessionToken);
			deRegisterDevice.setDeviceId(deviceID);
			
			//System.out.println("system is >>>>>>>>> "+deRegisterDevice.getSystem());
			
			 jsonObject= appDeRegisterDeviceBusiness.deRegisterDevice(deRegisterDevice);
		} catch (Exception e) {
			
			//System.out.println("inside catch block !!!!!!!!!");

			 e.printStackTrace();
			 
			 logger.error("deregisterdevice error"+e);
			 
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			 jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		}
		
		if(logger.isInfoEnabled()){
			logger.info("deRegisterDevice End");
		}
		
		return jsonObject;
	}

	
	

}
