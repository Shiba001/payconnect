package com.controller;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.VerifyDeviceBusiness;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.UnlockDeviceModel;

@Controller
public class UnlockDeviceController {
	
	
	private final Logger logger = Logger.getLogger(UnlockDeviceController.class);

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private VerifyDeviceBusiness verifyDeviceBusiness;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/unlockdevice", method = RequestMethod.POST)
	public void unlockDevice(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws IOException {

		JSONObject jsonObject = new JSONObject();

		try {

			if (logger.isInfoEnabled()) {
				logger.info("unlockdevice start");
				
			}

			UnlockDeviceModel unlockDeviceModel=new UnlockDeviceModel();

			unlockDeviceModel.setDeviceId(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("deviceid")));
			
			unlockDeviceModel.setUnlockCode(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("unlockcode")));

						
		   UnlockDeviceModel deviceModel= verifyDeviceBusiness.UnlockDevice(unlockDeviceModel.getDeviceId(),unlockDeviceModel.getUnlockCode());
		   
		  
		   
		   if(! deviceModel.getUnlockCode().equalsIgnoreCase(unlockDeviceModel.getUnlockCode())){
			   
			   jsonObject.put("ResponseCode", "201");
				jsonObject.put("Responsedetails", "Invalid code");
		   }

		   else if (Objects.nonNull(deviceModel)) {

				jsonObject.put("ResponseCode", "200");
				jsonObject.put("Responsedetails", "success");
				
				verifyDeviceBusiness.activeDevice(unlockDeviceModel.getDeviceId());
				

			}

		

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.getWriter().write(jsonObject.toJSONString());

		}

		catch (DataNotFound e) {
			e.printStackTrace();
			 httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			 httpServletResponse.getWriter().write(responseGenerator("201", e.getMessage()));
			 logger.info("unlockdevice DataNotFound Exception: ", e);
		}

		catch (InvalidInputException e) {
			e.printStackTrace();
			 httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			 httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
			 logger.info("unlockdevice InvalidInputException", e);
			
		} catch (Exception e) {
			e.printStackTrace();

			 httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			httpServletResponse.getWriter().write(somethingWenWrong());

			logger.info("unlockdevice API Exception: ", e);
		}

		finally {
			if (logger.isInfoEnabled()) {
				logger.info("resetPassword End");
			}
		}

	}

	@SuppressWarnings("unchecked")
	private String somethingWenWrong() {
		final JSONObject jsonObject = new JSONObject();
		jsonObject.put(messageUtil.getBundle("response.code"),
				messageUtil.getBundle("wrong.error.code"));
		jsonObject.put(messageUtil.getBundle("error"),
				messageUtil.getBundle("wrong.message"));

		return jsonObject.toJSONString();
	}
	
	 //this method is used to generate JSON response
    @SuppressWarnings("unchecked")
    private final String responseGenerator(String responseCode, String responseDetails){
	final JSONObject jsonObject = new JSONObject();
	
	jsonObject.put("ResponseCode", responseCode);
	jsonObject.put("ResponseDetails", responseDetails);
	
	return jsonObject.toJSONString();
    }


}
