package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.poweraccess.model.SessionToken;
import com.sun.jersey.api.client.ClientResponse;

@Controller
public class ProductList {
	
	
	private final Logger logger = Logger.getLogger(ProductList.class);

	@Autowired
	private MessageUtil messageUtil;

	
	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;
	

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	
	
	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;


	@Value("${error.400}")
	private String error400;
	
	@Value("${error.401}")
	private String error401;
	
	@Value("${error.402}")
	private String error402;
	
	@Value("${error.403}")
	private String error403;
	
	@Value("${error.404}")
	private String error404;
	
	@Value("${error.405}")
	private String error405;
	
	@Value("${error.408}")
	private String error408;
	
	@Value("${error.500}")
	private String error500;
	
	@Value("${error.502}")
	private String error502;
	
	@Value("${error.503}")
	private String error503;
	
	@Value("${error.504}")
	private String error504;
	
	@Value("${error.other}")
	private String errorOther;


	
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "api/getproductlist", method = RequestMethod.POST)
	public void getProductList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();

		Gson json = new Gson();

		
		logger.info("getproductlist API");

		try {

		

			@SuppressWarnings("rawtypes")
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			
			String apiVersion = Common.decode(Common.PRIVATE_KEY, request.getParameter("apiversion"));
			
			if (apiVersion == null)
				apiVersion = "";

			String sessionToken = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			if (sessionToken == null)
				sessionToken = "";

			String userId = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			if (userId == null)
				userId = "";
			
			
			String corpId = Common.decode(Common.PRIVATE_KEY,
					 request.getParameter("corpid"));

			if (corpId == null)
				corpId = "";
			
			
			String productList = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("productlist"));

			if (productList == null)
				productList = "";

	

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {
				
				logger.info("power access getproductlist API");

				//System.out.println("inside get product list 1");

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!sessionToken.equals("")) {

					@SuppressWarnings("unused")
					HttpSession powerAccessSessionToken = request.getSession(false);

			
                     @SuppressWarnings("unused")
					String clientIp = Common.getUserIp(request);
                     
                     SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
 							.getPowerAccessSessionToken(userId.trim());


					if (powerSessionTokenByUserId.getSessionToken().equals(sessionToken)) {

						String url = apiUrlPayconnect + "getProductList";

						String inputString = Common.productListApiInputString(apiVersion, corpId, userId, productList, systemValueFromApi);
						
                        ClientResponse client_response = Common.payconnectApiAcess(url, inputString, "post");
						
						//System.out.println("Axis Server Status for getproductlist:" + client_response.getStatus());

						logger.info("Axis Server Status for getproductlist:" + client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							
							JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
							
							logger.info("Axis api response for getproductlist:" + jsonObject.toString());
							
							jsonObject = Common.load(jsonObject);
							
							String jsonStringFinal = jsonObject.toString();
							
						
							
							out.print(jsonStringFinal);

						} else {

							JSONObject jsonObject = new JSONObject();
							
							String errorMsg = this.getServerErrorMsg(responseStatus);
							
							jsonObject.put("ResponseCode", responseStatus);
							
							jsonObject.put("Responsedetails", errorMsg);

							logger.info(responseStatus + " : " + errorMsg);
							
							jsonObject = Common.load(jsonObject);
							
							String jsonStringFinal = jsonObject.toString();
							
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");
						
						map.put("error", "Invalid SessionToken");
						
						logger.info("498 : Invalid SessionToken.");
						
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						
						JSONObject jsonObject = (JSONObject) obj;
						
						jsonObject = Common.load(jsonObject);
						
						String jsonStringFinal = jsonObject.toString();
						
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "498");
					
					map.put("error", "SessionToken is Missing");

					logger.info("498 : SessionToken is Missing.");
					
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					
					JSONObject jsonObject = (JSONObject) obj;
					
					jsonObject = Common.load(jsonObject);
					
					String jsonStringFinal = jsonObject.toString();
					
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInController == 2) {

				logger.info("paypro getproductlist API");
				
				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!sessionToken.equals("")) {

					@SuppressWarnings("unused")
					HttpSession payProSessionToken = request.getSession(false);

					@SuppressWarnings("unused")
					String clientIp = Common.getUserIp(request);

					

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness.getPayproSessionTokenByUser(userId.trim());


					if (payProSessionTokenByUserId.getSessionToken().equals(sessionToken)) {

						String url = apiUrlPayconnect + "getProductList";

						
						String inputString = Common.productListApiInputString(apiVersion, corpId, userId, productList, systemValueFromApi);

						ClientResponse client_response = Common.payconnectApiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getproductlist:"+ client_response.getStatus());
						
						String jsonString = client_response.getEntity(String.class);
						
						String responseStatus = String.valueOf(client_response.getStatus());
						
						//System.out.println("Axis Server Status for getproductlist response status:" + responseStatus);

						if (responseStatus.equals("200")) {
							
							Object obj = parser.parse(jsonString);
							
							JSONObject jsonObject = (JSONObject) obj;
							
							logger.info("getproductlist api response from axis api "+ jsonObject.toString());
							
							// jsonObject.put("ip", c.getIp(request));//change
							
							jsonObject = Common.load(jsonObject);
							
							String jsonStringFinal = jsonObject.toString();
							
							out.print(jsonStringFinal);
						} else {
						
							JSONObject jsonObject = new JSONObject();
							
							String errorMsg = this.getServerErrorMsg(responseStatus);
							
							jsonObject.put("ResponseCode", responseStatus);
							
							jsonObject.put("Responsedetails", errorMsg);
							
							
							logger.info(responseStatus + " : " + errorMsg);
							
							jsonObject = Common.load(jsonObject);
							
							String jsonStringFinal = jsonObject.toString();
							
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");
						
						map.put("error", "Invalid SessionToken");
						
						// map.put("ip", c.getIp(request));//change
						
						logger.info("498 : Invalid SessionToken.");
						
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						
						JSONObject jsonObject = (JSONObject) obj;
						
						jsonObject = Common.load(jsonObject);
						
						String jsonStringFinal = jsonObject.toString();
						
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "498");

					map.put("error", "SessionToken is Missing");

					// map.put("ip", c.getIp(request));//change

					logger.info("498 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("Getproductlist API Exception: ", e);

		}

		out.close();
	}


	
	
	
	
	// To generate server error msg
		public String getServerErrorMsg(String errorCode) {
			HashMap<String, String> errorMap = new HashMap<String, String>();
			errorMap.put("400", error400);
			errorMap.put("401", error401);
			errorMap.put("402", error402);
			errorMap.put("403", error403);
			errorMap.put("404", error404);
			errorMap.put("405", error405);
			errorMap.put("408", error408);
			errorMap.put("500", error500);
			errorMap.put("502", error502);
			errorMap.put("503", error503);
			errorMap.put("504", error504);

			if (errorMap.get(errorCode) != null) {
				return (String) errorMap.get(errorCode);
			} else {
				return errorOther;
			}

		}


}
