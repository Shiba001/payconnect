package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.CheckSmsConfirmationBusiness;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PayconnectOtacBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.axisbankcmspaypro.entity.PhoneMatchSetting;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.payconnect.model.PayconnectOtacModel;
import com.poweraccess.model.OtacModel;
import com.poweraccess.model.SessionToken;

@Controller
public class CheckSmsConfirmationController {
	
	
	private final Logger logger = Logger.getLogger(CheckSmsConfirmationController.class);

	@Autowired
	private MessageUtil messageUtil;

	
	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;
	

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	
	@Autowired
	private PayconnectOtacBusiness PayconnectOtacBusiness;
	
    @Autowired
	private CheckSmsConfirmationBusiness checkSmsConfirmationBusiness;
	

	


	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/checksmsconfirmation", method = RequestMethod.POST)
	public void checkSmsConfirmation(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		//System.out.println("inside checksmsconfirmation controller ");

		PrintWriter out = response.getWriter();

		Gson json = new Gson();

		
		logger.info("checkSmsConfirmation API");

		try {
			
			
         HashMap<String, String> headerMap = (new Common()).getHeadersInfo(request);
			
		
			
			String sessionToken = Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("session_token"));
			
			//System.out.println(sessionToken);
			
			if (sessionToken == null)
				sessionToken = "";
			
			String userId =Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("user_id"));
			
			if (userId == null)
				userId = "";
			
			//System.out.println(userId);

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
			
			
			
			if (systemValueFromApi == null)
				systemValueFromApi = "";
			


			if (systemValueFromApi.equalsIgnoreCase("1")) {

				logger.info("inside power access checkSmsConfirmation");

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!sessionToken.equals("")) {

					@SuppressWarnings("unused")
					HttpSession powerAccessSessionToken = request.getSession(false);

			
                 
                     SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
 							.getPowerAccessSessionToken(userId.trim());
                     
                   

					if (powerSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
						
						
					OtacModel otacModel=PayconnectOtacBusiness.getOtacByUserAndSystemValue(userId, systemValueFromApi);
					
					if(otacModel.getVerifyDate()!=null){
						
						map.put("ResponseCode", "202");
						
						map.put("Responsedetails", "Already verified");
						
						logger.info("202 : Already verified");
						
					}
					
					else{
						
						
						if(otacModel.getStatus().equalsIgnoreCase("1")){
							
						
						PhoneMatchSetting phoneMatchSetting=checkSmsConfirmationBusiness.getPhoneMatchSettingsByApplication(systemValueFromApi);
						
						String sameStatus=phoneMatchSetting.getPhonenumbersame();
						
						//System.out.println("sameStatus status is >>>>>> "+sameStatus);
						
						String diffStatus = phoneMatchSetting.getPhonenumberdifferent();
						
						//System.out.println("different status is >>>>>> "+diffStatus);
							
							
							
							
							if(otacModel.getIsMatch().equalsIgnoreCase("1")){
								
								
								if (sameStatus.equalsIgnoreCase("0")) {
									
									map.put("ResponseCode", "203");
									
									map.put("Responsedetails","Success");
									
								} else {
									map.put("ResponseCode", "200");
									
									map.put("Responsedetails","Success");
									

								}
								
								
								
							}
							
							
							else{
								
								
								if (diffStatus.equalsIgnoreCase("0")) {
									
									map.put("ResponseCode", "203");
									
									map.put("Responsedetails", "The sender phone number is not matching with the registered mobile number");
									
									logger.info("203 : The sender phone number is not matching with the registered mobile number.");
									
								} else {
									map.put("ResponseCode", "200");
									
									map.put("Responsedetails","The sender phone number is not matching with the registered mobile number");
									
									
									logger.info("200 : The sender phone number is not matching with the registered mobile number.");
								}
								
							}
							
							
							    String pattern = "yyyy-MM-dd HH:mm:ss";

								SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

								String formattedCreateDate = simpleDateFormat.format(new Date());

								Date date = simpleDateFormat.parse(formattedCreateDate);
							
								
							 PayconnectOtacModel payconnectOtacModel=new PayconnectOtacModel();	
							 
							 payconnectOtacModel.setUserId(userId); 
							 
							 payconnectOtacModel.setSystem(systemValueFromApi);
							 
							 payconnectOtacModel.setVerifyDate(new Timestamp(date.getTime()));

						     PayconnectOtacBusiness.UpdateOctacModelForCheckSmsConfirmation(payconnectOtacModel);
							
							
							
						}
						 
						
						
						else{
							
							map.put("ResponseCode", "202");
							
							map.put("Responsedetails", "Not Received");
							
							logger.info("202 : Not Received");
							
						}
						
					}
					
					
					String jsonString = json.toJson(map);
					
				
					JSONObject jsonObject =(JSONObject) parser.parse(jsonString);
					
					logger.info("power access checksmsconfirmation" +jsonObject.toString());
				
					jsonObject = Common.load(jsonObject);
					
					String jsonStringFinal = jsonObject.toString();
					
					out.print(jsonStringFinal);
						

				

					} else {
						
						map.put("ResponseCode", "498");
						
					    map.put("error", "Invalid SessionToken");
						
						logger.info("498 : Invalid SessionToken.");
						
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						
						JSONObject jsonObject = (JSONObject) obj;
						
						jsonObject = Common.load(jsonObject);
						
						String jsonStringFinal = jsonObject.toString();
						
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "498");
					
					map.put("error", "SessionToken is Missing");

					logger.info("498 : SessionToken is Missing.");
					
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					
					JSONObject jsonObject = (JSONObject) obj;
					
					jsonObject = Common.load(jsonObject);
					
					String jsonStringFinal = jsonObject.toString();
					
					out.print(jsonStringFinal);

				}

			}
			
			

			else if (systemValueFromApi.equalsIgnoreCase("2")) {

				

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!sessionToken.equals("")) {

					@SuppressWarnings("unused")
					HttpSession payProSessionToken = request.getSession(false);

					@SuppressWarnings("unused")
					String clientIp = Common.getUserIp(request);

					

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness.getPayproSessionTokenByUser(userId.trim());

					

					if (payProSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
						
						OtacModel otacModel=PayconnectOtacBusiness.getOtacByUserAndSystemValue(userId, systemValueFromApi);
						
						if(otacModel.getVerifyDate()!=null){
							
							map.put("ResponseCode", "202");
							
							map.put("Responsedetails", "Already verified");
							
							logger.info("202 : Already verified");
							
						}
						
						else{
							
							
							if(otacModel.getStatus().equalsIgnoreCase("1")){
								
							
							PhoneMatchSetting phoneMatchSetting=checkSmsConfirmationBusiness.getPhoneMatchSettingsByApplication(systemValueFromApi);
							
							String sameStatus=phoneMatchSetting.getPhonenumbersame();
							
							//System.out.println("sameStatus status is >>>>>> "+sameStatus);
							
							String diffStatus = phoneMatchSetting.getPhonenumberdifferent();
							
							//System.out.println("different status is >>>>>> "+diffStatus);
								
								
								
								
								if(otacModel.getIsMatch().equalsIgnoreCase("1")){
									
									
									if (sameStatus.equalsIgnoreCase("0")) {
										
										map.put("ResponseCode", "203");
										
										map.put("Responsedetails","Success");
										
									} else {
										map.put("ResponseCode", "200");
										
										map.put("Responsedetails","Success");
										

									}
									
									
									
								}
								
								
								else{
									
									
									if (diffStatus.equalsIgnoreCase("0")) {
										
										map.put("ResponseCode", "203");
										
										map.put("Responsedetails", "The sender phone number is not matching with the registered mobile number");
										
										logger.info("203 : The sender phone number is not matching with the registered mobile number.");
										
									} else {
										map.put("ResponseCode", "200");
										
										map.put("Responsedetails","The sender phone number is not matching with the registered mobile number");
										
										
										logger.info("200 : The sender phone number is not matching with the registered mobile number.");
									}
									
								}
								
								
								    String pattern = "yyyy-MM-dd HH:mm:ss";

									SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

									String formattedCreateDate = simpleDateFormat.format(new Date());

									Date date = simpleDateFormat.parse(formattedCreateDate);
								
									
								 PayconnectOtacModel payconnectOtacModel=new PayconnectOtacModel();	
								 
								 payconnectOtacModel.setUserId(userId); 
								 
								 payconnectOtacModel.setSystem(systemValueFromApi);
								 
								 payconnectOtacModel.setVerifyDate(new Timestamp(date.getTime()));

							     PayconnectOtacBusiness.UpdateOctacModelForCheckSmsConfirmation(payconnectOtacModel);
								
								
								
							}
							 
							
					
							
							
							else{
								
								map.put("ResponseCode", "202");
								
								map.put("Responsedetails", "Not Received");
								
								logger.info("202 : Not Received");
								
							}
							
						}
						
						
						String jsonString = json.toJson(map);
						
					
						JSONObject jsonObject =(JSONObject) parser.parse(jsonString);
					
						jsonObject = Common.load(jsonObject);
						
						String jsonStringFinal = jsonObject.toString();
						
						out.print(jsonStringFinal);
						

				

					} else {
						map.put("ResponseCode", "498");
						
						map.put("error", "Invalid SessionToken");
						
					
						logger.info("498 : Invalid SessionToken.");
						
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						
						JSONObject jsonObject = (JSONObject) obj;
						
						jsonObject = Common.load(jsonObject);
						
						String jsonStringFinal = jsonObject.toString();
						
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "498");

					map.put("error", "SessionToken is Missing");

				  logger.info("498 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("Getproductlist API Exception: ", e);

		}

		out.close();
	}



}
