package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PayconnectOtacBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.poweraccess.model.OtacModel;
import com.poweraccess.model.SessionToken;

@Controller
public class Otacvalidation {
	
	
	private final Logger logger = Logger.getLogger(Otacvalidation.class);


	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private PayconnectOtacBusiness payconnectOtacBusiness;

	
	@Autowired
	private PayconnectOtacBusiness PayconnectOtacBusiness;

	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	

	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;


	@Value("${otac.min}")
	private int otacMin;

	@Value("${otac.max}")
	private int otacMax;

	@Value("${otac.validity.sec}")
	private int otacValidity;

	@Value("${oatc.sms.api.path}")
	private String otacSmsApiPath;

	@Value("${oatc.sms.api.username}")
	private String otacSmsApiUsername;

	@Value("${oatc.sms.api.password}")
	private String otacSmsApiPassword;

	@Value("${oatc.sms.api.ctype}")
	private String otacSmsApiCtype;

	@Value("${oatc.sms.api.alert}")
	private String otacSmsApiAlert;

	@Value("${oatc.sms.api.msgtype}")
	private String otacSmsApiMsgType;

	@Value("${oatc.sms.api.priority}")
	private String otacSmsApiPriority;

	@Value("${oatc.sms.api.sender}")
	private String otacSmsApiSender;

	@Value("${oatc.sms.api.msg}")
	private String otacSmsApiMsg;

	@Value("${oatc.sms.api.decode}")
	private String otacSmsApiDecode;

	@Value("${error.400}")
	private String error400;
	
	@Value("${error.401}")
	private String error401;
	
	@Value("${error.402}")
	private String error402;
	
	@Value("${error.403}")
	private String error403;
	
	@Value("${error.404}")
	private String error404;
	
	@Value("${error.405}")
	private String error405;
	
	@Value("${error.408}")
	private String error408;
	
	@Value("${error.500}")
	private String error500;
	
	@Value("${error.502}")
	private String error502;
	
	@Value("${error.503}")
	private String error503;
	
	@Value("${error.504}")
	private String error504;
	
	@Value("${error.other}")
	private String errorOther;

	@Value("${otac.sms.gateway.server}")
	private String smsServer;
	
	
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/otacvalidation", method = RequestMethod.POST)
	public void validateOtac(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();

	

		Gson json = new Gson();

		

		logger.info("Validation otac API");

		try {
			
			@SuppressWarnings("rawtypes")
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String userID = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			

			if (userID == null)
				userID = "";

			String session_token = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

		

			if (session_token == null)
				session_token = "";

			String otac = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("otac"));

			//System.out.println("otac is " + otac);

			if (otac == null)
				otac = "";

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			//System.out.println("system value is " + systemValueFromApi);

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {

				logger.info("inside power access  otac Validation ");

				JSONParser parser = new JSONParser();

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

				

					HttpSession powerAccessSessionToken = request.getSession(false);

                   String clientIp = Common.getUserIp(request);
					
					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness.getPowerAccessSessionToken(userID.trim());

					

					if (powerSessionTokenByUserId.getSessionToken().equals(session_token)) {

						if (userID.equals("") || otac.equals("")) {

							map.put("ResponseCode", "202");

							map.put("Responsedetails","Otac or user id can't be blank");

							logger.info("202 : Otac or user id can't be blank");

						}

						else {
							/******* Otac Implementation Start *******/

							

							OtacModel otacModel = payconnectOtacBusiness.getOtacByUserAndSystemValue(userID,systemValueFromApi);

							if (otacModel != null) {

								if (otac.equals(otacModel.getOtac())) {

									if (otacModel.getIsValidated().equals("0")) {

										Date d1 = null;

										Date d2 = null;

										SimpleDateFormat format = new SimpleDateFormat(
												"MM/dd/yyyy HH:mm:ss");

										String currentDate = format
												.format(new Date());

										String createDate = format
												.format(otacModel
														.getCreateDate());

										d1 = format.parse(String
												.valueOf(createDate));

										d2 = format.parse(currentDate);

										// in milliseconds
										long diff = d2.getTime() - d1.getTime();

										long diffSeconds = diff / 1000;

										System.out
												.println("difference second >>>>>>>>>>>> "
														+ otacValidity);

										System.out
												.println("difference second +++++++++++ >>>>>>>>>>>> "
														+ diffSeconds);

										if (diffSeconds < otacValidity) {

											otacModel.setIsValidated("1");

											// otacModel.setIsValidated("1");//logic
											// here

											// otacService.updateOtac(otacModel);//logic
											// here

											payconnectOtacBusiness
													.updateOtac(otacModel);

											map.put("ResponseCode", "200");

											map.put("Responsedetails",
													"Success");

											map.put("Userid", userID);

											map.put("sessiontoken",
													session_token);

											logger.info("200 : Validation otac API-Success");

										} else {

											map.put("ResponseCode", "202");

											map.put("Responsedetails",
													"OTP Expired");

											logger.info("202 : OTP Expired");
										}

									} else {
										map.put("ResponseCode", "202");

										map.put("Responsedetails",
												"OTP is already used");

										logger.info("202 : OTP is already used");
									}
								} else {
									map.put("ResponseCode", "202");

									map.put("Responsedetails", "Invalid OTP");

									logger.info("202 : Invalid OTP");
								}

							} else {
								map.put("ResponseCode", "202");

								map.put("Responsedetails",
										"User ID and System Value are can't be null");

								logger.info("202 : User ID can't be null");
							}

							/******* Otac Implementation End *******/

						}

						String jsonString = json.toJson(map);
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = Common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						logger.info("power access  otac Validation final response "+jsonStringFinal);
						out.print(jsonStringFinal);

					}

					else {

						map.put("ResponseCode", "498");

						map.put("error", "Invalid Session Token.");

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = Common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {

					map.put("error", "Session Token is missing.");
					// map.put("ip", c.getIp(request));
					logger.info("210 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = Common.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			if (systemValueInController == 2) {

				logger.info("inside paypro otac Validation ");

				JSONParser parser = new JSONParser();

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession payProSessionToken = request.getSession(false);


					String clientIp = Common.getUserIp(request);

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness.getPayproSessionTokenByUser(userID.trim());

					/*
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userID)
					 * .getSessionToken().equals(session_token)) {
					 */

					if ( payProSessionTokenByUserId.getSessionToken().equals(session_token)) {

						if (userID.equals("") || otac.equals("")) {

							map.put("ResponseCode", "202");

							map.put("Responsedetails",
									"Otac or user id can't be blank");

							logger.info("202 : Otac or user id can't be blank");

						} else {
							/******* Otac Implementation Start *******/

						

							OtacModel otacModel = payconnectOtacBusiness
									.getOtacByUserAndSystemValue(userID,
											systemValueFromApi);

							if (otacModel != null) {

								if (otac.equals(otacModel.getOtac())) {

									if (otacModel.getIsValidated().equals("0")) {

										Date d1 = null;
										Date d2 = null;

										SimpleDateFormat format = new SimpleDateFormat(
												"MM/dd/yyyy HH:mm:ss");
										String currentDate = format
												.format(new Date());
										String createDate = format
												.format(otacModel
														.getCreateDate());

										d1 = format.parse(String
												.valueOf(createDate));
										d2 = format.parse(currentDate);

										// in milliseconds
										long diff = d2.getTime() - d1.getTime();

										long diffSeconds = diff / 1000;

										if (diffSeconds < otacValidity) {

											// otacModel.setIsValidated("1");

											// otacService.updateOtac(otacModel);

											otacModel.setIsValidated("1");

											payconnectOtacBusiness
													.updateOtac(otacModel);

											map.put("ResponseCode", "200");

											map.put("Responsedetails",
													"Success");

											logger.info("200 : Validation otac API-Success");

											map.put("Userid", userID);

											map.put("sessiontoken",
													session_token);

										} else {

											map.put("ResponseCode", "202");

											map.put("Responsedetails",
													"Otac Expired");

											logger.info("202 : Otac Expired");
										}

									} else {
										map.put("ResponseCode", "202");
										map.put("Responsedetails",
												"Otac is already used");

										logger.info("202 : Otac is already used");
									}
								} else {
									map.put("ResponseCode", "202");
									map.put("Responsedetails", "Invalid Otac");

									logger.info("202 : Invalid Otac");
								}

							} else {

								map.put("ResponseCode", "202");

								map.put("Responsedetails",
										"Otac or user id can't be null");

								logger.info("202 : User ID can't be null");
							}

							/******* Otac Implementation End *******/

						}

						String jsonString = json.toJson(map);
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						
						logger.info("paypro otac Validation final response "+jsonStringFinal);
						
						out.print(jsonStringFinal);

					} else {
						map.put("ResponseCode", "498");

						map.put("error", "Invalid Session Token.");

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = Common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");

					map.put("error", "Session Token is missing.");

					logger.info("498 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("In the otac Validation API Exception: ", e);

		}
		out.close();
	}

	


}
