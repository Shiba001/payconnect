package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.AppDeRegisterDeviceBusiness;
import com.axisbankcmspaypro.business.AppErrorCodeBusiness;
import com.axisbankcmspaypro.business.AppMenuBusiness;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.poweraccess.model.SessionToken;

@Controller
public class MenuController {
	
	
	@Autowired
	private AppMenuBusiness appMenuBusiness;
	
	@Autowired
	public MessageUtil messageUtil;
	
	@Autowired
	private AppErrorCodeBusiness appErrorCodeBusiness;
	
	@Autowired
	private AppDeRegisterDeviceBusiness appDeRegisterDeviceBusiness;
	
	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;
	
	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	
	private static final Logger logger = Logger.getLogger(MenuController.class);
	
	
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "api/getmenuitem", method = RequestMethod.POST)
	public void corpBatchIdList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		if(logger.isInfoEnabled()){
			
			logger.info("MenuController API - (api/MenuController) -Start");
		}
		
		PrintWriter out = response.getWriter();

		try {

			HashMap<String, String> headerMap = (new Common()).getHeadersInfo(request);
			
		
			
			String session_token = Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("session_token"));
			
			//System.out.println(session_token);
			
			if (session_token == null)
				session_token = "";
			
			String userid =Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			
			//System.out.println(userid);

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
			
			
			
			if (systemValueFromApi == null)
				systemValueFromApi = "";
			

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			
			
			
			
			 if (systemValueInController == 1) {
				 
				 logger.info("power access MenuController API");
				 
				 @SuppressWarnings("unused")
				HttpSession powerAccessSessionToken = request.getSession(false);

					//String clientIp = Common.getUserIp(request);

					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userid.trim());

			    if (!session_token.equals("")) {
			    	
				
				if (powerSessionTokenByUserId.getSessionToken().equals(session_token)) {
					
					
					JSONObject	jsonObject = appMenuBusiness.getAllMenuByUser(userid, systemValueFromApi);
					
					
					    response.getWriter().write(jsonObject.toString());
				
						
						
				} else {
					
				    JSONObject jsonObject = new JSONObject();
				    
				    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
				    jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    out.print(jsonObject.toString());
				}
			} else {
			    JSONObject jsonObject = new JSONObject();
			    
			    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
			    jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));
					
			    logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));
					
			    out.print(jsonObject.toString());
			}
		}
			
			
			
			
			
			else if (systemValueInController == 2) {
				
				 logger.info("inside paypro MenuController");
				
				@SuppressWarnings("unused")
				HttpSession payProSessionToken = request.getSession(false);
				
				

			    if (!session_token.equals("")) {
			    	
				PayProSessionToken payProSessionTokenByUserId=payProSessionTokenBusiness.getPayproSessionTokenByUser(userid.trim());
				
				if (payProSessionTokenByUserId.getSessionToken().equals(session_token)) {
					
					
					JSONObject	jsonObject = appMenuBusiness.getAllMenuByUser(userid, systemValueFromApi);
					
					
					
					    response.getWriter().write(jsonObject.toString());
					
						
						
				} else {
				    JSONObject jsonObject = new JSONObject();
				    
				    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
				    jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    out.print(jsonObject.toString());
				}
			} else {
			    JSONObject jsonObject = new JSONObject();
			    
			    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
			    jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));
					
			    logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));
					
			    out.print(jsonObject.toString());
			}
		}
			
			
			
	} catch (Exception e) {
	    e.printStackTrace();
			
	    JSONObject jsonObject = new JSONObject();
			
	    jsonObject.put("error", messageUtil.getBundle("wrong.message"));
			
	    String jsonStringFinal = jsonObject.toString();
			
	    out.print(jsonStringFinal);
			
	    logger.error("MenuController API Exception", e);
		
	} finally{
	    if(logger.isInfoEnabled()){
		logger.info("MenuController API - (api/MenuController) -End");
	    }
	}
		
}
	
	
	
  

}
