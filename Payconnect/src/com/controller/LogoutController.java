package com.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.AppLogOutBusiness;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.Logout;
import com.payconnect.url.intercepter.IPTracker;

@Controller
public class LogoutController implements IPTracker{
	
	@Autowired
	private AppLogOutBusiness appLogOutBusiness;
	
	@Autowired
	private MessageUtil messageUtil;
	
	private final Logger logger = Logger.getLogger(AccountList.class);
	
	/**
	 * this URL is used to log out from the system
	 * @since 04-Mar-2016
	 * @author Avishek Seal
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/logout", method = RequestMethod.POST)
	public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		//System.out.println("inside new logout !!!!!!!!!!!");
		
		logger.info("inside logout api");
		
		
	    try {
		final Common common = new Common();
		
		@SuppressWarnings("rawtypes")
		HashMap headerMap = (common).getHeadersInfo(request);

		String session_token = null;
		
		if(headerMap.get("session_token") != null) {
			session_token = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("session_token"));
		}

		String userid = null;
		
		if(headerMap.get("user_id") != null){
			userid = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
		}

		String mpin = null;
		
		if(request.getParameter("mpin") != null) {
			mpin = Common.decode(Common.PRIVATE_KEY, (String) request.getParameter("mpin"));
		}

		String systemValueFromApi = null;
		
		if(request.getParameter("system") != null) {
			systemValueFromApi = Common.decode(Common.PRIVATE_KEY, request.getParameter("system"));
		}

		String deviceID = null;
		
		if(request.getParameter("deviceid") != null) {
			deviceID = Common.decode(Common.PRIVATE_KEY, request.getParameter("deviceid"));
		}
		
		Logout logout = new Logout();
		logout.setSessionToken(session_token);
		logout.setSystem(systemValueFromApi);
		logout.setUserId(userid);
		logout.setDeviceId(deviceID);
		logout.setMpin(mpin);
		
		SESSION_IP_POOL.remove(session_token);
		
		response.getWriter().write(appLogOutBusiness.logoutBusiness(logout).toJSONString());
	    } catch (Exception e) {
	    	e.printStackTrace();
	    logger.info("logout api error "+e);	
		final JSONObject jsonObject = new JSONObject();
		
		jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
		jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		
		response.getWriter().write(jsonObject.toJSONString());
	    }
	}

}
