package com.controller;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.RmInformationBusiness;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.RMInformationModel;

@Controller
public class RmController {
	
	
	private final Logger logger = Logger.getLogger(UnlockDeviceController.class);

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private RmInformationBusiness rmInformationBusiness;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/rminformation", method = RequestMethod.GET)
	public void rmInformation(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws IOException {

		JSONObject jsonObject = new JSONObject();

		try {

			if (logger.isInfoEnabled()) {
				logger.info("rmInformation start");
				
			}

			

			
						
		   RMInformationModel rmInformationModel= rmInformationBusiness.getRmInformation();
		   
		 
		   
		   if(Objects.nonNull(rmInformationModel)){
			   
			 

				jsonObject.put("SupportDesk", rmInformationModel.getSupportDesk());
				
				jsonObject.put("RmName", rmInformationModel.getRmName());
				
				jsonObject.put("Email", rmInformationModel.getEmail());
				
				jsonObject.put("PhoneNumber", rmInformationModel.getPhoneNo());
				
				jsonObject.put("ResponseCode", "200");
				
				jsonObject.put("Responsedetails", "Success");
			   
				
		   }
		   
		  
		 

		   httpServletResponse.setStatus(HttpServletResponse.SC_OK);
		   
			JSONObject finalJson=Common.load(jsonObject);
			
			httpServletResponse.getWriter().write(finalJson.toJSONString());

		}

	

	 catch (Exception e) {
			e.printStackTrace();

			logger.info("rmInformation: ", e);
		}

		finally {
			if (logger.isInfoEnabled()) {
				logger.info("rmInformation End");
			}
		}

	}

}
