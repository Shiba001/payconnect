/**
 * @author Supratim Sarkar
 *
 */

package com.controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.poweraccess.model.SessionToken;
import com.sun.jersey.api.client.ClientResponse;




@Controller
public class BalanceComparisonController {
	
	private final Logger logger = Logger.getLogger(BalanceComparisonController.class);
	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;
	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	@Autowired
	private MessageUtil messageUtil;
	
	
	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;
	
	@Value("${error.400}")
	private String error400;
	@Value("${error.401}")
	private String error401;
	@Value("${error.402}")
	private String error402;
	@Value("${error.403}")
	private String error403;
	@Value("${error.404}")
	private String error404;
	@Value("${error.405}")
	private String error405;
	@Value("${error.408}")
	private String error408;
	@Value("${error.500}")
	private String error500;
	@Value("${error.502}")
	private String error502;
	@Value("${error.503}")
	private String error503;
	@Value("${error.504}")
	private String error504;
	@Value("${error.other}")
	private String errorOther;
	
	
	
	/**
	 * This class is use to get balance comparison 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	
	@SuppressWarnings({ "unused", "unchecked" })
	@RequestMapping(value = "api/batchwiseprtransactionwisebalancecomparison", method = RequestMethod.POST)
	public void balanceComparison(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		if(logger.isInfoEnabled()){
			
			logger.info("batchwiseprtransactionwisebalancecomparison API - (api/batchwiseprtransactionwisebalancecomparison) -Start");
		}
		
		
		PrintWriter out = response.getWriter();

		try {

			HashMap<String, String> headerMap = (new Common()).getHeadersInfo(request);
			String apiVersion = "";
			if (apiVersion == null)
				apiVersion = "";
			
			String session_token = Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			
			String userid =Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			
     		
			
			String corpId =Common.decode(Common.PRIVATE_KEY,request.getParameter("corpid"));
			if (corpId == null)
				corpId = "";
			
			String id = Common.decode(Common.PRIVATE_KEY, request.getParameter("id"));
			if (id == null)
				id = "";
			String isBatch = Common.decode(Common.PRIVATE_KEY,request.getParameter("isbatch"));
			if (isBatch == null)
				isBatch = "";

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {
				
				logger.info("inside power access batchwiseprtransactionwisebalancecomparison");

				//System.out.println("inside batchwiseprtransactionwisebalancecomparison system value 1 !!!!");
				//HttpSession powerAccessSessionToken = request.getSession(false);
				SessionToken powerSessionToken=powerAccessTokenBusiness.getPowerAccessSessionToken(userid.trim());
				//System.out.println(powerSessionToken.getUserID() +" <==> "+ powerSessionToken.getSessionToken() + " <==> " + session_token);
				
				//String clientIp = c.getUserIp(request);
				
				if (!session_token.equals("")) {
					
					/*clientIp.equals(powerAccessSessionToken.getAttribute("client_ip")) &&*/ /*(powerAccessSessionToken.getId()).equals(session_token) &&*/
					/*!(powerAccessSessionToken.isNew()) &&*/
					if ( powerSessionToken.getSessionToken().equals(session_token)) {

						String url = apiUrlPayconnect + "getBalanceCompare"; // URL from axis bank
						//System.out.println("URL: "+url);
						String inputString= Common.getBalanceCompareJSONData(apiVersion, corpId, userid, systemValueFromApi, id,isBatch);
						//System.out.println("InputString Data from my End: "+inputString);
						ClientResponse client_response = Common.payconnectApiAcess(url,inputString, "post");
						//System.out.println("Status from Axis End: "+client_response.getStatus());
						logger.info("Axis Server Status for getBalanceCompare:"+ client_response.getStatus());
						String jsonString = client_response.getEntity(String.class);
						String responseStatus = String.valueOf(client_response.getStatus());

						if (responseStatus.equals("200")) {
							
							//System.out.println("Axis Server Status for batchwiseprtransactionwisebalancecomparison***:"+ client_response.getStatus());
							JSONParser jsonParser = new JSONParser();
							JSONObject jsonObject=(JSONObject) jsonParser.parse(jsonString);
							//System.out.println("axis end json response >>>>>>>>> " + jsonObject.toString());
							logger.info("axis end json response for getBalanceCompare:"+ jsonObject.toString());
							jsonObject = Common.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
							
						} else {
							
							//System.out.println("Error Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info("axis end response for getBalanceCompare:"+responseStatus + " : " + errorMsg);
							//jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
						jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
						//map1.put("ip", c.getIp(request));//change
						logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
						out.print(jsonObject.toString());
					}
				} else {
					
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
					jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));
					//map1.put("ip", c.getIp(request));//change
					logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));
					out.print(jsonObject.toString());
				}

			}
			else if (systemValueInJava == 2) {
				
				logger.info("inside paypro batchwiseprtransactionwisebalancecomparison");


				//System.out.println("inside batchwiseprtransactionwisebalancecomparison system value 2 !!!!");
				//HttpSession powerAccessSessionToken = request.getSession(false);
				//String clientIp = c.getUserIp(request);
				
				if (!session_token.equals("")) {

					PayProSessionToken payProSessionToken=payProSessionTokenBusiness.getPayproSessionTokenByUser(userid.trim());
					//System.out.println(payProSessionToken.getUserId() + payProSessionToken.getSessionToken() + " == " + session_token);
					
					//String clientIp = c.getUserIp(request);
					
					/*clientIp.equals(payProSession.getAttribute("client_ip")) &&*/ /*(payProSession.getId()).equals(session_token) &&*/
					if ( /*!(powerAccessSessionToken.isNew()) &&*/  payProSessionToken.getSessionToken().equals(session_token)) {

						String url = apiUrlPayconnect + "getBalanceCompare"; // URL from axis bank
						//System.out.println("URL: "+url);
						String inputString= Common.getBalanceCompareJSONData(apiVersion, corpId, userid, systemValueFromApi, id,isBatch);
						//System.out.println("InputString Data from my End: "+inputString);
						ClientResponse client_response = Common.payconnectApiAcess(url,inputString, "post");
						//System.out.println("Status from Axis End: "+client_response.getStatus());
						logger.info("Axis Server Status for getBalanceCompare:"+ client_response.getStatus());
						String jsonString = client_response.getEntity(String.class);
						String responseStatus = String.valueOf(client_response.getStatus());

						if (responseStatus.equals("200")) {
							
							//System.out.println("Axis Server Status for getuseraccountreport***:"+ client_response.getStatus());
							JSONParser jsonParser = new JSONParser();
							JSONObject jsonObject=(JSONObject) jsonParser.parse(jsonString);
							logger.info("Axis api json response from getBalanceCompare:"+ jsonObject.toString());
							jsonObject = Common.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
							
						} else {
							
							//System.out.println("Error Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info("Axis api response from getBalanceCompare:"+responseStatus + " : " + errorMsg);
							//jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}
					} else {
						
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
						jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
						//map1.put("ip", c.getIp(request));//change
						logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
						out.print(jsonObject.toString());
					}
				} else {
					
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
					jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));
					//map1.put("ip", c.getIp(request));//change
					logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));
					out.print(jsonObject.toString());
				}
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", messageUtil.getBundle("wrong.message"));
			String jsonStringFinal = jsonObject.toString();
			out.print(jsonStringFinal);
			logger.error("batchwiseprtransactionwisebalancecomparison API Exception", e);
		}
		
		if(logger.isInfoEnabled()){
			
			logger.info("batchwiseprtransactionwisebalancecomparison API - (api/batchwiseprtransactionwisebalancecomparison) -End");
		}
	}
	
	
	
	// To generate server error msg
		public String getServerErrorMsg(String errorCode) {
			HashMap<String,String> errorMap = new HashMap<String,String>();
			errorMap.put("400", error400);
			errorMap.put("401", error401);
			errorMap.put("402", error402);
			errorMap.put("403", error403);
			errorMap.put("404", error404);
			errorMap.put("405", error405);
			errorMap.put("408", error408);
			errorMap.put("500", error500);
			errorMap.put("502", error502);
			errorMap.put("503", error503);
			errorMap.put("504", error504);

			if (errorMap.get(errorCode) != null) {
				return (String) errorMap.get(errorCode);
			} else {
				return errorOther;
			}

		}

}
