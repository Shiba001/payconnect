/**
 * 
 */
package com.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PasswordChangeInWebBusiness;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.payconnect.model.PasswordChangeInWebModel;

/**
 * @author Avishek Seal
 *
 */
@Controller
public class AxisPasswordChangeWebCallBackController {
    
    @Autowired
    private PasswordChangeInWebBusiness passwordChangeInWebBusiness;
    
    /**
     * this URL is used to update the password change in web value for the specific user of a speific system
     * @since 09-Mar-2016
     * @author Avishek Seal
     * @param passwordChangeInWebModel
     * @param httpServletResponse
     * @throws IOException
     */
    @RequestMapping(value = "/update-password-change-in-web", method = RequestMethod.POST)
    public void updatePasswordChangeInWeb(@RequestBody PasswordChangeInWebModel passwordChangeInWebModel, HttpServletResponse httpServletResponse) throws IOException{
	try {
	    passwordChangeInWebBusiness.updatePasswordChangeInWeb(passwordChangeInWebModel);
	    httpServletResponse.setStatus(HttpServletResponse.SC_OK);
	    httpServletResponse.getWriter().write(responseGenerator("200", "Success"));
	} catch (DataNotFound e) {
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
	} catch (InvalidInputException e) {
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
	}  catch (Exception e) {
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(responseGenerator("500", "Internal Server Error"));
	}
	
    }
    
    //this method is used to generate JSON response
    @SuppressWarnings("unchecked")
    private final String responseGenerator(String responseCode, String responseDetails){
	final JSONObject jsonObject = new JSONObject();
	
	jsonObject.put("ResponseCode", responseCode);
	jsonObject.put("ResponseDetails", responseDetails);
	
	return jsonObject.toJSONString();
    }
}
