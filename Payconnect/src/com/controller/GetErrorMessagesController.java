package com.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisbankcmspaypro.business.AppErrorCodeBusiness;
import com.common.Common;
import com.common.MessageUtil;

@Controller
public class GetErrorMessagesController {
	
	
private static final Logger logger = Logger.getLogger(GetErrorMessagesController.class);
	
	
	@Autowired
	public MessageUtil messageUtil;
	@Autowired
	private AppErrorCodeBusiness appErrorCodeBusiness;
	
	
	
	/**
	 * Get Error Code & Messages API
	 * @author Supratim Sarkar
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/geterrormessages", method = RequestMethod.POST)
	public @ResponseBody JSONObject getAllErrorCode(HttpServletRequest request) throws Exception{
		if(logger.isInfoEnabled()){
			logger.info("getAllErrorCode start");
			logger.info("/api/geterrormessages API");
		}
		
		
		String system = Common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
		JSONObject jsonObject = new JSONObject();
		//System.out.println("system>>>>>>>>>"+system);
		try {
			
			jsonObject = appErrorCodeBusiness.getAllErrorCode(system);
			
		} catch (Exception e) {

			 e.printStackTrace();
			 
			 logger.error("geterrormessages error "+e);
			 
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			 jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		}
		
		
		
		if(logger.isInfoEnabled()){
			logger.info("getAllErrorCode End");
		}
		
		return jsonObject;
	}
	
	
}
