package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import java_cup.runtime.lr_parser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayconnectOtacBusiness;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.payconnect.model.PayconnectOtacModel;
import com.poweraccess.model.OtacModel;

@Controller
public class MatchRecipientController {
	
	private final Logger logger = Logger.getLogger(MatchRecipientController.class);

	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private PayconnectOtacBusiness PayconnectOtacBusiness;

	
	@Value("${otac.sms.gateway.server}")
	private String smsServer;
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/matchrecipient", method = RequestMethod.POST)
	public void getProductList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();

		Gson json = new Gson();
		
	
		String recipientMobileNumber =null;
		
		
		String encriptString =null;
		
		String encript_decode = null;
		
		String encript_split[]=null;
		
		String userId=null;
		
		String corpId=null;
		
		String mobileNo=null;
		
		String systemValueFromUI=null;
		
		String deviceId=null;
		
		HashMap< String, String>hashMap=new HashMap<String, String>();
		
		

		
		logger.info("matchRecipient API");

		try {
			
			
			
			if(smsServer.equalsIgnoreCase("axis")){
				
				logger.info("inside axis matchrecipient");
				
				recipientMobileNumber = Common.decode(Common.PRIVATE_KEY, request.getParameter("recipient_mobile_number"));
				
				encriptString =Common.decode(Common.PRIVATE_KEY, request.getParameter("enc_str"));
			}
			
			else{
				
				//System.out.println("inside indus part");
				
				recipientMobileNumber = Common.decode(Common.PRIVATE_KEY, request.getParameter("from"));
				
				  //System.out.println(" recipientMobileNumber mobile no is "+recipientMobileNumber);
				
				encriptString =Common.decode(Common.PRIVATE_KEY, request.getParameter("message"));
				
				//System.out.println("encript string "+encriptString);
				
			}
			
			
			if (recipientMobileNumber.length() > 10) {

					int length = recipientMobileNumber.length();

					int lengthNeeded = length - 10;

					recipientMobileNumber = recipientMobileNumber.substring(lengthNeeded, length);
					
					logger.info("inside axis matchrecipient recipientMobileNumber "+recipientMobileNumber);
				}
			 
			 
			
		     if(recipientMobileNumber==null || encriptString==null){
		    	 
		    	 hashMap.put("ResponseCode", messageUtil.getBundle("matchrecipient.inputs.blank.responsecode"));

		    	 hashMap.put("Responsedetails", messageUtil.getBundle("matchrecipient.inputs"));
				
				
			}
		     
		     
		     
		     else{
		    	 
		    		String[] encript_space_split = encriptString.split(" ");
		    		
		    		
		    		
		    		if(smsServer.equalsIgnoreCase("axis")){
		    			
		    			logger.info("inside axis matchrecipient smsServer ");
		    			
		    			@SuppressWarnings("deprecation")
						String axisDecodedString = URLDecoder.decode(encript_space_split[1]);
		    			
		    			//encript_decode = Common.decode(Common.PRIVATE_KEY, decodedString);
		    			
		    			encript_decode = axisDecodedString;
		    			
		    			logger.info("encript_decode string" + encript_decode);
		    		}
		    		
		    		else{
		    			
		    			
		    			
		    			encript_decode = encript_space_split[1];
		    		}
		    		
		    		
		    		  encript_split = encript_decode.split("~");
		    		 
		    		  deviceId=encript_split[0];
		    		  
		    		  logger.info("encript_decode deviceId" + deviceId);
		    		  
		    		  userId= encript_split[1];
		    		  
		    		  logger.info("encript_decode userId" + userId);
		    		  
		    		  corpId=encript_split[2];
		    		  
		    		  logger.info("encript_decode corpId" + corpId);
		    		  
					  mobileNo = encript_split[3];
					  
					  logger.info("encript_decode mobileNo" + mobileNo);
					  
					 // System.out.println("mobile no is "+mobileNo);
					  
					  systemValueFromUI=encript_split[4];
					  
					  
					  if (mobileNo.length() > 10) {
						  
							int length = mobileNo.length();
							
							int lengthNeeded = length - 10;
							
							mobileNo = mobileNo.substring(lengthNeeded, length);
							
						}
					  
					  
					    String pattern = "yyyy-MM-dd HH:mm:ss";

						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						String formattedCreateDate = simpleDateFormat.format(new Date());

						Date date = simpleDateFormat.parse(formattedCreateDate);
						
					  
					  
				
					if(mobileNo.equalsIgnoreCase(recipientMobileNumber) && systemValueFromUI.equalsIgnoreCase("1")){
						  
						logger.info("inside power access matchrecipient");
						
						OtacModel  otacModel = PayconnectOtacBusiness.getOtacByUserAndSystemValue(userId, "1");
						
						
							
							 if(otacModel !=null && otacModel.getIsValidated().equalsIgnoreCase("1")){
								
								
									PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();
									
																
									payconnectOtacModel.setUserId(userId);
									payconnectOtacModel.setStatus("1");
									payconnectOtacModel.setIsMatch("1");
									payconnectOtacModel.setSmsReceiveDate(new Timestamp(date.getTime()));
									payconnectOtacModel.setEncriptStr(encript_space_split[1]);
									payconnectOtacModel.setMobileNo(recipientMobileNumber);
									payconnectOtacModel.setDecriptStr(encript_decode);
									payconnectOtacModel.setDeviceId(deviceId);
									payconnectOtacModel.setSystem("1");
									
									
									PayconnectOtacBusiness.UpdateOctacModelForMatchRecipient(payconnectOtacModel);
									
									
									hashMap.put("ResponseCode", "200");
									
									hashMap.put("Responsedetails", "Matched");
									
								
								
							}
							 
							 
							 

							 else if(otacModel !=null &&  otacModel.getIsValidated().equalsIgnoreCase("0")){
								
								
									
								    hashMap.put("ResponseCode", "200");
									
									hashMap.put("Responsedetails", "OTP not yet validated");
									
							
								
							}
							 
							 
						
							
							 else if(otacModel==null){
								 
								
								 hashMap.put("ResponseCode", "202");
								 
								 hashMap.put("Responsedetails", "invalid User Id or system");
									 
								
								
							}
							 
							 
					
						  
					  }
					
					
					
					
					
					

					else if(mobileNo.equalsIgnoreCase(recipientMobileNumber) && systemValueFromUI.equalsIgnoreCase("2")){
						  
						logger.info("inside paypro matchrecipient");
						
						OtacModel  otacModel = PayconnectOtacBusiness.getOtacByUserAndSystemValue(userId, "2");
							
							 if(otacModel !=null && otacModel.getIsValidated().equalsIgnoreCase("1") ){
								
								
							
									
									PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();
									
																
									payconnectOtacModel.setUserId(userId);
									payconnectOtacModel.setStatus("1");
									payconnectOtacModel.setIsMatch("1");
									payconnectOtacModel.setSmsReceiveDate(new Timestamp(date.getTime()));
									payconnectOtacModel.setEncriptStr(encript_space_split[1]);
									payconnectOtacModel.setMobileNo(recipientMobileNumber);
									payconnectOtacModel.setDecriptStr(encript_decode);
									payconnectOtacModel.setDeviceId(deviceId);
									payconnectOtacModel.setSystem("2");
									
									
									PayconnectOtacBusiness.UpdateOctacModelForMatchRecipient(payconnectOtacModel);
									
									
									hashMap.put("ResponseCode", "200");
									
									hashMap.put("Responsedetails", "Matched");

									
								
							
								
								
							}
							 
							 

							 else if(otacModel !=null &&  otacModel.getIsValidated().equalsIgnoreCase("0")){
								
								
									
								    hashMap.put("ResponseCode", "200");
									
									hashMap.put("Responsedetails", "OTP not yet validated");
									
							
								
							}
							 
							 
							 
							 
							
							 else if(otacModel==null){
								 
								
								 hashMap.put("ResponseCode", "202");
								 
								 hashMap.put("Responsedetails", "invalid User Id or system");
									 
								
								
							}
							
							  
						
						  
						
					  }
					
					
					  
					  else{
						  
						  
						 // System.out.println("inside else part >>>>>>>>>>>>>>");
						  
						  
						  OtacModel  otcModel = PayconnectOtacBusiness.getOtacByUserAndSystemValue(userId, systemValueFromUI);
						  
						  
						  if(otcModel!=null && otcModel.getIsValidated().equalsIgnoreCase("1")){
							  
							
					
								  PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();
									
									
									payconnectOtacModel.setUserId(userId);
									payconnectOtacModel.setStatus("1");
									payconnectOtacModel.setIsMatch("0");
									payconnectOtacModel.setSmsReceiveDate(new Timestamp(date.getTime()));
									payconnectOtacModel.setEncriptStr(encript_space_split[1]);
									payconnectOtacModel.setMobileNo(recipientMobileNumber);
									payconnectOtacModel.setDecriptStr(encript_decode);
									payconnectOtacModel.setDeviceId(deviceId);
									payconnectOtacModel.setSystem(systemValueFromUI);
									
									
									
									PayconnectOtacBusiness.UpdateOctacModelForMatchRecipient(payconnectOtacModel);
									
									
									hashMap.put("ResponseCode", "200");
									
									hashMap.put("Responsedetails", "Not Matched");
								  
							 
							 
							  
						  }
						  
						  
						  
						  
						  else if(otcModel !=null && otcModel.getIsValidated().equalsIgnoreCase("0")){
							  
							 
							    hashMap.put("ResponseCode", "200");
								
								hashMap.put("Responsedetails", "OTP not yet validated");
							  
						  }
						  
						  
						  else if (otcModel==null){
							  
							  
							    hashMap.put("ResponseCode", "202");
								 
								 hashMap.put("Responsedetails", "invalid User Id or system");
									 
							  
						  }
						  
						  
						  
						  
					  }
				
		     }
			
			
		        String output = json.toJson(hashMap);
		        
                out.print(output);
	
			

		} catch (Exception e) {
			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();
			
			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("MatchRecipientController API Exception: ", e);

		}

		out.close();
	}


	
	

}
