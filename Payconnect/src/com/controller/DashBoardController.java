package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.poweraccess.model.SessionToken;
import com.sun.jersey.api.client.ClientResponse;

@Controller
public class DashBoardController {
	
	
	private final Logger logger = Logger.getLogger(DashBoardController.class);

	@Autowired
	private MessageUtil messageUtil;

	
	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;
	

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	
	
	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;


	@Value("${error.400}")
	private String error400;
	
	@Value("${error.401}")
	private String error401;
	
	@Value("${error.402}")
	private String error402;
	
	@Value("${error.403}")
	private String error403;
	
	@Value("${error.404}")
	private String error404;
	
	@Value("${error.405}")
	private String error405;
	
	@Value("${error.408}")
	private String error408;
	
	@Value("${error.500}")
	private String error500;
	
	@Value("${error.502}")
	private String error502;
	
	@Value("${error.503}")
	private String error503;
	
	@Value("${error.504}")
	private String error504;
	
	@Value("${error.other}")
	private String errorOther;
	
	
	
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/getuseraccountreport", method = RequestMethod.POST)
	public void dashBoard(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		// LinkedHashMap outPutMap = new LinkedHashMap();
		//Common c = new Common();
		Gson json = new Gson();

		logger.info("Get User Account Report API");

		try {

			HashMap<String, String> headerMap = (new Common())
					.getHeadersInfo(request);
			
			

			String userID = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			if (userID == null)
				userID = "";

			//System.out.println("userID in java is " + userID);

			String session_token = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			if (session_token == null)
				session_token = "";

			//System.out.println("session_token in java is " + session_token);


			String api_version = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("apiversion"));
			
			if (api_version == null)
				api_version = "";

			String corpId = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("corpid"));
			
			if (corpId == null)
				corpId = "";


			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			//System.out.println("system value is " + systemValueFromApi);

			//System.out.println("system value in java is " + systemValueFromApi);

			if (systemValueFromApi == null)

				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				//System.out.println("Inside getuseraccountreport  1  !!!!!!!!");
				
				logger.info(" inside power access Get User Account Report");

				HashMap<String, String> map = new HashMap<String, String>();
				
				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					@SuppressWarnings("unused")
					HttpSession powerAccessSessionToken = request.getSession(false);
					
					SessionToken powerSessionToken = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userID.trim());


					//String clientIp = Common.getUserIp(request);
					
					if (powerSessionToken.getSessionToken().equals(
							session_token)) {

						String url = apiUrlPayconnect + "getuserDashBoard"; // URL
																			// from
																			// axis
																			// bank

						//System.out.println("URL: " + url);

						String inputString = Common
								.getUserDashboradReport(api_version, userID,
										systemValueFromApi, corpId);
                        
						logger.info("input string passes to power access dashboard api "+ inputString);
					

						ClientResponse client_response = Common
								.payconnectApiAcess(url, inputString, "post");

						logger.info("Axis Server Status for power access getuserDashBoard:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());
						System.out
								.println("Axis Server Status for getuseraccountreport***:"
										+ client_response.getStatus());

						if (responseStatus.equals("200")) {
							System.out
									.println("Axis Server Status for getuseraccountreport***:"
											+ client_response.getStatus());

							JSONParser parser = new JSONParser();
							JSONObject jsonObject = (JSONObject) parser
									.parse(jsonString);
							
							logger.info("getuserDashBoard api response for power access from axis api "+ jsonObject.toString());
							
							System.out
									.println("axis end json response >>>>>>>>> "
											+ jsonObject.toString());

							jsonObject = Common.load(jsonObject);
							
							String jsonStringFinal = jsonObject.toString();
							
							out.print(jsonStringFinal);
						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);

							System.out
									.println("Axis Server Status for getuseraccountreport***:"
											+ client_response.getStatus());
							System.out
									.println("Axis Server errorMsg for getuseraccountreport:"
											+ errorMsg);

							logger.info("Axis Server errorMsg for getuseraccountreport "+responseStatus + " : " + errorMsg);
							// jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
						
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", messageUtil
								.getBundle("error.invalid.sessiontoken.code"));
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("498 :"
								+ messageUtil
										.getBundle("error.invalid.sessiontoken.code"));
						jsonErrorMessage = json.toJson(map);

						JSONParser parser = new JSONParser();
						JSONObject jsonObject = (JSONObject) parser
								.parse(jsonErrorMessage);

						jsonObject = Common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", messageUtil
							.getBundle("error.sessiontoken.missing.code"));
					map.put("error",
							messageUtil.getBundle("error.sessiontoken.message"));
					// map.put("ip", c.getIp(request));//change
					logger.info("403 :"
							+ messageUtil
									.getBundle("error.sessiontoken.message"));
					jsonErrorMessage = json.toJson(map);

					JSONParser parser = new JSONParser();

					JSONObject jsonObject = (JSONObject) parser
							.parse(jsonErrorMessage);

					jsonObject = Common.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				//System.out.println("Inside getuseraccountreport  2  !!!!!!!!");
				
				logger.info(" inside paypro Get User Account Report");

				JSONParser parser = new JSONParser();
				HashMap<String, String> map = new HashMap<String, String>();
				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					@SuppressWarnings("unused")
					HttpSession payProSessionToken = request.getSession(false);

					PayProSessionToken payProSessionTokenFromDb = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userID.trim());

	

					//String clientIp = Common.getUserIp(request);
					
					if (payProSessionTokenFromDb
							.getSessionToken().equals(session_token)) {

						String url = apiUrlPayconnect + "getuserDashBoard"; // URL
																			// from
																			// axis
																			// bank
						//System.out.println(url);

						String inputString = Common
								.getUserDashboradReport(api_version, userID,
										systemValueFromApi, corpId);

						//System.out.println(inputString);

						ClientResponse client_response = Common
								.payconnectApiAcess(url, inputString, "post");

						logger.info("Axis Server Status for paypro getuserDashBoard:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							
							logger.info("Axis api response for paypro getuserDashBoard:"
									+ jsonObject.toString());

							jsonObject = Common.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);

							System.out
									.println("Axis Server Status for getuseraccountreport***:"
											+ client_response.getStatus());
							System.out
									.println("Axis Server errorMsg for getuseraccountreport:"
											+ errorMsg);

							logger.info("Axis Server errorMsg for paypro getuseraccountreport:"+responseStatus + " : " + errorMsg);
							jsonObject = Common.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
						
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", messageUtil
								.getBundle("error.invalid.sessiontoken.code"));
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("498 :"
								+ messageUtil
										.getBundle("error.invalid.sessiontoken.code"));
						jsonErrorMessage = json.toJson(map);

						JSONObject jsonObject = (JSONObject) parser
								.parse(jsonErrorMessage);

						jsonObject = Common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);
					}
				} else {
					map.put("ResponseCode", messageUtil
							.getBundle("error.sessiontoken.missing.code"));
					map.put("error",
							messageUtil.getBundle("error.sessiontoken.message"));
					// map.put("ip", c.getIp(request));//change
					logger.info("403 :"
							+ messageUtil
									.getBundle("error.sessiontoken.message"));
					jsonErrorMessage = json.toJson(map);
					JSONObject jsonObject = (JSONObject) parser
							.parse(jsonErrorMessage);
					jsonObject = Common.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error("Get User Account Report API Exception: ", e);
		}
		out.close();
	}

	
	
	
	
	       // To generate server error msg
			public String getServerErrorMsg(String errorCode) {
				HashMap<String, String> errorMap = new HashMap<String, String>();
				errorMap.put("400", error400);
				errorMap.put("401", error401);
				errorMap.put("402", error402);
				errorMap.put("403", error403);
				errorMap.put("404", error404);
				errorMap.put("405", error405);
				errorMap.put("408", error408);
				errorMap.put("500", error500);
				errorMap.put("502", error502);
				errorMap.put("503", error503);
				errorMap.put("504", error504);

				if (errorMap.get(errorCode) != null) {
					return (String) errorMap.get(errorCode);
				} else {
					return errorOther;
				}

			}

	
	
	
	

}
