/**
 * 
 */
package com.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.MPINContracts;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.axisbankcmspaypro.exception.SessionTokenMissing;
import com.axisbankcmspaypro.exception.UserIsBlocked;
import com.common.Common;
import com.google.gson.Gson;
import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.MPINRequestModel;
import com.payconnect.model.PayProSessionTokenModel;
import com.payconnect.model.PowerAccessSessionTokenModel;
import com.payconnect.url.intercepter.IPTracker;

/**
 * @author Avishek Seal
 *
 */
@Controller
public class MPINController  implements IPTracker{

    private final Logger logger = Logger.getLogger(MPINController.class);
    
    @Autowired
    private PayProSessionTokenBusiness payProSessionTokenBusiness;

    @Autowired
    private PowerAccessTokenBusiness powerAccessTokenBusiness;
    
    @Autowired
    private MPINContracts mpinContracts;
    /**
     * this URL is used to create or validate MPIN
     * @since 25-Feb-2016
     * @author Avishek Seal
     * @param httpServletRequest
     * @param httpServletResponse
     * @throws IOException
     * @throws Exception
     */
    @RequestMapping(value ="/api/creatempin", method = RequestMethod.POST)
    public void createOrValidateMPIN(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, Exception{
	boolean isCreateMpin = false;
	
	try {
	    if(logger.isInfoEnabled()) {
		logger.info("createOrValidateMPIN() - start");
	    }

	    final Common common = new Common();
		  
	    Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
	    
	    String userID = null;
	    String deviceName = null;
	    
	    if(StringUtils.isNotBlank(headerMap.get("user_id"))){
	    	userID = Common.decode(Common.PRIVATE_KEY,headerMap.get("user_id"));
	    }
	    
	    if(StringUtils.isNotBlank(httpServletRequest.getParameter("devicename"))) {
	    	
		deviceName = httpServletRequest.getParameter("devicename");
	   
		System.out.println("device name"+deviceName);
	    }
	    
	    if(StringUtils.isNotBlank(userID) && StringUtils.isNotBlank(deviceName)) { //create mpin
	    	
	    logger.info("inside create mpin");
	    	
		isCreateMpin = true;
		
		String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
		
		String sessionToken = Common.decode(Common.PRIVATE_KEY, headerMap.get("session_token"));
		
		
				
		String presentSessionToken = null;

		if(StringUtils.isBlank(sessionToken)){
			throw new SessionTokenMissing();
		}
		
		if(StringUtils.equals(system, "1")) {
		    presentSessionToken = powerAccessTokenBusiness.getPowerAccessSessionToken(userID.trim()).getSessionToken();
		} else if(StringUtils.equals(system, "2")) {
		    presentSessionToken = payProSessionTokenBusiness.getPayproSessionTokenByUser(userID.trim()).getSessionToken();
		}
		
		if(StringUtils.equals(sessionToken, presentSessionToken)){
		    final MPINRequestModel mpinRequestModel = new MPINRequestModel();
		    
		    mpinRequestModel.setDeviceType(httpServletRequest.getParameter("devicetype"));
		    mpinRequestModel.setDeviceToken(httpServletRequest.getParameter("devicetoken"));
		    mpinRequestModel.setDeviceName(deviceName);
		    mpinRequestModel.setDeviiceID(httpServletRequest.getParameter("deviceid"));
		    mpinRequestModel.setLatitude(httpServletRequest.getParameter("latitude"));
		    mpinRequestModel.setLongitude(httpServletRequest.getParameter("longitude"));
		    mpinRequestModel.setmPin(Common.decode(Common.PRIVATE_KEY,httpServletRequest.getParameter("mpin")));
		    mpinRequestModel.setSystem(system);
		    mpinRequestModel.setUserID(userID);
		    mpinRequestModel.setFlag("0");
		    
		    //mpinRequestModel.setFlag(httpServletRequest.getParameter("flag"));//newly added 
		    
		   
		    
		    if(StringUtils.isNotBlank(mpinRequestModel.getDeviceName()) && StringUtils.isNotBlank(mpinRequestModel.getmPin()) && StringUtils.isNotBlank(mpinRequestModel.getSystemValueFromApi()) && StringUtils.isNotBlank(mpinRequestModel.getDeviceType())){
		    	
		    	
		    	
		    	mpinContracts.createMPIN(mpinRequestModel);
			
			final HttpSession newSession = httpServletRequest.getSession(true);
			
			SESSION_IP_POOL.remove(sessionToken);
			
			SESSION_IP_POOL.put(newSession.getId(), httpServletRequest.getRemoteAddr());
			
			newSession.setAttribute("poweraccess_session_token",newSession.getId());
			
			if(StringUtils.equals(system, "1")) {
			    PowerAccessSessionTokenModel powerAccessSessionTokenModel = new PowerAccessSessionTokenModel();
			    
			    powerAccessSessionTokenModel.setSessionToken(newSession.getId());
			    powerAccessSessionTokenModel.setUserID(mpinRequestModel.getUserID());
			    
			    powerAccessTokenBusiness.saveOrUpdatePowerAccessSessionToken(powerAccessSessionTokenModel);
			} else if(StringUtils.equals(system, "2")) {
			    PayProSessionTokenModel payProSessionTokenModel = new PayProSessionTokenModel();
			    
			    payProSessionTokenModel.setSessionToken(newSession.getId());
			    payProSessionTokenModel.setUserID(mpinRequestModel.getUserID());
			    
			    payProSessionTokenBusiness.saveOrUpdatePayProSessionToken(payProSessionTokenModel);
			}
			
			System.out.println("valid flag is "+mpinRequestModel.getFlag());
			final LoginResponseModel loginResponseModel = mpinContracts.validateMPIN(mpinRequestModel.getDeviiceID(), mpinRequestModel.getmPin(), mpinRequestModel.getLatitude(), mpinRequestModel.getLongitude(),mpinRequestModel.getFlag(), mpinRequestModel.getSystemValueFromApi());
			
			loginResponseModel.setSessionToken(newSession.getId());
			
			//System.out.println(loginResponseModel.toJsonString());
			httpServletResponse.getWriter().write(loginResponseModel.toJsonString());
		    } else {
			httpServletResponse.getWriter().write(failed(isCreateMpin));
		    }
		} else {
			httpServletResponse.getWriter().write(invalidSessionToken());
		}
	    } else { // validate mpin
	    	
	    	logger.info("inside validate mpin");
	    	
		final MPINRequestModel mpinRequestModel = new MPINRequestModel();
		    
		mpinRequestModel.setDeviiceID(httpServletRequest.getParameter("deviceid"));
		mpinRequestModel.setmPin(Common.decode(Common.PRIVATE_KEY,httpServletRequest.getParameter("mpin")));
		System.out.println("mpin is "+mpinRequestModel.getmPin());
		mpinRequestModel.setLatitude(httpServletRequest.getParameter("latitude"));
		mpinRequestModel.setLongitude(httpServletRequest.getParameter("longitude"));
		mpinRequestModel.setFlag(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("flag")));
		System.out.println("flag is "+ mpinRequestModel.getFlag());
		mpinRequestModel.setSystem(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system")));
		
		System.out.println("system is "+ mpinRequestModel.getSystemValueFromApiInteger());
		
		/* mpinRequestModel.setUserID(headerMap.get("user_id"));// newly added
*/		
		final LoginResponseModel loginResponseModel = mpinContracts.validateMPIN(mpinRequestModel.getDeviiceID(), mpinRequestModel.getmPin(), mpinRequestModel.getLatitude(), mpinRequestModel.getLongitude(),mpinRequestModel.getFlag(),mpinRequestModel.getSystemValueFromApi());
		
		final HttpSession newSession = httpServletRequest.getSession(true);
		
		SESSION_IP_POOL.put(newSession.getId(), httpServletRequest.getRemoteAddr());
		
		if(StringUtils.equals(loginResponseModel.getSystem(), "1")) {
		    PowerAccessSessionTokenModel powerAccessSessionTokenModel = new PowerAccessSessionTokenModel();
		    
		    powerAccessSessionTokenModel.setSessionToken(newSession.getId());
		    powerAccessSessionTokenModel.setUserID(loginResponseModel.getUserID());
		    
         
		    powerAccessTokenBusiness.saveOrUpdatePowerAccessSessionToken(powerAccessSessionTokenModel);
		} else if(StringUtils.equals(loginResponseModel.getSystem(), "2")) {
		    PayProSessionTokenModel payProSessionTokenModel = new PayProSessionTokenModel();
		    
		    payProSessionTokenModel.setSessionToken(newSession.getId());
		    payProSessionTokenModel.setUserID(loginResponseModel.getUserID());
		    
		  
		    
		    payProSessionTokenBusiness.saveOrUpdatePayProSessionToken(payProSessionTokenModel);
		}
		
		loginResponseModel.setSessionToken(newSession.getId());
		
		httpServletResponse.getWriter().write(loginResponseModel.toJsonString());
	   }
	} catch (DataNotFound dataNotFound) {
	    if(logger.isInfoEnabled()) {
		logger.info("createOrValidateMPIN() - DataNotFoundException "+dataNotFound);
	    }
	    dataNotFound.printStackTrace();
	    httpServletResponse.getWriter().write(failed(isCreateMpin));
	} catch (UserIsBlocked userIsBlocked) {
	    if(logger.isInfoEnabled()) {
		logger.info("createOrValidateMPIN() - UserIsBlockedException "+userIsBlocked);
	    }
	    userIsBlocked.printStackTrace();
	    httpServletResponse.getWriter().write(failed(isCreateMpin));
	} catch (SessionTokenMissing sessionTokenMissing) {
	    if(logger.isInfoEnabled()) {
		logger.info("createOrValidateMPIN() - SessionTokenMissingException "+sessionTokenMissing);
	    }
	    
	    httpServletResponse.getWriter().write(sessionTokenIsMissing());
	}catch(InvalidInputException inputException){
		if(logger.isInfoEnabled()) {
			logger.info("createOrValidateMPIN() - Exception "+inputException);
		    }
		inputException.printStackTrace();
		
		if(StringUtils.isNotBlank(inputException.getCountLeft())) {
			httpServletResponse.getWriter().write(failed(isCreateMpin, inputException.getCountLeft()));
		} else {
			httpServletResponse.getWriter().write(failed(isCreateMpin));
		}
		
	}catch(Exception exception) {
	    if(logger.isInfoEnabled()) {
		logger.info("createOrValidateMPIN() - Exception "+exception);
	    }
	    exception.printStackTrace();
	    httpServletResponse.getWriter().write(somethingWentWrong());
	}finally {
	    if(logger.isInfoEnabled()) {
		logger.info("createOrValidateMPIN() - end");
	    }
	}
    }
    
    private String failed(boolean isCreateMpin) throws Exception{
	Map<String, String> map = new HashMap<String, String>();
	
	map.put("ResponseCode", "201");
	map.put("Responsedetails", "Failure");
	
	if(isCreateMpin) {
	    map.put("Message", "Fail to create MPIN");
	} else {
	    map.put("Message", "Invalid MPIN");
	}
	
	final Gson gson = new Gson();
	
	final String jsonErrorMessage = gson.toJson(map);
	
	final JSONParser parser = new JSONParser();

	final Object obj = parser.parse(jsonErrorMessage);
	JSONObject jsonObject = (JSONObject) obj;
	jsonObject = Common.load(jsonObject);
	
	return jsonObject.toString();
    }
    
    private String failed(boolean isCreateMpin, String attemptLeft) throws Exception{
    	Map<String, String> map = new HashMap<String, String>();
    	
    	map.put("ResponseCode", "201");
    	map.put("Responsedetails", "Failure");
    	map.put("MpinAttemptLeft", attemptLeft);
    	
    	if(isCreateMpin) {
    	    map.put("Message", "Fail to create MPIN");
    	} else {
    	    map.put("Message", "Invalid MPIN");
    	}
    	
    	final Gson gson = new Gson();
    	
    	final String jsonErrorMessage = gson.toJson(map);
    	
    	final JSONParser parser = new JSONParser();

    	final Object obj = parser.parse(jsonErrorMessage);
    	JSONObject jsonObject = (JSONObject) obj;
    	jsonObject = Common.load(jsonObject);
    	
    	return jsonObject.toString();
        }
    
    private String somethingWentWrong() throws Exception{
	Map<String, String> map = new HashMap<String, String>();
	
	map.put("error", "Something went wrong. Please try again later.");
	
	final Gson gson = new Gson();
	
	final String jsonErrorMessage = gson.toJson(map);
	
	final JSONParser parser = new JSONParser();

	final Object obj = parser.parse(jsonErrorMessage);
	JSONObject jsonObject = (JSONObject) obj;
	jsonObject = Common.load(jsonObject);
	
	return jsonObject.toString();
    }
    
    
    private String invalidSessionToken() throws Exception{
    	Map<String, String> map = new HashMap<String, String>();
    	
    	map.put("ResponseCode", "498");
    	map.put("error", "Invalid SessionToken");
    	
    	final Gson gson = new Gson();
    	
    	String jsonErrorMessage = gson.toJson(map);
    	
    	final JSONParser parser = new JSONParser();

    	Object obj = parser.parse(jsonErrorMessage);
    	JSONObject jsonObject = (JSONObject) obj;
    	jsonObject = Common.load(jsonObject);
    	
    	return jsonObject.toString();
        }
    
    
   
    
    private String sessionTokenIsMissing() throws Exception{
    	Map<String, String> map = new HashMap<String, String>();
    	
    	map.put("ResponseCode", "498");
    	
    	map.put("error", "SessionToken is Missing");
    	
    	final Gson gson = new Gson();
    	
    	String jsonErrorMessage = gson.toJson(map);
    	
    	final JSONParser parser = new JSONParser();

    	Object obj = parser.parse(jsonErrorMessage);
    	JSONObject jsonObject = (JSONObject) obj;
    	jsonObject = Common.load(jsonObject);
    	
    	return jsonObject.toString();
        }
    
}
