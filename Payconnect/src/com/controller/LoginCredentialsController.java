package com.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.axisbank.service.PayProCredentialProtocol;
import com.axisbank.service.PowerAccessCredentialProtocol;
import com.axisbankcmspaypro.business.DeviceBusiness;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PayconnectOtacBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.DeviceModel;
import com.payconnect.model.LoginRequestModel;
import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.PayProSessionTokenModel;
import com.payconnect.model.PayconnectOtacModel;
import com.payconnect.model.PowerAccessSessionTokenModel;
import com.payconnect.url.intercepter.IPTracker;

@Controller
public class LoginCredentialsController implements IPTracker{

	@Autowired
	private PayProCredentialProtocol credentialService;

	@Autowired
	private PowerAccessCredentialProtocol powerAccessCredentialService;

	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;

	@Autowired
	private PayconnectOtacBusiness PayconnectOtacBusiness;
	
	@Autowired
	private DeviceBusiness deviceBusiness;

	@Autowired
	private MessageUtil messageUtil;

	/*@Autowired
	private Common common;*/

	@Value("${otac.min}")
	private int otacMin;

	@Value("${otac.max}")
	private int otacMax;

	private final Logger logger = Logger
			.getLogger(LoginCredentialsController.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/login", method = RequestMethod.POST)
	public void login(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		logger.info("Inside the login api");
		
		try {
			LoginRequestModel loginRequestModel = new LoginRequestModel();

			loginRequestModel.setUserName(request.getParameter("username"));
			loginRequestModel.setUserPassward(request.getParameter("password"));
			loginRequestModel.setDeviceID(request.getParameter("deviceid"));
			loginRequestModel.setSystemValueFromApi(request.getParameter("system"));
			loginRequestModel.setCaptcha(request.getParameter("captcha"));
			loginRequestModel.setCorporateID(request.getParameter("corpid"));
			loginRequestModel.setApiVersion(request.getParameter("apiversion"));
			loginRequestModel.setLatitude(request.getParameter("latitude"));
			loginRequestModel.setLongitude(request.getParameter("longitude"));
			loginRequestModel.setClientIp(request.getRemoteAddr());

			if (loginRequestModel.getSystemValueFromApiInteger() == 2) {
				
				logger.info("inside paypro login");
				
				String mobileNo = "";
	
				LoginResponseModel loginResponseModel = credentialService.login(loginRequestModel);

				if (loginResponseModel.isSuccess()) {
					if (request.getSession().getAttribute("paypro_session_token") != null) {
						request.getSession().invalidate();
					}

					HttpSession payProSessionToken = request.getSession(true);

					String sessionId = payProSessionToken.getId();

					//payProSessionToken.setAttribute("paypro_session_token", sessionId);
					//payProSessionToken.setAttribute("client_ip", Common.getUserIp(request));

					loginResponseModel.setSessionToken(sessionId);
					loginResponseModel.setUserID(loginRequestModel.getUserName());
					loginResponseModel.setCorpID(loginRequestModel.getCorporateID());

					PayProSessionTokenModel payProSessionTokenModel = new PayProSessionTokenModel();
					
					payProSessionTokenModel.setUserID(loginRequestModel.getUserName());
					payProSessionTokenModel.setSessionToken(loginResponseModel.getSessionToken());

					payProSessionTokenBusiness.saveOrUpdatePayProSessionToken(payProSessionTokenModel);
					
					
					//insert device id into device table
					
					DeviceModel deviceModel=new DeviceModel();
					
					deviceModel.setDeviceId(loginRequestModel.getDeviceID());
					
					deviceBusiness.saveOrUpdateDeviceId(deviceModel);

					PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();

					String pattern = "yyyy-MM-dd HH:mm:ss";

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

					String formattedCreateDate = simpleDateFormat.format(new Date());

					Date date = simpleDateFormat.parse(formattedCreateDate);

					payconnectOtacModel.setOtac(Common.generateOtac(otacMin, otacMax));
					payconnectOtacModel.setUserId(loginRequestModel.getUserName());
					payconnectOtacModel.setRegisteredPhone(loginResponseModel.getMobileNumber());
					//payconnectOtacModel.setRegisteredPhone("7059624650");
					
					payconnectOtacModel.setDeviceId(loginRequestModel.getDeviceID());
					payconnectOtacModel.setCreateDate(new Timestamp(date.getTime()));
					payconnectOtacModel.setSystem(loginRequestModel.getSystemValueFromApiInteger().toString());

					PayconnectOtacBusiness.insertOrUpdateOctacModel(payconnectOtacModel);

					if (payconnectOtacModel.getRegisteredPhone().length() == 10) {
						mobileNo = "91"+ payconnectOtacModel.getRegisteredPhone();

						logger.info(" paypro login mobile no in if part " + mobileNo);
					} else {
						mobileNo = payconnectOtacModel.getRegisteredPhone();
						logger.info(" paypro login mobile no in else part " + mobileNo);
					}
					

					String smsGetWayResponse = PayconnectOtacBusiness.getResponseFromSmsApi(payconnectOtacModel.getRegisteredPhone(), payconnectOtacModel.getOtac());
                    
					SESSION_IP_POOL.put(sessionId, request.getRemoteAddr());
					
					logger.info("inside paypro login smsGetWay Response from axis  "+smsGetWayResponse);
				}
				
				
				
             else if(loginResponseModel.isInvalidCredential()){
					
					
	                final DeviceModel  deviceMdl=new DeviceModel();
	                
	                deviceMdl.setDeviceId(loginRequestModel.getDeviceID());
					
					deviceBusiness.saveOrUpdateDeviceId(deviceMdl);
					
				}  

				response.getWriter().write(loginResponseModel.toJsonString());

			} else if (loginRequestModel.getSystemValueFromApiInteger() == 1) {
				
				logger.info("inside power access login");
				
				
				String mobileNo = "";

				LoginResponseModel powerAcessloginResponseModel = powerAccessCredentialService.login(loginRequestModel);

				if (powerAcessloginResponseModel.isSuccess()) {
					if (request.getSession().getAttribute(
							"poweraccess_session_token") != null) {
						request.getSession().invalidate();
					}

					HttpSession powerAccessSessionToken = request.getSession(true);

					String sessionId = powerAccessSessionToken.getId();
					
					//powerAccessSessionToken.setAttribute("poweraccess_session_token",sessionId);
					
					//powerAccessSessionToken.setAttribute("client_ip", Common.getUserIp(request));
					
					powerAcessloginResponseModel.setSessionToken(sessionId);

					PowerAccessSessionTokenModel powerAccessSessionTokenModel = new PowerAccessSessionTokenModel();

					powerAccessSessionTokenModel.setUserID(powerAcessloginResponseModel.getUserID());
					powerAccessSessionTokenModel.setSessionToken(powerAcessloginResponseModel.getSessionToken());

					powerAccessTokenBusiness.saveOrUpdatePowerAccessSessionToken(powerAccessSessionTokenModel);
					

					//insert device id into device table
					
					final DeviceModel deviceModel=new DeviceModel();
					
					deviceModel.setDeviceId(loginRequestModel.getDeviceID());
					
					deviceBusiness.saveOrUpdateDeviceId(deviceModel);

					

					PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();

					String pattern = "yyyy-MM-dd HH:mm:ss";

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

					String formattedCreateDate = simpleDateFormat.format(new Date());

					Date date = simpleDateFormat.parse(formattedCreateDate);

					payconnectOtacModel.setOtac(Common.generateOtac(otacMin, otacMax));
					payconnectOtacModel.setUserId(powerAcessloginResponseModel.getUserID());
					payconnectOtacModel.setRegisteredPhone(powerAcessloginResponseModel.getMobileNumber());
					
					//payconnectOtacModel.setRegisteredPhone("7059624650");
					
					payconnectOtacModel.setDeviceId(loginRequestModel.getDeviceID());
					payconnectOtacModel.setCreateDate(new Timestamp(date.getTime()));
					payconnectOtacModel.setSystem(loginRequestModel.getSystemValueFromApiInteger().toString());
					
					PayconnectOtacBusiness.insertOrUpdateOctacModel(payconnectOtacModel);

					if (payconnectOtacModel.getRegisteredPhone().length() == 10) {
						mobileNo = "91"+payconnectOtacModel.getRegisteredPhone();
						logger.info("power access login mobile no in if part " + mobileNo);
					} else {
						mobileNo = payconnectOtacModel.getRegisteredPhone();
						logger.info("power access login mobile no in else part " + mobileNo);
					}

					String smsGetWayResponse = PayconnectOtacBusiness.getResponseFromSmsApi(payconnectOtacModel.getRegisteredPhone(), payconnectOtacModel.getOtac());
					SESSION_IP_POOL.put(sessionId, request.getRemoteAddr());
					logger.info("power access login smsGetWayResponse from Axis "+smsGetWayResponse);
				}
				
				else if(powerAcessloginResponseModel.isInvalidCredential()){
					
					
	                final DeviceModel  deviceMdl=new DeviceModel();
	                
	                deviceMdl.setDeviceId(loginRequestModel.getDeviceID());
					
					deviceBusiness.saveOrUpdateDeviceId(deviceMdl);
					
					
				}

				response.getWriter().write(powerAcessloginResponseModel.toJsonString());

			}

		} catch (Exception e) {
			e.printStackTrace();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", messageUtil.getBundle("wrong.message"));
			String jsonStringFinal = jsonObject.toString();
			response.getWriter().write(jsonStringFinal);
			logger.error("login api exception message "+e.getMessage());
			logger.error("In the login API Exception: ", e);
			
		}

	}
}
