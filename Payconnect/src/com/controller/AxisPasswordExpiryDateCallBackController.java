package com.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PasswordExpiryDateBusiness;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidDateException;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.payconnect.model.PasswordExpiryDateModel;

@Controller
public class AxisPasswordExpiryDateCallBackController {
	
	@Autowired
	private PasswordExpiryDateBusiness passwordExpiryDateBusiness;
	
	/**
     * this URL is used to update the password expiry date value for the specific user of a speific system
     * @since 09-Mar-2016
     * @param passwordChangeInWebModel
     * @param httpServletResponse
     * @throws IOException
     */
    @RequestMapping(value = "/update-pwd-expiry-date", method = RequestMethod.POST)
    public void updatePasswordChangeInWeb(@RequestBody PasswordExpiryDateModel passwordExpiryDateModel, HttpServletResponse httpServletResponse) throws IOException{
	try {
		
		passwordExpiryDateBusiness.updatePasswordExpiry(passwordExpiryDateModel);
	    httpServletResponse.setStatus(HttpServletResponse.SC_OK);
	    httpServletResponse.getWriter().write(responseGenerator("200", "Success"));
	} catch (DataNotFound e) {
		e.printStackTrace();
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
	} catch (InvalidInputException e) {
		e.printStackTrace();
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
	}
	
	catch(InvalidDateException e){
		e.printStackTrace();
		 httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		 httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
		
	}
	catch (Exception e) {
		e.printStackTrace();
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(responseGenerator("500", "Internal Server Error"));
	}
	
    }
    
    //this method is used to generate JSON response
    @SuppressWarnings("unchecked")
    private final String responseGenerator(String responseCode, String responseDetails){
	final JSONObject jsonObject = new JSONObject();
	
	jsonObject.put("ResponseCode", responseCode);
	jsonObject.put("ResponseDetails", responseDetails);
	
	return jsonObject.toJSONString();
    }

}
