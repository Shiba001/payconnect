/*package com.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.AppLogOutBusiness;
import com.axisbankcmspaypro.business.PayConnectUserBusiness;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PayconnectOtacBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.dao.PayProSessionTokenDAO;
import com.axisbankcmspaypro.dao.PowerAccessSessionTokenDAO;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.InvalidCredentials;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.jersey.api.client.ClientResponse;
import com.payconnect.model.Logout;
import com.poweraccess.model.OtacModel;
import com.poweraccess.model.PhoneMatchSetting;
import com.poweraccess.model.SessionToken;
import com.poweraccess.model.VersionManagement;
import com.poweraccess.service.CmsService;
import com.poweraccess.service.OtacService;
import com.poweraccess.service.SessionTokenService;

@Controller
public class APIController implements InvalidCredentials {

	private final Logger logger = Logger.getLogger(APIController.class);

	@Autowired
	private SessionTokenService sessionTokenService;

	@Autowired
	private CmsService cmsService;

	@Autowired
	private OtacService otacService;

	// added by arup paul
	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private PayconnectOtacBusiness payconnectOtacBusiness;

	@Autowired
	private PowerAccessSessionTokenDAO powerAccessSessionTokenDAO;

	@Autowired
	private PayProSessionTokenDAO payProSessionTokenDAO;

	@Autowired
	private PayconnectOtacBusiness PayconnectOtacBusiness;

	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	@Autowired
	private AppLogOutBusiness appLogOutBusiness;
	
	@Autowired
	private PayConnectUserBusiness payConnectUserBusiness;

	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;

	@Value("${axis.api.path}")
	private String apiUrl;

	@Value("${application.url}")
	private String appUrl;

	@Value("${application.host}")
	private String appHost;

	@Value("${otac.min}")
	private int otacMin;

	@Value("${otac.max}")
	private int otacMax;

	@Value("${otac.validity.sec}")
	private int otacValidity;

	@Value("${oatc.sms.api.path}")
	private String otacSmsApiPath;

	@Value("${oatc.sms.api.username}")
	private String otacSmsApiUsername;

	@Value("${oatc.sms.api.password}")
	private String otacSmsApiPassword;

	@Value("${oatc.sms.api.ctype}")
	private String otacSmsApiCtype;

	@Value("${oatc.sms.api.alert}")
	private String otacSmsApiAlert;

	@Value("${oatc.sms.api.msgtype}")
	private String otacSmsApiMsgType;

	@Value("${oatc.sms.api.priority}")
	private String otacSmsApiPriority;

	@Value("${oatc.sms.api.sender}")
	private String otacSmsApiSender;

	@Value("${oatc.sms.api.msg}")
	private String otacSmsApiMsg;

	@Value("${oatc.sms.api.decode}")
	private String otacSmsApiDecode;

	@Value("${error.400}")
	private String error400;
	@Value("${error.401}")
	private String error401;
	@Value("${error.402}")
	private String error402;
	@Value("${error.403}")
	private String error403;
	@Value("${error.404}")
	private String error404;
	@Value("${error.405}")
	private String error405;
	@Value("${error.408}")
	private String error408;
	@Value("${error.500}")
	private String error500;
	@Value("${error.502}")
	private String error502;
	@Value("${error.503}")
	private String error503;
	@Value("${error.504}")
	private String error504;
	@Value("${error.other}")
	private String errorOther;

	@Value("${otac.sms.gateway.server}")
	private String smsServer;

	// For local
	// String workingDir = System.getProperty("user.dir");

	// For Production Or UAT
	String workingDir = System.getProperty("catalina.home") + "/" + "webapps"
			+ "/" + "AxisBank" + "/" + "WEB-INF/filesStore";

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws
	 * @throws NoPassGivenException
	 * 
	 *//*

	
	 * @SuppressWarnings("unchecked")
	 * 
	 * @RequestMapping(value = "/api/poweraccess/login", method =
	 * RequestMethod.POST) public void doLogin(HttpServletRequest request,
	 * HttpServletResponse response) throws IOException {
	 * 
	 * PrintWriter out = response.getWriter();
	 * 
	 * HashMap outPut = new HashMap();
	 * 
	 * Common c = new Common();
	 * 
	 * String jsonString = null;
	 * 
	 * Gson json = new Gson();
	 * 
	 * SessionToken objSessionToken1 = null;
	 * 
	 * logger.info("In the login api");
	 * 
	 * try {
	 * 
	 * HashMap headerMap = (new Common()).getHeadersInfo(request);
	 * 
	 * String api_key = "";
	 * 
	 * if (api_key == null) api_key = "";
	 * 
	 * String userNameFromApp = c.decode(Common.PRIVATE_KEY,
	 * request.getParameter("username"));
	 * 
	 * String userName = userNameFromApp.toLowerCase();
	 * 
	 * if (userName == null) userName = "";
	 * 
	 * String userPassward = c.decode(Common.PRIVATE_KEY,
	 * request.getParameter("password"));
	 * 
	 * if (userPassward == null) userPassward = "";
	 * 
	 * String deviceID = c.decode(Common.PRIVATE_KEY,
	 * request.getParameter("deviceid"));
	 * 
	 * if (deviceID == null) deviceID = "";
	 * 
	 * String cropID = c.decode(Common.PRIVATE_KEY,
	 * request.getParameter("corpid"));
	 * 
	 * if (cropID == null) cropID = "";
	 * 
	 * String captcha = c.decode(Common.PRIVATE_KEY,
	 * request.getParameter("captcha"));
	 * 
	 * if (captcha == null)
	 * 
	 * captcha = "";
	 * 
	 * String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
	 * request.getParameter("system"));
	 * 
	 * if (systemValueFromApi == null) systemValueFromApi = "";
	 * 
	 * int systemValueInController = Integer.parseInt(systemValueFromApi);
	 * 
	 * if (systemValueInController == 2) {
	 * 
	 * System.out.println("inside login api sytem value 2 !!!!!!!");
	 * 
	 * JSONParser parser = new JSONParser();
	 * 
	 * String url = apiUrl + "login";
	 * 
	 * if (userName != null && userPassward != null) {
	 * 
	 * String inputString = "{\"api_key\": \"" + api_key + "\", \"username\":\""
	 * + userName.trim() + "\",\"password\":\"" + userPassward.trim() +
	 * "\",\"deviceid\":\"" + deviceID + "\"}";
	 * 
	 * ClientResponse client_response = Common.apiAcess(url, inputString,
	 * "post");
	 * 
	 * jsonString = client_response.getEntity(String.class);
	 * 
	 * logger.info("Axis Server Status for login:" +
	 * client_response.getStatus());
	 * 
	 * System.out.println("Axis Server Status for login:" +
	 * client_response.getStatus());
	 * 
	 * String responseStatus = String.valueOf(client_response .getStatus());
	 * 
	 * JSONObject jsonObject = null;
	 * 
	 * String jsonStringForCaptcha = null;
	 * 
	 * if (responseStatus.equals("200")) {
	 * 
	 * // if jsonString 200-->create session token Object obj =
	 * parser.parse(jsonString);
	 * 
	 * jsonObject = (JSONObject) obj;
	 * 
	 * if (jsonString.contains("200")) { // create a session token
	 *//******************
	 * Session token tracked from browser and save in DB
	 ********************//*
	
	 * HttpSession sessionToken = request.getSession();
	 * 
	 * String sessionId = sessionToken.getId();
	 * 
	 * String username = (String) jsonObject.get("Userid");
	 * 
	 * String mobile = String.valueOf(jsonObject .get("MobileNo"));
	 * 
	 * System.out.println("mobile no is.............. " + mobile);
	 * 
	 * sessionToken.setAttribute("session_id", sessionId);
	 * 
	 * sessionToken.setAttribute("client_ip", c.getUserIp(request));
	 *//******************
	 * End of Session token tracked from browser and save in DB
	 ********************//*
	
	 * 
	 * String sessionToken = Common.generateSessionToken();
	 * 
	 * SessionToken objSessionToken = new SessionToken();
	 * 
	 * String session_id = sessionToken.getId();
	 * 
	 * objSessionToken.setSessionToken(session_id);// session // token // to be
	 * // String // to // save // in // DB. if (userName != null &&
	 * !userName.trim().equals("")) {// userID // and // userName // is // same?
	 * objSessionToken.setUserID(username);
	 * 
	 * //
	 * System.out.println("Session:"+sessionTokenService.getSessionToken(username
	 * ));
	 * 
	 * if (sessionTokenService .getSessionToken(username) != null) {
	 * 
	 * sessionTokenService .deleteToken(objSessionToken);
	 * 
	 * sessionTokenService .addSessionToken(objSessionToken); } else {
	 * 
	 * sessionTokenService .addSessionToken(objSessionToken); }
	 * 
	 * objSessionToken1 = sessionTokenService .getSessionToken(username);// get
	 * the // session // token to // be // returned
	 * 
	 * // ata ki kaj karche soumya da. arup paul
	 * 
	 * // extra field added by arup paul
	 * 
	 * jsonObject.put("corpID", "");
	 * 
	 * jsonObject.put("Userid", userName);
	 * 
	 * jsonObject.put("session_token", session_id);// set // in // the
	 * jsonObject.put("UserName", userName);// object
	 * 
	 * jsonObject.put("EmailID", "");
	 * 
	 * jsonObject.put("LastPwdChgDate", "");
	 * 
	 * jsonObject.put("PwdExpiryDate", "");
	 * 
	 * jsonObject.put("LastLogonDate", "");
	 * 
	 * jsonObject.put("UserStatus", "");
	 * 
	 * jsonObject.put("PwdChangeinWeb", "0");
	 * 
	 * // jsonObject.put("session_token", session_id);
	 * 
	 * // jsonObject.put("ip", c.getIp(request)); // //change
	 * 
	 * }
	 *//******* Otac Implementation Start *******//*
	
	 * 
	 * String pattern = "yyyy-MM-dd HH:mm:ss";
	 * 
	 * SimpleDateFormat format = new SimpleDateFormat( pattern);
	 * 
	 * String formattedCreateDate = format .format(new Date());
	 * 
	 * Date date = format.parse(formattedCreateDate);
	 * 
	 * Timestamp timestamp = new Timestamp(date.getTime());
	 * 
	 * OtacModel otacModel = new OtacModel();
	 * 
	 * String otac = c.generateOtac(otacMin, otacMax);
	 * 
	 * otacModel.setOtac(otac); otacModel.setUserID(username);
	 * otacModel.setRegisteredPhone(mobile); otacModel.setStatus("0");
	 * otacModel.setCreateDate(timestamp); otacModel.setIsMatch("0");
	 * otacModel.setIsValidated("0"); otacModel.setSystem(systemValueFromApi);
	 * 
	 * otacService.addOtac(otacModel);
	 * 
	 * if (mobile.length() == 10) { mobile = "91" + mobile; }
	 * 
	 * // --------------- Axis Sms gateway start // --------------------//
	 * 
	 * String sms_url = "";
	 * 
	 * if (smsServer.equals("axis")) { sms_url = otacSmsApiPath + "?dcode=" +
	 * otacSmsApiDecode + "&userid=" + otacSmsApiUsername + "&pwd=" +
	 * otacSmsApiPassword + "&ctype=" + otacSmsApiCtype + "&alert=" +
	 * otacSmsApiAlert + "&msgtype=" + otacSmsApiMsgType + "&priority=" +
	 * otacSmsApiPriority + "&sender=" + otacSmsApiSender + "&pno=" + mobile +
	 * "&msgtxt=" + otac + otacSmsApiMsg; } else { sms_url =
	 * "http://alerts.solutionsinfini.com/api/v3/index.php?method=sms" +
	 * "&api_key=A6f8b45980b586dd9a2fc8281e9dc5b34&to=" + mobile +
	 * "&sender=MYRTAX" +
	 * "&message=Hello,%20Welcome%20to%20MakeYourTax.com.%20The%20OTP%20for%20registration%20is%20"
	 * + otac + "&format=json&custom=1,2&flash=0"; }
	 * 
	 * logger.info("SMS URL Login" + sms_url);
	 * 
	 * ClientResponse sms_response = Common.apiAcess( sms_url, "", "get");
	 * 
	 * String sms_str = sms_response .getEntity(String.class);
	 * 
	 * logger.info("SMS Response Login" + sms_str);
	 * 
	 * String smsResponseStatus = String .valueOf(sms_response.getStatus());
	 * 
	 * if (!smsResponseStatus.equals("200")) {
	 * 
	 * String errorMsg = this .getServerErrorMsg(smsResponseStatus);
	 * 
	 * logger.info("SMS Response Status: " + smsResponseStatus); }
	 * 
	 * // --------------- Axis Sms gateway end // --------------------// //
	 * jsonObject.put("otac", otac); // to be commentted // out for live
	 * 
	 * // jsonObject.put("ip", c.getIp(request));
	 *//******* Otac Implementation End *******//*
	
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * jsonObject = new JSONObject(); System.out
	 * .println("inside invalid else part......................"); String
	 * errorMsg = this .getServerErrorMsg(responseStatus);
	 * jsonObject.put("ResponseCode", responseStatus);
	 * jsonObject.put("Responsedetails", errorMsg);
	 * 
	 * // jsonObject.put("ip", c.getIp(request)); logger.info(responseStatus +
	 * " : " + errorMsg); }
	 * 
	 * jsonObject = c.load(jsonObject);
	 * 
	 * String jsonStringFinal = jsonObject.toString();
	 * 
	 * System.out.println("final response ........" + jsonStringFinal);
	 * 
	 * out.print(jsonStringFinal); }
	 * 
	 * }
	 * 
	 * else if (systemValueInController == 1) {
	 * 
	 * System.out.println("inside login api sytem value 1 !!!!!!!");
	 * 
	 * JSONParser parser = new JSONParser();
	 * 
	 * String url = apiUrl + "login";
	 * 
	 * if (userName != null && userPassward != null) {
	 * 
	 * // System.out.println("inside first if part !!!!!!");
	 * 
	 * String inputString = "{\"api_key\": \"" + api_key + "\", \"username\":\""
	 * + userName.trim() + "\",\"password\":\"" + userPassward.trim() +
	 * "\",\"deviceid\":\"" + deviceID + "\"}";
	 * 
	 * ClientResponse client_response = Common.apiAcess(url, inputString,
	 * "post");
	 * 
	 * jsonString = client_response.getEntity(String.class);
	 * 
	 * logger.info("Axis Server Status for login:" +
	 * client_response.getStatus());
	 * 
	 * String responseStatus = String.valueOf(client_response .getStatus());
	 * 
	 * System.out.println("response status............ " + responseStatus);
	 * 
	 * JSONObject jsonObject = null;
	 * 
	 * if (responseStatus.equals("200")) {
	 * 
	 * System.out.println("inside second if part");
	 * 
	 * // if jsonString 200-->create session token
	 * 
	 * Object obj = parser.parse(jsonString);
	 * 
	 * jsonObject = (JSONObject) obj;// akhane add kara ache // invalid
	 * credentials // hale
	 * 
	 * System.out .println("axis end response..................." +
	 * jsonObject.toString());// axis end er // main // response
	 * 
	 * if (jsonString.contains("200")) {
	 * 
	 * System.out.println("inside third if part"); // create a session token
	 *//******************
	 * Session token tracked from browser and save in DB
	 ********************//*
	
	 * 
	 * HttpSession sessionToken = request.getSession();
	 * 
	 * String sessionId = sessionToken.getId();
	 * 
	 * String username = (String) jsonObject.get("Userid");
	 * 
	 * System.out .println("user name.........." + username);
	 * 
	 * String mobile = String.valueOf(jsonObject .get("MobileNo"));
	 * 
	 * System.out.println("mobile no ......." + mobile);
	 * 
	 * sessionToken.setAttribute("session_id", sessionId);
	 * 
	 * sessionToken.setAttribute("client_ip", c.getUserIp(request));
	 *//******************
	 * End of Session token tracked from browser and save in DB
	 ********************//*
	
	 * 
	 * SessionToken objSessionToken = new SessionToken();
	 * 
	 * String session_id = sessionToken.getId();
	 * 
	 * objSessionToken.setSessionToken(session_id);// session // token // to be
	 * // String // to // save // in // DB. if (userName != null &&
	 * !userName.equals("")) {// userID // and // userName // is // same?
	 * objSessionToken.setUserID(username);
	 * 
	 * //
	 * System.out.println("Session:"+sessionTokenService.getSessionToken(username
	 * ));
	 * 
	 * if (sessionTokenService .getSessionToken(username) != null) {
	 * 
	 * sessionTokenService .deleteToken(objSessionToken); sessionTokenService
	 * .addSessionToken(objSessionToken); } else {
	 * 
	 * sessionTokenService .addSessionToken(objSessionToken); }
	 * 
	 * objSessionToken1 = sessionTokenService .getSessionToken(username);
	 * 
	 * // get the // session // token to // be // returned
	 * 
	 * // jsonObject.put("session_token", // session_id);// set // in
	 * 
	 * jsonObject.put("corpID", "");
	 * 
	 * jsonObject.put("Userid", userName);
	 * 
	 * jsonObject.put("session_token", session_id);// set // in // the
	 * jsonObject.put("UserName", userName);// object
	 * 
	 * jsonObject.put("EmailID", "");
	 * 
	 * jsonObject.put("LastPwdChgDate", "");
	 * 
	 * jsonObject.put("PwdExpiryDate", "");
	 * 
	 * jsonObject.put("LastLogonDate", "");
	 * 
	 * jsonObject.put("UserStatus", "");
	 * 
	 * jsonObject.put("PwdChangeinWeb", "0"); // the // object
	 * 
	 * // jsonObject.put("ip", c.getIp(request));
	 * 
	 * // change
	 * 
	 * }
	 *//******* Otac Implementation Start *******//*
	
	 * 
	 * String pattern = "yyyy-MM-dd HH:mm:ss";
	 * 
	 * SimpleDateFormat format = new SimpleDateFormat( pattern);
	 * 
	 * String formattedCreateDate = format .format(new Date());
	 * 
	 * Date date = format.parse(formattedCreateDate);
	 * 
	 * Timestamp timestamp = new Timestamp(date.getTime());
	 * 
	 * OtacModel otacModel = new OtacModel();
	 * 
	 * String otac = c.generateOtac(otacMin, otacMax);
	 * 
	 * otacModel.setOtac(otac); otacModel.setUserID(username);
	 * otacModel.setRegisteredPhone(mobile); otacModel.setStatus("0");
	 * otacModel.setCreateDate(timestamp); otacModel.setIsMatch("0");
	 * otacModel.setIsValidated("0"); otacModel.setSystem(systemValueFromApi);
	 * 
	 * otacService.addOtac(otacModel);
	 * 
	 * if (mobile.length() == 10) { mobile = "91" + mobile; } // ---------------
	 * Axis Sms gateway start // --------------------// String sms_url = "";
	 * 
	 * if (smsServer.equals("axis")) { sms_url = otacSmsApiPath + "?dcode=" +
	 * otacSmsApiDecode + "&userid=" + otacSmsApiUsername + "&pwd=" +
	 * otacSmsApiPassword + "&ctype=" + otacSmsApiCtype + "&alert=" +
	 * otacSmsApiAlert + "&msgtype=" + otacSmsApiMsgType + "&priority=" +
	 * otacSmsApiPriority + "&sender=" + otacSmsApiSender + "&pno=" + mobile +
	 * "&msgtxt=" + otac + otacSmsApiMsg; }
	 * 
	 * else { sms_url =
	 * "http://alerts.solutionsinfini.com/api/v3/index.php?method=sms" +
	 * "&api_key=A6f8b45980b586dd9a2fc8281e9dc5b34&to=" + mobile +
	 * "&sender=MYRTAX" +
	 * "&message=Hello,%20Welcome%20to%20MakeYourTax.com.%20The%20OTP%20for%20registration%20is%20"
	 * + otac + "&format=json&custom=1,2&flash=0"; }
	 * 
	 * logger.info("SMS URL Login" + sms_url);
	 * 
	 * // System.out.println("SMS URL: "+sms_url);
	 * 
	 * ClientResponse sms_response = Common.apiAcess( sms_url, "", "get");
	 * 
	 * String sms_str = sms_response .getEntity(String.class);
	 * 
	 * logger.info("SMS Response Login" + sms_str);
	 * 
	 * String smsResponseStatus = String .valueOf(sms_response.getStatus());
	 * 
	 * if (!smsResponseStatus.equals("200")) {
	 * 
	 * String errorMsg = this .getServerErrorMsg(smsResponseStatus);
	 * 
	 * logger.info("SMS Response Status: " + smsResponseStatus); }
	 *//******* Otac Implementation End *******//*
	
	 * 
	 * } }
	 * 
	 * else {
	 * 
	 * jsonObject = new JSONObject();
	 * 
	 * String errorMsg = this .getServerErrorMsg(responseStatus);
	 * 
	 * jsonObject.put("ResponseCode", responseStatus);
	 * 
	 * jsonObject.put("Responsedetails", errorMsg);
	 * 
	 * logger.info(responseStatus + " : " + errorMsg);
	 * 
	 * }
	 * 
	 * jsonObject = c.load(jsonObject);
	 * 
	 * String jsonStringFinal = jsonObject.toString();
	 * 
	 * System.out.println("final is catch :" + jsonStringFinal);// akhane //
	 * ache // final // response // invalid // credentials
	 * 
	 * // out.print(jsonStringFinal);//print hochhe
	 * 
	 * // added by arup paul
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } catch (Exception e) {
	 * 
	 * e.printStackTrace();
	 * 
	 * JSONObject jsonObject = new JSONObject();
	 * 
	 * jsonObject.put("error", messageUtil.getBundle("wrong.message"));
	 * 
	 * String jsonStringFinal = jsonObject.toString();
	 * 
	 * out.print(jsonStringFinal);
	 * 
	 * logger.error("In the login API Exception: ", e);
	 * 
	 * }
	 * 
	 * out.close();
	 * 
	 * }
	 

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "/api/resendotac", method = RequestMethod.POST)
	public void resendOtac(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		Common c = new Common();
		Gson json = new Gson();

		// HttpSession sessionToken = null;

		logger.info("Resend otac API");

		try {

			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String userID = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("userid"));

			// System.out.println("in controller user id is>>>>>>>>>>>>>>> "+userID);

			if (userID == null)
				userID = "";

			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			// System.err.println("session token from mobile end >>>>>>>>>>> "+
			// session_token);

			if (session_token == null)
				session_token = "";

			String mobileno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("mobileno"));

			if (mobileno == null)
				mobileno = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {

				System.out.println("inside resend octac 1 !!!!!!");

				JSONParser parser = new JSONParser();

				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession powerAccessSessionToken = request
							.getSession(false);

					String clientIp = c.getUserIp(request);

					// System.out.println("inside arup session token >>>>>>>>>>>>>> "+
					// powerAccessSessionToken.getId());

					// System.err.println("session token from mobile app >>>>>>>>>>>>>>>>>> "+
					// session_token);

					// System.out.println("client ip is >>>>>>>>>>>>>"+sessionToken.getAttribute("client_ip"));

					// System.out.println("machine ip  ip is >>>>>>>>>>>>>"+clientIp);

					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userID.trim());

					// System.out.println("in octac controller "+powerSessionToken.getSessionToken());

					
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService
					 * .getSessionToken(userID).getSessionToken
					 * ().equals(session_token)) {
					 

					if (clientIp.equals(powerAccessSessionToken
							.getAttribute("client_ip"))
							&& !(powerAccessSessionToken.isNew())
							&& (powerAccessSessionToken.getId())
									.equals(session_token)
							&& powerSessionTokenByUserId.getSessionToken()
									.equals(session_token)) {

						if (userID.equals("") || mobileno.equals("")) {

							map.put("ResponseCode",
									messageUtil
											.getBundle("failed.sessiontoken.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.failed.sessiontoken"));

							logger.info("202 : Failed To send");

						} else {

							*//******* Otac Implementation Start *******//*

							String pattern = "yyyy-MM-dd HH:mm:ss";

							SimpleDateFormat format = new SimpleDateFormat(
									pattern);

							String formattedCreateDate = format
									.format(new Date());

							Date date = format.parse(formattedCreateDate);

							Timestamp timestamp = new Timestamp(date.getTime());

							String otac = c.generateOtac(otacMin, otacMax);

							// ....................modified by Arup
							// paul........................................//

							PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();

							payconnectOtacModel.setOtac(otac);

							payconnectOtacModel.setUserId(userID);

							payconnectOtacModel.setDeviceId("Not updated");

							payconnectOtacModel.setRegisteredPhone(mobileno);

							payconnectOtacModel.setCreateDate(timestamp);

							payconnectOtacModel.setSystem(systemValueFromApi);

							// ................................................................//

							payconnectOtacBusiness
									.insertOrUpdateOctacModel(payconnectOtacModel);

							if (mobileno.length() == 10) {
								mobileno = "91" + mobileno;
							}

							String sms_url = "";

							if (smsServer.equals("axis")) {

								sms_url = otacSmsApiPath + "?dcode="
										+ otacSmsApiDecode + "&userid="
										+ otacSmsApiUsername + "&pwd="
										+ otacSmsApiPassword + "&ctype="
										+ otacSmsApiCtype + "&alert="
										+ otacSmsApiAlert + "&msgtype="
										+ otacSmsApiMsgType + "&priority="
										+ otacSmsApiPriority + "&sender="
										+ otacSmsApiSender + "&pno="
										+ "9051589129" + "&msgtxt=" + otac
										+ otacSmsApiMsg;
							}

							else {
								sms_url = "http://alerts.solutionsinfini.com/api/v3/index.php?method=sms"
										+ "&api_key=A6f8b45980b586dd9a2fc8281e9dc5b34&to="
										+ "9051589129"
										+ "&sender=MYRTAX"
										+ "&message=Hello,%20Welcome%20to%20MakeYourTax.com.%20The%20OTP%20for%20registration%20is%20"
										+ otac
										+ "&format=json&custom=1,2&flash=0";
							}

							logger.info("SMS URL Resend:" + sms_url);

							ClientResponse sms_response = Common.apiAcess(
									sms_url, "", "get");

							String sms_str = sms_response
									.getEntity(String.class);

							String responseStatus = String.valueOf(sms_response
									.getStatus());

							logger.info("SMS Response Resend: " + sms_str);

							*//******* Otac Implementation End *******//*

							if (!responseStatus.equals("200")) {

								String errorMsg = this
										.getServerErrorMsg(responseStatus);

								logger.info("SMS Response Status:"
										+ responseStatus);
							}

							map.put("ResponseCode",
									messageUtil
											.getBundle("resendotac.success.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.success.responsedetails"));

							logger.info("200 : Resend otac API-Success");

						}

						String jsonString = json.toJson(map);

						Object obj = parser.parse(jsonString);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					} else {

						map.put("ResponseCode",
								messageUtil
										.getBundle("resendotac.invalid.sessiontoken.responsedetails"));

						map.put("error", messageUtil
								.getBundle("resendotac.invalid.sessiontoken"));

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {

					map.put("ResponseCode", messageUtil
							.getBundle("resendotac.missing.sessiontoken"));

					map.put("error",
							messageUtil
									.getBundle("resendotac.missing.sessiontoken.responsedetails"));

					logger.info("210 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = c.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

			if (systemValueInController == 2) {

				System.out.println("inside resend octac 2 !!!!!!");

				JSONParser parser = new JSONParser();

				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession payProSessionToken = request.getSession(false);

					String clientIp = c.getUserIp(request);

					System.out.println("machine ip  ip is >>>>>>>>>>>>>"
							+ clientIp);

					// SessionToken
					// powerSessionToken=powerAccessSessionTokenDAO.getPayPowerSessionToken(userID);

					// System.out.println("inside 2 user id is "+userID );

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userID.trim());

					if (clientIp.equals(payProSessionToken
							.getAttribute("client_ip"))
							&& payProSessionTokenByUserId.getSessionToken()
									.equals(session_token)) {

						if (userID.equals("") || mobileno.equals("")) {

							map.put("ResponseCode",
									messageUtil
											.getBundle("failed.sessiontoken.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.failed.sessiontoken"));

							logger.info("202 : Failed To send");

						} else {

							String pattern = "yyyy-MM-dd HH:mm:ss";

							SimpleDateFormat format = new SimpleDateFormat(
									pattern);

							String formattedCreateDate = format
									.format(new Date());

							Date date = format.parse(formattedCreateDate);

							Timestamp timestamp = new Timestamp(date.getTime());

							String otac = c.generateOtac(otacMin, otacMax);

							PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();

							payconnectOtacModel.setOtac(otac);

							payconnectOtacModel.setUserId(userID);

							payconnectOtacModel.setDeviceId("Not updated");

							payconnectOtacModel.setRegisteredPhone(mobileno);

							payconnectOtacModel.setCreateDate(timestamp);

							payconnectOtacModel.setSystem(systemValueFromApi);

							payconnectOtacBusiness
									.insertOrUpdateOctacModel(payconnectOtacModel);

							String smsGetWayResponse = PayconnectOtacBusiness
									.getResponseFromSmsApi("9051589129",
											payconnectOtacModel.getOtac());

							System.err.println("smsGetWayResponse >>>>>>>"
									+ smsGetWayResponse);

							logger.info("smsGetWayResponse >>>>>>> "
									+ smsGetWayResponse);

							map.put("ResponseCode",
									messageUtil
											.getBundle("resendotac.success.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.success.responsedetails"));

							logger.info("200 : Resend otac API-Success");

						}

						String jsonString = json.toJson(map);

						Object obj = parser.parse(jsonString);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					} else {

						map.put("ResponseCode",
								messageUtil
										.getBundle("resendotac.invalid.sessiontoken.responsedetails"));

						map.put("error", messageUtil
								.getBundle("resendotac.invalid.sessiontoken"));

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {

					map.put("ResponseCode", messageUtil
							.getBundle("resendotac.missing.sessiontoken"));

					map.put("error",
							messageUtil
									.getBundle("resendotac.missing.sessiontoken.responsedetails"));

					logger.info("403 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = c.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			logger.error("Resend otac API Exception: ", e);

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

		}

		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "/api/otacvalidation", method = RequestMethod.POST)
	public void validateOtac(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();

		Common common = new Common();

		Gson json = new Gson();

		// String enc_split[];

		// HttpSession sessionToken = null;

		logger.info("Validation otac API");

		try {
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String userID = common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			System.out.println("user id is " + userID);

			if (userID == null)
				userID = "";

			String session_token = common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			System.out.println("session token is " + session_token);

			if (session_token == null)
				session_token = "";

			String otac = common.decode(Common.PRIVATE_KEY,
					request.getParameter("otac"));

			System.out.println("otac is " + otac);

			if (otac == null)
				otac = "";

			String systemValueFromApi = common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			System.out.println("system value is " + systemValueFromApi);

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {

				System.out.println("inside octac validation 1 !!!!!!!!!");

				JSONParser parser = new JSONParser();

				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					// sessionToken = request.getSession();

					HttpSession powerAccessSessionToken = request
							.getSession(false);

					// SessionToken objSessionToken1 = new SessionToken();

					// objSessionToken1.setSessionToken(session_token);

					// objSessionToken1.setUserID(userID);

					// SessionToken objSessionToken =
					// sessionTokenService.getUserByToken(objSessionToken1);

					// String clientIp = common.getUserIp(request);

					String clientIp = common.getUserIp(request);

					System.out.println("machine ip  ip is >>>>>>>>>>>>>"
							+ clientIp);

					// SessionToken
					// powerSessionToken=powerAccessSessionTokenDAO.getPayPowerSessionToken(userID);

					// System.out.println("inside 2 user id is "+userID );

					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userID.trim());

					
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userID)
					 * .getSessionToken().equals(session_token)) {
					 

					
					 * if (clientIp.equals(powerAccessSessionToken.getAttribute(
					 * "client_ip")) && !(powerAccessSessionToken.isNew()) &&
					 * (powerAccessSessionToken.getId()).equals(session_token)
					 * && powerSessionTokenByUserId.getSessionToken().equals(
					 * session_token)) {
					 

					if (powerSessionTokenByUserId.getSessionToken().equals(
							session_token)) {

						if (userID.equals("") || otac.equals("")) {

							map.put("ResponseCode", "202");

							map.put("Responsedetails",
									"Otac or user id can't be blank");

							logger.info("202 : Otac or user id can't be blank");

						}

						else {
							*//******* Otac Implementation Start *******//*

							// ArrayList otacList = (ArrayList)
							// otacService.getLastOtacList(userID,
							// systemValueFromApi);

							// OtacModel otacModel = (OtacModel)
							// otacList.get(0);

							OtacModel otacModel = payconnectOtacBusiness
									.getOtacByUserAndSystemValue(userID,
											systemValueFromApi);

							if (otacModel != null) {

								if (otac.equals(otacModel.getOtac())) {

									if (otacModel.getIsValidated().equals("0")) {

										Date d1 = null;

										Date d2 = null;

										SimpleDateFormat format = new SimpleDateFormat(
												"MM/dd/yyyy HH:mm:ss");

										String currentDate = format
												.format(new Date());

										String createDate = format
												.format(otacModel
														.getCreateDate());

										d1 = format.parse(String
												.valueOf(createDate));

										d2 = format.parse(currentDate);

										// in milliseconds
										long diff = d2.getTime() - d1.getTime();

										long diffSeconds = diff / 1000;

										System.out
												.println("difference second >>>>>>>>>>>> "
														+ otacValidity);

										System.out
												.println("difference second +++++++++++ >>>>>>>>>>>> "
														+ diffSeconds);

										if (diffSeconds < otacValidity) {

											otacModel.setIsValidated("1");

											// otacModel.setIsValidated("1");//logic
											// here

											// otacService.updateOtac(otacModel);//logic
											// here

											payconnectOtacBusiness
													.updateOtac(otacModel);

											map.put("ResponseCode", "200");

											map.put("Responsedetails",
													"Success");

											map.put("Userid", userID);

											map.put("sessiontoken",
													session_token);

											logger.info("200 : Validation otac API-Success");

										} else {

											map.put("ResponseCode", "202");

											map.put("Responsedetails",
													"OTP Expired");

											logger.info("202 : OTP Expired");
										}

									} else {
										map.put("ResponseCode", "202");

										map.put("Responsedetails",
												"OTP is already used");

										logger.info("202 : OTP is already used");
									}
								} else {
									map.put("ResponseCode", "202");

									map.put("Responsedetails", "Invalid OTP");

									logger.info("202 : Invalid OTP");
								}

							} else {
								map.put("ResponseCode", "202");

								map.put("Responsedetails",
										"User ID and System Value are can't be null");

								logger.info("202 : User ID can't be null");
							}

							*//******* Otac Implementation End *******//*

						}

						String jsonString = json.toJson(map);
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}

					else {

						map.put("ResponseCode", "498");

						map.put("error", "Invalid Session Token.");

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {

					map.put("error", "Session Token is missing.");
					// map.put("ip", c.getIp(request));
					logger.info("210 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = common.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			if (systemValueInController == 2) {

				System.out.println("inside octac validation 2 !!!!!!!!!");

				JSONParser parser = new JSONParser();

				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession payProSessionToken = request.getSession(false);

					// SessionToken objSessionToken1 = new SessionToken();
					// objSessionToken1.setSessionToken(session_token);
					// objSessionToken1.setUserID(userID);
					// SessionToken objSessionToken =
					// sessionTokenService.getUserByToken(objSessionToken1);

					String clientIp = common.getUserIp(request);

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userID.trim());

					
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userID)
					 * .getSessionToken().equals(session_token)) {
					 

					if (clientIp.equals(payProSessionToken
							.getAttribute("client_ip"))
							&& !(payProSessionToken.isNew())
							&& (payProSessionToken.getId())
									.equals(session_token)
							&& payProSessionTokenByUserId.getSessionToken()
									.equals(session_token)) {

						if (userID.equals("") || otac.equals("")) {

							map.put("ResponseCode", "202");

							map.put("Responsedetails",
									"Otac or user id can't be blank");

							logger.info("202 : Otac or user id can't be blank");

						} else {
							*//******* Otac Implementation Start *******//*

							// ArrayList otacList = (ArrayList)
							// otacService.getLastOtacList(userID,
							// systemValueFromApi);

							// OtacModel otacModel = (OtacModel)
							// otacList.get(0);

							OtacModel otacModel = payconnectOtacBusiness
									.getOtacByUserAndSystemValue(userID,
											systemValueFromApi);

							if (otacModel != null) {

								if (otac.equals(otacModel.getOtac())) {

									if (otacModel.getIsValidated().equals("0")) {

										Date d1 = null;
										Date d2 = null;

										SimpleDateFormat format = new SimpleDateFormat(
												"MM/dd/yyyy HH:mm:ss");
										String currentDate = format
												.format(new Date());
										String createDate = format
												.format(otacModel
														.getCreateDate());

										d1 = format.parse(String
												.valueOf(createDate));
										d2 = format.parse(currentDate);

										// in milliseconds
										long diff = d2.getTime() - d1.getTime();

										long diffSeconds = diff / 1000;

										if (diffSeconds < otacValidity) {

											// otacModel.setIsValidated("1");

											// otacService.updateOtac(otacModel);

											otacModel.setIsValidated("1");

											payconnectOtacBusiness
													.updateOtac(otacModel);

											map.put("ResponseCode", "200");

											map.put("Responsedetails",
													"Success");

											logger.info("200 : Validation otac API-Success");

											map.put("Userid", userID);

											map.put("sessiontoken",
													session_token);

										} else {

											map.put("ResponseCode", "202");

											map.put("Responsedetails",
													"Otac Expired");

											logger.info("202 : Otac Expired");
										}

									} else {
										map.put("ResponseCode", "202");
										map.put("Responsedetails",
												"Otac is already used");

										logger.info("202 : Otac is already used");
									}
								} else {
									map.put("ResponseCode", "202");
									map.put("Responsedetails", "Invalid Otac");

									logger.info("202 : Invalid Otac");
								}

							} else {

								map.put("ResponseCode", "202");

								map.put("Responsedetails",
										"Otac or user id can't be null");

								logger.info("202 : User ID can't be null");
							}

							*//******* Otac Implementation End *******//*

						}

						String jsonString = json.toJson(map);
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					} else {
						map.put("ResponseCode", "498");

						map.put("error", "Invalid Session Token.");

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = common.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");

					map.put("error", "Session Token is missing.");

					logger.info("498 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("In the login API Exception: ", e);

		}
		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/getproductlist", method = RequestMethod.POST)
	public void getProductList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();

		Gson json = new Gson();

		Common c = new Common();

		// HttpSession sessionToken = null;

		logger.info("Getproductlist API");

		try {

			String product_list = c.decode(Common.PRIVATE_KEY,
					request.getParameter("product_list"));

			if (product_list == null)
				product_list = "";

			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String api_key = "";
			if (api_key == null)
				api_key = "";

			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			if (session_token == null)
				session_token = "";

			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			if (userid == null)
				userid = "";

			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));

			if (mpin == null)
				mpin = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {

				System.out.println("inside get product list 1");

				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession powerAccessSessionToken = request
							.getSession(false);

					// sessionToken = request.getSession();
					// SessionToken objSessionToken1 = new SessionToken();
					// objSessionToken1.setSessionToken(session_token);
					// objSessionToken1.setUserID(userid);
					// SessionToken objSessionToken =
					// sessionTokenService.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userid.trim());

					
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userid)
					 * .getSessionToken().equals(session_token)) {
					 

					if (clientIp.equals(powerAccessSessionToken
							.getAttribute("client_ip"))
							&& powerSessionTokenByUserId.getSessionToken()
									.equals(session_token)) {

						String url = apiUrl + "get_product_list";

						String inputString = Common.productListApiInputString(
								api_key, userid, session_token, mpin,
								product_list);

						
						 * String inputString = "{\"api_key\" : \"" +
						 * api_key.trim() + "\",\"userid\" : \"" + userid.trim()
						 * + "\",\"session_token\" : \""//need to chnage +
						 * session_token.trim() + "\",\"Mpin\" : \"" +
						 * mpin.trim() + "\",\"Product_list\" : \"" +
						 * product_list.trim() + "\"}";
						 

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getproductlist:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);

						} else {

							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);

							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "SessionToken is Missing");

					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInController == 2) {

				System.out.println("inside get product list 2");

				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession payProSessionToken = request.getSession(false);

					String clientIp = c.getUserIp(request);

					// sessionToken = request.getSession();

					// SessionToken objSessionToken1 = new SessionToken();

					// objSessionToken1.setSessionToken(session_token);

					// objSessionToken1.setUserID(userid);

					// SessionToken objSessionToken =
					// sessionTokenService.getUserByToken(objSessionToken1);

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userid.trim());

					
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userid)
					 * .getSessionToken().equals(session_token)) {
					 

					if (clientIp.equals(payProSessionToken
							.getAttribute("client_ip"))
							&& payProSessionTokenByUserId.getSessionToken()
									.equals(session_token)) {

						String url = apiUrl + "get_product_list";

						
						 * String inputString = "{\"api_key\" : \"" +
						 * api_key.trim() + "\",\"userid\" : \"" + userid.trim()
						 * + "\",\"session_token\" : \"" + session_token.trim()
						 * + "\",\"Mpin\" : \"" + mpin.trim() +
						 * "\",\"Product_list\" : \"" + product_list.trim() +
						 * "\"}";
						 

						String inputString = Common.productListApiInputString(
								api_key, userid, session_token, mpin,
								product_list);

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getproductlist:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");

					map.put("error", "SessionToken is Missing");

					// map.put("ip", c.getIp(request));//change

					logger.info("210 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = c.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("Getproductlist API Exception: ", e);

		}

		out.close();
	}

	@RequestMapping(value = "/api/checksmsconfirmation1", method = RequestMethod.POST)
	public void checkSmsConfirmation(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
		Common c = new Common();
		Gson json = new Gson();
		String enc_split[];
		HttpSession sessionToken = null;

		logger.info("Check Sms Confirmation API");

		try {
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String userID = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userID == null)
				userID = "";

			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out.println("inside checksmsconfirmation 1 !!!!!!!!");

				JSONParser parser = new JSONParser();
				HashMap map = new HashMap();
				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					sessionToken = request.getSession();

					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userID);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userID)
									.getSessionToken().equals(session_token)) {

						if (userID.equals("")) {
							map.put("ResponseCode", "202");
							map.put("Responsedetails",
									"User ID should not be blank");
							// map.put("ip", c.getIp(request));
							logger.info("202 : User ID should not be blank");
						} else {
							*//******* Otac Implementation Start *******//*

							ArrayList otacList = (ArrayList) otacService
									.getLastOtacList(userID, systemValueFromApi);
							OtacModel otacModel = (OtacModel) otacList.get(0);
							// System.out.println("Otac in checksmsconfirmation:"+otacModel.getVerifyDate());
							if (otacModel.getVerifyDate() != null) {
								map.put("ResponseCode", "202");
								map.put("Responsedetails", "Already verified");
								// map.put("ip", c.getIp(request));
								logger.info("202 : Already verified");
							} else {
								if (otacModel.getStatus().equals("1")) {

									// ------------------
									PhoneMatchSetting phoneSettings = otacService
											.getPhoneConfig();

									String sameStatus = phoneSettings
											.getPhonenumbersame();
									String diffStatus = phoneSettings
											.getPhonenumberdifferent();
									// ------------------

									if (otacModel.getIsMatch().equals("1")) {

										if (sameStatus.equals("0")) {
											map.put("ResponseCode", "203");
											map.put("Responsedetails",
													"Success");
											// map.put("ip", c.getIp(request));
										} else {
											map.put("ResponseCode", "200");
											map.put("Responsedetails",
													"Success");
											// map.put("ip", c.getIp(request));

										}

									} else {

										if (diffStatus.equals("0")) {
											map.put("ResponseCode", "203");
											map.put("Responsedetails",
													"The sender phone number is not matching with the registered mobile number");
											// map.put("ip", c.getIp(request));
											logger.info("203 : The sender phone number is not matching with the registered mobile number.");
										} else {
											map.put("ResponseCode", "200");
											map.put("Responsedetails",
													"The sender phone number is not matching with the registered mobile number");
											// map.put("ip", c.getIp(request));
											logger.info("200 : The sender phone number is not matching with the registered mobile number.");
										}

									}

									String pattern = "yyyy-MM-dd HH:mm:ss";
									SimpleDateFormat format = new SimpleDateFormat(
											pattern);
									String formattedCreateDate = format
											.format(new Date());
									Date date = format
											.parse(formattedCreateDate);
									Timestamp timestamp = new Timestamp(
											date.getTime());

									otacModel.setVerifyDate(timestamp);
									otacService.updateOtac(otacModel);
								} else {
									map.put("ResponseCode", "202");
									map.put("Responsedetails", "Not Received");
									// map.put("ip", c.getIp(request));
									logger.info("202 : Not Received");
								}
							}

							*//******* Otac Implementation End *******//*

						}

						String jsonString = json.toJson(map);
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;
						// jsonObject.put("ip", c.getIp(request));//change
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid Session Token.");
						// map.put("ip", c.getIp(request));
						logger.info("210 : Invalid Session Token.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "Session Token is missing.");
					// map.put("ip", c.getIp(request));
					logger.info("210 : Session Token is missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			if (systemValueInJava == 2) {

				System.out.println("inside checksmsconfirmation 2 !!!!!!!!");

				JSONParser parser = new JSONParser();
				HashMap map = new HashMap();
				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					sessionToken = request.getSession();

					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userID);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userID)
									.getSessionToken().equals(session_token)) {

						if (userID.equals("")) {

							map.put("ResponseCode", "202");

							map.put("Responsedetails",
									"User ID should not be blank");

							logger.info("202 : User ID should not be blank");

						} else {
							*//******* Otac Implementation Start *******//*

							ArrayList otacList = (ArrayList) otacService
									.getLastOtacList(userID, systemValueFromApi);

							OtacModel otacModel = (OtacModel) otacList.get(0);

							if (otacModel.getVerifyDate() != null) {

								map.put("ResponseCode", "202");

								map.put("Responsedetails", "Already verified");

								// map.put("ip", c.getIp(request));

								logger.info("202 : Already verified");
							} else {
								if (otacModel.getStatus().equals("1")) {

									// ------------------
									PhoneMatchSetting phoneSettings = otacService
											.getPhoneConfig();

									String sameStatus = phoneSettings
											.getPhonenumbersame();

									String diffStatus = phoneSettings
											.getPhonenumberdifferent();
									// ------------------

									if (otacModel.getIsMatch().equals("1")) {

										if (sameStatus.equals("0")) {

											map.put("ResponseCode", "203");

											map.put("Responsedetails",
													"Success");

										} else {
											map.put("ResponseCode", "200");

											map.put("Responsedetails",
													"Success");

										}

									} else {

										if (diffStatus.equals("0")) {

											map.put("ResponseCode", "203");

											map.put("Responsedetails",
													"The sender phone number is not matching with the registered mobile number");

											logger.info("203 : The sender phone number is not matching with the registered mobile number.");
										} else {

											map.put("ResponseCode", "200");

											map.put("Responsedetails",
													"The sender phone number is not matching with the registered mobile number");

											logger.info("200 : The sender phone number is not matching with the registered mobile number.");
										}

									}

									String pattern = "yyyy-MM-dd HH:mm:ss";
									SimpleDateFormat format = new SimpleDateFormat(
											pattern);
									String formattedCreateDate = format
											.format(new Date());
									Date date = format
											.parse(formattedCreateDate);
									Timestamp timestamp = new Timestamp(
											date.getTime());

									otacModel.setVerifyDate(timestamp);
									otacService.updateOtac(otacModel);
								} else {
									map.put("ResponseCode", "202");

									map.put("Responsedetails", "Not Received");

									logger.info("202 : Not Received");
								}
							}

							*//******* Otac Implementation End *******//*

						}

						String jsonString = json.toJson(map);
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;
						// jsonObject.put("ip", c.getIp(request));//change
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					} else {
						map.put("ResponseCode", "498");

						map.put("error", "Invalid Session Token.");

						logger.info("210 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}

				} else {

					map.put("ResponseCode", "403");

					map.put("error", "Session Token is missing.");

					logger.info("210 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = c.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

			else {

				System.out
						.println("inside wrong  checksmsconfirmation  !!!!!!!!");

				HashMap map = new HashMap();

				JSONObject jsonObject = new JSONObject();

				map.put("Res ponseCode", "202");

				map.put("Responsedetails", "Wrong System Value");
				// to do for pay pro login api

				jsonObject = c.load(jsonObject);

				String jsonStringFinal = jsonObject.toString();

				out.print(jsonStringFinal);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Check Sms Confirmation API Exception: ", e);
		}
		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws NoTextGivenException
	 * @throws NoPassGivenException
	 *//*

	@RequestMapping(value = "/api/matchRecipient1", method = RequestMethod.POST)
	public void receiveFromSMSGateway(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();

		Common c = new Common();
		

		Gson json = new Gson();

		String enc_split[];

		HashMap mp = new HashMap();

		logger.info("Match Recipient API");

		String recipient_mobile_number = null;

		String enc_str = null;

		try {

			if (smsServer.equals("axis")) {

				recipient_mobile_number = request
						.getParameter("recipient_mobile_number");

				enc_str = request.getParameter("enc_str");

			} else {
				recipient_mobile_number = request.getParameter("from");

				enc_str = request.getParameter("message");
			}

			if (enc_str == null || recipient_mobile_number == null) {

				mp.put("ResponseCode", "202");

				mp.put("Responsedetails", "Inputs should not be null");

				// mp.put("ip", c.getIp(request));

				logger.info("202 : Inputs should not be null");

			} else {

				String[] enc_space_split = enc_str.split(" ");

				String enc_decode = "";

				if (smsServer.equals("axis")) {

					@SuppressWarnings("deprecation")
					String decodedStr = URLDecoder.decode(enc_space_split[1]);

					enc_decode = c.decode(Common.PRIVATE_KEY, decodedStr);

				} else {
					enc_decode = c.decode(Common.PRIVATE_KEY,
							enc_space_split[1]);
				}

				enc_split = enc_decode.split("~");
				// deviceiduseridmobileno

				String mobile_no = enc_split[2];
				// System.out.println("Mobile:"+mobile_no);
				// System.out.println("Recepient:"+recipient_mobile_number);

				if (recipient_mobile_number.length() > 10) {

					int length = recipient_mobile_number.length();

					int length_needed = length - 10;

					recipient_mobile_number = recipient_mobile_number
							.substring(length_needed, length);
				}

				if (mobile_no.length() > 10) {
					int length = mobile_no.length();
					int length_needed = length - 10;
					mobile_no = mobile_no.substring(length_needed, length);
				}

				String pattern = "yyyy-MM-dd HH:mm:ss";
				SimpleDateFormat format = new SimpleDateFormat(pattern);
				String formattedCreateDate = format.format(new Date());
				Date date = format.parse(formattedCreateDate);
				Timestamp timestamp = new Timestamp(date.getTime());

				if (mobile_no.equals(recipient_mobile_number)) {

					// OtacModel otacModel = new OtacModel();
					// System.out.println("User:"+enc_split[1]);
					// OtacModel otacModel =
					// otacService.getLastOtacList(enc_split[1]);

					// here change
					// ArrayList otacList = (ArrayList)
					// otacService.getLastOtacList(enc_split[1]);

					ArrayList otacList = (ArrayList) otacService
							.getLastOtacListForMatchReceipent(enc_split[1]);

					OtacModel otacModel = (OtacModel) otacList.get(0);

					// System.out.println("Model:"+otacModel);
					// System.out.println("Otac in matchrecipient:"+otacModel.getOtac());

					if (otacModel != null) {

						String isValidated = otacModel.getIsValidated();

						if (isValidated.equals("1")) {
							// System.out.println(otacModel.getRegisteredPhone());
							otacModel.setUserID(enc_split[1]);
							otacModel.setStatus("1");
							otacModel.setIsMatch("1");
							otacModel.setSmsReceiveDate(timestamp);
							otacModel.setEncStr(enc_space_split[1]);
							otacModel.setMobileNo(recipient_mobile_number);
							otacModel.setDecStr(enc_decode);
							otacModel.setDeviceId(enc_split[0]);

							otacService.updateOtac(otacModel);
							mp.put("ResponseCode", "200");
							mp.put("Responsedetails", "Matched");
							// mp.put("ip", c.getIp(request));
						} else {
							mp.put("ResponseCode", "202");
							mp.put("Responsedetails", "OTP not yet validated");
							// mp.put("ip", c.getIp(request));

							logger.info("202 : OTP not yet validated.");
						}

					} else {
						mp.put("ResponseCode", "202");
						mp.put("Responsedetails", "User Id can't be null");
						// mp.put("ip", c.getIp(request));
						logger.info("202 : User Id can't be null.");
					}

				} else {

					// here i change it

					// ArrayList otacList = (ArrayList)
					// otacService.getLastOtacList(enc_split[1]);

					ArrayList otacList = (ArrayList) otacService
							.getLastOtacListForMatchReceipent(enc_split[1]);

					OtacModel otacModel = (OtacModel) otacList.get(0);
					// System.out.println("Model:"+otacModel);
					// System.out.println("Otac in matchrecipient:"+otacModel.getOtac());

					if (otacModel != null) {

						String isValidated = otacModel.getIsValidated();

						if (isValidated.equals("1")) {
							System.out.println(otacModel.getRegisteredPhone());

							otacModel.setUserID(enc_split[1]);
							otacModel.setStatus("1");
							otacModel.setIsMatch("0");
							otacModel.setSmsReceiveDate(timestamp);
							otacModel.setEncStr(enc_space_split[1]);
							otacModel.setMobileNo(recipient_mobile_number);
							otacModel.setDecStr(enc_decode);
							otacModel.setDeviceId(enc_split[0]);

							otacService.updateOtac(otacModel);

							mp.put("ResponseCode", "202");
							mp.put("Responsedetails", "Not Matched");
							// mp.put("ip", c.getIp(request));
							logger.info("202 : Not Matched.");
						} else {
							mp.put("ResponseCode", "202");
							mp.put("Responsedetails", "OTP not yet validated");
							// mp.put("ip", c.getIp(request));
							logger.info("202 : OTP not yet validated.");
						}
					} else {
						mp.put("ResponseCode", "202");
						mp.put("Responsedetails", "User Id can't be null");
						// mp.put("ip", c.getIp(request));
						logger.info("202 : User Id can't be null.");
					}
				}
			}

			String output = json.toJson(mp);

			out.print(output);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Match Recipient API Exception: ", e);
		}
		out.close();

	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/getuseraccountreport1", method = RequestMethod.POST)
	public void dashBoard(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		// LinkedHashMap outPutMap = new LinkedHashMap();
		Common c = new Common();
		Gson json = new Gson();

		logger.info("Get User Account Report API");

		try {

			HashMap<String, String> headerMap = (new Common())
					.getHeadersInfo(request);

			String api_version = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("api_version"));
			if (api_version == null)
				api_version = "";

			String corpId = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("corpId"));
			if (corpId == null)
				corpId = "";

			String userID = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			if (userID == null)
				userID = "";

			System.out.println("userID in java is " + userID);

			String session_token = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			if (session_token == null)
				session_token = "";

			System.out.println("session_token in java is " + session_token);

			
			 * String mPin = Common.decode(Common.PRIVATE_KEY, (String)
			 * headerMap.get("mpin"));
			 * 
			 * if (mPin == null) mPin = "";
			 * 
			 * System.out.println("mPin value in java is " + mPin);
			 

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			System.out.println("system value is " + systemValueFromApi);

			System.out.println("system value in java is " + systemValueFromApi);

			if (systemValueFromApi == null)

				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out.println("Inside getuseraccountreport  1  !!!!!!!!");

				HashMap<String, String> map = new HashMap<String, String>();
				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession powerAccessSessionToken = request
							.getSession(false);
					SessionToken powerSessionToken = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userID.trim());

					System.out.println(powerSessionToken.getUserID() + " <==> "
							+ powerSessionToken.getSessionToken() + " <==> "
							+ session_token);

					String clientIp = c.getUserIp(request);
					
					 * clientIp.equals(powerAccessSessionToken.getAttribute(
					 * "client_ip")) &&
					 
						 * (powerAccessSessionToken.getId()).equals(session_token
						 * ) &&
						 
					 !(powerAccessSessionToken.isNew()) && 
					if (powerSessionToken.getSessionToken().equals(
							session_token)) {

						String url = apiUrlPayconnect + "getuserDashBoard"; // URL
																			// from
																			// axis
																			// bank

						System.out.println("URL: " + url);

						String inputString = Common
								.getUserDashboradReport(api_version, userID,
										systemValueFromApi, corpId);

						System.out.println("Input JSON DATA FROM MY SIDE: "
								+ inputString);

						ClientResponse client_response = Common
								.payconnectApiAcess(url, inputString, "post");

						logger.info("Axis Server Status for getuseraccountreport:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());
						System.out
								.println("Axis Server Status for getuseraccountreport***:"
										+ client_response.getStatus());

						if (responseStatus.equals("200")) {
							System.out
									.println("Axis Server Status for getuseraccountreport***:"
											+ client_response.getStatus());

							JSONParser parser = new JSONParser();
							JSONObject jsonObject = (JSONObject) parser
									.parse(jsonString);

							System.out
									.println("axis end json response >>>>>>>>> "
											+ jsonObject.toString());

							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);

							System.out
									.println("Axis Server Status for getuseraccountreport***:"
											+ client_response.getStatus());
							System.out
									.println("Axis Server errorMsg for getuseraccountreport:"
											+ errorMsg);

							logger.info(responseStatus + " : " + errorMsg);
							// jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							System.out.println("jsonStringFinal"
									+ jsonStringFinal);
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", messageUtil
								.getBundle("error.invalid.sessiontoken.code"));
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("498 :"
								+ messageUtil
										.getBundle("error.invalid.sessiontoken.code"));
						jsonErrorMessage = json.toJson(map);

						JSONParser parser = new JSONParser();
						JSONObject jsonObject = (JSONObject) parser
								.parse(jsonErrorMessage);

						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", messageUtil
							.getBundle("error.sessiontoken.missing.code"));
					map.put("error",
							messageUtil.getBundle("error.sessiontoken.message"));
					// map.put("ip", c.getIp(request));//change
					logger.info("403 :"
							+ messageUtil
									.getBundle("error.sessiontoken.message"));
					jsonErrorMessage = json.toJson(map);

					JSONParser parser = new JSONParser();

					JSONObject jsonObject = (JSONObject) parser
							.parse(jsonErrorMessage);

					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				System.out.println("Inside getuseraccountreport  2  !!!!!!!!");

				JSONParser parser = new JSONParser();
				HashMap<String, String> map = new HashMap<String, String>();
				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession payProSession = request.getSession(false);

					PayProSessionToken payProSessionToken = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userID.trim());

					System.out.println(payProSessionToken.getUserId()
							+ payProSessionToken.getSessionToken() + " == "
							+ session_token);

					String clientIp = c.getUserIp(request);
					
					 * clientIp.equals(payProSession.getAttribute("client_ip"))
					 * &&
					  (payProSession.getId()).equals(session_token) && 
					if (  !(powerAccessSessionToken.isNew()) && payProSessionToken
							.getSessionToken().equals(session_token)) {

						String url = apiUrlPayconnect + "getuserDashBoard"; // URL
																			// from
																			// axis
																			// bank
						System.out.println(url);

						String inputString = Common
								.getUserDashboradReport(api_version, userID,
										systemValueFromApi, corpId);

						System.out.println(inputString);

						ClientResponse client_response = Common
								.payconnectApiAcess(url, inputString, "post");

						logger.info("Axis Server Status for getuseraccountreport:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;

							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);

							System.out
									.println("Axis Server Status for getuseraccountreport***:"
											+ client_response.getStatus());
							System.out
									.println("Axis Server errorMsg for getuseraccountreport:"
											+ errorMsg);

							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							System.out.println("jsonStringFinal"
									+ jsonStringFinal);
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", messageUtil
								.getBundle("error.invalid.sessiontoken.code"));
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("498 :"
								+ messageUtil
										.getBundle("error.invalid.sessiontoken.code"));
						jsonErrorMessage = json.toJson(map);

						JSONObject jsonObject = (JSONObject) parser
								.parse(jsonErrorMessage);

						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);
					}
				} else {
					map.put("ResponseCode", messageUtil
							.getBundle("error.sessiontoken.missing.code"));
					map.put("error",
							messageUtil.getBundle("error.sessiontoken.message"));
					// map.put("ip", c.getIp(request));//change
					logger.info("403 :"
							+ messageUtil
									.getBundle("error.sessiontoken.message"));
					jsonErrorMessage = json.toJson(map);
					JSONObject jsonObject = (JSONObject) parser
							.parse(jsonErrorMessage);
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error("Get User Account Report API Exception: ", e);
		}
		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "/api/creatempin1", method = RequestMethod.POST)
	public void createMpin1(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		HashMap outPut = new HashMap();
		Gson json = new Gson();
		Common c = new Common();
		String jsonString1 = null;
		SessionToken objSessionToken1 = null;
		String sessionToken_fromUI = null;
		HttpSession sessionToken = null; // this is for pre exist browser based
											// checking
		String session_id = null;

		// request.getSessionToken == exists
		*//**
		 * if exists return that in response otherwise create new same in
		 * validate mpin.
		 *//*
		logger.info("Create mpin API");

		try {

			sessionToken = request.getSession();// session returned

			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String api_key = "";

			if (api_key == null)
				api_key = "";

			// sessionToken_fromUI =c.decode(Common.PRIVATE_KEY,
			// request.getParameter("session_token"));

			String mPin = c.decode(Common.PRIVATE_KEY,
					request.getParameter("Mpin"));

			if (mPin == null)
				mPin = "";

			String deviceID = c.decode(Common.PRIVATE_KEY,
					request.getParameter("DeviceID"));

			if (deviceID == null)
				deviceID = "";

			String deviceName = c.decode(Common.PRIVATE_KEY,
					request.getParameter("DeviceName"));

			if (deviceName == null)
				deviceName = "";

			String deviceToken = c.decode(Common.PRIVATE_KEY,
					request.getParameter("devicetoken"));

			if (deviceToken == null)
				deviceToken = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("System"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";
			
			

			int systemValueInJava = Integer.parseInt(systemValueFromApi);
			

			if (systemValueInJava == 1) {

				System.out.println("inside create mpin 1 value !!!!!!!");

				JSONParser parser = new JSONParser();

				String url = apiUrl + "createvalidatempin";
				String inputString = "";

				if (request.getParameter("latitude") == null
						|| request.getParameter("longitude") == null) {

					inputString = "{\"api_key\": \"" + api_key.trim()
							+ "\",\"Mpin\": \"" + mPin.trim()
							+ "\",\"DeviceID\":\"" + deviceID.trim()
							+ "\",\"DeviceName\":\"" + deviceName + "\" }";
				} else {

					String latitude = c.decode(Common.PRIVATE_KEY,
							request.getParameter("latitude"));

					String longitude = c.decode(Common.PRIVATE_KEY,
							request.getParameter("longitude"));

					inputString = "{\"api_key\": \"" + api_key.trim()
							+ "\",\"Mpin\": \"" + mPin.trim()
							+ "\",\"DeviceID\":\"" + deviceID.trim()
							+ "\",\"DeviceName\":\"" + deviceName
							+ "\" ,\"Latitude\": \"" + latitude.trim()
							+ "\",\"Longitude\":\"" + longitude.trim() + "\"}";
				}

				ClientResponse client_response = Common.apiAcess(url,
						inputString, "post");

				logger.info("Axis Server Status for creatempin:"
						+ client_response.getStatus());

				String jsonString = client_response.getEntity(String.class);
				String responseStatus = String.valueOf(client_response
						.getStatus());

				if (responseStatus.equals("200")) {
					HashMap map = json.fromJson(jsonString, HashMap.class);
					if (map.get("Responsedetails").equals("Success")) {
						String userID = null;
						if (!deviceName.equals("")) // create mpin
						{
							userID = c.decode(Common.PRIVATE_KEY,
									(String) headerMap.get("user_id"));
							sessionToken_fromUI = c.decode(Common.PRIVATE_KEY,
									request.getParameter("session_token"));// browser
																			// based

							String clientIp = c.getUserIp(request);

							if (clientIp.equals(sessionToken
									.getAttribute("client_ip"))
									&& !(sessionToken.isNew())
									&& (sessionToken.getId())
											.equals(sessionToken_fromUI)
									&& sessionTokenService
											.getSessionToken(userID)
											.getSessionToken()
											.equals(sessionToken_fromUI)) {
								map.put("ResponseCode", "200");
								map.put("session_token", sessionToken_fromUI);
								// map.put("ip", c.getIp(request));
							} else {
								map = new HashMap();
								map.put("ResponseCode", "210");
								map.put("error", "Invalid SessionToken");
								// map.put("ip", c.getIp(request));//change
								logger.info("210 : Invalid SessionToken");

							}
						} else { // validate mpin

							if (!(sessionToken.isNew()))// if pre-exists then
														// invalidate
								sessionToken.invalidate();

							sessionToken = request.getSession();
							sessionToken.setAttribute("session_id",
									sessionToken.getId());
							sessionToken.setAttribute("client_ip",
									c.getUserIp(request));
							String session_id_validate_mpin = sessionToken
									.getId();

							userID = (String) map.get("Userid");
							// String sessionToken =
							// Common.generateSessionToken();
							map.put("ResponseCode", "200");
							map.put("session_token", session_id_validate_mpin);// session
																				// id
																				// returned
							// map.put("ip", c.getIp(request));
							// jsonString1 = json.toJson(map);
							SessionToken objSessionToken = new SessionToken();
							objSessionToken
									.setSessionToken(session_id_validate_mpin);
							if (userID != null && !userID.equals("")) {
								objSessionToken.setUserID(userID);
								objSessionToken1 = sessionTokenService
										.getSessionToken(objSessionToken
												.getUserID());
								if (objSessionToken1 != null) {
									sessionTokenService
											.deleteToken(objSessionToken);
									sessionTokenService
											.addSessionToken(objSessionToken);
								} else {
									sessionTokenService
											.addSessionToken(objSessionToken);
								}

							}
						}

						jsonString1 = json.toJson(map);
						Object obj = parser.parse(jsonString1);
						JSONObject jsonObject = (JSONObject) obj;
						// jsonObject.put("ip", c.getIp(request));//change
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					} else {
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;
						// jsonObject.put("ip", c.getIp(request));
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					// System.out.println("Response:"+responseStatus);
					JSONObject jsonObject = new JSONObject();
					String errorMsg = this.getServerErrorMsg(responseStatus);
					jsonObject.put("ResponseCode", responseStatus);
					jsonObject.put("Responsedetails", errorMsg);
					// jsonObject.put("ip", c.getIp(request));
					logger.info(responseStatus + " : " + errorMsg);
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				// to do for pay pro business logic

				System.out.println("inside create mpin 2 value !!!!!!!");

				JSONParser parser = new JSONParser();

				String url = apiUrl + "createvalidatempin";

				String inputString = "";

				if (request.getParameter("latitude") == null
						|| request.getParameter("longitude") == null) {

					inputString = "{\"api_key\": \"" + api_key.trim()
							+ "\",\"Mpin\": \"" + mPin.trim()
							+ "\",\"DeviceID\":\"" + deviceID.trim()
							+ "\",\"DeviceName\":\"" + deviceName + "\" }";
				} else {

					String latitude = c.decode(Common.PRIVATE_KEY,
							request.getParameter("latitude"));

					String longitude = c.decode(Common.PRIVATE_KEY,
							request.getParameter("longitude"));

					inputString = "{\"api_key\": \"" + api_key.trim()
							+ "\",\"Mpin\": \"" + mPin.trim()
							+ "\",\"DeviceID\":\"" + deviceID.trim()
							+ "\",\"DeviceName\":\"" + deviceName
							+ "\" ,\"Latitude\": \"" + latitude.trim()
							+ "\",\"Longitude\":\"" + longitude.trim() + "\"}";
				}

				ClientResponse client_response = Common.apiAcess(url,
						inputString, "post");

				logger.info("Axis Server Status for creatempin:"
						+ client_response.getStatus());

				String jsonString = client_response.getEntity(String.class);
				String responseStatus = String.valueOf(client_response
						.getStatus());

				if (responseStatus.equals("200")) {
					HashMap map = json.fromJson(jsonString, HashMap.class);
					if (map.get("Responsedetails").equals("Success")) {
						String userID = null;
						if (!deviceName.equals("")) // create mpin
						{
							userID = c.decode(Common.PRIVATE_KEY,
									(String) headerMap.get("user_id"));
							sessionToken_fromUI = c.decode(Common.PRIVATE_KEY,
									request.getParameter("session_token"));// browser
																			// based

							String clientIp = c.getUserIp(request);

							if (clientIp.equals(sessionToken
									.getAttribute("client_ip"))
									&& !(sessionToken.isNew())
									&& (sessionToken.getId())
											.equals(sessionToken_fromUI)
									&& sessionTokenService
											.getSessionToken(userID)
											.getSessionToken()
											.equals(sessionToken_fromUI)) {

								// added by new field arup paul

								map.put("ResponseCode", "200");

								map.put("Responsedetails", "Success");

								map.put("session_token", sessionToken_fromUI);

								map.put("Firstname", "Xyz");

								map.put("Lastname", "Xyz");

								map.put("CurrentDateTime",
										"03/06/2015 18:06:12");

								map.put("LastloginDateTime",
										"02/06/2015 18:06:12");

								map.put("corpID", "");

								map.put("UserName", "");

								map.put("LastPwdChgDate", "");

								map.put("PwdExpiryDate", "");

								map.put("UserStatus", "");

								map.put("PwdChangeinWeb", "0");

								// map.put("ip", c.getIp(request));

							} else {

								map = new HashMap();

								map.put("ResponseCode", "210");

								map.put("error", "Invalid SessionToken");

								logger.info("210 : Invalid SessionToken");

							}
						} else { // validate mpin

							if (!(sessionToken.isNew()))// if pre-exists then
														// invalidate
								sessionToken.invalidate();

							sessionToken = request.getSession();
							sessionToken.setAttribute("session_id",
									sessionToken.getId());
							sessionToken.setAttribute("client_ip",
									c.getUserIp(request));
							String session_id_validate_mpin = sessionToken
									.getId();

							userID = (String) map.get("Userid");

							map.put("ResponseCode", "200");

							map.put("session_token", session_id_validate_mpin);// session

							SessionToken objSessionToken = new SessionToken();
							objSessionToken
									.setSessionToken(session_id_validate_mpin);
							if (userID != null && !userID.equals("")) {
								objSessionToken.setUserID(userID);
								objSessionToken1 = sessionTokenService
										.getSessionToken(objSessionToken
												.getUserID());
								if (objSessionToken1 != null) {
									sessionTokenService
											.deleteToken(objSessionToken);
									sessionTokenService
											.addSessionToken(objSessionToken);
								} else {
									sessionTokenService
											.addSessionToken(objSessionToken);
								}

							}
						}

						jsonString1 = json.toJson(map);
						Object obj = parser.parse(jsonString1);
						JSONObject jsonObject = (JSONObject) obj;
						// jsonObject.put("ip", c.getIp(request));//change
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					} else {
						Object obj = parser.parse(jsonString);
						JSONObject jsonObject = (JSONObject) obj;
						// jsonObject.put("ip", c.getIp(request));
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					// System.out.println("Response:"+responseStatus);
					JSONObject jsonObject = new JSONObject();
					String errorMsg = this.getServerErrorMsg(responseStatus);
					jsonObject.put("ResponseCode", responseStatus);
					jsonObject.put("Responsedetails", errorMsg);
					// jsonObject.put("ip", c.getIp(request));
					logger.info(responseStatus + " : " + errorMsg);
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			logger.error("Create mpin API Exception", e);

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("In the login API Exception: ", e);

		}

		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "/api/getaccountlist", method = RequestMethod.POST)
	public void accountList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();

		HttpSession sessionToken = null;

		logger.info("Get Account list API");

		try {

			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";
			String type = c.decode(Common.PRIVATE_KEY,
					request.getParameter("type"));
			if (type == null)
				type = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out.println("get account list system value 1 !!!!!!!");

				JSONParser parser = new JSONParser();
				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					sessionToken = request.getSession();

					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);
					*//**
					 * condition for checking session token presists and resides
					 * in DB.
					 * 
					 *//*
					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getaccountlist";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\", \"Type\" : \""
								+ type.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");
						logger.info("Axis Server Status for getaccountlist:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));
					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

			if (systemValueInJava == 2) {

				System.out.println("get account list system value 2 !!!!!!!");

				JSONParser parser = new JSONParser();
				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				
				 * if(!(sessionToken.isNew()) &&
				 * ((String)(sessionToken.getAttribute(
				 * "session_id"))).equals(sessionToken_fromUI) &&
				 * sessionTokenService
				 * .getSessionToken(userID).getSessionToken().equals
				 * (sessionToken_fromUI)){ map.put("ResponseCode", "200");
				 * map.put("session_token", sessionToken_fromUI); }
				 
				if (!session_token.equals("")) {

					sessionToken = request.getSession();

					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);
					*//**
					 * condition for checking session token presists and resides
					 * in DB.
					 * 
					 *//*
					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getaccountlist";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\", \"Type\" : \""
								+ type.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");
						logger.info("Axis Server Status for getaccountlist:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));
						logger.info("498 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));
					logger.info("403 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			e.printStackTrace();

			out.print(jsonStringFinal);

			logger.info("Get Account list API Exception: ", e);
		}

		out.close();
	}
	
	
	

	@RequestMapping(value = "api/getcorpbatchidlist", method = RequestMethod.POST)
	public void getCorpBatchIdList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Common c = new Common();
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		HttpSession sessionToken = null;

		logger.info("GetCorpbatChidList API");

		try {

			String FromDate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("fromdate"));
			if (FromDate == null)
				FromDate = "";
			String ToDate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("todate"));
			if (ToDate == null)
				ToDate = "";

			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out.println("inside getcorpbatchidlist list 1 ");

				JSONParser parser = new JSONParser();
				HashMap map = new HashMap();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getcorpbatchid";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",  \"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"FromDate\" : \""
								+ FromDate.trim() + "\",\"ToDate\" : \""
								+ ToDate.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getcorpbatchidlist:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change

					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			if (systemValueInJava == 2) {

				System.out.println("inside getcorpbatchidlist list 2 ");

				// to do for paypro business logic

				JSONParser parser = new JSONParser();
				HashMap map = new HashMap();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getcorpbatchid";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",  \"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"FromDate\" : \""
								+ FromDate.trim() + "\",\"ToDate\" : \""
								+ ToDate.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getcorpbatchidlist:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("498 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {

					map.put("ResponseCode", "403");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change

					logger.info("403 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error("GetCorpbatChidList API Exception: ", e);
		}
		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/getbankbatchidlist", method = RequestMethod.POST)
	public void getBankBatchIdList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();
		HttpSession sessionToken = null;

		logger.info("GetBankBatChidList API");

		try {

			String FromDate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("fromdate"));
			if (FromDate == null)
				FromDate = "";
			String ToDate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("todate"));
			if (ToDate == null)
				ToDate = "";

			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out.println("inside system value 1 GetBankBatChidList ");

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getbankbatchid";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",  \"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"FromDate\" : \""
								+ FromDate.trim() + "\",\"ToDate\" : \""
								+ ToDate.trim() + "\"}";
						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getbankbatchidlist:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change
					logger.info("210 : SessionToken is Missing");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				// to do for pay pro business logic

				System.out.println("inside system value 2 GetBankBatChidList ");

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getbankbatchid";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",  \"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"FromDate\" : \""
								+ FromDate.trim() + "\",\"ToDate\" : \""
								+ ToDate.trim() + "\"}";
						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getbankbatchidlist:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");

						map.put("error", "Invalid SessionToken");

						// map.put("ip", c.getIp(request));//change

						logger.info("498 : Invalid SessionToken");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");

					map.put("error", "SessionToken is Missing");

					// map.put("ip", c.getIp(request));//change

					logger.info("403 : SessionToken is Missing");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error("GetBankBatChidList API Exception: ", e);
		}
		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/gettransactionwiseauthorizationadvancesearch1", method = RequestMethod.POST)
	public void getTransactionWiseAuthorizationAdvanceSearch(
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();
		HttpSession sessionToken = null;
		logger.info("Transactionwiseauthorizationadvancesearch API");

		try {

			
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			
			System.out.println("session token is "+session_token);

			if (session_token == null)
				session_token = "";

			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			if (userid == null)
				userid = "";

			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));

			if (mpin == null)
				mpin = "";

			String apiVersion = c.decode(Common.PRIVATE_KEY,
					request.getParameter("ApiVersion"));

			if (apiVersion == null)
				apiVersion = "";

			String corpId = c.decode(Common.PRIVATE_KEY,
					request.getParameter("CorpID"));

			if (corpId == null)
				corpId = "";

			String pageno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pageno"));

			if (pageno == null)
				pageno = "";

			String pageSize = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pagesize"));

			if (pageSize == null)
				pageSize = "";

			String batch_id = c.decode(Common.PRIVATE_KEY,
					request.getParameter("batch_id"));

			if (batch_id == null)
				batch_id = "";

			String account_id = c.decode(Common.PRIVATE_KEY,
					request.getParameter("account_id"));

			if (account_id == null)
				account_id = "";

			String isonhold = c.decode(Common.PRIVATE_KEY,
					request.getParameter("isonhold"));
			if (isonhold == null)
				isonhold = "";

			String transactionamountfrom = c.decode(Common.PRIVATE_KEY,
					request.getParameter("transactionamountfrom"));

			if (transactionamountfrom == null)
				transactionamountfrom = "";

			String transactionamountto = c.decode(Common.PRIVATE_KEY,
					request.getParameter("transactionamountto"));

			if (transactionamountto == null)
				transactionamountto = "";

			String banktransid = c.decode(Common.PRIVATE_KEY,
					request.getParameter("banktransid"));

			if (banktransid == null)
				banktransid = "";

			String beneficiaryaccount = c.decode(Common.PRIVATE_KEY,
					request.getParameter("beneficiaryaccount"));

			if (beneficiaryaccount == null)
				beneficiaryaccount = "";

			String beneficiaryname = c.decode(Common.PRIVATE_KEY,
					request.getParameter("beneficiaryname"));

			if (beneficiaryname == null)
				beneficiaryname = "";

			String corporateref = c.decode(Common.PRIVATE_KEY,
					request.getParameter("corporateref"));
			if (corporateref == null)
				corporateref = "";

			String corporateAccount = c.decode(Common.PRIVATE_KEY,
					request.getParameter("CorporateAccount"));
			if (corporateAccount == null)
				corporateAccount = "";

			String productName = c.decode(Common.PRIVATE_KEY,
					request.getParameter("ProductName"));
			if (productName == null)
				productName = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {

				System.out
						.println("inside gettransactionwiseauthorizationadvancesearch system value1 ");

				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					// sessionToken = request.getSession();
					// SessionToken objSessionToken1 = new SessionToken();
					// objSessionToken1.setSessionToken(session_token);
					// objSessionToken1.setUserID(userid);
					// SessionToken objSessionToken =
					// sessionTokenService.getUserByToken(objSessionToken1);

					HttpSession powerAccessSessionToken = request
							.getSession(false);

					String clientIp = c.getUserIp(request);

					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userid.trim());
					
					System.out.println("user id is "+userid);
					
					System.out.println("session token for user is "+ powerSessionTokenByUserId.getSessionToken());

					
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userid)
					 * .getSessionToken().equals(session_token)) {
					 

					if (clientIp.equals(powerAccessSessionToken
							.getAttribute("client_ip"))
							&& powerSessionTokenByUserId.getSessionToken()
									.equals(session_token)) {

						String url = apiUrlPayconnect + "getAuthAdvSearch";

						
						 * String inputString = "{\"api_key\" : \"" +
						 * api_key.trim() + "\",\"session_token\" : \"" +
						 * session_token.trim() + "\",\"userid\" : \"" +
						 * userid.trim() + "\",\"Mpin\" : \"" + mpin.trim() +
						 * "\",\"pageno\" : \"" + pageno.trim() +
						 * "\",\"pagesize\" : \"" + pagesize.trim() +
						 * "\",\"batch_id\" : \"" + batch_id.trim() +
						 * "\",\"account_id\" : \"" + account_id.trim() +
						 * "\",\"isOnHold\" : \"" + isonhold.trim() +
						 * "\",\"TransactionAmountFrom\" : \"" +
						 * transactionamountfrom.trim() +
						 * "\",\"TransactionAmountTo\" : \"" +
						 * transactionamountto.trim() +
						 * "\",\"BankTransID\" : \"" + banktransid.trim() +
						 * "\",\"BeneficiaryAccount\" : \"" +
						 * beneficiaryaccount.trim() +
						 * "\",\"BeneficiaryCode\" : \"" +
						 * beneficiarycode.trim() +
						 * "\",\"BeneficiaryIFSC\" : \"" +
						 * beneficiaryifsc.trim() +
						 * "\",\"BeneficiaryName\" : \"" +
						 * beneficiaryname.trim() + "\",\"CorporateRef\" : \"" +
						 * corporateref.trim() + "\"}";
						 

						String inputString = Common
								.getTransactionwiseAuthorizationAdvanceSearchApiInputString(
										apiVersion, corpId, userid, systemValueFromApi,
										pageno, pageSize, batch_id, account_id,
										isonhold, transactionamountfrom,
										transactionamountto, banktransid,
										beneficiaryaccount, beneficiaryname,
										corporateref, corporateAccount,
										productName);

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for gettransactionwiseauthorizationadvancesearch:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						System.out.println(" axis response status is "
								+ responseStatus);

						if (responseStatus.equals("200")) {

							// Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) parser
									.parse(jsonString);

							jsonObject = c.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							System.out
									.println("final json string from axis end >>>>>>>>>>>>>>>>>> "
											+ jsonStringFinal);

							out.print(jsonStringFinal);

						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);

							jsonObject.put("ResponseCode", responseStatus);

							jsonObject.put("Responsedetails", errorMsg);

							logger.info(responseStatus + " : " + errorMsg);

							jsonObject = c.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						}

					} else {

						map.put("ResponseCode", "210");

						map.put("error", "Invalid SessionToken");

						logger.info("210 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map);

						// Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) parser
								.parse(jsonErrorMessage);

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");

					map.put("error", "SessionToken is Missing");

					logger.info("210 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = c.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInController == 2) {

				System.out
						.println("inside gettransactionwiseauthorizationadvancesearch system value2 ");

				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					// sessionToken = request.getSession();
					// SessionToken objSessionToken1 = new SessionToken();
					// objSessionToken1.setSessionToken(session_token);
					// objSessionToken1.setUserID(userid);
					// SessionToken objSessionToken =
					// sessionTokenService.getUserByToken(objSessionToken1);

					HttpSession payProSessionToken = request.getSession(false);

					String clientIp = c.getUserIp(request);

					System.out.println("machine ip  ip is >>>>>>>>>>>>>"
							+ clientIp);

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userid.trim());

					
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userid)
					 * .getSessionToken().equals(session_token)) {
					 

					if (clientIp.equals(payProSessionToken
							.getAttribute("client_ip"))
							&& payProSessionTokenByUserId.getSessionToken()
									.equals(session_token)) {

						String url = apiUrlPayconnect + "getAuthAdvSearch";

						
						 * String inputString = "{\"api_key\" : \"" +
						 * api_key.trim() + "\",\"session_token\" : \"" +
						 * session_token.trim() + "\",\"userid\" : \"" +
						 * userid.trim() + "\",\"Mpin\" : \"" + mpin.trim() +
						 * "\",\"pageno\" : \"" + pageno.trim() +
						 * "\",\"pagesize\" : \"" + pagesize.trim() +
						 * "\",\"batch_id\" : \"" + batch_id.trim() +
						 * "\",\"account_id\" : \"" + account_id.trim() +
						 * "\",\"isOnHold\" : \"" + isonhold.trim() +
						 * "\",\"TransactionAmountFrom\" : \"" +
						 * transactionamountfrom.trim() +
						 * "\",\"TransactionAmountTo\" : \"" +
						 * transactionamountto.trim() +
						 * "\",\"BankTransID\" : \"" + banktransid.trim() +
						 * "\",\"BeneficiaryAccount\" : \"" +
						 * beneficiaryaccount.trim() +
						 * "\",\"BeneficiaryCode\" : \"" +
						 * beneficiarycode.trim() +
						 * "\",\"BeneficiaryIFSC\" : \"" +
						 * beneficiaryifsc.trim() +
						 * "\",\"BeneficiaryName\" : \"" +
						 * beneficiaryname.trim() + "\",\"CorporateRef\" : \"" +
						 * corporateref.trim() + "\"}";
						 
						
						

						String inputString = Common
								.getTransactionwiseAuthorizationAdvanceSearchApiInputString(
										apiVersion, corpId, userid, systemValueFromApi,
										pageno, pageSize, batch_id, account_id,
										isonhold, transactionamountfrom,
										transactionamountto, banktransid,
										beneficiaryaccount, beneficiaryname,
										corporateref, corporateAccount,
										productName);
						

						ClientResponse client_response = Common.apiAcess(url,inputString, "post");

						logger.info("Axis Server Status for gettransactionwiseauthorizationadvancesearch:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						System.out
								.println("axis end status code is >>>>>>>>>> "
										+ responseStatus);

						if (responseStatus.equals("200")) {

							Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) obj;

							jsonObject = c.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							System.out
									.println("axis end response is >>>>>>>>>> "
											+ jsonStringFinal);

							out.print(jsonStringFinal);

						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);

							jsonObject.put("ResponseCode", responseStatus);

							jsonObject.put("Responsedetails", errorMsg);

							logger.info(responseStatus + " : " + errorMsg);

							jsonObject = c.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "401");

						map.put("error", "Invalid SessionToken");

						logger.info("401 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");

					map.put("error", "SessionToken is Missing");

					logger.info("403 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = c.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error(
					"Transactionwiseauthorizationadvancesearch API Exception: ",
					e);
		}

		out.close();
	}

	
	
	@RequestMapping(value = "api/actionontransactionorbatch1", method = RequestMethod.POST)
	public void getActionOnTransactionOrBatch(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();
		HttpSession sessionToken = null;
		logger.info("Actionontransactionorbatch API");

		try {

			String actionfor = c.decode(Common.PRIVATE_KEY,
					request.getParameter("actionfor"));
			if (actionfor == null)
				actionfor = "";
			String id = c
					.decode(Common.PRIVATE_KEY, request.getParameter("id"));
			if (id == null)
				id = "";
			String actiontype = c.decode(Common.PRIVATE_KEY,
					request.getParameter("actiontype"));
			if (actiontype == null)
				actiontype = "";
			String remarks = c.decode(Common.PRIVATE_KEY,
					request.getParameter("remarks"));
			if (remarks == null)
				remarks = "";
			String account_id = c.decode(Common.PRIVATE_KEY,
					request.getParameter("account_id"));
			if (account_id == null)
				account_id = "";
			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out
						.println("System value 1 for actionontransactionorbatch");

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {
					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "actionontransactionorbatch";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"account_id\" : \""
								+ account_id.trim() + "\",\"actionFor\" : \""
								+ actionfor.trim() + "\",\"id\" : \""
								+ id.trim() + "\",\"actiontype\" : \""
								+ actiontype.trim() + "\",\"Remarks\" : \""
								+ remarks.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");
						logger.info("Axis Server Status for actionontransactionorbatch:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change
					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				// to do for paypro business

				System.out
						.println("System value 2 for actionontransactionorbatch");

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {
					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "actionontransactionorbatch";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"account_id\" : \""
								+ account_id.trim() + "\",\"actionFor\" : \""
								+ actionfor.trim() + "\",\"id\" : \""
								+ id.trim() + "\",\"actiontype\" : \""
								+ actiontype.trim() + "\",\"Remarks\" : \""
								+ remarks.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");
						logger.info("Axis Server Status for actionontransactionorbatch:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("498 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change
					logger.info("403 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("Actionontransactionorbatch API Exception: ", e);
		}

		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/logout", method = RequestMethod.POST)
	public void logout(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();

		logger.info("In the logout api");

		try {

			System.out.println("inside logout api !!!!!!!!!!!!");

		

			HashMap headerMap = (new Common()).getHeadersInfo(request);
			
			String api_key = "";
			
			if (api_key == null)
				api_key = "";
			
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			
			if (session_token == null)
				session_token = "";
			
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			
			if (userid == null)
				userid = "";
			
			
			String deviceId = c.decode(Common.PRIVATE_KEY,
					request.getParameter("deviceid"));

			System.out.println(">>>>>>>>>>>> " + deviceId);

			if (deviceId == null)
				deviceId = "";
			
			String mpin = c.decode(Common.PRIVATE_KEY, request.getParameter("mpin"));
			
			if (mpin == null)
				mpin = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				// HttpSession sessionToken = null;

				if (!session_token.equals("")) {

					HttpSession powerAccessSessionToken = request
							.getSession(false);

					PayConnectUser payConnectUser = payConnectUserBusiness
							.getByUserIdAndApplication(userid,
									systemValueFromApi, deviceId);

					if (payConnectUser == null) {

						JSONObject jsonObject = new JSONObject();
						jsonObject.put(messageUtil.getBundle("response.code"),
								messageUtil.getBundle("wrong.error.code"));
						jsonObject.put(messageUtil.getBundle("error"),
								messageUtil.getBundle("wrong.message"));
						out.print(jsonObject);
						return;
					}

					SessionToken powerSessionToken = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userid.trim());

					System.out.println(powerSessionToken.getUserID() + " <==> "
							+ powerSessionToken.getSessionToken() + " <==> "
							+ session_token);

					String clientIp = c.getUserIp(request);

					
					 * clientIp.equals(powerAccessSessionToken.getAttribute(
					 * "client_ip")) &&
					 
						 * (powerAccessSessionToken.getId()).equals(session_token
						 * ) &&
						 
					 !(powerAccessSessionToken.isNew()) && 
					if (powerSessionToken.getSessionToken().equals(
							session_token)) {

						String url = apiUrl + "user_logout";
						// String inputString = "{\"api_key\" : \""
						// + api_key.trim() + "\",\"userid\" : \""
						// + userid.trim() + "\",\"session_token\" : \""
						// + session_token.trim() + "\",\"Mpin\" : \""
						// + mpin.trim() + "\",\"device_id\" : \""
						// + device_id.trim() + "\"}";

						String inputString = Common.getPowerAccessLogout(userid,
										session_token, mpin, deviceId);
						
					
						
						System.out.println("InputString: " + inputString);
						

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for logout:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();// paypro_session_token

							powerAccessSessionToken
									.removeAttribute("poweraccess_session_token");
							// sessionTokenService.deleteToken(objSessionToken);
							powerAccessSessionToken.invalidate();
							out.print(jsonStringFinal);

						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", messageUtil
								.getBundle("error.invalid.sessiontoken.code"));
						map.put("error", messageUtil
								.getBundle("invalid.sessiontoken.message"));
						// map.put("ip", c.getIp(request));//change
						logger.info(messageUtil
								.getBundle("error.invalid.sessiontoken.code")
								+ ":"
								+ messageUtil
										.getBundle("invalid.sessiontoken.message"));
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						// jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", messageUtil
							.getBundle("error.sessiontoken.missing.code"));
					map.put("error",
							messageUtil.getBundle("error.sessiontoken.message"));
					// map.put("ip", c.getIp(request));//change
					logger.info(messageUtil
							.getBundle("error.sessiontoken.missing.code")
							+ ":"
							+ messageUtil
									.getBundle("error.sessiontoken.message"));
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					// jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {
				System.out.println("Inside Logut API - 2");
				Logout logout = new Logout();
				logout.setSessionToken(session_token);
				logout.setSystem(systemValueFromApi);
				logout.setUserId(userid);
				logout.setDeviceId(deviceId);
				out.print(appLogOutBusiness.logoutBusiness(logout, request));

			}

		} catch (Exception e) {
			e.printStackTrace();

			logger.error("logout api Exception: ", e);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put(messageUtil.getBundle("response.code"),
					messageUtil.getBundle("wrong.error.code"));
			jsonObject.put(messageUtil.getBundle("error"),
					messageUtil.getBundle("wrong.message"));
			out.print(jsonObject);
		}
		out.close();
	}

	*//**
	 * this URL is used to log out from the system
	 * @since 04-Mar-2016
	 * @author Avishek Seal
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*
	@RequestMapping(value = "api/logout", method = RequestMethod.POST)
	public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException{
	    try {
		final Common common = new Common();
		
		HashMap headerMap = (common).getHeadersInfo(request);

		String session_token = common.decode(Common.PRIVATE_KEY, (String) headerMap.get("session_token"));

		String userid = common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));

		String mpin = common.decode(Common.PRIVATE_KEY, (String) request.getParameter("mpin"));

		String systemValueFromApi = common.decode(Common.PRIVATE_KEY, request.getParameter("system"));

		String deviceID = common.decode(Common.PRIVATE_KEY, request.getParameter("deviceid"));
		
		Logout logout = new Logout();
		logout.setSessionToken(session_token);
		logout.setSystem(systemValueFromApi);
		logout.setUserId(userid);
		logout.setDeviceId(deviceID);
		logout.setMpin(mpin);
		
		response.getWriter().write(appLogOutBusiness.logoutBusiness(logout).toJSONString());
	    } catch (Exception e) {
		final JSONObject jsonObject = new JSONObject();
		
		jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
		jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		
		response.getWriter().write(jsonObject.toJSONString());
	    }
	}
	*//******************************* begin statement enquiry ***************************************************************//*

	@RequestMapping(value = "/api/statementenquiry1", method = RequestMethod.POST)
	public void getStatementEnquiry(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		URI contextUrl = URI.create(request.getRequestURL().toString()).resolve(request.getContextPath());
		
		LinkedHashMap outPut = new LinkedHashMap();
		
		Gson json = new Gson();
		ClientResponse client_response = null;
		PrintWriter out1 = response.getWriter();
		String apiName = "StatementEnquiry";
		Common c = new Common();

		logger.info("Statement Enquiry API");

		try {
			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String AccountName = c.decode(Common.PRIVATE_KEY,
					request.getParameter("accountname"));
			if (AccountName == null)
				AccountName = "";
			String AccountNo = c.decode(Common.PRIVATE_KEY,
					request.getParameter("accountno"));
			if (AccountNo == null)
				AccountNo = "";
			String FromDate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("fromdate"));
			if (FromDate == null)
				FromDate = "";
			String ToDate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("todate"));
			if (ToDate == null)
				ToDate = "";
			String StatementFormat = c.decode(Common.PRIVATE_KEY,
					request.getParameter("statementformat"));
			if (StatementFormat == null)
				StatementFormat = "";
			//*** optional params ***//*
			String Pagesize = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pagesize"));
			if (Pagesize == null)
				Pagesize = "";
			String Pageno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pageno"));
			if (Pageno == null)
				Pageno = "";
			String Filetype = c.decode(Common.PRIVATE_KEY,
					request.getParameter("filetype"));
			if (Filetype == null)
				Filetype = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out
						.println("inside statementenquiry system value 1 !!!!!");

				String url = apiUrl + "statementenquiry";

				HashMap map1 = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				HttpSession sessionToken = null;
				if (!session_token.equals("")) {
					sessionToken = request.getSession();// browser session
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"AccountName\" : \""
								+ AccountName.trim() + "\",\"AccountNo\" : \""
								+ AccountNo.trim() + "\",\"FromDate\" : \""
								+ FromDate.trim() + "\",\"ToDate\" : \""
								+ ToDate.trim() + "\",\"StatementFormat\" : \""
								+ StatementFormat.trim()
								+ "\",\"Pagesize\" : \"" + Pagesize.trim()
								+ "\",\"Pageno\" : \"" + Pageno.trim()
								+ "\",\"Filetype\" : \"" + Filetype.trim()
								+ "\"}";
						
						String method = "post";
						
						client_response = Common.apiAcess(url, inputString, method);
						
						String jsonString = client_response.getEntity(String.class);
						
						String responseStatus = String.valueOf(client_response.getStatus());

						if (responseStatus.equals("200")) {

							int Count = 0;
							
							int flagforPDF = 0;
							
							int flagforExcel = 0;
							
							int flag = 0;
							
							int flag_for_single_data = 0;
							
							String extForExcel = null;
							
							String extForPDF = null;

							ArrayList arr = new ArrayList();
							
							HashMap map = json.fromJson(jsonString, HashMap.class);

							// ******************excel sheet section**********************//*
							
							LinkedHashMap m = new LinkedHashMap();
							
							XSSFWorkbook workbook = new XSSFWorkbook();
							
							Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
							
							int n = 2;
							// Set keyset = data.keySet();
							int rownum = 0;
							
							XSSFSheet sheet = null;
							
							// *******pdf
							// section****************************************//*

							PdfPTable table = new PdfPTable(7);
							
							Document document = new Document(PageSize.A4, 10,10, 10, 10);
							
							PdfWriter writer = null;
							
							ArrayList row = null;
							
							LinkedTreeMap rowMap = null;

							if (!map.get("StatementInfo").equals("")) {

							//***** for statement 2 ***************//*
								if (StatementFormat.equals("2")) {

									File file = new File(workingDir + "/"
											+ apiName + "_" + userid + ".pdf");
									extForPDF = FilenameUtils
											.getExtension(workingDir + "/"
													+ apiName + "_" + userid
													+ ".pdf");
									writer = PdfWriter.getInstance(document,
											new FileOutputStream(file));
									document.open();

									table.setWidthPercentage(100);
									
									table.setSpacingBefore(10f);
									
									table.setSpacingAfter(10f);

									PdfPCell cell1 = new PdfPCell(new Paragraph("Transaction Date"));
									
									cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
									
									cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
									
									PdfPCell cell2 = new PdfPCell(new Paragraph("Cheque No"));
									
									cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
									
									cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
									
									PdfPCell cell3 = new PdfPCell(new Paragraph("Particulars"));
									
									cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
									
									cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
									
									PdfPCell cell4 = new PdfPCell(new Paragraph("Debit"));
									
									cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
									
									cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
									
									PdfPCell cell5 = new PdfPCell(new Paragraph("Credit"));
									
									cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
									
									cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
									
									PdfPCell cell6 = new PdfPCell(new Paragraph("Balance Amount"));
									
									cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
									
									cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
									
									PdfPCell cell7 = new PdfPCell(new Paragraph("Sol"));
									
									cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
									
									cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

									table.addCell(cell1);
									
									table.addCell(cell2);
									
									table.addCell(cell3);
									
									table.addCell(cell4);
									
									table.addCell(cell5);
									
									table.addCell(cell6);
									
									table.addCell(cell7);

									flagforPDF = 1;
								}

								if (StatementFormat.equals("3")) {

									sheet = workbook.createSheet("Statement Enquiry Data");
									
									data.put(1, new Object[] {
											"Transaction Date", "Cheque No",
											"Particulars", "Debit", "Credit",
											"Balance Amount", "Sol" });
									
									flagforExcel = 1;
								}

								// looping*********************************************

								if (map.get("StatementInfo") instanceof ArrayList) {

									row = (ArrayList) map.get("StatementInfo");

									for (int j = 0; j < row.size(); j++) {
										
										LinkedTreeMap tree = (LinkedTreeMap) row.get(j);
										
										Count++;
										
										if (flagforPDF == 1) {

											table.addCell(tree.get("transctiondate").toString());
											table.addCell(tree.get("chequeNo").toString());
											table.addCell(tree.get("particulars").toString());
											table.addCell(tree.get("debit").toString());
											table.addCell(tree.get("credit").toString());
											table.addCell(tree.get("balanceammount").toString());
											table.addCell(tree.get("sol").toString());

										}
										
										if (flagforExcel == 1) {
											data.put(n, new Object[] {

															tree.get("transctiondate").toString(),
															tree.get("chequeNo").toString(),
															tree.get("particulars").toString(),
															tree.get("debit").toString(),
															tree.get("credit").toString(),
															tree.get("balanceammount").toString(),
															tree.get("sol").toString() });
											n++;
											
											flagforExcel = 1;
										}

									}

								}

								else {

									rowMap = (LinkedTreeMap) map.get("StatementInfo");

									if (flagforPDF == 1) {

										table.addCell(rowMap.get(
												"transctiondate").toString());
										table.addCell(rowMap.get("chequeNo")
												.toString());
										table.addCell(rowMap.get("particulars")
												.toString());
										table.addCell(rowMap.get("debit")
												.toString());
										table.addCell(rowMap.get("credit")
												.toString());
										table.addCell(rowMap.get(
												"balanceammount").toString());
										table.addCell(rowMap.get("sol")
												.toString());

									}

									if (flagforExcel == 1) {
										data.put(n, new Object[] {

														rowMap.get(
																"transctiondate")
																.toString(),
														rowMap.get("chequeNo")
																.toString(),
														rowMap.get(
																"particulars")
																.toString(),
														rowMap.get("debit")
																.toString(),
														rowMap.get("credit")
																.toString(),
														rowMap.get(
																"balanceammount")
																.toString(),
														rowMap.get("sol")
																.toString() });
										n++;
										flagforExcel = 1;
									}

									flag_for_single_data = 1;

								}

								if (flagforExcel == 1) {
									Set keyset = data.keySet();
									for (Object key : keyset) {
										Row row1 = sheet.createRow(rownum++);
										Object[] objArr = data.get(key);
										int cellnum = 0;
										for (Object obj : objArr) {
											Cell cell = row1
													.createCell(cellnum++);
											if (obj instanceof String)
												cell.setCellValue((String) obj);
											else if (obj instanceof Integer)
												cell.setCellValue((Integer) obj);
										}
									}

									try {
										FileOutputStream out = new FileOutputStream(
												new File(workingDir + "/"
														+ apiName + "_"
														+ userid + ".xlsx"));
										extForExcel = FilenameUtils
												.getExtension(workingDir + "/"
														+ apiName + "_"
														+ userid + ".xlsx");
										workbook.write(out);
										out.close();
									} catch (FileNotFoundException fe) {
										fe.printStackTrace();
									} catch (IOException ie) {
										ie.printStackTrace();
									}

								}

								flag = 1;

							}

							if (flagforPDF == 1) {
								Paragraph paragraph = new Paragraph();
								paragraph.add(table);
								document.add(paragraph);
								document.close();
								writer.close();
							}

							if (map.size() >= 1 && StatementFormat.equals("1")) {

								outPut.put("ResponseCode", "200");
								outPut.put("Responsedetails", "Success");
								// outPut.put("ip", c.getIp(request));
								if (flag == 1 && flag_for_single_data == 0) {

									outPut.put("Totalcount",
											map.get("Totalcount"));
									outPut.put("StatementInfo", row);
								} else if (flag == 1
										&& flag_for_single_data == 1) {
									outPut.put("Totalcount", 1);
									outPut.put("StatementInfo", rowMap);
								}

								else {
									outPut.put("Totalcount", "");
									outPut.put("StatementInfo", "");
								}

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);

								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else if (map.size() >= 1
									&& (StatementFormat.equals("2") || StatementFormat
											.equals("3"))) {
								String msg = null;
								outPut.put("ResponseCode", "200");
								outPut.put("Responsedetails", "Success");
								// outPut.put("ip", c.getIp(request));//change
								if (flagforPDF == 1) {

									msg = appUrl + "/pdf/" + apiName + "_"
											+ userid + "." + extForPDF;
								} else if (flagforExcel == 1)
									msg = appUrl + "/excel/" + apiName + "_"
											+ userid + "." + extForExcel;
								else
									msg = "";

								outPut.put("Statementdownloadlink", msg);

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else {
								outPut.put("ResponseCode", "500");
								outPut.put("error",
										"Something went wrong. Please try again later.");
								// outPut.put("ip", c.getIp(request));//change
								logger.info("500 : Something went wrong. Please try again later.");

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);
							}
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out1.print(jsonStringFinal);
						}
					} // end for validity of session token
					else {
						map1.put("ResponseCode", "210");
						map1.put("error", "Invalid SessionToken");
						// map1.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map1);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out1.print(jsonStringFinal);

					}
				} else {
					map1.put("ResponseCode", "210");
					map1.put("error", "SessionToken is Missing");
					// map1.put("ip", c.getIp(request));//change
					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map1);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out1.print(jsonStringFinal);

				}

			}
			
			

			

		} catch (Exception e) {
			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out1.print(jsonStringFinal);

			logger.error("Statement Enquiry API Exception: ", e);
		}

	}

	*//***************************************** end of statement enquiry ********************************//*

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*
	@RequestMapping(value = "api/getaccountbalance1", method = RequestMethod.POST)
	public void getAccountBalance(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();
		HttpSession sessionToken = null;

		logger.info("Accountbalance API");

		try {

			String account_number = c.decode(Common.PRIVATE_KEY,
					request.getParameter("account_number"));
			if (account_number == null)
				account_number = "";
			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out.println("getaccountbalance system value 1");

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {
					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getaccountbalance";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"account_number\" : \""
								+ account_number.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getaccountbalance:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}
					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change

					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				System.out.println("getaccountbalance system value 2");

				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {
					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getaccountbalance";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"account_number\" : \""
								+ account_number.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getaccountbalance:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}
					} else {
						map.put("ResponseCode", "498");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change
						logger.info("498 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change

					logger.info("403 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error("Accountbalance API Exception: ", e);
		}

		out.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/batchwiseprtransactionwisebalancecomparison", method = RequestMethod.POST)
	public void getBatchTransactionWiseBalanceCOmparison(
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();
		HttpSession sessionToken = null;

		logger.info("Batchwiseprtransactionwisebalancecomparison API");

		try {

			String id = c
					.decode(Common.PRIVATE_KEY, request.getParameter("id"));
			if (id == null)
				id = "";
			String isbatch = c.decode(Common.PRIVATE_KEY,
					request.getParameter("isbatch"));
			if (isbatch == null)
				isbatch = "";
			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out
						.println("inside batchwiseprtransactionwisebalancecomparison system value 1 ");

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl
								+ "batchwiseprtransactionwisebalancecomparison";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"id\" : \"" + id.trim()
								+ "\",\"IsBatch\" : \"" + isbatch.trim()
								+ "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for batchwiseprtransactionwisebalancecomparison:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "210");
						map.put("error", "Invalid SessionToken");
						// map.put("ip", c.getIp(request));//change

						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "210");
					map.put("error", "SessionToken is Missing");
					// map.put("ip", c.getIp(request));//change

					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				System.out
						.println("inside batchwiseprtransactionwisebalancecomparison system value 1 ");

				HashMap map = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl
								+ "batchwiseprtransactionwisebalancecomparison";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"id\" : \"" + id.trim()
								+ "\",\"IsBatch\" : \"" + isbatch.trim()
								+ "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for batchwiseprtransactionwisebalancecomparison:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {
							Object obj = parser.parse(jsonString);
							JSONObject jsonObject = (JSONObject) obj;
							// jsonObject.put("ip", c.getIp(request));//change
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");

						map.put("error", "Invalid SessionToken");

						// map.put("ip", c.getIp(request));//change

						logger.info("498 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "403");

					map.put("error", "SessionToken is Missing");

					// map.put("ip", c.getIp(request));//change

					logger.info("403 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error(
					"Batchwiseprtransactionwisebalancecomparison API Exception: ",
					e);
		}

		out.close();
	}

	
	
	
	

	
	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/getransactiondetails1", method = RequestMethod.POST)
	public void getTransactionDetails(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out1 = response.getWriter();

		Gson json = new Gson();
		
		Common c = new Common();
		URI contextUrl = URI.create(request.getRequestURL().toString())
				.resolve(request.getContextPath());
		String apiName = "Transactiondetails";
		HttpSession sessionToken = null;

		logger.info("Transactiondetails API");

		try {

			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";

			String batch_id = c.decode(Common.PRIVATE_KEY,
					request.getParameter("batch_id"));
			if (batch_id == null)
				batch_id = "";
			String account_id = c.decode(Common.PRIVATE_KEY,
					request.getParameter("account_id"));
			if (account_id == null)
				account_id = "";
			String pageno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pageno"));
			if (pageno == null)
				pageno = "";
			String pagesize = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pagesize"));
			if (pagesize == null)
				pagesize = "";
			String isonhold = c.decode(Common.PRIVATE_KEY,
					request.getParameter("isonhold"));
			if (isonhold == null)
				isonhold = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out
						.println("inside getransactiondetails system value 1");

				String extForExcel = null;
				String extForPDF = null;

				HashMap map1 = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();

					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getransactiondetails";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"batch_id\" : \""
								+ batch_id.trim() + "\", \"account_id\" :\""
								+ account_id.trim() + "\",\"pageno\" : \""
								+ pageno.trim() + "\",\"pagesize\" : \""
								+ pagesize.trim() + "\",\"isOnHold\" : \""
								+ isonhold.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getransactiondetails:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							ArrayList transactionDetails = null;

							HashMap map = json.fromJson(jsonString,
									HashMap.class);

							LinkedHashMap outPut = new LinkedHashMap();

							XSSFWorkbook workbook = null;

							PdfWriter writer = null;

							Document document = new Document();

							int flag = 0;

							PdfPTable table = null;

							if (!map.get("TransactionDetails").equals("")) {

								*//************************************** PDF Generation ****************************************//*

								table = new PdfPTable(9);

								java.util.Date date = new java.util.Date();
								Random rand = new Random();

								int randomCode = rand.nextInt();
								File file = new File(workingDir + "/" + apiName
										+ "_" + userid + ".pdf");
								extForPDF = FilenameUtils
										.getExtension(workingDir + "/"
												+ apiName + "_" + userid
												+ ".pdf");
								writer = PdfWriter.getInstance(document,
										new FileOutputStream(file));
								document.open();

								table.setWidthPercentage(100);
								table.setSpacingBefore(10f);
								table.setSpacingAfter(10f);
								PdfPCell cell1 = new PdfPCell(new Paragraph(
										"Transaction ID"));
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell2 = new PdfPCell(new Paragraph(
										"Authorisation Status"));
								cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell3 = new PdfPCell(new Paragraph(
										"Benificiary Name"));
								cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell4 = new PdfPCell(new Paragraph(
										"Product Name"));
								cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell5 = new PdfPCell(new Paragraph(
										"Corporate Ref"));
								cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell6 = new PdfPCell(new Paragraph(
										"Corporate Account"));
								cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell7 = new PdfPCell(new Paragraph(
										"Transaction Amount"));
								cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell8 = new PdfPCell(new Paragraph(
										"Benificiary Account"));
								cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell9 = new PdfPCell(new Paragraph(
										"Transaction Description"));
								cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);
								table.addCell(cell1);
								table.addCell(cell2);
								table.addCell(cell3);
								table.addCell(cell4);
								table.addCell(cell5);
								table.addCell(cell6);
								table.addCell(cell7);
								table.addCell(cell8);
								table.addCell(cell9);
								*//************************************** Excel Sheet Generation ***********************************//*

								workbook = new XSSFWorkbook();
								Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
								int n = 2;
								int rownum = 0;
								XSSFSheet sheet = workbook
										.createSheet("Batch Data");
								data.put(1, new Object[] { "Transaction ID",
										"Authorisation Status",
										"Benificiary Name", "Product Name",
										"Corporate Ref", "Corporate Account",
										"Transaction Amount",
										"Benificiary Account",
										"Transaction Description" });

								if (map.get("TransactionDetails") instanceof ArrayList) {

									transactionDetails = (ArrayList) map
											.get("TransactionDetails");

									*//**********************************************************************************************//*
									for (int i = 0; i < transactionDetails
											.size(); i++) {
										LinkedTreeMap tree = (LinkedTreeMap) transactionDetails
												.get(i);
										*//*********************** PDF Data generation **************//*
										table.addCell(tree.get("TransactionID")
												.toString());
										table.addCell(tree.get(
												"AuthorisationStatus")
												.toString());
										table.addCell(tree.get(
												"BenificiaryName").toString());
										table.addCell(tree.get("ProductName")
												.toString());
										table.addCell(tree.get("CorporateRef")
												.toString());

										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("CorporateAccount")))));
										table.addCell(tree.get(
												"TransactionAmount").toString());
										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("BenificiaryAccount")))));
										table.addCell(tree.get(
												"TransactionDescription")
												.toString());
										data.put(
												n,
												new Object[] {
														tree.get(
																"TransactionID")
																.toString(),
														tree.get(
																"AuthorisationStatus")
																.toString(),
														tree.get(
																"BenificiaryName")
																.toString(),
														tree.get("ProductName")
																.toString(),
														tree.get("CorporateRef")
																.toString()
																.trim(),
														String.valueOf(c
																.formatDecimal(tree
																		.get("CorporateAccount"))),
														tree.get(
																"TransactionAmount")
																.toString(),
														String.valueOf((c
																.formatDecimal(tree
																		.get("BenificiaryAccount")
																		.toString()))),
														tree.get(
																"TransactionDescription")
																.toString() });
										n++;
										flag = 1;
									}

								} else {

									LinkedTreeMap transactionDetailsMap = (LinkedTreeMap) map
											.get("TransactionDetails");
									*//**********************************************************************************************//*

									*//*********************** PDF Data generation **************//*
									table.addCell(transactionDetailsMap.get(
											"TransactionID").toString());
									table.addCell(transactionDetailsMap.get(
											"AuthorisationStatus").toString());
									table.addCell(transactionDetailsMap.get(
											"BenificiaryName").toString());
									table.addCell(transactionDetailsMap.get(
											"ProductName").toString());
									table.addCell(transactionDetailsMap.get(
											"CorporateRef").toString());
									table.addCell(String.valueOf((c
											.formatDecimal(transactionDetailsMap
													.get("CorporateAccount")))));
									table.addCell(transactionDetailsMap.get(
											"TransactionAmount").toString());
									table.addCell(String.valueOf((c
											.formatDecimal(transactionDetailsMap
													.get("BenificiaryAccount")))));
									table.addCell(transactionDetailsMap.get(
											"TransactionDescription")
											.toString());

									data.put(
											n,
											new Object[] {
													transactionDetailsMap.get(
															"TransactionID")
															.toString(),
													transactionDetailsMap
															.get("AuthorisationStatus")
															.toString(),
													transactionDetailsMap.get(
															"BenificiaryName")
															.toString(),
													transactionDetailsMap.get(
															"ProductName")
															.toString(),
													transactionDetailsMap
															.get("CorporateRef")
															.toString().trim(),
													String.valueOf(c
															.formatDecimal(transactionDetailsMap
																	.get("CorporateAccount"))),
													transactionDetailsMap
															.get("TransactionAmount")
															.toString(),
													String.valueOf((c
															.formatDecimal(transactionDetailsMap
																	.get("BenificiaryAccount")))),
													transactionDetailsMap
															.get("TransactionDescription")
															.toString() });
									n++;
									flag = 1;

								}

								Set keyset = data.keySet();
								for (Object key : keyset) {
									Row row1 = sheet.createRow(rownum++);
									Object[] objArr = data.get(key);
									int cellnum = 0;
									for (Object obj : objArr) {
										Cell cell = row1.createCell(cellnum++);
										if (obj instanceof String)
											cell.setCellValue((String) obj);
										else if (obj instanceof Integer)
											cell.setCellValue((Integer) obj);
									}
								}

							}

							if (flag == 1) {
								try {

									FileOutputStream out = new FileOutputStream(
											new File(workingDir + "/" + apiName
													+ "_" + userid + ".xlsx"));
									extForExcel = FilenameUtils
											.getExtension(workingDir + "/"
													+ apiName + "_" + userid
													+ ".xlsx");
									workbook.write(out);
									out.close();
								} catch (FileNotFoundException fe) {
									fe.printStackTrace();
								} catch (IOException ie) {
									ie.printStackTrace();
								}
								document.add(table);
								document.close();
								writer.close();
							}
							if (map.size() >= 1) {

								String msgForPDF = null;
								String msgForExcel = null;

								map.put("ResponseCode", "200");
								map.put("Responsedetails", "Success");

								if (flag == 1) {

									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;

									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;

									map.put("PdfDownloadLink", msgForPDF);
									map.put("ExcelDownloadLink", msgForExcel);
								} else {
									map.put("PdfDownloadLink", "");
									map.put("ExcelDownloadLink", "");
								}

								Object obj = parser.parse(json.toJson(map));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else {
								outPut.put("ResponseCode", "500");
								outPut.put("error",
										"Something went wrong. Please try again later.");

								logger.info("500 : Something went wrong. Please try again later.");

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							}
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out1.print(jsonStringFinal);
						}

					} else {
						map1.put("ResponseCode", "210");
						map1.put("error", "Invalid SessionToken");
						// map1.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map1);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out1.print(jsonStringFinal);

					}
				} else {
					map1.put("ResponseCode", "210");
					map1.put("error", "SessionToken is Missing");
					// map1.put("ip", c.getIp(request));//change
					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map1);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out1.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				System.out
						.println("inside getransactiondetails system value 2");

				String extForExcel = null;
				String extForPDF = null;

				HashMap map1 = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();

					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getransactiondetails";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"batch_id\" : \""
								+ batch_id.trim() + "\", \"account_id\" :\""
								+ account_id.trim() + "\",\"pageno\" : \""
								+ pageno.trim() + "\",\"pagesize\" : \""
								+ pagesize.trim() + "\",\"isOnHold\" : \""
								+ isonhold.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getransactiondetails:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							ArrayList transactionDetails = null;

							HashMap map = json.fromJson(jsonString,
									HashMap.class);
							LinkedHashMap outPut = new LinkedHashMap();
							XSSFWorkbook workbook = null;
							PdfWriter writer = null;
							Document document = new Document();
							int flag = 0;
							PdfPTable table = null;
							if (!map.get("TransactionDetails").equals("")) {

								*//************************************** PDF Generation ****************************************//*

								table = new PdfPTable(9);

								java.util.Date date = new java.util.Date();
								Random rand = new Random();

								int randomCode = rand.nextInt();
								File file = new File(workingDir + "/" + apiName
										+ "_" + userid + ".pdf");
								extForPDF = FilenameUtils
										.getExtension(workingDir + "/"
												+ apiName + "_" + userid
												+ ".pdf");
								writer = PdfWriter.getInstance(document,
										new FileOutputStream(file));
								document.open();

								table.setWidthPercentage(100);
								table.setSpacingBefore(10f);
								table.setSpacingAfter(10f);
								PdfPCell cell1 = new PdfPCell(new Paragraph(
										"Transaction ID"));
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell2 = new PdfPCell(new Paragraph(
										"Authorisation Status"));
								cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell3 = new PdfPCell(new Paragraph(
										"Benificiary Name"));
								cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell4 = new PdfPCell(new Paragraph(
										"Product Name"));
								cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell5 = new PdfPCell(new Paragraph(
										"Corporate Ref"));
								cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell6 = new PdfPCell(new Paragraph(
										"Corporate Account"));
								cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell7 = new PdfPCell(new Paragraph(
										"Transaction Amount"));
								cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell8 = new PdfPCell(new Paragraph(
										"Benificiary Account"));
								cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell9 = new PdfPCell(new Paragraph(
										"Transaction Description"));
								cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);
								table.addCell(cell1);
								table.addCell(cell2);
								table.addCell(cell3);
								table.addCell(cell4);
								table.addCell(cell5);
								table.addCell(cell6);
								table.addCell(cell7);
								table.addCell(cell8);
								table.addCell(cell9);
								*//************************************** Excel Sheet Generation ***********************************//*

								workbook = new XSSFWorkbook();
								Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
								int n = 2;
								int rownum = 0;
								XSSFSheet sheet = workbook
										.createSheet("Batch Data");
								data.put(1, new Object[] { "Transaction ID",
										"Authorisation Status",
										"Benificiary Name", "Product Name",
										"Corporate Ref", "Corporate Account",
										"Transaction Amount",
										"Benificiary Account",
										"Transaction Description" });

								if (map.get("TransactionDetails") instanceof ArrayList) {

									transactionDetails = (ArrayList) map
											.get("TransactionDetails");

									*//**********************************************************************************************//*
									for (int i = 0; i < transactionDetails
											.size(); i++) {
										LinkedTreeMap tree = (LinkedTreeMap) transactionDetails
												.get(i);
										*//*********************** PDF Data generation **************//*
										table.addCell(tree.get("TransactionID")
												.toString());
										table.addCell(tree.get(
												"AuthorisationStatus")
												.toString());
										table.addCell(tree.get(
												"BenificiaryName").toString());
										table.addCell(tree.get("ProductName")
												.toString());
										table.addCell(tree.get("CorporateRef")
												.toString());

										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("CorporateAccount")))));
										table.addCell(tree.get(
												"TransactionAmount").toString());
										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("BenificiaryAccount")))));
										table.addCell(tree.get(
												"TransactionDescription")
												.toString());
										data.put(
												n,
												new Object[] {
														tree.get(
																"TransactionID")
																.toString(),
														tree.get(
																"AuthorisationStatus")
																.toString(),
														tree.get(
																"BenificiaryName")
																.toString(),
														tree.get("ProductName")
																.toString(),
														tree.get("CorporateRef")
																.toString()
																.trim(),
														String.valueOf(c
																.formatDecimal(tree
																		.get("CorporateAccount"))),
														tree.get(
																"TransactionAmount")
																.toString(),
														String.valueOf((c
																.formatDecimal(tree
																		.get("BenificiaryAccount")
																		.toString()))),
														tree.get(
																"TransactionDescription")
																.toString() });
										n++;
										flag = 1;
									}

								} else {

									LinkedTreeMap transactionDetailsMap = (LinkedTreeMap) map
											.get("TransactionDetails");
									*//**********************************************************************************************//*

									*//*********************** PDF Data generation **************//*
									table.addCell(transactionDetailsMap.get(
											"TransactionID").toString());
									table.addCell(transactionDetailsMap.get(
											"AuthorisationStatus").toString());
									table.addCell(transactionDetailsMap.get(
											"BenificiaryName").toString());
									table.addCell(transactionDetailsMap.get(
											"ProductName").toString());
									table.addCell(transactionDetailsMap.get(
											"CorporateRef").toString());
									table.addCell(String.valueOf((c
											.formatDecimal(transactionDetailsMap
													.get("CorporateAccount")))));
									table.addCell(transactionDetailsMap.get(
											"TransactionAmount").toString());
									table.addCell(String.valueOf((c
											.formatDecimal(transactionDetailsMap
													.get("BenificiaryAccount")))));
									table.addCell(transactionDetailsMap.get(
											"TransactionDescription")
											.toString());

									data.put(
											n,
											new Object[] {
													transactionDetailsMap.get(
															"TransactionID")
															.toString(),
													transactionDetailsMap
															.get("AuthorisationStatus")
															.toString(),
													transactionDetailsMap.get(
															"BenificiaryName")
															.toString(),
													transactionDetailsMap.get(
															"ProductName")
															.toString(),
													transactionDetailsMap
															.get("CorporateRef")
															.toString().trim(),
													String.valueOf(c
															.formatDecimal(transactionDetailsMap
																	.get("CorporateAccount"))),
													transactionDetailsMap
															.get("TransactionAmount")
															.toString(),
													String.valueOf((c
															.formatDecimal(transactionDetailsMap
																	.get("BenificiaryAccount")))),
													transactionDetailsMap
															.get("TransactionDescription")
															.toString() });
									n++;
									flag = 1;

								}

								Set keyset = data.keySet();
								for (Object key : keyset) {
									Row row1 = sheet.createRow(rownum++);
									Object[] objArr = data.get(key);
									int cellnum = 0;
									for (Object obj : objArr) {
										Cell cell = row1.createCell(cellnum++);
										if (obj instanceof String)
											cell.setCellValue((String) obj);
										else if (obj instanceof Integer)
											cell.setCellValue((Integer) obj);
									}
								}

							}

							if (flag == 1) {
								try {

									FileOutputStream out = new FileOutputStream(
											new File(workingDir + "/" + apiName
													+ "_" + userid + ".xlsx"));
									extForExcel = FilenameUtils
											.getExtension(workingDir + "/"
													+ apiName + "_" + userid
													+ ".xlsx");
									workbook.write(out);
									out.close();
								} catch (FileNotFoundException fe) {
									fe.printStackTrace();
								} catch (IOException ie) {
									ie.printStackTrace();
								}
								document.add(table);
								document.close();
								writer.close();
							}
							if (map.size() >= 1) {

								String msgForPDF = null;
								String msgForExcel = null;

								map.put("ResponseCode", "200");
								map.put("Responsedetails", "Success");

								if (flag == 1) {

									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;

									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;

									map.put("PdfDownloadLink", msgForPDF);
									map.put("ExcelDownloadLink", msgForExcel);
								} else {
									map.put("PdfDownloadLink", "");
									map.put("ExcelDownloadLink", "");
								}

								Object obj = parser.parse(json.toJson(map));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else {
								outPut.put("ResponseCode", "500");
								outPut.put("error",
										"Something went wrong. Please try again later.");

								logger.info("500 : Something went wrong. Please try again later.");

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							}
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out1.print(jsonStringFinal);
						}

					} else {
						map1.put("ResponseCode", "498");
						map1.put("error", "Invalid SessionToken");
						// map1.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map1);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out1.print(jsonStringFinal);

					}
				} else {
					map1.put("ResponseCode", "403");
					map1.put("error", "SessionToken is Missing");
					// map1.put("ip", c.getIp(request));//change
					logger.info("403 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map1);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out1.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Transactiondetails API Exception: ", e);

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out1.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("In the login API Exception: ", e);

		}
		out1.close();
	}

	*//****************************** session validation left ****************************//*

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/getecollectionreport1", method = RequestMethod.POST)
	public void getECollectionReport(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out1 = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();
		URI contextUrl = URI.create(request.getRequestURL().toString())
				.resolve(request.getContextPath());
		String apiName = "ECollectionReport";
		HttpSession sessionToken = null;

		logger.info("Getecollectionreport API");

		try {

			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";

			String pagesize = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pagesize"));
			if (pagesize == null)
				pagesize = "";
			String pageno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pageno"));
			if (pageno == null)
				pageno = "";
			String startdate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("startdate"));
			if (startdate == null)
				startdate = "";
			String enddate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("enddate"));
			if (enddate == null)
				enddate = "";
			String paymentmode = c.decode(Common.PRIVATE_KEY,
					request.getParameter("paymentmode"));
			if (paymentmode == null)
				paymentmode = "";
			String UTRno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("utrno"));
			if (UTRno == null)
				UTRno = "";
			String transactionamountfrom = c.decode(Common.PRIVATE_KEY,
					request.getParameter("transactionamountfrom"));
			if (transactionamountfrom == null)
				transactionamountfrom = "";
			String transactionamountto = c.decode(Common.PRIVATE_KEY,
					request.getParameter("transactionamountto"));
			if (transactionamountto == null)
				transactionamountto = "";
			String creditaccountno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("creditaccountno"));
			if (creditaccountno == null)
				creditaccountno = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out
						.println("inside getecollectionreport system value 1 ");

				String extForExcel = null;
				String extForPDF = null;

				HashMap map1 = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getecollectionreport";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"pagesize\" : \""
								+ pagesize.trim() + "\", \"pageno\" :\""
								+ pageno.trim() + "\",\"startdate\" : \""
								+ startdate.trim() + "\",\"enddate\" : \""
								+ enddate.trim() + "\",\"paymentmode\" : \""
								+ paymentmode.trim() + "\",\"UTRno\" : \""
								+ UTRno.trim()
								+ "\",\"transactionamountfrom\" : \""
								+ transactionamountfrom.trim()
								+ "\",\"transactionamountto\" : \""
								+ transactionamountto.trim()
								+ "\",\"creditaccountno\" : \""
								+ creditaccountno.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getecollectionreport:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							ArrayList collectionReports = null;
							int flag = 0;
							LinkedHashMap outPut = new LinkedHashMap();
							HashMap map = json.fromJson(jsonString,
									HashMap.class);
							XSSFWorkbook workbook = new XSSFWorkbook();
							PdfPTable table = null;
							PdfWriter writer = null;
							Document document = new Document();
							if (!map.get("ECollectionReports").equals("")) {

								*//************************************** PDF Generation ****************************************//*

								table = new PdfPTable(7);

								java.util.Date date = new java.util.Date();
								Random rand = new Random();
								int randomCode = rand.nextInt();
								File file = new File(workingDir + "/" + apiName
										+ "_" + userid + ".pdf");
								extForPDF = FilenameUtils
										.getExtension(workingDir + "/"
												+ apiName + "_" + userid
												+ ".pdf");
								writer = PdfWriter.getInstance(document,
										new FileOutputStream(file));
								document.open();

								table.setWidthPercentage(100);
								table.setSpacingBefore(10f);
								table.setSpacingAfter(10f);

								PdfPCell cell1 = new PdfPCell(new Paragraph(
										"Payment Mode"));
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell2 = new PdfPCell(new Paragraph(
										"Transaction Date"));
								cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell3 = new PdfPCell(new Paragraph(
										"Transaction Amount"));
								cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell4 = new PdfPCell(new Paragraph(
										"Sender Name"));
								cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell5 = new PdfPCell(new Paragraph(
										"UTR No."));
								cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell6 = new PdfPCell(new Paragraph(
										"Corporate Account No"));
								cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell7 = new PdfPCell(new Paragraph(
										"Sender Information"));
								cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
								table.addCell(cell1);
								table.addCell(cell2);
								table.addCell(cell3);
								table.addCell(cell4);
								table.addCell(cell5);
								table.addCell(cell6);
								table.addCell(cell7);

								*//************************************** Excel Sheet Generation ***********************************//*

								Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
								int n = 2;
								int rownum = 0;
								XSSFSheet sheet = workbook
										.createSheet("Batch Data");
								data.put(1, new Object[] { "Payment Mode",
										"Transaction Date",
										"Transaction Amount", "Sender Name",
										"UTR No.", "Corparate Account No",
										"Sender Information" });

								*//**********************************************************************************************//*

								if (map.get("ECollectionReports") instanceof ArrayList) {

									collectionReports = (ArrayList) map
											.get("ECollectionReports");

									for (int i = 0; i < collectionReports
											.size(); i++) {
										LinkedTreeMap tree = (LinkedTreeMap) collectionReports
												.get(i);
										*//*********************** PDF Data generation **************//*
										table.addCell(tree.get("paymentmode")
												.toString());
										table.addCell(tree.get(
												"transactiondate").toString());
										table.addCell(tree.get(
												"transactionamount").toString());
										table.addCell(tree.get("sendername")
												.toString());
										table.addCell(tree.get("UTRno")
												.toString());
										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("corparateaccountno")))));
										table.addCell(tree.get("senderinfo")
												.toString());
										data.put(
												n,
												new Object[] {
														tree.get("paymentmode")
																.toString(),
														tree.get(
																"transactiondate")
																.toString(),
														tree.get(
																"transactionamount")
																.toString(),
														tree.get("sendername")
																.toString(),
														tree.get("UTRno")
																.toString()
																.trim(),
														String.valueOf(c
																.formatDecimal(tree
																		.get("corparateaccountno"))),
														tree.get("senderinfo")
																.toString(), });
										n++;
										flag = 1;
									}

								} else {

									LinkedTreeMap collectionReportsMap = (LinkedTreeMap) map
											.get("ECollectionReports");

									table.addCell(collectionReportsMap.get(
											"paymentmode").toString());
									table.addCell(collectionReportsMap.get(
											"transactiondate").toString());
									table.addCell(collectionReportsMap.get(
											"transactionamount").toString());
									table.addCell(collectionReportsMap.get(
											"sendername").toString());
									table.addCell(collectionReportsMap.get(
											"UTRno").toString());
									table.addCell(String.valueOf((c
											.formatDecimal(collectionReportsMap
													.get("corparateaccountno")))));
									table.addCell(collectionReportsMap.get(
											"senderinfo").toString());
									data.put(
											n,
											new Object[] {
													collectionReportsMap.get(
															"paymentmode")
															.toString(),
													collectionReportsMap.get(
															"transactiondate")
															.toString(),
													collectionReportsMap
															.get("transactionamount")
															.toString(),
													collectionReportsMap.get(
															"sendername")
															.toString(),
													collectionReportsMap
															.get("UTRno")
															.toString().trim(),
													String.valueOf(c
															.formatDecimal(collectionReportsMap
																	.get("corparateaccountno"))),
													collectionReportsMap.get(
															"senderinfo")
															.toString(), });
									n++;
									flag = 1;

								}
								Set keyset = data.keySet();
								for (Object key : keyset) {
									Row row1 = sheet.createRow(rownum++);
									Object[] objArr = data.get(key);
									int cellnum = 0;
									for (Object obj : objArr) {
										Cell cell = row1.createCell(cellnum++);
										if (obj instanceof String)
											cell.setCellValue((String) obj);
										else if (obj instanceof Integer)
											cell.setCellValue((Integer) obj);
									}
								}
							}

							if (flag == 1) {
								try {
									FileOutputStream out = new FileOutputStream(
											new File(workingDir + "/" + apiName
													+ "_" + userid + ".xlsx"));
									extForExcel = FilenameUtils
											.getExtension(workingDir + "/"
													+ apiName + "_" + userid
													+ ".xlsx");
									workbook.write(out);
									out.close();
								} catch (FileNotFoundException fe) {
									fe.printStackTrace();
								} catch (IOException ie) {
									ie.printStackTrace();
								}
								document.add(table);
								document.close();
								writer.close();
							}
							if (map.size() >= 1) {
								String msgForPDF = null;
								String msgForExcel = null;
								map.put("ResponseCode", "200");
								if (flag == 1) {
									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;
									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;
									map.put("PdfDownloadLink", msgForPDF);
									map.put("ExcelDownloadLink", msgForExcel);
								} else {
									map.put("PdfDownloadLink", "");
									map.put("ExcelDownloadLink", "");
								}

								Object obj = parser.parse(json.toJson(map));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else {
								outPut.put("ResponseCode", "500");
								outPut.put("error",
										"Something went wrong. Please try again later.");
								// outPut.put("ip", c.getIp(request));//change

								logger.info("500 : Something went wrong. Please try again later.");

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							}
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out1.print(jsonStringFinal);
						}
					} else {
						map1.put("ResponseCode", "210");
						map1.put("error", "Invalid SessionToken");
						// map1.put("ip", c.getIp(request));//change
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map1);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out1.print(jsonStringFinal);

					}
				} else {
					map1.put("ResponseCode", "210");
					map1.put("error", "SessionToken is Missing");
					// map1.put("ip", c.getIp(request));//change
					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map1);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out1.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				// to do for paypro business logic

				System.out
						.println("inside getecollectionreport system value 2 ");

				String extForExcel = null;
				String extForPDF = null;

				HashMap map1 = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "getecollectionreport";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"pagesize\" : \""
								+ pagesize.trim() + "\", \"pageno\" :\""
								+ pageno.trim() + "\",\"startdate\" : \""
								+ startdate.trim() + "\",\"enddate\" : \""
								+ enddate.trim() + "\",\"paymentmode\" : \""
								+ paymentmode.trim() + "\",\"UTRno\" : \""
								+ UTRno.trim()
								+ "\",\"transactionamountfrom\" : \""
								+ transactionamountfrom.trim()
								+ "\",\"transactionamountto\" : \""
								+ transactionamountto.trim()
								+ "\",\"creditaccountno\" : \""
								+ creditaccountno.trim() + "\"}";

						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getecollectionreport:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							ArrayList collectionReports = null;
							int flag = 0;
							LinkedHashMap outPut = new LinkedHashMap();
							HashMap map = json.fromJson(jsonString,
									HashMap.class);
							XSSFWorkbook workbook = new XSSFWorkbook();
							PdfPTable table = null;
							PdfWriter writer = null;
							Document document = new Document();
							if (!map.get("ECollectionReports").equals("")) {

								*//************************************** PDF Generation ****************************************//*

								table = new PdfPTable(7);

								java.util.Date date = new java.util.Date();
								Random rand = new Random();
								int randomCode = rand.nextInt();
								File file = new File(workingDir + "/" + apiName
										+ "_" + userid + ".pdf");
								extForPDF = FilenameUtils
										.getExtension(workingDir + "/"
												+ apiName + "_" + userid
												+ ".pdf");
								writer = PdfWriter.getInstance(document,
										new FileOutputStream(file));
								document.open();

								table.setWidthPercentage(100);
								table.setSpacingBefore(10f);
								table.setSpacingAfter(10f);

								PdfPCell cell1 = new PdfPCell(new Paragraph(
										"Payment Mode"));
								cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell2 = new PdfPCell(new Paragraph(
										"Transaction Date"));
								cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell3 = new PdfPCell(new Paragraph(
										"Transaction Amount"));
								cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell4 = new PdfPCell(new Paragraph(
										"Sender Name"));
								cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell5 = new PdfPCell(new Paragraph(
										"UTR No."));
								cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell6 = new PdfPCell(new Paragraph(
										"Corporate Account No"));
								cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
								PdfPCell cell7 = new PdfPCell(new Paragraph(
										"Sender Information"));
								cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
								table.addCell(cell1);
								table.addCell(cell2);
								table.addCell(cell3);
								table.addCell(cell4);
								table.addCell(cell5);
								table.addCell(cell6);
								table.addCell(cell7);

								*//************************************** Excel Sheet Generation ***********************************//*

								Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
								int n = 2;
								int rownum = 0;
								XSSFSheet sheet = workbook
										.createSheet("Batch Data");
								data.put(1, new Object[] { "Payment Mode",
										"Transaction Date",
										"Transaction Amount", "Sender Name",
										"UTR No.", "Corparate Account No",
										"Sender Information" });

								*//**********************************************************************************************//*

								if (map.get("ECollectionReports") instanceof ArrayList) {

									collectionReports = (ArrayList) map
											.get("ECollectionReports");

									for (int i = 0; i < collectionReports
											.size(); i++) {
										LinkedTreeMap tree = (LinkedTreeMap) collectionReports
												.get(i);
										*//*********************** PDF Data generation **************//*
										table.addCell(tree.get("paymentmode")
												.toString());
										table.addCell(tree.get(
												"transactiondate").toString());
										table.addCell(tree.get(
												"transactionamount").toString());
										table.addCell(tree.get("sendername")
												.toString());
										table.addCell(tree.get("UTRno")
												.toString());
										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("corparateaccountno")))));
										table.addCell(tree.get("senderinfo")
												.toString());
										data.put(
												n,
												new Object[] {
														tree.get("paymentmode")
																.toString(),
														tree.get(
																"transactiondate")
																.toString(),
														tree.get(
																"transactionamount")
																.toString(),
														tree.get("sendername")
																.toString(),
														tree.get("UTRno")
																.toString()
																.trim(),
														String.valueOf(c
																.formatDecimal(tree
																		.get("corparateaccountno"))),
														tree.get("senderinfo")
																.toString(), });
										n++;
										flag = 1;
									}

								} else {

									LinkedTreeMap collectionReportsMap = (LinkedTreeMap) map
											.get("ECollectionReports");

									table.addCell(collectionReportsMap.get(
											"paymentmode").toString());
									table.addCell(collectionReportsMap.get(
											"transactiondate").toString());
									table.addCell(collectionReportsMap.get(
											"transactionamount").toString());
									table.addCell(collectionReportsMap.get(
											"sendername").toString());
									table.addCell(collectionReportsMap.get(
											"UTRno").toString());
									table.addCell(String.valueOf((c
											.formatDecimal(collectionReportsMap
													.get("corparateaccountno")))));
									table.addCell(collectionReportsMap.get(
											"senderinfo").toString());
									data.put(
											n,
											new Object[] {
													collectionReportsMap.get(
															"paymentmode")
															.toString(),
													collectionReportsMap.get(
															"transactiondate")
															.toString(),
													collectionReportsMap
															.get("transactionamount")
															.toString(),
													collectionReportsMap.get(
															"sendername")
															.toString(),
													collectionReportsMap
															.get("UTRno")
															.toString().trim(),
													String.valueOf(c
															.formatDecimal(collectionReportsMap
																	.get("corparateaccountno"))),
													collectionReportsMap.get(
															"senderinfo")
															.toString(), });
									n++;
									flag = 1;

								}
								Set keyset = data.keySet();
								for (Object key : keyset) {
									Row row1 = sheet.createRow(rownum++);
									Object[] objArr = data.get(key);
									int cellnum = 0;
									for (Object obj : objArr) {
										Cell cell = row1.createCell(cellnum++);
										if (obj instanceof String)
											cell.setCellValue((String) obj);
										else if (obj instanceof Integer)
											cell.setCellValue((Integer) obj);
									}
								}
							}

							if (flag == 1) {
								try {
									FileOutputStream out = new FileOutputStream(
											new File(workingDir + "/" + apiName
													+ "_" + userid + ".xlsx"));
									extForExcel = FilenameUtils
											.getExtension(workingDir + "/"
													+ apiName + "_" + userid
													+ ".xlsx");
									workbook.write(out);
									out.close();
								} catch (FileNotFoundException fe) {
									fe.printStackTrace();
								} catch (IOException ie) {
									ie.printStackTrace();
								}
								document.add(table);
								document.close();
								writer.close();
							}
							if (map.size() >= 1) {
								String msgForPDF = null;
								String msgForExcel = null;
								map.put("ResponseCode", "200");
								if (flag == 1) {
									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;
									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;
									map.put("PdfDownloadLink", msgForPDF);
									map.put("ExcelDownloadLink", msgForExcel);
								} else {
									map.put("PdfDownloadLink", "");
									map.put("ExcelDownloadLink", "");
								}

								Object obj = parser.parse(json.toJson(map));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else {
								outPut.put("ResponseCode", "500");
								outPut.put("error",
										"Something went wrong. Please try again later.");
								// outPut.put("ip", c.getIp(request));//change

								logger.info("500 : Something went wrong. Please try again later.");

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							}
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out1.print(jsonStringFinal);
						}
					} else {

						map1.put("ResponseCode", "498");

						map1.put("error", "Invalid SessionToken");

						// map1.put("ip", c.getIp(request));//change

						logger.info("498 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map1);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = c.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out1.print(jsonStringFinal);

					}
				} else {

					map1.put("ResponseCode", "403");

					map1.put("error", "SessionToken is Missing");

					// map1.put("ip", c.getIp(request));//change

					logger.info("403 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map1);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out1.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out1.print(jsonStringFinal);

			logger.error("Getecollectionreport API Exception: ", e);
		}
		out1.close();
	}

	*//**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 *//*

	@RequestMapping(value = "api/gettransactionreportlist1", method = RequestMethod.POST)
	public void getTransactionReportList(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out1 = response.getWriter();
		Gson json = new Gson();
		Common c = new Common();
		URI contextUrl = URI.create(request.getRequestURL().toString())
				.resolve(request.getContextPath());
		String apiName = "TransactionReportList";
		HttpSession sessionToken = null;

		logger.info("Transactionreportlist API");
		try {

			HashMap headerMap = (new Common()).getHeadersInfo(request);
			String api_key = "";
			if (api_key == null)
				api_key = "";
			String session_token = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			if (session_token == null)
				session_token = "";
			String userid = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			if (userid == null)
				userid = "";
			String mpin = c.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("mpin"));
			if (mpin == null)
				mpin = "";
			String pageno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pageno"));
			if (pageno == null)
				pageno = "";
			String pagesize = c.decode(Common.PRIVATE_KEY,
					request.getParameter("pagesize"));
			if (pagesize == null)
				pagesize = "";
			String startdate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("startdate"));
			if (startdate == null)
				startdate = "";
			String enddate = c.decode(Common.PRIVATE_KEY,
					request.getParameter("enddate"));
			if (enddate == null)
				enddate = "";
			String Product = c.decode(Common.PRIVATE_KEY,
					request.getParameter("product"));
			if (Product == null)
				Product = "";
			String Status = c.decode(Common.PRIVATE_KEY,
					request.getParameter("status"));
			if (Status == null)
				Status = "";
			String Corpbatchid = c.decode(Common.PRIVATE_KEY,
					request.getParameter("corpbatchid"));
			if (Corpbatchid == null)
				Corpbatchid = "";
			String Bankbatchid = c.decode(Common.PRIVATE_KEY,
					request.getParameter("bankbatchid"));
			if (Bankbatchid == null)
				Bankbatchid = "";
			String Corporateaccountno = c.decode(Common.PRIVATE_KEY,
					request.getParameter("corporateaccountno"));

			if (Corporateaccountno == null)
				Corporateaccountno = "";
			String TransactionAmountFrom = c.decode(Common.PRIVATE_KEY,
					request.getParameter("transactionamountfrom"));
			if (TransactionAmountFrom == null)
				TransactionAmountFrom = "";
			String TransactionAmountTo = c.decode(Common.PRIVATE_KEY,
					request.getParameter("transactionamountto"));
			if (TransactionAmountTo == null)
				TransactionAmountTo = "";
			String BeneficiaryCode = c.decode(Common.PRIVATE_KEY,
					request.getParameter("beneficiarycode"));
			if (BeneficiaryCode == null)
				BeneficiaryCode = "";
			String BeneficiaryName = c.decode(Common.PRIVATE_KEY,
					request.getParameter("beneficiaryname"));
			if (BeneficiaryName == null)
				BeneficiaryName = "";
			String BeneficiaryAccount = c.decode(Common.PRIVATE_KEY,
					request.getParameter("beneficiaryaccount"));
			if (BeneficiaryAccount == null)
				BeneficiaryAccount = "";
			String BeneficiaryIFSC = c.decode(Common.PRIVATE_KEY,
					request.getParameter("beneficiaryifsc"));
			if (BeneficiaryIFSC == null)
				BeneficiaryIFSC = "";
			String BankTransID = c.decode(Common.PRIVATE_KEY,
					request.getParameter("banktransid"));
			if (BankTransID == null)
				BankTransID = "";
			String TransUTR = c.decode(Common.PRIVATE_KEY,
					request.getParameter("transutr"));
			if (TransUTR == null)
				TransUTR = "";
			String CorporateRef = c.decode(Common.PRIVATE_KEY,
					request.getParameter("corporateref"));
			if (CorporateRef == null)
				CorporateRef = "";
			String Cheque = c.decode(Common.PRIVATE_KEY,
					request.getParameter("cheque"));
			if (Cheque == null)
				Cheque = "";

			String systemValueFromApi = c.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInJava = Integer.parseInt(systemValueFromApi);

			if (systemValueInJava == 1) {

				System.out
						.println("system value 1 for gettransactionreportlist ");

				String extForExcel = null;
				String extForPDF = null;
				int Count = 0;
				int flag = 0;
				int flag_for_single_data = 0;
				HashMap map2 = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "gettransactionreportlist";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"pageno\" : \""
								+ pageno.trim() + "\", \"pagesize\" :\""
								+ pagesize.trim() + "\",\"startdate\" : \""
								+ startdate.trim() + "\",\"enddate\" : \""
								+ enddate.trim() + "\",\"Product\" : \""
								+ Product.trim() + "\",\"Status\" : \""
								+ Status.trim() + "\",\"Corpbatchid\" : \""
								+ Corpbatchid.trim()
								+ "\",\"Bankbatchid\" : \""
								+ Bankbatchid.trim()
								+ "\",\"Corporateaccountno\" : \""
								+ Corporateaccountno.trim()
								+ "\",\"TransactionAmountFrom\" : \""
								+ TransactionAmountFrom.trim()
								+ "\",\"TransactionAmountTo\" : \""
								+ TransactionAmountTo.trim()
								+ "\",\"BeneficiaryCode\" : \""
								+ BeneficiaryCode.trim()
								+ "\",\"BeneficiaryName\" : \""
								+ BeneficiaryName.trim()
								+ "\",\"BeneficiaryAccount\" : \""
								+ BeneficiaryAccount.trim()
								+ "\",\"BeneficiaryIFSC\" : \""
								+ BeneficiaryIFSC.trim()
								+ "\",\"BankTransID\" : \""
								+ BankTransID.trim() + "\",\"TransUTR\" : \""
								+ TransUTR.trim() + "\",\"CorporateRef\" : \""
								+ CorporateRef.trim() + "\",\"Cheque\" : \""
								+ Cheque.trim() + "\"}";
						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for gettransactionreportlist:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							HashMap map = json.fromJson(jsonString,
									HashMap.class);
							HashMap map1 = new HashMap();
							LinkedHashMap outPut = new LinkedHashMap();
							PdfWriter writer = null;
							PdfPTable table = null;

							ArrayList transactionReportList = null;
							LinkedTreeMap tree1 = null;

							Document document = new Document();
							XSSFWorkbook workbook = new XSSFWorkbook();
							*//************** checking for map inner data not null ********************//*
							if (!map.get(
									"CUR_Filter_Transaction_Report_list_DB")
									.equals("")) {

								*//************************************** PDF Generation ****************************************//*

								table = new PdfPTable(8);
								java.util.Date date = new java.util.Date();
								Random rand = new Random();
								int randomCode = rand.nextInt();

								File file = new File(workingDir + "/" + apiName
										+ "_" + userid + ".pdf");
								extForPDF = FilenameUtils
										.getExtension(workingDir + "/"
												+ apiName + "_" + userid
												+ ".pdf");
								writer = PdfWriter.getInstance(document,
										new FileOutputStream(file));
								document.open();

								table.setWidthPercentage(100); // Width 100%
								table.setSpacingBefore(10f); // Space before
																// table
								table.setSpacingAfter(10f);

								
								 * PdfPCell cell1 = new PdfPCell(new
								 * Paragraph("Transaction Id"));
								 * cell1.setHorizontalAlignment
								 * (Element.ALIGN_CENTER);
								 * cell1.setVerticalAlignment
								 * (Element.ALIGN_MIDDLE);
								 

								PdfPCell cell2 = new PdfPCell(new Paragraph(
										"Benificiary Name"));
								cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell3 = new PdfPCell(new Paragraph(
										"Product Name"));
								cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell4 = new PdfPCell(new Paragraph(
										"Corporate Ref"));
								cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell5 = new PdfPCell(new Paragraph(
										"Corporate Account"));
								cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell6 = new PdfPCell(new Paragraph(
										"Transaction Amount"));
								cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell7 = new PdfPCell(new Paragraph(
										"Transaction Description"));
								cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell8 = new PdfPCell(new Paragraph(
										"Transaction Status"));
								cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell9 = new PdfPCell(new Paragraph(
										"Transaction UTR / Cheque"));
								cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);

								// table.addCell(cell1);
								table.addCell(cell2);
								table.addCell(cell3);
								table.addCell(cell4);
								table.addCell(cell5);
								table.addCell(cell6);
								table.addCell(cell7);
								table.addCell(cell8);
								table.addCell(cell9);

								*//************************************** Excel Sheet Generation ***********************************//*

								Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
								int n = 2;

								int rownum = 0;
								XSSFSheet sheet = workbook
										.createSheet("Batch Data");
								data.put(1, new Object[] { // "Transaction Id",
										"Benificiary Name", "Product Name",
												"Corporate Ref",
												"Corporate Account",
												"Transaction Amount",
												"Transaction Description",
												"Transaction Status",
												"Transaction UTR / Cheque" });

								*//**********************************************************************************************//*

								if (map.get("CUR_Filter_Transaction_Report_list_DB") instanceof ArrayList) {

									transactionReportList = (ArrayList) map
											.get("CUR_Filter_Transaction_Report_list_DB");

									for (int i = 0; i < transactionReportList
											.size(); i++) {
										LinkedTreeMap tree = (LinkedTreeMap) transactionReportList
												.get(i);
										Count++;

										*//*********************** PDF Data generation **************//*
										// table.addCell(tree.get("TransactionId").toString());
										table.addCell(tree.get(
												"BenificiaryName").toString());
										table.addCell(tree.get("ProductName")
												.toString());
										table.addCell(tree.get("CorporateRef")
												.toString());
										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("CorporateAccount")))));
										table.addCell(tree.get(
												"TransactionAmount").toString());
										table.addCell(tree.get(
												"TransactionDescription")
												.toString());
										table.addCell(tree.get(
												"TransactionStatus").toString());
										table.addCell(tree.get(
												"TransactionUTROrCheque")
												.toString());

										data.put(
												n,
												new Object[] { // tree.get("TransactionId").toString(),
														tree.get(
																"BenificiaryName")
																.toString(),
														tree.get("ProductName")
																.toString(),
														tree.get("CorporateRef")
																.toString(),
														String.valueOf(c
																.formatDecimal(tree
																		.get("CorporateAccount"))),
														tree.get(
																"TransactionAmount")
																.toString(),
														tree.get(
																"TransactionDescription")
																.toString(),
														tree.get(
																"TransactionStatus")
																.toString(),
														tree.get(
																"TransactionUTROrCheque")
																.toString() });
										n++;

									}

									flag = 1;
								}

								else {

									tree1 = (LinkedTreeMap) map
											.get("CUR_Filter_Transaction_Report_list_DB");

									// table.addCell(tree1.get("TransactionId").toString());
									table.addCell(tree1.get("BenificiaryName")
											.toString());
									table.addCell(tree1.get("ProductName")
											.toString());
									table.addCell(tree1.get("CorporateRef")
											.toString());

									table.addCell(String.valueOf((c
											.formatDecimal(tree1
													.get("CorporateAccount")))));
									table.addCell(tree1
											.get("TransactionAmount")
											.toString());
									table.addCell(tree1.get(
											"TransactionDescription")
											.toString());
									table.addCell(tree1
											.get("TransactionStatus")
											.toString());
									table.addCell(tree1.get(
											"TransactionUTROrCheque")
											.toString());

									data.put(
											n,
											new Object[] { // tree1.get("TransactionId").toString(),
													tree1.get("BenificiaryName")
															.toString(),
													tree1.get("ProductName")
															.toString(),
													tree1.get("CorporateRef")
															.toString(),
													String.valueOf(c
															.formatDecimal(tree1
																	.get("CorporateAccount"))),
													tree1.get(
															"TransactionAmount")
															.toString(),
													tree1.get(
															"TransactionDescription")
															.toString(),
													tree1.get(
															"TransactionStatus")
															.toString(),
													tree1.get(
															"TransactionUTROrCheque")
															.toString() });
									n++;

									flag_for_single_data = 1;
								}

								Set keyset = data.keySet();
								for (Object key : keyset) {// check
									Row row1 = sheet.createRow(rownum++);
									Object[] objArr = data.get(key);
									int cellnum = 0;
									for (Object obj : objArr) {
										Cell cell = row1.createCell(cellnum++);
										if (obj instanceof String)
											cell.setCellValue((String) obj);
										else if (obj instanceof Integer)
											cell.setCellValue((Integer) obj);
									}

								}

							}

							if (flag == 1 || flag_for_single_data == 1) {

								try {
									FileOutputStream out = new FileOutputStream(
											new File(workingDir + "/" + apiName
													+ "_" + userid + ".xlsx"));
									extForExcel = FilenameUtils
											.getExtension(workingDir + "/"
													+ apiName + "_" + userid
													+ ".xlsx");
									workbook.write(out);
									out.close();
								} catch (FileNotFoundException fe) {

									fe.printStackTrace();
								} catch (IOException ie) {
									ie.printStackTrace();
								}
								document.add(table);
								document.close();
								writer.close();
							}

							if (map.size() >= 1) {
								String msgForPDF = null;
								String msgForExcel = null;
								outPut.put("ResponseCode", "200");
								outPut.put("Responsedetails", "Success");
								if (flag == 1) {
									// outPut.put("TotalCount", Count);
									outPut.put("TotalCount",
											map.get("TotalCount"));
									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;
									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;

									outPut.put("PdfDownloadLink", msgForPDF);
									outPut.put("ExcelDownloadLink", msgForExcel);
									outPut.put("TransactionReportList",
											transactionReportList);
								} else if (flag_for_single_data == 1) {
									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;
									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;
									// outPut.put("TotalCount", 1);
									outPut.put("TotalCount",
											map.get("TotalCount"));
									outPut.put("PdfDownloadLink", msgForPDF);
									outPut.put("ExcelDownloadLink", msgForExcel);
									outPut.put("TransactionReportList", tree1);
								} else {
									// outPut.put("TotalCount", "");
									outPut.put("TotalCount",
											map.get("TotalCount"));
									outPut.put("PdfDownloadLink", "");
									outPut.put("ExcelDownloadLink", "");
									outPut.put("TransactionReportList", "");
								}

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else {
								outPut.put("ResponseCode", "500");
								// outPut.put("ip", c.getIp(request));//change
								outPut.put("error",
										"Something went wrong. Please try again later.");

								logger.info("500 : Something went wrong. Please try again later.");

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							}
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out1.print(jsonStringFinal);
						}
					} else {
						map2.put("ResponseCode", "210");
						// map2.put("ip", c.getIp(request));//change
						map2.put("error", "Invalid SessionToken");
						logger.info("210 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map2);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out1.print(jsonStringFinal);

					}
				} else {
					map2.put("ResponseCode", "210");
					// map2.put("ip", c.getIp(request));//change
					map2.put("error", "SessionToken is Missing");
					logger.info("210 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map2);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out1.print(jsonStringFinal);

				}

			}

			else if (systemValueInJava == 2) {

				System.out
						.println("system value 2 for gettransactionreportlist ");

				String extForExcel = null;
				String extForPDF = null;
				int Count = 0;
				int flag = 0;
				int flag_for_single_data = 0;
				HashMap map2 = new HashMap();
				JSONParser parser = new JSONParser();
				String jsonErrorMessage = null;
				if (!session_token.equals("")) {

					sessionToken = request.getSession();
					SessionToken objSessionToken1 = new SessionToken();
					objSessionToken1.setSessionToken(session_token);
					objSessionToken1.setUserID(userid);
					SessionToken objSessionToken = sessionTokenService
							.getUserByToken(objSessionToken1);

					String clientIp = c.getUserIp(request);

					if (clientIp.equals(sessionToken.getAttribute("client_ip"))
							&& !(sessionToken.isNew())
							&& (sessionToken.getId()).equals(session_token)
							&& sessionTokenService.getSessionToken(userid)
									.getSessionToken().equals(session_token)) {

						String url = apiUrl + "gettransactionreportlist";
						String inputString = "{\"api_key\" : \""
								+ api_key.trim() + "\",\"session_token\" : \""
								+ session_token.trim() + "\",\"userid\" : \""
								+ userid.trim() + "\",\"Mpin\" : \""
								+ mpin.trim() + "\",\"pageno\" : \""
								+ pageno.trim() + "\", \"pagesize\" :\""
								+ pagesize.trim() + "\",\"startdate\" : \""
								+ startdate.trim() + "\",\"enddate\" : \""
								+ enddate.trim() + "\",\"Product\" : \""
								+ Product.trim() + "\",\"Status\" : \""
								+ Status.trim() + "\",\"Corpbatchid\" : \""
								+ Corpbatchid.trim()
								+ "\",\"Bankbatchid\" : \""
								+ Bankbatchid.trim()
								+ "\",\"Corporateaccountno\" : \""
								+ Corporateaccountno.trim()
								+ "\",\"TransactionAmountFrom\" : \""
								+ TransactionAmountFrom.trim()
								+ "\",\"TransactionAmountTo\" : \""
								+ TransactionAmountTo.trim()
								+ "\",\"BeneficiaryCode\" : \""
								+ BeneficiaryCode.trim()
								+ "\",\"BeneficiaryName\" : \""
								+ BeneficiaryName.trim()
								+ "\",\"BeneficiaryAccount\" : \""
								+ BeneficiaryAccount.trim()
								+ "\",\"BeneficiaryIFSC\" : \""
								+ BeneficiaryIFSC.trim()
								+ "\",\"BankTransID\" : \""
								+ BankTransID.trim() + "\",\"TransUTR\" : \""
								+ TransUTR.trim() + "\",\"CorporateRef\" : \""
								+ CorporateRef.trim() + "\",\"Cheque\" : \""
								+ Cheque.trim() + "\"}";
						ClientResponse client_response = Common.apiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for gettransactionreportlist:"
								+ client_response.getStatus());
						String jsonString = client_response
								.getEntity(String.class);
						String responseStatus = String.valueOf(client_response
								.getStatus());

						if (responseStatus.equals("200")) {

							HashMap map = json.fromJson(jsonString,
									HashMap.class);
							HashMap map1 = new HashMap();
							LinkedHashMap outPut = new LinkedHashMap();
							PdfWriter writer = null;
							PdfPTable table = null;

							ArrayList transactionReportList = null;
							LinkedTreeMap tree1 = null;

							Document document = new Document();
							XSSFWorkbook workbook = new XSSFWorkbook();
							*//************** checking for map inner data not null ********************//*
							if (!map.get(
									"CUR_Filter_Transaction_Report_list_DB")
									.equals("")) {

								*//************************************** PDF Generation ****************************************//*

								table = new PdfPTable(8);
								java.util.Date date = new java.util.Date();
								Random rand = new Random();
								int randomCode = rand.nextInt();

								File file = new File(workingDir + "/" + apiName
										+ "_" + userid + ".pdf");
								extForPDF = FilenameUtils
										.getExtension(workingDir + "/"
												+ apiName + "_" + userid
												+ ".pdf");
								writer = PdfWriter.getInstance(document,
										new FileOutputStream(file));
								document.open();

								table.setWidthPercentage(100); // Width 100%
								table.setSpacingBefore(10f); // Space before
																// table
								table.setSpacingAfter(10f);

								
								 * PdfPCell cell1 = new PdfPCell(new
								 * Paragraph("Transaction Id"));
								 * cell1.setHorizontalAlignment
								 * (Element.ALIGN_CENTER);
								 * cell1.setVerticalAlignment
								 * (Element.ALIGN_MIDDLE);
								 

								PdfPCell cell2 = new PdfPCell(new Paragraph(
										"Benificiary Name"));
								cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell3 = new PdfPCell(new Paragraph(
										"Product Name"));
								cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell4 = new PdfPCell(new Paragraph(
										"Corporate Ref"));
								cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell5 = new PdfPCell(new Paragraph(
										"Corporate Account"));
								cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell6 = new PdfPCell(new Paragraph(
										"Transaction Amount"));
								cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell7 = new PdfPCell(new Paragraph(
										"Transaction Description"));
								cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell8 = new PdfPCell(new Paragraph(
										"Transaction Status"));
								cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);

								PdfPCell cell9 = new PdfPCell(new Paragraph(
										"Transaction UTR / Cheque"));
								cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
								cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);

								// table.addCell(cell1);
								table.addCell(cell2);
								table.addCell(cell3);
								table.addCell(cell4);
								table.addCell(cell5);
								table.addCell(cell6);
								table.addCell(cell7);
								table.addCell(cell8);
								table.addCell(cell9);

								*//************************************** Excel Sheet Generation ***********************************//*

								Map<Integer, Object[]> data = new TreeMap<Integer, Object[]>();
								int n = 2;

								int rownum = 0;
								XSSFSheet sheet = workbook
										.createSheet("Batch Data");
								data.put(1, new Object[] { // "Transaction Id",
										"Benificiary Name", "Product Name",
												"Corporate Ref",
												"Corporate Account",
												"Transaction Amount",
												"Transaction Description",
												"Transaction Status",
												"Transaction UTR / Cheque" });

								*//**********************************************************************************************//*

								if (map.get("CUR_Filter_Transaction_Report_list_DB") instanceof ArrayList) {

									transactionReportList = (ArrayList) map
											.get("CUR_Filter_Transaction_Report_list_DB");

									for (int i = 0; i < transactionReportList
											.size(); i++) {
										LinkedTreeMap tree = (LinkedTreeMap) transactionReportList
												.get(i);
										Count++;

										*//*********************** PDF Data generation **************//*
										// table.addCell(tree.get("TransactionId").toString());
										table.addCell(tree.get(
												"BenificiaryName").toString());
										table.addCell(tree.get("ProductName")
												.toString());
										table.addCell(tree.get("CorporateRef")
												.toString());
										table.addCell(String.valueOf((c.formatDecimal(tree
												.get("CorporateAccount")))));
										table.addCell(tree.get(
												"TransactionAmount").toString());
										table.addCell(tree.get(
												"TransactionDescription")
												.toString());
										table.addCell(tree.get(
												"TransactionStatus").toString());
										table.addCell(tree.get(
												"TransactionUTROrCheque")
												.toString());

										data.put(
												n,
												new Object[] { // tree.get("TransactionId").toString(),
														tree.get(
																"BenificiaryName")
																.toString(),
														tree.get("ProductName")
																.toString(),
														tree.get("CorporateRef")
																.toString(),
														String.valueOf(c
																.formatDecimal(tree
																		.get("CorporateAccount"))),
														tree.get(
																"TransactionAmount")
																.toString(),
														tree.get(
																"TransactionDescription")
																.toString(),
														tree.get(
																"TransactionStatus")
																.toString(),
														tree.get(
																"TransactionUTROrCheque")
																.toString() });
										n++;

									}

									flag = 1;
								}

								else {

									tree1 = (LinkedTreeMap) map
											.get("CUR_Filter_Transaction_Report_list_DB");

									// table.addCell(tree1.get("TransactionId").toString());
									table.addCell(tree1.get("BenificiaryName")
											.toString());
									table.addCell(tree1.get("ProductName")
											.toString());
									table.addCell(tree1.get("CorporateRef")
											.toString());

									table.addCell(String.valueOf((c
											.formatDecimal(tree1
													.get("CorporateAccount")))));
									table.addCell(tree1
											.get("TransactionAmount")
											.toString());
									table.addCell(tree1.get(
											"TransactionDescription")
											.toString());
									table.addCell(tree1
											.get("TransactionStatus")
											.toString());
									table.addCell(tree1.get(
											"TransactionUTROrCheque")
											.toString());

									data.put(
											n,
											new Object[] { // tree1.get("TransactionId").toString(),
													tree1.get("BenificiaryName")
															.toString(),
													tree1.get("ProductName")
															.toString(),
													tree1.get("CorporateRef")
															.toString(),
													String.valueOf(c
															.formatDecimal(tree1
																	.get("CorporateAccount"))),
													tree1.get(
															"TransactionAmount")
															.toString(),
													tree1.get(
															"TransactionDescription")
															.toString(),
													tree1.get(
															"TransactionStatus")
															.toString(),
													tree1.get(
															"TransactionUTROrCheque")
															.toString() });
									n++;

									flag_for_single_data = 1;
								}

								Set keyset = data.keySet();
								for (Object key : keyset) {// check
									Row row1 = sheet.createRow(rownum++);
									Object[] objArr = data.get(key);
									int cellnum = 0;
									for (Object obj : objArr) {
										Cell cell = row1.createCell(cellnum++);
										if (obj instanceof String)
											cell.setCellValue((String) obj);
										else if (obj instanceof Integer)
											cell.setCellValue((Integer) obj);
									}

								}

							}

							if (flag == 1 || flag_for_single_data == 1) {

								try {
									FileOutputStream out = new FileOutputStream(
											new File(workingDir + "/" + apiName
													+ "_" + userid + ".xlsx"));
									extForExcel = FilenameUtils
											.getExtension(workingDir + "/"
													+ apiName + "_" + userid
													+ ".xlsx");
									workbook.write(out);
									out.close();
								} catch (FileNotFoundException fe) {

									fe.printStackTrace();
								} catch (IOException ie) {
									ie.printStackTrace();
								}
								document.add(table);
								document.close();
								writer.close();
							}

							if (map.size() >= 1) {
								String msgForPDF = null;
								String msgForExcel = null;
								outPut.put("ResponseCode", "200");
								outPut.put("Responsedetails", "Success");
								if (flag == 1) {
									// outPut.put("TotalCount", Count);
									outPut.put("TotalCount",
											map.get("TotalCount"));
									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;
									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;

									outPut.put("PdfDownloadLink", msgForPDF);
									outPut.put("ExcelDownloadLink", msgForExcel);
									outPut.put("TransactionReportList",
											transactionReportList);
								} else if (flag_for_single_data == 1) {
									msgForPDF = appUrl + "/pdf/" + apiName
											+ "_" + userid + "." + extForPDF;
									msgForExcel = appUrl + "/excel/" + apiName
											+ "_" + userid + "." + extForExcel;
									// outPut.put("TotalCount", 1);
									outPut.put("TotalCount",
											map.get("TotalCount"));
									outPut.put("PdfDownloadLink", msgForPDF);
									outPut.put("ExcelDownloadLink", msgForExcel);
									outPut.put("TransactionReportList", tree1);
								} else {
									// outPut.put("TotalCount", "");
									outPut.put("TotalCount",
											map.get("TotalCount"));
									outPut.put("PdfDownloadLink", "");
									outPut.put("ExcelDownloadLink", "");
									outPut.put("TransactionReportList", "");
								}

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								// jsonObject.put("ip",
								// c.getIp(request));//change
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							} else {
								outPut.put("ResponseCode", "500");
								// outPut.put("ip", c.getIp(request));//change
								outPut.put("error",
										"Something went wrong. Please try again later.");

								logger.info("500 : Something went wrong. Please try again later.");

								Object obj = parser.parse(json.toJson(outPut));
								JSONObject jsonObject = (JSONObject) obj;
								jsonObject = c.load(jsonObject);
								String jsonStringFinal = jsonObject.toString();
								out1.print(jsonStringFinal);

							}
						} else {
							// System.out.println("Response:"+responseStatus);
							JSONObject jsonObject = new JSONObject();
							String errorMsg = this
									.getServerErrorMsg(responseStatus);
							jsonObject.put("ResponseCode", responseStatus);
							jsonObject.put("Responsedetails", errorMsg);
							// jsonObject.put("ip", c.getIp(request));//change
							logger.info(responseStatus + " : " + errorMsg);
							jsonObject = c.load(jsonObject);
							String jsonStringFinal = jsonObject.toString();
							out1.print(jsonStringFinal);
						}
					} else {
						map2.put("ResponseCode", "498");
						// map2.put("ip", c.getIp(request));//change
						map2.put("error", "Invalid SessionToken");
						logger.info("498 : Invalid SessionToken.");
						jsonErrorMessage = json.toJson(map2);

						Object obj = parser.parse(jsonErrorMessage);
						JSONObject jsonObject = (JSONObject) obj;
						jsonObject = c.load(jsonObject);
						String jsonStringFinal = jsonObject.toString();
						out1.print(jsonStringFinal);

					}
				} else {
					map2.put("ResponseCode", "403");
					// map2.put("ip", c.getIp(request));//change
					map2.put("error", "SessionToken is Missing");
					logger.info("403 : SessionToken is Missing.");
					jsonErrorMessage = json.toJson(map2);

					Object obj = parser.parse(jsonErrorMessage);
					JSONObject jsonObject = (JSONObject) obj;
					jsonObject = c.load(jsonObject);
					String jsonStringFinal = jsonObject.toString();
					out1.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out1.print(jsonStringFinal);

			logger.error("Transactionreportlist API Exception: ", e);
		}

		out1.close();
	}

	@RequestMapping(value = "/pdf/{userid}", method = RequestMethod.GET)
	public void getPDF(HttpServletRequest request,
			HttpServletResponse response, @PathVariable("userid") String userid)
			throws IOException {

		logger.info("pdf API");
		String filename = userid + ".pdf";
		File file = new File(workingDir + "/", filename);
		response.setHeader("Content-Type", request.getServletContext()
				.getMimeType(filename));
		response.setHeader("Content-Length", String.valueOf(file.length()));
		response.setHeader("Content-Disposition",
				"inline; filename=\"" + file.getName() + "\"");
		Files.copy(file.toPath(), response.getOutputStream());
	}

	@RequestMapping(value = "/excel/{userid}", method = RequestMethod.GET)
	public void getExcel(HttpServletRequest request,
			HttpServletResponse response, @PathVariable("userid") String userid)
			throws IOException {

		logger.info("excel API");
		String filename = userid + ".xlsx";
		File file = new File(workingDir + "/", filename);// In tomcat
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Length", String.valueOf(file.length()));
		response.setHeader("Content-Disposition",
				"inline; filename=\"" + file.getName() + "\"");
		Files.copy(file.toPath(), response.getOutputStream());
	}

	@RequestMapping(value = "/api/latestVersion", method = RequestMethod.GET)
	public void latestVersion(HttpServletResponse response,
			HttpServletRequest request) {

		logger.info("LatestVersion API");
		try {
			PrintWriter out = response.getWriter();
			Gson json = new Gson();
			Common c = new Common();
			JSONParser parser = new JSONParser();
			VersionManagement objVersion = cmsService.getLatestVersion();
			if (objVersion != null && objVersion.getVersion() > 0) {
				LinkedHashMap map = new LinkedHashMap();
				map.put("ResponseCode", "200");
				map.put("Version", objVersion.getVersion());
				// map.put("ip", c.getIp(request));
				if (objVersion.getUpgrade().equals("Optional"))
					map.put("Upgrade", "0");
				else
					map.put("Upgrade", "1");
				Object obj = parser.parse(json.toJson(map));
				JSONObject jsonObject = (JSONObject) obj;
				jsonObject = c.load(jsonObject);
				// jsonObject.put("ip", c.getIp(request));//change
				String jsonStringFinal = jsonObject.toString();
				out.print(jsonStringFinal);

			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("LatestVersion API Exception: ", e);
		}
	}

	// To generate server error msg
	public String getServerErrorMsg(String errorCode) {
		HashMap errorMap = new HashMap();
		errorMap.put("400", error400);
		errorMap.put("401", error401);
		errorMap.put("402", error402);
		errorMap.put("403", error403);
		errorMap.put("404", error404);
		errorMap.put("405", error405);
		errorMap.put("408", error408);
		errorMap.put("500", error500);
		errorMap.put("502", error502);
		errorMap.put("503", error503);
		errorMap.put("504", error504);

		// errorOther
		// System.out.println("Error:"+(String) errorMap.get(errorCode));
		if (errorMap.get(errorCode) != null) {
			return (String) errorMap.get(errorCode);
		} else {
			return errorOther;
		}

	}

	

}
*/