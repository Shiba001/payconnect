package com.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisbank.service.PayproReportService;
import com.axisbank.service.PowerAccessReportService;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.ECollectionReportRequestModel;
import com.payconnect.model.ECollectionResponseModel;
import com.payconnect.model.StatementEnqueryRequestModel;
import com.payconnect.model.StatementEnqueryResponseModel;
import com.payconnect.model.TransactionReportRequestModel;
import com.payconnect.model.TransactionReportResponseModel;
import com.poweraccess.model.SessionToken;

@Controller
public class ReportController {
	
	private final Logger logger=Logger.getLogger(ReportController.class);
	
	@Autowired
	private PowerAccessReportService powerAccessReportService;
	
	
	@Autowired
	private PayproReportService payproReportService;
	
	
	@Autowired
	private MessageUtil messageUtil;

	
	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;
	

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	
	@Autowired
	private DirectoryUtil directoryUtil;
	
	@Value("${server.url}")
	private String serverURL;

	private static final int BUFFER_SIZE = 4096;
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@RequestMapping(value = "/api/statementenquiry", method = RequestMethod.POST)
	public void getStatementEnquireReport(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();

		logger.info("statementenquiry API");

		try {
			@SuppressWarnings("rawtypes")
			HashMap headerMap = (new Common()).getHeadersInfo(request);
			
			
			String sessionToken = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			
			if (sessionToken == null)
				sessionToken = "";
			
			//System.out.println(sessionToken);
			
			String userId = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));
			
			if (userId == null)
				userId = "";
			
			//System.out.println(userId);
			
			String apiVersion = Common.decode(Common.PRIVATE_KEY,
					 request.getParameter("apiversion"));
			
			if (apiVersion == null)
				apiVersion = "";
			
			
			String corpId = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("corpid"));
			
			if (corpId == null)
				corpId = "";
			
			
			
			
			String accountName = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("accountname"));
			
			if (accountName == null)
				accountName = "";
			
			

			String accountNo = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("accountno"));
			
			if (accountNo == null)
				accountNo = "";
			
			
			
			String fromDate = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("fromdate"));
			
			
			
			if (fromDate == null)
				fromDate = "";
			
			String toDate = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("todate"));
			
			
			
			if (toDate == null)
				toDate = "";
			
			
			
			String statementFormat = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("statementformat"));
			
			if (statementFormat == null)
				statementFormat = "";
			
			
			
			String pageSize = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("pagesize"));
			
			if (pageSize == null)
				pageSize = "";
			
			
			
			String pageNo = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("pageno"));
			
			if (pageNo == null)
				pageNo = "";
			
			
			String fileType = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("filetype"));
			
			if (fileType == null)
				fileType = "";
			
			

			String systemValueFromApp = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));
			
			
			if (systemValueFromApp == null)
				systemValueFromApp = "";
			
			
		
			
			int systemValueInController = Integer.parseInt(systemValueFromApp);
			

			
			 if (systemValueInController == 1) {
				 
				 logger.info("inside power access statementenquiry");
				 
			    StatementEnqueryRequestModel statementEnqueryRequestModel=new StatementEnqueryRequestModel();
			    
			    statementEnqueryRequestModel.setApiVersion(apiVersion);
			    statementEnqueryRequestModel.setCorporateCode(corpId);
			    statementEnqueryRequestModel.setUserID(userId);
			    statementEnqueryRequestModel.setCorporateAccountNumber(accountNo);
			    statementEnqueryRequestModel.setAccountName(accountName);
			    
			    if(StringUtils.isNotBlank(fromDate)){
			    	
			    	  statementEnqueryRequestModel.setStartDate(new Date(fromDate.replace("-", "/")));
			    }
			    
			   
			    
			   
			    if(StringUtils.isNotBlank(toDate)){
			    	
			    	statementEnqueryRequestModel.setEndDate(new Date(toDate.replace("-", "/")));
			    }
			    
			   
			    statementEnqueryRequestModel.setStatementType(statementFormat);
			    statementEnqueryRequestModel.setPageNumber(pageNo);
			    statementEnqueryRequestModel.setRecordPerPage(pageSize);
			    statementEnqueryRequestModel.setFileType(fileType);
			    statementEnqueryRequestModel.setSystem(systemValueFromApp);
			    
			    if (!sessionToken.equals("")) {
				SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness.getPowerAccessSessionToken(userId.trim());
				
				if (powerSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
					StatementEnqueryResponseModel statementEnqueryResponseModel=powerAccessReportService.getStatementEnqueryResponseModel(statementEnqueryRequestModel, getApplicationURL(request));
					response.getWriter().write(statementEnqueryResponseModel.toJsonString());
				} else {
				    JSONObject jsonObject = new JSONObject();
				    
				    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
				    jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    out.print(jsonObject.toString());
				}
			} else {
			    JSONObject jsonObject = new JSONObject();
			    
			    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
			    jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));
					
			    logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));
					
			    out.print(jsonObject.toString());
			}
		} else if (systemValueInController == 2) {
			
			 logger.info("inside paypro statementenquiry");
			
		    StatementEnqueryRequestModel statementEnqueryRequestModel=new StatementEnqueryRequestModel();
		    
		    statementEnqueryRequestModel.setApiVersion(apiVersion);
		    statementEnqueryRequestModel.setCorporateCode(corpId);
		    statementEnqueryRequestModel.setUserID(userId);
		    statementEnqueryRequestModel.setCorporateAccountNumber(accountNo);
		    statementEnqueryRequestModel.setAccountName(accountName);

		    if(StringUtils.isNotBlank(fromDate)){
		    	 statementEnqueryRequestModel.setStartDate(new Date(fromDate.replace("-", "/")));
		    }

		    if(StringUtils.isNotBlank(toDate)){
		    	 statementEnqueryRequestModel.setEndDate(new Date(toDate.replace("-", "/")));
		    }
		    
		    
		    statementEnqueryRequestModel.setStatementType(statementFormat);
		    statementEnqueryRequestModel.setPageNumber(pageNo);
		    statementEnqueryRequestModel.setRecordPerPage(pageSize);
		    statementEnqueryRequestModel.setFileType(fileType);
		    statementEnqueryRequestModel.setSystem(systemValueFromApp);

		    if (!sessionToken.equals("")) {

			PayProSessionToken payProSessionTokenByUserId=payProSessionTokenBusiness.getPayproSessionTokenByUser(userId.trim());
					
			if (payProSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
			    StatementEnqueryResponseModel statementEnqueryResponseModel=payproReportService.getStatementEnqueryResponseModel(statementEnqueryRequestModel, getApplicationURL(request));
			    
			    if (statementEnqueryResponseModel.isSuccess()) {
				response.getWriter().write(statementEnqueryResponseModel.toJsonString());
			    }
			} else {
			    JSONObject jsonObject = new JSONObject();
					    
			    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
			    jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
					    
			    logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));

			    out.print(jsonObject.toString());
			}

		    } else {
			JSONObject jsonObject = new JSONObject();
				    
			jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
			jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));
						
			logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));

			out.print(jsonObject.toString());
		    }
			
		}

		} catch (Exception e) {
		    e.printStackTrace();
			
		    JSONObject jsonObject = new JSONObject();

		    jsonObject.put("error", messageUtil.getBundle("wrong.message"));

		    String jsonStringFinal = jsonObject.toString();

		    out.print(jsonStringFinal);

		    logger.error("statementenquiry API Exception: ", e);
		    
		    
		}
		
		out.close();
	}
	
	@SuppressWarnings({ "unchecked" })
	@RequestMapping(value = "/api/gettransactionreportlist", method = RequestMethod.POST)
	public void getTransactionListReport(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();

		logger.info("getTransactionListReport API");

		try {		

		    HashMap<String, String> headerMap = (new Common()).getHeadersInfo(request);

		    String sessionToken = Common.decode(Common.PRIVATE_KEY, headerMap.get("session_token"));
			
		    String userId = Common.decode(Common.PRIVATE_KEY, headerMap.get("user_id"));
			
		    String systemValueFromApp = Common.decode(Common.PRIVATE_KEY, request.getParameter("system"));
	
		    int systemValueInController = Integer.parseInt(systemValueFromApp);
		    
		    TransactionReportRequestModel transactionReportRequestModel = new TransactionReportRequestModel();
		    
		    String amountFrom = "";
		    String amountTo = "";
		    String batchId = "";
		    String benificiaryAccountNumber = "";
		    String benificiaryIfscCode = "";
		    String benificiaryName = "";
		    String chequeNumber = "";
		    String corpAccountNumber = "";
		    String endDate = "";
		    String fileName = "";
		    String productCode = "";
		    String recPerPage = "";
		    String startDate = "";
		    String statusCode = "";
		    String transactionId = "";
		    String transactionUtr = "";
		    String vendorCode = "";
		    String version = "";
		    String pageNo = "";
		    String corporateID = "";
		    String benificiaryCode = "";
		    String corporateRef = "";
		    String corporateBatchID = "";
		    String payDocNumber = "";
		    
		    if(request.getParameter("transactionamountfrom") != null){
			amountFrom = Common.decode(Common.PRIVATE_KEY, request.getParameter("transactionamountfrom"));
		    }
		    
                    if(request.getParameter("transactionamountto") != null){
                	amountTo = Common.decode(Common.PRIVATE_KEY, request.getParameter("transactionamountto"));
                    }
                    
                    if(request.getParameter("bankbatchid") != null){
                	batchId = Common.decode(Common.PRIVATE_KEY, request.getParameter("bankbatchid"));
                    }
                    
                    if(request.getParameter("beneficiaryaccount") != null){
                	benificiaryAccountNumber = Common.decode(Common.PRIVATE_KEY, request.getParameter("beneficiaryaccount"));
                    }
                    
                    if(request.getParameter("beneficiaryifsc") != null){
                	benificiaryIfscCode = Common.decode(Common.PRIVATE_KEY, request.getParameter("beneficiaryifsc"));
                    }
                    
                    if(request.getParameter("beneficiaryname") != null){
                	benificiaryName = Common.decode(Common.PRIVATE_KEY, request.getParameter("beneficiaryname"));
                    }		    
                    
                    if(request.getParameter("cheque") != null){
                	chequeNumber = Common.decode(Common.PRIVATE_KEY, request.getParameter("cheque"));
                    }
                    
                    if(request.getParameter("corporateaccountno") != null){
                	corpAccountNumber = Common.decode(Common.PRIVATE_KEY, request.getParameter("corporateaccountno"));
                    }
                    
                    if(request.getParameter("corpid") != null){
                	corporateID = Common.decode(Common.PRIVATE_KEY, request.getParameter("corpid"));
                    }
                    
                    if(request.getParameter("product") != null){
                	productCode = Common.decode(Common.PRIVATE_KEY, request.getParameter("product"));
                    }
                    
                    if(request.getParameter("pagesize") != null){
                	recPerPage = Common.decode(Common.PRIVATE_KEY, request.getParameter("pagesize"));
                    }
                    
                    if(request.getParameter("startdate") != null){
                	startDate = Common.decode(Common.PRIVATE_KEY, request.getParameter("startdate"));
                    }
                    
                    if(request.getParameter("status") != null){
                	statusCode = Common.decode(Common.PRIVATE_KEY, request.getParameter("status"));
                    }
                    
                    if(request.getParameter("banktransid") != null){
                	transactionId = Common.decode(Common.PRIVATE_KEY, request.getParameter("banktransid"));
                    }
                    
                    if(request.getParameter("transutr") != null){
                	transactionUtr = Common.decode(Common.PRIVATE_KEY, request.getParameter("transutr"));
                    }
                    
                    if(request.getParameter("system") != null){
                	version = Common.decode(Common.PRIVATE_KEY, request.getParameter("apiversion"));
                    }
                    
                    if(request.getParameter("pageno") != null){
                	pageNo = Common.decode(Common.PRIVATE_KEY, request.getParameter("pageno"));
                    }
                    
                    if(request.getParameter("enddate") != null){
                	endDate = Common.decode(Common.PRIVATE_KEY, request.getParameter("enddate"));
                    }
                    
                    if(request.getParameter("corpbatchid") != null){
                	corporateBatchID = Common.decode(Common.PRIVATE_KEY, request.getParameter("corpbatchid"));
                    }
                    
                    if(request.getParameter("beneficiarycode") != null){
                	benificiaryCode = Common.decode(Common.PRIVATE_KEY, request.getParameter("beneficiarycode"));
                    }
                    
                    if(request.getParameter("corporateref") != null){
                	corporateRef = Common.decode(Common.PRIVATE_KEY, request.getParameter("corporateref"));
                    }
                    
                    if(request.getParameter("vendorCode") != null){
                	vendorCode = Common.decode(Common.PRIVATE_KEY, request.getParameter("vendorCode"));
                    }
                    
                    if(request.getParameter("paydocnumber") != null){
                	vendorCode = Common.decode(Common.PRIVATE_KEY, request.getParameter("paydocnumber"));
                    }
                    
		    transactionReportRequestModel.setAmountFrom(amountFrom);
		    transactionReportRequestModel.setAmountTo(amountTo);
		    transactionReportRequestModel.setBatchId(batchId);
		    transactionReportRequestModel.setBenificiaryAccountNumber(benificiaryAccountNumber);
		    transactionReportRequestModel.setBenificiaryIfscCode(benificiaryIfscCode);
		    transactionReportRequestModel.setBenificiaryName(benificiaryName);
		    transactionReportRequestModel.setChequeNumber(chequeNumber);
		    transactionReportRequestModel.setCorpAccountNumber(corpAccountNumber);
		    transactionReportRequestModel.setEndDate(endDate);
		    transactionReportRequestModel.setFileName(fileName);
		    transactionReportRequestModel.setLoginID(userId);
		    transactionReportRequestModel.setPageNo(pageNo);
		    transactionReportRequestModel.setProductCode(productCode);
		    transactionReportRequestModel.setRecPerPage(recPerPage);
		    transactionReportRequestModel.setStartDate(startDate);
		    transactionReportRequestModel.setStatusCode(statusCode);
		    transactionReportRequestModel.setSystem(systemValueFromApp);
		    transactionReportRequestModel.setTransactionId(transactionId);
		    transactionReportRequestModel.setTransactionUtr(transactionUtr);
		    transactionReportRequestModel.setVendorCode(vendorCode);
		    transactionReportRequestModel.setVersion(version);
		    transactionReportRequestModel.setCorporateID(corporateID);
		    transactionReportRequestModel.setCorporateRef(corporateRef);
		    transactionReportRequestModel.setBenificiaryCode(benificiaryCode);
		    transactionReportRequestModel.setCorporateBatchID(corporateBatchID);
		    transactionReportRequestModel.setPayDocNumber(payDocNumber);
			
		    if (systemValueInController == 1) {
		    	
		    	logger.info("inside poweraccess getTransactionListReport");
		    	
			if (!sessionToken.equals("")) {
			    SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness.getPowerAccessSessionToken(userId.trim());

			    if (powerSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
				TransactionReportResponseModel transactionReportResponseModel = powerAccessReportService.getTransactionReportResponseModel(transactionReportRequestModel, getApplicationURL(request));
				    
				response.getWriter().write(transactionReportResponseModel.toJsonString());
			    } else {
				JSONObject jsonObject = new JSONObject();
				    
				jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
				jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				out.print(jsonObject.toString());
			    }
			} else {
			    JSONObject jsonObject = new JSONObject();
			    
			    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
			    jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));
					
			    logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));
					
			    out.print(jsonObject.toString());
			}
		    } else if (systemValueInController == 2) {
		    	
		    	logger.info("inside paypro getTransactionListReport");
		    	
			if (!sessionToken.equals("")) {
				PayProSessionToken payProSessionTokenByUserId=payProSessionTokenBusiness.getPayproSessionTokenByUser(userId.trim());
			    
				if (payProSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
					TransactionReportResponseModel transactionReportResponseModel=payproReportService.getTransactionReportResponseModel(transactionReportRequestModel, getApplicationURL(request));

					if (transactionReportResponseModel.isSuccess()) {
						out.print(transactionReportResponseModel.toJsonString());
					}
			    } else {
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
				jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));

				logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));

				out.print(jsonObject.toString());
			    }
			} else {
			    JSONObject jsonObject = new JSONObject();
			
			    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
			    jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));

			    logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));

			    out.print(jsonObject.toString());
			}
		    }
		} catch (Exception e) {
		    e.printStackTrace();

		    JSONObject jsonObject = new JSONObject();
			
		    jsonObject.put("error", messageUtil.getBundle("wrong.message"));

		    String jsonStringFinal = jsonObject.toString();

		    out.print(jsonStringFinal);

		    logger.error("getTransactionListReport API Exception: ", e);
		}
		
		out.close();
	}
	
	/**
	 * this URL is called to get the E Collection
	 * @since 08-Mar-2016
	 * @author Avishek Seal
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @throws IOException 
	 */
	@RequestMapping(value ="/api/getecollectionreport", method = RequestMethod.POST)
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void getECollectionReport(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception{
		
		PrintWriter out = httpServletResponse.getWriter();
	    try {
	    	
	    	
	    	logger.info("getecollectionreport API");
	    	
		HashMap<String, String> headerMap = (new Common()).getHeadersInfo(httpServletRequest);

		String sessionToken = Common.decode(Common.PRIVATE_KEY, headerMap.get("session_token"));
			
		String userId = Common.decode(Common.PRIVATE_KEY, headerMap.get("user_id"));
			
		String systemValueFromApp = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
		
		if(StringUtils.isNotBlank(sessionToken)) {
		    ECollectionReportRequestModel eCollectionRequest = new ECollectionReportRequestModel();
			
			String ammountFrom = null;
			String ammountTo = null;
			String corporateAccountNumber = null;
			String corporateCode = null;
			String endDate = null;
			Integer pageNumber = null;
			String productName = null;
			Integer recordPerPage = null;
			String startDate = null;
			String transactionalUTR = null;
			String version = null;
			
			if(httpServletRequest.getParameter("apiversion") != null){
			    version = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("apiversion"));
			}
			
			if(httpServletRequest.getParameter("corpid") != null){
			    corporateCode = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("corpid"));
			}
			
			if(httpServletRequest.getParameter("pagesize") != null){
			    recordPerPage = Integer.parseInt(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("pagesize")));
			}
			
			if(httpServletRequest.getParameter("pageno") != null){
			    pageNumber = Integer.parseInt(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("pageno")));
			}
			
			if(httpServletRequest.getParameter("startdate") != null){
			    startDate = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("startdate"));
			}
			
			if(httpServletRequest.getParameter("enddate") != null){
			    endDate = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("enddate"));
			}
			
			if(httpServletRequest.getParameter("paymentmode") != null){
			    productName = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("paymentmode"));
			}
			
			if(httpServletRequest.getParameter("utrno") != null){
			    transactionalUTR = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("utrno"));
			}
			
			if(httpServletRequest.getParameter("transactionamountfrom") != null){
			    ammountFrom = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("transactionamountfrom"));
			}
			
			if(httpServletRequest.getParameter("transactionamountto") != null){
			    ammountTo = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("transactionamountto"));
			}
			
			if(httpServletRequest.getParameter("creditaccountno") != null){
			    corporateAccountNumber = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("creditaccountno"));
			}
			
			
			
			eCollectionRequest.setAmmountFrom(ammountFrom);
			eCollectionRequest.setAmmountTo(ammountTo);
			eCollectionRequest.setCorporateAccountNumber(corporateAccountNumber);
			eCollectionRequest.setCorporateCode(corporateCode);
			if(StringUtils.isNotBlank(endDate)){
				
				eCollectionRequest.setEndDate(new Date(endDate.replace("-", "/")));
			}
			
			eCollectionRequest.setLogingID(userId);
			eCollectionRequest.setPageNumber(pageNumber);
			eCollectionRequest.setProductName(productName);
			eCollectionRequest.setRecordPerPage(recordPerPage);
			if (StringUtils.isNotBlank(startDate)) {
				eCollectionRequest.setStartDate(new Date(startDate.replace("-", "/")));
			}
			
			
			eCollectionRequest.setTransactionalUTR(transactionalUTR);
			eCollectionRequest.setVersion(version);
			
			if (StringUtils.equals(systemValueFromApp, "1")) {
				
				logger.info("power access getecollectionreport");
				
				//System.out.println("inside system 1");
				
			    SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness.getPowerAccessSessionToken(userId.trim());
				
			    if (powerSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
				ECollectionResponseModel eCollectionResponseModel = powerAccessReportService.getECollectionResponseModel(eCollectionRequest, getApplicationURL(httpServletRequest));
				
				if(eCollectionResponseModel.isSuccess()) {
				    httpServletResponse.getWriter().write(eCollectionResponseModel.toJsonString());
				}
			    }
			    
			    
			    else {
			    	
                     JSONObject jsonObject = new JSONObject();
				    
				    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
				    jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    out.print(jsonObject.toString());
				}
			}
			
			
			
			else if (StringUtils.equals(systemValueFromApp, "2")) {
				
				logger.info("paypro getecollectionreport");
				
			    PayProSessionToken payProSessionTokenByUserId=payProSessionTokenBusiness.getPayproSessionTokenByUser(userId.trim());
				
			    if (payProSessionTokenByUserId.getSessionToken().equals(sessionToken)) {
				ECollectionResponseModel eCollectionResponseModel =payproReportService.getECollectionResponseModel(eCollectionRequest, getApplicationURL(httpServletRequest));
				
				if (eCollectionResponseModel.isSuccess()) {
				    httpServletResponse.getWriter().write(eCollectionResponseModel.toJsonString());
				}
				
			    } 
			    
			    
			    else {
               JSONObject jsonObject = new JSONObject();
				    
				    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
				    jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    logger.error(messageUtil.getBundle("error.invalid.sessiontoken.code") +" : "+messageUtil.getBundle("invalid.sessiontoken.message"));
				    
				    out.print(jsonObject.toString());
				}
			}
		} else {
		    JSONObject jsonObject = new JSONObject();

		    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.sessiontoken.missing.code"));
		    jsonObject.put("error",   messageUtil.getBundle("error.sessiontoken.message"));

		    logger.error(messageUtil.getBundle("error.sessiontoken.missing.code") +" : "+ messageUtil.getBundle("error.sessiontoken.message"));

		    httpServletResponse.getWriter().write(jsonObject.toString());
		}
		
	    } catch (Exception e) {
	    	
	    	e.printStackTrace();
	    	
	    	    JSONObject jsonObject = new JSONObject();
				
			    jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			    String jsonStringFinal = jsonObject.toString();

			    out.print(jsonStringFinal);

			    logger.error("getTransactionListReport API Exception: ", e);
	    	
		
	    }
	    finally {
	    	
	    	out.close();
		
	    }
	}
	
	/**
	 * this method is used to fetch the actual file from the server location for downloading
	 * @since 07-Mar-2016
	 * @author Avishek Seal
	 * @param path
	 * @param httpServletRequest
	 * @param httpServletResponse
	 */
	@RequestMapping(value = "/api/download", method = RequestMethod.GET)
	public void downloadFile(@RequestParam(value = "path") String path, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
	    final File filePath = new File(actualPath(path));
	    
	    try(FileInputStream inputStream = new FileInputStream(filePath); ServletOutputStream outStream = httpServletResponse.getOutputStream();){

		String mimeType = httpServletRequest.getServletContext().getMimeType(filePath.getAbsolutePath());
		
		if (mimeType == null) {
		    mimeType = "application/octet-stream";
		}

		httpServletResponse.setContentType(mimeType);
		httpServletResponse.setContentLength((int) filePath.length());

		String headerKey = "Content-Disposition";
		
		String headerValue = String.format("attachment; filename=\"%s\"", filePath.getName());
		
		httpServletResponse.setHeader(headerKey, headerValue);
		
		byte[] buffer = new byte[BUFFER_SIZE];
	
		int bytesRead = -1;

		while ((bytesRead = inputStream.read(buffer)) != -1) {
		    outStream.write(buffer, 0, bytesRead);
		}

		inputStream.close();
		outStream.close();
	    } catch(Exception exception) {
		exception.printStackTrace();
	    }
	}
	
	private final String actualPath(String relativePath){
	    final StringBuilder actualPath = new StringBuilder();
	   
	    actualPath.append(directoryUtil.getReportDumpingPath());
	    actualPath.append("/"+relativePath);
	    
	    return actualPath.toString();
	}
	
	 private final String getApplicationURL(HttpServletRequest request){
		//return request.getScheme()+"://"+request.getServerName()+":"+request.getLocalPort()+request.getServletContext().getContextPath();
		 logger.info("Report server URL "+serverURL);
		 return serverURL;
		
	 }
}
