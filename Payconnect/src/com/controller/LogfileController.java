/**
 * 
 */
package com.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.axisbank.service.LogFileService;
import com.payconnect.model.LogFileModel;

/**
 * @author Avishek Seal
 *
 */
@Controller
public class LogfileController {
    
    private static final int BUFFER_SIZE = 4096;
    
    @Autowired
    private LogFileService logfilFileService;
    
    @RequestMapping(value = "/log-files", method = RequestMethod.POST)
    public @ResponseBody List<LogFileModel> getLogFileList(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
	try {
	    return logfilFileService.getListOfLogFile();
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }
    
    @RequestMapping(value = "/log-file", method = RequestMethod.GET)
    public ModelAndView redirectToLogfileViewerPage(@RequestParam(value = "user", required = true) String user,@RequestParam(value = "pass", required = true) String password, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
	if(StringUtils.equals(user, "admin") && StringUtils.equals(password, "pass@123")) {
	    httpServletResponse.setStatus(HttpServletResponse.SC_OK);
	    return new ModelAndView("log");
	}
	
	httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	return null;
    }
    
    @RequestMapping(value = "/log-download", method = RequestMethod.GET)
    public void downloadLogFile(@RequestParam(value = "fileName") String logFile, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
	final File filePath = new File(logfilFileService.getLogFilePath(logFile));
	    
	    try(FileInputStream inputStream = new FileInputStream(filePath); ServletOutputStream outStream = httpServletResponse.getOutputStream();){

		String mimeType = httpServletRequest.getServletContext().getMimeType(filePath.getAbsolutePath());
		
		if (mimeType == null) {
		    mimeType = "application/octet-stream";
		}

		httpServletResponse.setContentType(mimeType);
		httpServletResponse.setContentLength((int) filePath.length());

		String headerKey = "Content-Disposition";
		
		String headerValue = String.format("attachment; filename=\"%s\"", filePath.getName());
		
		httpServletResponse.setHeader(headerKey, headerValue);
		
		byte[] buffer = new byte[BUFFER_SIZE];
	
		int bytesRead = -1;

		while ((bytesRead = inputStream.read(buffer)) != -1) {
		    outStream.write(buffer, 0, bytesRead);
		}

		inputStream.close();
		outStream.close();
	    } catch(Exception exception) {
		exception.printStackTrace();
	    }
    }
    
}
