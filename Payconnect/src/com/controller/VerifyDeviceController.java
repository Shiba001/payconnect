package com.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.VerifyDeviceBusiness;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.UnlockDeviceModel;
import com.payconnect.model.VerifyDeviceModel;

@Controller
public class VerifyDeviceController {

	private final Logger logger = Logger.getLogger(VerifyDeviceController.class);

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private VerifyDeviceBusiness verifyDeviceBusiness;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/verifydevice", method = RequestMethod.POST)
	public void accountList(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws IOException {

		JSONObject jsonObject = new JSONObject();

		try {

			if (logger.isInfoEnabled()) {
				logger.info("verifydevice start");
				logger.info("/api/verifydevice API");
			}

			VerifyDeviceModel verifyDeviceModel = new VerifyDeviceModel();

			verifyDeviceModel.setDeviceId(httpServletRequest.getParameter("deviceid"));

	       UnlockDeviceModel unlockDeviceModel=  verifyDeviceBusiness.getVerifyDevice(verifyDeviceModel);
	       
	     
	       
	       if(StringUtils.isBlank(unlockDeviceModel.getUnlockCode())){
	    	 
	    	   jsonObject.put("unlockcode", "0");
	       }
	       
	       if(StringUtils.isNotBlank(unlockDeviceModel.getUnlockCode())){
	    	   
	    	   jsonObject.put("unlockcode", "1");
	       }
	       
	       
			if (unlockDeviceModel.getDeviceStatus().equalsIgnoreCase("A")) {

				jsonObject.put("ResponseCode", "201");
				jsonObject.put("Responsedetails", "app unlocked");

			}

			else if (unlockDeviceModel.getDeviceStatus().equalsIgnoreCase("D")) {

				jsonObject.put("ResponseCode", "200");
				jsonObject.put("Responsedetails", "app locked");
			}

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			JSONObject finalJson=Common.load(jsonObject);
			httpServletResponse.getWriter().write(finalJson.toJSONString());

		}

		
		catch (InvalidInputException e) {
			e.printStackTrace();
			 httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			 httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
			 logger.info("verifydevice API InvalidInputException: ", e);
			
		} catch (Exception e) {
			e.printStackTrace();

			 httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			httpServletResponse.getWriter().write(somethingWenWrong());

			logger.info("verifydevice API Exception: ", e);
		}

		finally {
			if (logger.isInfoEnabled()) {
				logger.info("resetPassword End");
			}
		}

	}

	@SuppressWarnings("unchecked")
	private String somethingWenWrong() {
		final JSONObject jsonObject = new JSONObject();
		jsonObject.put(messageUtil.getBundle("response.code"),
				messageUtil.getBundle("wrong.error.code"));
		jsonObject.put(messageUtil.getBundle("error"),
				messageUtil.getBundle("wrong.message"));

		return jsonObject.toJSONString();
	}
	
	 //this method is used to generate JSON response
    @SuppressWarnings("unchecked")
    private final String responseGenerator(String responseCode, String responseDetails){
	final JSONObject jsonObject = new JSONObject();
	
	jsonObject.put("ResponseCode", responseCode);
	jsonObject.put("ResponseDetails", responseDetails);
	
	return jsonObject.toJSONString();
    }

}
