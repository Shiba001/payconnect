package com.controller;

import org.springframework.stereotype.Controller;


@Controller
public class APIPayproController {

/*	private static final Logger logger = Logger.getLogger(APIPayproController.class);
	
	@Autowired
	private AppMenuBusiness appMenuBusiness;
	@Autowired
	public MessageUtil messageUtil;
	@Autowired
	private AppErrorCodeBusiness appErrorCodeBusiness;
	@Autowired
	private AppDeRegisterDeviceBusiness appDeRegisterDeviceBusiness;
	

	Common common = new Common();
	*/
	/*@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/getmenuitem", method = RequestMethod.POST)
	public @ResponseBody JSONObject getAllMenuItemByUser(HttpServletRequest request) throws Exception{
		
		if(logger.isInfoEnabled()){
			logger.info("getAllMenuItemByUser start");
			logger.info("/api/getmenuitem API");
		}
		
		
		
		HashMap<String, String> headerMap = common.getHeadersInfo(request);

		
		@SuppressWarnings("static-access")
		String userId = common.decode(Common.PRIVATE_KEY,(String) headerMap.get("user_id"));
		
		@SuppressWarnings("static-access")
		String session_token = common.decode(Common.PRIVATE_KEY,(String) headerMap.get("session_token"));
		
		@SuppressWarnings("static-access")
		String system = common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
		
		JSONObject jsonObject = new JSONObject();
		
		try {
			
			
			
			jsonObject = appMenuBusiness.getAllMenuByUser(userId, system);
			
		} catch (Exception e) {
			 e.printStackTrace();
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			 jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		}
		
		if(logger.isInfoEnabled()){
			logger.info("getAllMenuItemByUser start");
		}
		
		
		return jsonObject;
	}
	*/
	/**
	 * Get Error Code & Messages API
	 * @author Supratim Sarkar
	 * @param request
	 * @return
	 */
	/*@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/geterrormessages", method = RequestMethod.POST)
	public @ResponseBody JSONObject getAllErrorCode(HttpServletRequest request) throws Exception{
		if(logger.isInfoEnabled()){
			logger.info("getAllErrorCode start");
			logger.info("/api/geterrormessages API");
		}
		
		@SuppressWarnings("static-access")
		String system = common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
		JSONObject jsonObject = new JSONObject();
		System.out.println("system>>>>>>>>>"+system);
		try {
			
			jsonObject = appErrorCodeBusiness.getAllErrorCode(system);
			
		} catch (Exception e) {

			 e.printStackTrace();
			 
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			 jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		}
		
		
		
		if(logger.isInfoEnabled()){
			logger.info("getAllErrorCode End");
		}
		
		return jsonObject;
	}
	
	*/
	/*@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/deregisterdevice", method = RequestMethod.POST)
	public @ResponseBody JSONObject deRegisterDevice(HttpServletRequest request) throws Exception{
		if(logger.isInfoEnabled()){
			logger.info("deRegisterDevice start");
			logger.info("/api/deregisterdevice API");
		}
		
		HashMap<String, String> headerMap = common.getHeadersInfo(request);
		
		@SuppressWarnings("static-access")
		String userId = common.decode(Common.PRIVATE_KEY,(String) headerMap.get("user_id"));
		@SuppressWarnings("static-access")
		String sessionToken = common.decode(Common.PRIVATE_KEY,(String) headerMap.get("session_token"));
		@SuppressWarnings("static-access")
		String system = common.decode(Common.PRIVATE_KEY,request.getParameter("system"));
		@SuppressWarnings("static-access")
		String deviceID = common.decode(Common.PRIVATE_KEY,request.getParameter("deviceid"));
		
		JSONObject jsonObject = new JSONObject();
		
		try {
			DeRegisterDevice deRegisterDevice = new DeRegisterDevice();
			deRegisterDevice.setSystem(system);
			deRegisterDevice.setUserId(userId);
			deRegisterDevice.setSessionToken(sessionToken);
			deRegisterDevice.setDeviceId(deviceID);
			
			//System.out.println("system is >>>>>>>>> "+deRegisterDevice.getSystem());
			
			return appDeRegisterDeviceBusiness.deRegisterDevice(deRegisterDevice);
		} catch (Exception e) {

			 e.printStackTrace();
			 
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			 jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		}
		
		if(logger.isInfoEnabled()){
			logger.info("deRegisterDevice End");
		}
		
		return jsonObject;
	}*/
	
	
}
