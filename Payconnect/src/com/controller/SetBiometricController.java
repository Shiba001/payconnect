package com.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.business.SetBiomatricBusiness;
import com.axisbankcmspaypro.exception.DataNotUpdateException;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.axisbankcmspaypro.exception.SessionTokenMissing;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.payconnect.model.SetBiomatricModel;

@Controller
public class SetBiometricController {

	private final Logger logger = Logger
			.getLogger(SetBiometricController.class);

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	
	@Autowired
	private SetBiomatricBusiness setBiomatricBusiness;

	@SuppressWarnings("unchecked")
	@RequestMapping(value ="/api/setbiometric", method = RequestMethod.POST)
	public void biomatricIndentity(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {

		

		try {

			if (logger.isInfoEnabled()) {
				logger.info("setbiometric start");
				
			}

			

                HashMap<String, String> headerMap = (new Common()).getHeadersInfo(httpServletRequest);
			
		
			
                String sessionToken = Common.decode(Common.PRIVATE_KEY, headerMap.get("session_token"));
			
			
		

			String system = Common.decode(Common.PRIVATE_KEY,
					httpServletRequest.getParameter("system"));
			
			System.out.println("System "+system);

			String userId = Common.decode(Common.PRIVATE_KEY, headerMap.get("user_id"));
			
			System.out.println("User id "+userId);

			String deviceId = httpServletRequest.getParameter("deviceid");

			String corpId = httpServletRequest.getParameter("corpid");

			String presentSessionToken = null;

			if (StringUtils.isBlank(sessionToken)) {
				throw new SessionTokenMissing();
			}

			if (StringUtils.equals(system, "1")) {
				presentSessionToken = powerAccessTokenBusiness
						.getPowerAccessSessionToken(userId.trim())
						.getSessionToken();
			} else if (StringUtils.equals(system, "2")) {
				presentSessionToken = payProSessionTokenBusiness
						.getPayproSessionTokenByUser(userId.trim())
						.getSessionToken();
			}
			
			if(StringUtils.equals(sessionToken, presentSessionToken)){
				
				SetBiomatricModel biomatricModel=new SetBiomatricModel();
				
				biomatricModel.setUserId(userId);
				biomatricModel.setCorpId(corpId);
				biomatricModel.setDeviceId(deviceId);
				biomatricModel.setSystem(system);
				
				
				
				SetBiomatricModel setBiomatricModel=setBiomatricBusiness.biomatricIndenfication(biomatricModel);
				
				
				
				 if(Objects.nonNull(setBiomatricModel)){
					
					setBiomatricBusiness.updatPayconnectUserFingerOn(setBiomatricModel);
					
					final JSONObject jsonObject = new JSONObject();
					
					jsonObject.put("Responsedetails", "Success");
					
					jsonObject.put("ResponseCode", "200");
					
					httpServletResponse.getWriter().write(jsonObject.toJSONString());
					
				}
				
				
			}
			
			else{
				  httpServletResponse.setStatus(HttpServletResponse.SC_BAD_GATEWAY);
				httpServletResponse.getWriter().write(invalidSessionToken());
			}
			
		
		}
		
		catch (SessionTokenMissing sessionTokenMissing) {
		    if(logger.isInfoEnabled()) {
			logger.info("setbiometric() - SessionTokenMissingException "+sessionTokenMissing);
		    }
		    httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
		    httpServletResponse.getWriter().write(sessionTokenIsMissing());
		
	}
		
		
		catch (InvalidInputException e) {
		    
			e.printStackTrace();
			 httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			 httpServletResponse.getWriter().write(responseGenerator("500", e.getMessage()));
			 logger.info("setbiometric API InvalidInputException: ", e);
		
	}
		
		catch(DataNotUpdateException e){
			
			 httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			 httpServletResponse.getWriter().write(dataBaseException());
			 logger.info("setbiometric API DataNotUpdateException: ", e);
		}
		
		
		
		
		catch (Exception e) {
			e.printStackTrace();

			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			httpServletResponse.getWriter().write(somethingWentWrong());

			logger.info("setbiometric API Exception: ", e);
		}

		finally {
			if (logger.isInfoEnabled()) {
				logger.info("setbiometric End");
			}
		}

	}

	

	 private String somethingWentWrong() throws Exception{
			Map<String, String> map = new HashMap<String, String>();
			
			map.put("error", "Something went wrong. Please try again later.");
			
			final Gson gson = new Gson();
			
			final String jsonErrorMessage = gson.toJson(map);
			
			final JSONParser parser = new JSONParser();

			final Object obj = parser.parse(jsonErrorMessage);
			JSONObject jsonObject = (JSONObject) obj;
			jsonObject = Common.load(jsonObject);
			
			return jsonObject.toString();
		    }
		    
		    
		    private String invalidSessionToken() throws Exception{
		    	Map<String, String> map = new HashMap<String, String>();
		    	
		    	map.put("ResponseCode", "498");
		    	map.put("error", "Invalid SessionToken");
		    	
		    	final Gson gson = new Gson();
		    	
		    	String jsonErrorMessage = gson.toJson(map);
		    	
		    	final JSONParser parser = new JSONParser();

		    	Object obj = parser.parse(jsonErrorMessage);
		    	JSONObject jsonObject = (JSONObject) obj;
		    	jsonObject = Common.load(jsonObject);
		    	
		    	return jsonObject.toString();
		        }
		    
		   
		    private String dataBaseException() throws Exception{
		    	Map<String, String> map = new HashMap<String, String>();
		    	
		    	map.put("ResponseCode", "201");
		    	map.put("Responsedetails", "Failed to set biometric");
		    	
		    	final Gson gson = new Gson();
		    	
		    	String jsonErrorMessage = gson.toJson(map);
		    	
		    	final JSONParser parser = new JSONParser();

		    	Object obj = parser.parse(jsonErrorMessage);
		    	JSONObject jsonObject = (JSONObject) obj;
		    	jsonObject = Common.load(jsonObject);
		    	
		    	return jsonObject.toString();
		        }
		   
		    
		    private String sessionTokenIsMissing() throws Exception{
		    	Map<String, String> map = new HashMap<String, String>();
		    	
		    	map.put("ResponseCode", "498");
		    	
		    	map.put("error", "SessionToken is Missing");
		    	
		    	final Gson gson = new Gson();
		    	
		    	String jsonErrorMessage = gson.toJson(map);
		    	
		    	final JSONParser parser = new JSONParser();

		    	Object obj = parser.parse(jsonErrorMessage);
		    	JSONObject jsonObject = (JSONObject) obj;
		    	jsonObject = Common.load(jsonObject);
		    	
		    	return jsonObject.toString();
		        }
		    
		    
		    
		    //this method is used to generate JSON response
		    @SuppressWarnings("unchecked")
		    private final String responseGenerator(String responseCode, String responseDetails){
			final JSONObject jsonObject = new JSONObject();
			
			jsonObject.put("ResponseCode", responseCode);
			jsonObject.put("ResponseDetails", responseDetails);
			
			return jsonObject.toJSONString();
		    }
		    

}
