package com.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisbankcmspaypro.business.ResetPassword;
import com.common.Common;
import com.common.MessageUtil;
import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.ResetPasswordModel;

@Controller
public class ResetPasswordController {

    private Logger logger = Logger.getLogger(ResetPasswordController.class);

    @Autowired
    private MessageUtil messageUtil;
   
    @Autowired
    private ResetPassword resetPasswordBusiness;
    
    

    @RequestMapping(value = "/api/resetpassword", method = RequestMethod.POST)
    public @ResponseBody void resetPassword(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
	try {
	    if (logger.isInfoEnabled()) {
		logger.info("resetPassword api start");
		logger.info("/api/resetpassword API");
	    }
	    
	    Common common = new Common();
	    HashMap<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
	    ResetPasswordModel resetPasswordModel = new ResetPasswordModel();
	    resetPasswordModel.setUserName(headerMap.get("user_id"));
	  
	    resetPasswordModel.setSessionToken(headerMap.get("session_token"));
	    resetPasswordModel.setCorporateCode(httpServletRequest.getParameter("corpid"));
	    resetPasswordModel.setSystemValueFromApi(httpServletRequest.getParameter("system"));
	    resetPasswordModel.setOldPassword(httpServletRequest.getParameter("oldpassword"));
	    resetPasswordModel.setPassword(httpServletRequest.getParameter("newpassword"));
	    resetPasswordModel.setDeviceID(httpServletRequest.getParameter("deviceid"));
	    resetPasswordModel.setApiVersion(httpServletRequest.getParameter("apiversion"));
	    resetPasswordModel.setSystemValueFromApi(httpServletRequest.getParameter("system"));
	    
	    if(StringUtils.equals(resetPasswordModel.getSystemValueFromApi(), "1")) {
	    	
	    	logger.info("inside power access resetpassword API");
	    	
	    	LoginResponseModel powerAccessloginResponseModel=resetPasswordBusiness.resetPasswordForPowerAccess(resetPasswordModel);
		
	    	if(powerAccessloginResponseModel.isSuccess()){
	    		powerAccessloginResponseModel.setSessionToken(resetPasswordModel.getSessionToken());
	    	}
	    	
	    	httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.getWriter().write(powerAccessloginResponseModel.toJsonString());
	    } else if(StringUtils.equals(resetPasswordModel.getSystemValueFromApi(), "2")) {
	    	
	    	logger.info("inside paypro resetpassword API");
	    	
		 LoginResponseModel payProloginResponseModel = resetPasswordBusiness.resetPasswordForPaypro(resetPasswordModel);
		
		if(payProloginResponseModel.isSuccess()){
			
			payProloginResponseModel.setCorpID(resetPasswordModel.getCorporateCode());
			payProloginResponseModel.setSessionToken(resetPasswordModel.getSessionToken());
			payProloginResponseModel.setUserID(resetPasswordModel.getUserName());  
		}
		
		httpServletResponse.setStatus(HttpServletResponse.SC_OK);
		httpServletResponse.getWriter().write(payProloginResponseModel.toJsonString());
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    logger.info("Resetpassword API Exception "+e);
	    logger.info("Resetpassword API Exception message "+e.getMessage());
	    httpServletResponse.setStatus(HttpServletResponse.SC_OK);
	    httpServletResponse.getWriter().write(somethingWenWrong());
	} finally{
	    if (logger.isInfoEnabled()) {
		    logger.info("resetPassword End");
		}
	}
    }
    
    @SuppressWarnings("unchecked")
    private String somethingWenWrong(){
	final JSONObject jsonObject = new JSONObject();
	jsonObject.put(messageUtil.getBundle("response.code"),  messageUtil.getBundle("wrong.error.code"));
	jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
	
	return jsonObject.toJSONString();
    }

}
