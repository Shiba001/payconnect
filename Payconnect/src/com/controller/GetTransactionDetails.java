package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.poweraccess.model.SessionToken;
import com.poweraccess.service.SessionTokenService;
import com.sun.jersey.api.client.ClientResponse;

@Controller
public class GetTransactionDetails {
	
	private final Logger logger = Logger.getLogger(GetTransactionDetails.class);

	@Autowired
	private SessionTokenService sessionTokenService;

	@Autowired
	private MessageUtil messageUtil;

	
	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;

	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;

	
	@Value("${error.400}")
	private String error400;
	@Value("${error.401}")
	private String error401;
	@Value("${error.402}")
	private String error402;
	@Value("${error.403}")
	private String error403;
	@Value("${error.404}")
	private String error404;
	@Value("${error.405}")
	private String error405;
	@Value("${error.408}")
	private String error408;
	@Value("${error.500}")
	private String error500;
	@Value("${error.502}")
	private String error502;
	@Value("${error.503}")
	private String error503;
	@Value("${error.504}")
	private String error504;
	@Value("${error.other}")
	private String errorOther;
	
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "api/getransactiondetails", method = RequestMethod.POST)
	public void getTransactionWiseAuthorizationAdvanceSearch(
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();
		
		
		logger.info("getransactiondetails API");

		try {
			
			
			@SuppressWarnings("rawtypes")
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String session_token = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));
			
			//System.out.println("session token is "+session_token);

			if (session_token == null)
				session_token = "";

			String userid = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			if (userid == null)
				userid = "";
			
			//System.out.println("user id is "+ userid);

		

			String apiVersion = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("apiversion"));

			if (apiVersion == null)
				apiVersion = "";
			
			//System.out.println("api version is  "+apiVersion);

			String corpId = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("corpid"));

			if (corpId == null)
				corpId = "";
			
			//System.out.println("corp id is  "+corpId);

			String pageno = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("pageno"));

			if (pageno == null)
				pageno = "";
			
			//System.out.println("page no is  "+pageno);

			String pageSize = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("pagesize"));

			if (pageSize == null)
				pageSize = "";
			
			//System.out.println("page size is  "+pageSize);

			String batch_id = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("batchid"));

			if (batch_id == null)
				batch_id = "";
			
			//System.out.println("batch id is  "+batch_id);

			String account_id = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("accountid"));

			if (account_id == null)
				account_id = "";
			
			//System.out.println("account_id id is  "+account_id);

			String isonhold = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("isonhold"));
			
			if (isonhold == null)
				isonhold = "";
            
			//System.out.println("isonhold is  "+isonhold);
			
			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";
			
			//System.out.println("systemValueFromApi is  "+systemValueFromApi);

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {

				//System.out.println("inside Transactiondetails system value 1 ");
				
				logger.info("inside power access getransactiondetails");

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					

					HttpSession powerAccessSessionToken = request.getSession(false);
					

					String clientIp = Common.getUserIp(request);

					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userid.trim());
					
					//System.out.println("session token by user id >>>>>>>>> "+powerSessionTokenByUserId.getSessionToken());
					
					//System.out.println("user id is "+ userid);

					/*
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userid)
					 * .getSessionToken().equals(session_token)) {
					 */

					if (powerSessionTokenByUserId.getSessionToken().equals(session_token)) {

						String url = apiUrlPayconnect + "getAuthTranDtl";
						
						//System.out.println("url for hit the api "+ url);

						String inputString = Common
								.getransactiondetails(
										apiVersion, corpId, userid,
										systemValueFromApi, pageno, pageSize,
										batch_id, account_id, isonhold);

						ClientResponse client_response = Common.payconnectApiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getAuthTranDtl:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

			

						if (responseStatus.equals("200")) {

							// Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) parser
									.parse(jsonString);
							
							logger.info("power access getAuthTranDtl api response from axis:"+ jsonObject.toString());

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							System.out
									.println("final json string from axis end >>>>>>>>>>>>>>>>>> "
											+ jsonStringFinal);

							out.print(jsonStringFinal);

						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);

							jsonObject.put("ResponseCode", responseStatus);

							jsonObject.put("Responsedetails", errorMsg);

							logger.info(responseStatus + " : " + errorMsg);

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						}

					} else {

						map.put("ResponseCode", "498");

						map.put("error", "Invalid SessionToken");

						logger.info("498 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map);

					
						JSONObject jsonObject = (JSONObject) parser
								.parse(jsonErrorMessage);

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {
					
					map.put("ResponseCode", "498");

					map.put("error", "SessionToken is Missing");

					logger.info("498 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInController == 2) {

				//System.out.println("inside Transactiondetails system value2 ");
				
				logger.info("inside paypro getransactiondetails");

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					
					HttpSession payProSessionToken = request.getSession(false);

					String clientIp = Common.getUserIp(request);

					//System.out.println("machine ip  ip is >>>>>>>>>>>>>" + clientIp);

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness.getPayproSessionTokenByUser(userid.trim());
					
					System.out.println("session token for user id >>>>>>>>>>>> " +payProSessionTokenByUserId.getSessionToken());

					/*
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userid)
					 * .getSessionToken().equals(session_token)) {
					 */

					if (payProSessionTokenByUserId.getSessionToken().equals(session_token)) {

						String url = apiUrlPayconnect + "getAuthTranDtl";

						String inputString = Common
								.getransactiondetails(
										apiVersion, corpId, userid,
										systemValueFromApi, pageno, pageSize,
										batch_id, account_id, isonhold);

						ClientResponse client_response = Common.payconnectApiAcess(url,
								inputString, "post");

						logger.info("Axis Server Status for getAuthTranDtl:"
								+ client_response.getStatus());

						String jsonString = client_response
								.getEntity(String.class);

						String responseStatus = String.valueOf(client_response
								.getStatus());

						System.out
								.println("axis end status code is >>>>>>>>>> "
										+ responseStatus);

						if (responseStatus.equals("200")) {

							Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) obj;
							
							logger.info("Axis api response for paypro getAuthTranDtl:"
									+ jsonObject.toString());

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							//System.out.println("axis end response is >>>>>>>>>> "+ jsonStringFinal);

							out.print(jsonStringFinal);

						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this.getServerErrorMsg(responseStatus);

							jsonObject.put("ResponseCode", responseStatus);

							jsonObject.put("Responsedetails", errorMsg);

							logger.info(responseStatus + " : " + errorMsg);

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						}

					} else {
						map.put("ResponseCode", "498");

						map.put("error", "Invalid SessionToken");

						logger.info("498 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {
					map.put("ResponseCode", "498");

					map.put("error", "SessionToken is Missing");

					logger.info("error : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			logger.error("Transactiondetails API Exception: ", e);
		}

		out.close();
	}

	
	
	
	
	// To generate server error msg
	
		public String getServerErrorMsg(String errorCode) {
			
			HashMap<String, String> errorMap = new HashMap<String, String>();
			errorMap.put("400", error400);
			errorMap.put("401", error401);
			errorMap.put("402", error402);
			errorMap.put("403", error403);
			errorMap.put("404", error404);
			errorMap.put("405", error405);
			errorMap.put("408", error408);
			errorMap.put("500", error500);
			errorMap.put("502", error502);
			errorMap.put("503", error503);
			errorMap.put("504", error504);

			if (errorMap.get(errorCode) != null) {
				return (String) errorMap.get(errorCode);
			} else {
				return errorOther;
			}

		}
		
	
}
