package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.poweraccess.model.SessionToken;
import com.sun.jersey.api.client.ClientResponse;

@Controller
public class ActionOnTransactionOrBatch {

	private final Logger logger = Logger.getLogger(ActionOnTransactionOrBatch.class);

	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;
	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	@Autowired
	private MessageUtil messageUtil;

	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;

	@Value("${error.400}")
	private String error400;
	@Value("${error.401}")
	private String error401;
	@Value("${error.402}")
	private String error402;
	@Value("${error.403}")
	private String error403;
	@Value("${error.404}")
	private String error404;
	@Value("${error.405}")
	private String error405;
	@Value("${error.408}")
	private String error408;
	@Value("${error.500}")
	private String error500;
	@Value("${error.502}")
	private String error502;
	@Value("${error.503}")
	private String error503;
	@Value("${error.504}")
	private String error504;
	@Value("${error.other}")
	private String errorOther;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "api/actionontransactionorbatch", method = RequestMethod.POST)
	public void getActionOnTransactionOrBatch(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		Gson json = new Gson();

		logger.info("inside actionontransactionorbatch API");

		
		try {

			@SuppressWarnings("rawtypes")
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String session_token = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			//System.out.println("session token is " + session_token);

			if (session_token == null)
				session_token = "";

			String userid = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			if (userid == null)
				userid = "";

			//System.out.println("user id is " + userid);

			String mpin = Common.decode(Common.PRIVATE_KEY,
					 request.getParameter("mpin"));

			if (mpin == null)
				mpin = "";

			//System.out.println("mpin is  " + mpin);

			String apiVersion = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("apiversion"));

			if (apiVersion == null)
				apiVersion = "";

			//System.out.println("api version is  " + apiVersion);

			String corpId = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("corpid"));

			if (corpId == null)
				corpId = "";

			//System.out.println("corp id is  " + corpId);

			String actionFor = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("actionfor"));

			if (actionFor == null)
				actionFor = "";

			//System.out.println("action for is  " + actionFor);

			String id = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("id"));

			if (id == null)
				id = "";

			//System.out.println("id is  " + id);

			String actionType = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("actiontype"));

			if (actionType == null)
				actionType = "";

			//System.out.println("action type is  " + actionType);

			String remarks = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("remarks"));

			if (remarks == null)
				remarks = "";

			//System.out.println("remark is  " + remarks);

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			//System.out.println("systemValueFromApi is  " + systemValueFromApi);

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {
				
				logger.info("inside power access actionontransactionorbatch");

				//System.out.println("System value 1 for actionontransactionorbatch");
				
				

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					

					HttpSession powerAccessSessionToken = request.getSession(false);

					String clientIp = Common.getUserIp(request);

					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness
							.getPowerAccessSessionToken(userid.trim());

					//System.out.println("session token by user id >>>>>>>>> "+ powerSessionTokenByUserId.getSessionToken());

					//System.out.println("user id is " + userid);

					/*
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService
					 * .getSessionToken(userid).getSessionToken
					 * ().equals(session_token)) {
					 */

					if (powerSessionTokenByUserId.getSessionToken().equals(
							session_token)) {

						String url = apiUrlPayconnect + "getAuthSubmit";
						
						//System.out.println("axis url is "+url);

						String inputString = Common
								.getActionOnTransactionOrBatchJSONData(
										apiVersion, corpId, userid,
										systemValueFromApi, actionFor, id,
										actionType, remarks);
						
						logger.info("input string passes to Axis getAuthSubmit api for power access  "+inputString );
						
						

						ClientResponse client_response = Common.payconnectApiAcess(url,inputString, "post");

						
						
						String responseStatus = String.valueOf(client_response.getStatus());
						
						logger.info("Power access getAuthSubmit Status from Axis api:"+ responseStatus);

						if (responseStatus.equals("200")) {
							
							String jsonString = client_response
									.getEntity(String.class);

							Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) obj;
							
							//System.out.println("axis end response >>>>>>>>>>>" +jsonObject.toString());
							
							logger.info("power access getAuthSubmit json response from axis api "+jsonObject.toString());

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();
							

							out.print(jsonStringFinal);

						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this.getServerErrorMsg(responseStatus);

							jsonObject.put("ResponseCode", responseStatus);

							jsonObject.put("Responsedetails", errorMsg);

                            logger.info(" power access getAuthSubmit api response status from axis api "+ responseStatus );
							
							logger.info("power access getAuthSubmit  error detais from axis api  " + errorMsg);
							
							logger.info("power access getAuthSubmit json response rest of success part from axis api    " + jsonObject.toString()); 
							

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						}

					} else {

						map.put("ResponseCode", "498");

						map.put("error", "Invalid SessionToken");

						logger.info("498 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();
						
						logger.info("power access actionontransactionorbatch api response inside invalid session token "+ jsonStringFinal);

						out.print(jsonStringFinal);

					}
				} else {

					map.put("ResponseCode", "498");

					map.put("error", "SessionToken is Missing");

					logger.info("498 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();
					
					logger.info("power access actionontransactionorbatch api response inside session token is missing "+ jsonStringFinal);

					out.print(jsonStringFinal);

				}

			}

			else if (systemValueInController == 2) {

				logger.info("inside paypro actionontransactionorbatch");

				//System.out.println("System value 2 for actionontransactionorbatch");

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				JSONParser parser = new JSONParser();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					/*
					 * sessionToken = request.getSession(); SessionToken
					 * objSessionToken1 = new SessionToken();
					 * objSessionToken1.setSessionToken(session_token);
					 * objSessionToken1.setUserID(userid); SessionToken
					 * objSessionToken =
					 * sessionTokenService.getUserByToken(objSessionToken1);
					 * 
					 * String clientIp = Common.getUserIp(request);
					 * 
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService.getSessionToken(userid)
					 * .getSessionToken().equals(session_token)) {
					 */

					HttpSession payProSessionToken = request.getSession(false);

					String clientIp = Common.getUserIp(request);

					//System.out.println("machine ip  ip is >>>>>>>>>>>>>"+ clientIp);

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userid.trim());

				

					if (payProSessionTokenByUserId.getSessionToken().equals(session_token)) {

						String url = apiUrlPayconnect+"getAuthSubmit";

						String inputString = Common
								.getActionOnTransactionOrBatchJSONData(
										apiVersion, corpId, userid,
										systemValueFromApi, actionFor, id,
										actionType, remarks);
						
						logger.info("input string passes to Axis getAuthSubmit api for paypro  "+inputString );

						ClientResponse client_response = Common.payconnectApiAcess(url, inputString, "post");

						//logger.info("Axis Server Status for actionontransactionorbatch:"+ client_response.getStatus());

						String jsonString = client_response.getEntity(String.class);

						String responseStatus = String.valueOf(client_response.getStatus());
						
						logger.info("Paypro axis Server Status for getAuthSubmit:"+ client_response.getStatus());

						if (responseStatus.equals("200")) {

							Object obj = parser.parse(jsonString);

							JSONObject jsonObject = (JSONObject) obj;
							
							logger.info("paypro getAuthSubmit json response from axis api "+jsonObject.toString());

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						} else {

							JSONObject jsonObject = new JSONObject();

							String errorMsg = this
									.getServerErrorMsg(responseStatus);

							jsonObject.put("ResponseCode", responseStatus);

							jsonObject.put("Responsedetails", errorMsg);
							
							logger.info(" paypro getAuthSubmit api response status from axis api "+ responseStatus );
								
							logger.info("paypro getAuthSubmit  error detais from axis api  " + errorMsg);
								
							logger.info("paypro getAuthSubmit json response rest of success part from axis api    " + jsonObject.toString()); 

							//logger.info(responseStatus + " : " + errorMsg);

							jsonObject = Common.load(jsonObject);

							String jsonStringFinal = jsonObject.toString();

							out.print(jsonStringFinal);
						}

					} else {

						map.put("ResponseCode", "498");

						map.put("error", "Invalid SessionToken");

						logger.info("498 : Invalid SessionToken.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();
						
						logger.info("paypro getAuthSubmit api response inside invalid session token "+ jsonStringFinal);

						out.print(jsonStringFinal);

					}
				} else {

					map.put("ResponseCode", "498");

					map.put("error", "SessionToken is Missing");

					logger.info("498 : SessionToken is Missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();
					
					logger.info("paypro getAuthSubmit api response inside SessionToken is Missing "+ jsonStringFinal);

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

			logger.error("Actionontransactionorbatch API Exception: ", e);
		}

		out.close();
	}

	// To generate server error msg
	public String getServerErrorMsg(String errorCode) {
		HashMap<String, String> errorMap = new HashMap<String, String>();
		errorMap.put("400", error400);
		errorMap.put("401", error401);
		errorMap.put("402", error402);
		errorMap.put("403", error403);
		errorMap.put("404", error404);
		errorMap.put("405", error405);
		errorMap.put("408", error408);
		errorMap.put("500", error500);
		errorMap.put("502", error502);
		errorMap.put("503", error503);
		errorMap.put("504", error504);

		if (errorMap.get(errorCode) != null) {
			return (String) errorMap.get(errorCode);
		} else {
			return errorOther;
		}

	}

}
