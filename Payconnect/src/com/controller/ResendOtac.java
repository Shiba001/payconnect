package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PayconnectOtacBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.Common;
import com.common.MessageUtil;
import com.google.gson.Gson;
import com.payconnect.model.PayconnectOtacModel;
import com.poweraccess.model.SessionToken;
import com.sun.jersey.api.client.ClientResponse;

@Controller
public class ResendOtac {
	
	
	private final Logger logger = Logger.getLogger(ResendOtac.class);


	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private PayconnectOtacBusiness payconnectOtacBusiness;

	
	@Autowired
	private PayconnectOtacBusiness PayconnectOtacBusiness;

	@Autowired
	private PayProSessionTokenBusiness payProSessionTokenBusiness;

	@Autowired
	private PowerAccessTokenBusiness powerAccessTokenBusiness;
	

	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;


	@Value("${otac.min}")
	private int otacMin;

	@Value("${otac.max}")
	private int otacMax;

	@Value("${otac.validity.sec}")
	private int otacValidity;

	@Value("${oatc.sms.api.path}")
	private String otacSmsApiPath;

	@Value("${oatc.sms.api.username}")
	private String otacSmsApiUsername;

	@Value("${oatc.sms.api.password}")
	private String otacSmsApiPassword;

	@Value("${oatc.sms.api.ctype}")
	private String otacSmsApiCtype;

	@Value("${oatc.sms.api.alert}")
	private String otacSmsApiAlert;

	@Value("${oatc.sms.api.msgtype}")
	private String otacSmsApiMsgType;

	@Value("${oatc.sms.api.priority}")
	private String otacSmsApiPriority;

	@Value("${oatc.sms.api.sender}")
	private String otacSmsApiSender;

	@Value("${oatc.sms.api.msg}")
	private String otacSmsApiMsg;

	@Value("${oatc.sms.api.decode}")
	private String otacSmsApiDecode;

	@Value("${error.400}")
	private String error400;
	
	@Value("${error.401}")
	private String error401;
	
	@Value("${error.402}")
	private String error402;
	
	@Value("${error.403}")
	private String error403;
	
	@Value("${error.404}")
	private String error404;
	
	@Value("${error.405}")
	private String error405;
	
	@Value("${error.408}")
	private String error408;
	
	@Value("${error.500}")
	private String error500;
	
	@Value("${error.502}")
	private String error502;
	
	@Value("${error.503}")
	private String error503;
	
	@Value("${error.504}")
	private String error504;
	
	@Value("${error.other}")
	private String errorOther;

	@Value("${otac.sms.gateway.server}")
	private String smsServer;
	

	
	/**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/resendotac", method = RequestMethod.POST)
	public void resendOtac(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		PrintWriter out = response.getWriter();
	
		Gson json = new Gson();

		

		logger.info("Resend otac API");

		try {

			@SuppressWarnings("rawtypes")
			HashMap headerMap = (new Common()).getHeadersInfo(request);

			String userID = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("user_id"));

			

			if (userID == null)
				userID = "";

			String session_token = Common.decode(Common.PRIVATE_KEY,
					(String) headerMap.get("session_token"));

			

			if (session_token == null)
				session_token = "";

			String mobileno = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("mobileno"));

			if (mobileno == null)
				mobileno = "";

			String systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
					request.getParameter("system"));

			if (systemValueFromApi == null)
				systemValueFromApi = "";

			int systemValueInController = Integer.parseInt(systemValueFromApi);

			if (systemValueInController == 1) {

				logger.info("inside power access resendotac");

				JSONParser parser = new JSONParser();

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession powerAccessSessionToken = request.getSession(false);

					//String clientIp = Common.getUserIp(request);


					SessionToken powerSessionTokenByUserId = powerAccessTokenBusiness.getPowerAccessSessionToken(userID.trim());

					

					/*
					 * if
					 * (clientIp.equals(sessionToken.getAttribute("client_ip"))
					 * && !(sessionToken.isNew()) &&
					 * (sessionToken.getId()).equals(session_token) &&
					 * sessionTokenService
					 * .getSessionToken(userID).getSessionToken
					 * ().equals(session_token)) {
					 */

					if (powerSessionTokenByUserId.getSessionToken().equals(session_token)) {

						if (userID.equals("") || mobileno.equals("")) {

							map.put("ResponseCode",
									messageUtil
											.getBundle("failed.sessiontoken.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.failed.sessiontoken"));

							logger.info("202 : Failed To send");

						} else {

							/******* Otac Implementation Start *******/

							String pattern = "yyyy-MM-dd HH:mm:ss";

							SimpleDateFormat format = new SimpleDateFormat(pattern);

							String formattedCreateDate = format.format(new Date());

							Date date = format.parse(formattedCreateDate);

							Timestamp timestamp = new Timestamp(date.getTime());

							String otac = Common.generateOtac(otacMin, otacMax);

							// ....................modified by Arup
							// paul........................................//

							PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();

							payconnectOtacModel.setOtac(otac);

							payconnectOtacModel.setUserId(userID);

							payconnectOtacModel.setDeviceId("Not updated");

							payconnectOtacModel.setRegisteredPhone(mobileno);

							payconnectOtacModel.setCreateDate(timestamp);

							payconnectOtacModel.setSystem(systemValueFromApi);

							// ................................................................//

							payconnectOtacBusiness
									.insertOrUpdateOctacModel(payconnectOtacModel);

							if (mobileno.length() == 10) {
								mobileno = "91" + mobileno;
							}

							String sms_url = "";

							if (smsServer.equals("axis")) {

								sms_url = otacSmsApiPath + "?dcode="
										+ otacSmsApiDecode + "&userid="
										+ otacSmsApiUsername + "&pwd="
										+ otacSmsApiPassword + "&ctype="
										+ otacSmsApiCtype + "&alert="
										+ otacSmsApiAlert + "&msgtype="
										+ otacSmsApiMsgType + "&priority="
										+ otacSmsApiPriority + "&sender="
										+ otacSmsApiSender + "&pno="
										+ mobileno + "&msgtxt=" + otac
										+ otacSmsApiMsg;
							}

							else {
								sms_url = "http://alerts.solutionsinfini.com/api/v3/index.php?method=sms"
										+ "&api_key=A6f8b45980b586dd9a2fc8281e9dc5b34&to="
										+ mobileno
										+ "&sender=MYRTAX"
										+ "&message=Hello,%20Welcome%20to%20MakeYourTax.com.%20The%20OTP%20for%20registration%20is%20"
										+ otac
										+ "&format=json&custom=1,2&flash=0";
							}

							logger.info("SMS URL resendotac:" + sms_url);

							ClientResponse sms_response = Common.payconnectApiAcess(
									sms_url, "", "get");

							String sms_str = sms_response
									.getEntity(String.class);

							String responseStatus = String.valueOf(sms_response
									.getStatus());

							logger.info("SMS Response resendotac from axis: " + sms_str);

							/******* Otac Implementation End *******/

							if (!responseStatus.equals("200")) {

								String errorMsg = this.getServerErrorMsg(responseStatus);
								
								logger.info("error message from sms gateway:"+ errorMsg);

								logger.info("SMS Response Status:"+ responseStatus);
							}

							map.put("ResponseCode",
									messageUtil
											.getBundle("resendotac.success.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.success.responsedetails"));

							logger.info("200 : Resend otac API-Success");

						}

						String jsonString = json.toJson(map);

						Object obj = parser.parse(jsonString);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();
						
						logger.info(" resendotac final json response "+jsonStringFinal);

						out.print(jsonStringFinal);

					} else {

						map.put("ResponseCode",
								messageUtil
										.getBundle("resendotac.invalid.sessiontoken.responsedetails"));

						map.put("error", messageUtil
								.getBundle("resendotac.invalid.sessiontoken"));

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {

					map.put("ResponseCode", messageUtil
							.getBundle("resendotac.missing.sessiontoken"));

					map.put("error",
							messageUtil
									.getBundle("resendotac.missing.sessiontoken.responsedetails"));

					logger.info("210 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

			if (systemValueInController == 2) {

				logger.info("inside paypro resendotac");

				JSONParser parser = new JSONParser();

				@SuppressWarnings("rawtypes")
				HashMap map = new HashMap();

				String jsonErrorMessage = null;

				if (!session_token.equals("")) {

					HttpSession payProSessionToken = request.getSession(false);

					//String clientIp = Common.getUserIp(request);

			

					

					PayProSessionToken payProSessionTokenByUserId = payProSessionTokenBusiness
							.getPayproSessionTokenByUser(userID.trim());

					if (payProSessionTokenByUserId.getSessionToken().equals(session_token)) {

						if (userID.equals("") || mobileno.equals("")) {

							map.put("ResponseCode",
									messageUtil
											.getBundle("failed.sessiontoken.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.failed.sessiontoken"));

							logger.info("202 : Failed To send");

						} else {

							String pattern = "yyyy-MM-dd HH:mm:ss";

							SimpleDateFormat format = new SimpleDateFormat(
									pattern);

							String formattedCreateDate = format
									.format(new Date());

							Date date = format.parse(formattedCreateDate);

							Timestamp timestamp = new Timestamp(date.getTime());

							String otac = Common.generateOtac(otacMin, otacMax);

							PayconnectOtacModel payconnectOtacModel = new PayconnectOtacModel();

							payconnectOtacModel.setOtac(otac);

							payconnectOtacModel.setUserId(userID);

							payconnectOtacModel.setDeviceId("Not updated");

							payconnectOtacModel.setRegisteredPhone(mobileno);

							payconnectOtacModel.setCreateDate(timestamp);

							payconnectOtacModel.setSystem(systemValueFromApi);

							payconnectOtacBusiness
									.insertOrUpdateOctacModel(payconnectOtacModel);

							String smsGetWayResponse = PayconnectOtacBusiness
									.getResponseFromSmsApi(payconnectOtacModel.getRegisteredPhone(),
											payconnectOtacModel.getOtac());
							logger.info("smsGetWayResponse response from axis"+ smsGetWayResponse);

							System.err.println("smsGetWayResponse >>>>>>>"
									+ smsGetWayResponse);


							map.put("ResponseCode",
									messageUtil
											.getBundle("resendotac.success.responsecode"));

							map.put("Responsedetails",
									messageUtil
											.getBundle("resendotac.success.responsedetails"));

							logger.info("200 : Resend otac API-Success");

						}

						String jsonString = json.toJson(map);

						Object obj = parser.parse(jsonString);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();
						
						logger.info("paypro resend otac final json "+jsonStringFinal);

						out.print(jsonStringFinal);

					} else {

						map.put("ResponseCode",
								messageUtil
										.getBundle("resendotac.invalid.sessiontoken.responsedetails"));

						map.put("error", messageUtil
								.getBundle("resendotac.invalid.sessiontoken"));

						logger.info("498 : Invalid Session Token.");

						jsonErrorMessage = json.toJson(map);

						Object obj = parser.parse(jsonErrorMessage);

						JSONObject jsonObject = (JSONObject) obj;

						jsonObject = Common.load(jsonObject);

						String jsonStringFinal = jsonObject.toString();

						out.print(jsonStringFinal);

					}
				} else {

					map.put("ResponseCode", messageUtil
							.getBundle("resendotac.missing.sessiontoken"));

					map.put("error",
							messageUtil
									.getBundle("resendotac.missing.sessiontoken.responsedetails"));

					logger.info("403 : Session Token is missing.");

					jsonErrorMessage = json.toJson(map);

					Object obj = parser.parse(jsonErrorMessage);

					JSONObject jsonObject = (JSONObject) obj;

					jsonObject = Common.load(jsonObject);

					String jsonStringFinal = jsonObject.toString();

					out.print(jsonStringFinal);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			logger.error("Resend otac API Exception: ", e);

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("error", messageUtil.getBundle("wrong.message"));

			String jsonStringFinal = jsonObject.toString();

			out.print(jsonStringFinal);

			e.printStackTrace();

		}

		out.close();
	}
	
	

	

	// To generate server error msg
		public String getServerErrorMsg(String errorCode) {
			HashMap<String,String> errorMap = new HashMap<String,String>();
			errorMap.put("400", error400);
			errorMap.put("401", error401);
			errorMap.put("402", error402);
			errorMap.put("403", error403);
			errorMap.put("404", error404);
			errorMap.put("405", error405);
			errorMap.put("408", error408);
			errorMap.put("500", error500);
			errorMap.put("502", error502);
			errorMap.put("503", error503);
			errorMap.put("504", error504);

			if (errorMap.get(errorCode) != null) {
				return (String) errorMap.get(errorCode);
			} else {
				return errorOther;
			}

		}

	
	

}
