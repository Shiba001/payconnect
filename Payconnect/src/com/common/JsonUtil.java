package com.common;

import java.util.ResourceBundle;

import org.springframework.stereotype.Component;

@Component
public class JsonUtil {
	
	
private static ResourceBundle resourceBundle = null;
	

	static {

		resourceBundle = ResourceBundle.getBundle("resources.jsonkey");
	}
	
	
	
	

	public String getBundle(String key) {

		return resourceBundle.getString(key);
	}

}
