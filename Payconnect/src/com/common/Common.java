package com.common;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

//Certificates











import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.io.*;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@Component
@Configurable
public class Common {

	private static final Logger logger = Logger.getLogger(Common.class);

	public static final String APIKEY = "gh610rt23eqwpll";
	public static final String PRIVATE_KEY = "29041992AXIS@INT04062015";
	// public static final String PRIVATE_KEY = "KEY";
	public static final String RESOURCE = "resources/axis_logo.png";

	// private static final String HEADER_X_FORWARDED_FOR = "X-FORWARDED-FOR";

	public static String getUserIp(HttpServletRequest request) {

		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteHost();
		}

		String[] ip_split = null;
		String ip = null;
		if (ipAddress.contains(",")) {
			ip_split = ipAddress.split(",");
			if (ip_split.length > 1) {
				int ip_length = ip_split.length;

				ip = ip_split[ip_length - 1];
			} else {
				ip = ip_split[0];
			}

			return ip;
		} else {
			return ipAddress;
		}

		/*
		 * String remoteAddr = request.getRemoteAddr(); String x; if ((x =
		 * request.getHeader(HEADER_X_FORWARDED_FOR)) != null) { remoteAddr = x;
		 * int idx = remoteAddr.indexOf(','); if (idx > -1) { remoteAddr =
		 * remoteAddr.substring(0, idx); } } return remoteAddr;
		 */
	}

	public void showTextPdf(Document doc, String text, int left, int top)
			throws DocumentException {
		/*
		 * Phrase phrase = new Phrase(text); PdfContentByte canvas =
		 * writer.getDirectContentUnder();
		 * 
		 * ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, phrase, left,
		 * top, 0);
		 */
		// doc.add(phrase);

		Paragraph p = new Paragraph(text);
		p.setIndentationLeft(left);
		p.setSpacingAfter(top);
		doc.add(p);

	}

	public void showPdfImage(PdfWriter writer, HttpServletRequest request)
			throws DocumentException, IOException {

		// String fileName = "/axis_logo.png"; //use forward slash to recognize
		// your file
		// String path = getClass().getResource(fileName).toString();

		URL resource = request.getServletContext().getResource(
				"classpath:resources/axis_logo.png");
		String relativePath = resource.getPath();

		URL url = this.getClass().getClassLoader()
				.getResource("classpath:resources/axis_logo.png");

		String imagePath = request.getContextPath() + "/axis_logo.png";

		Image img = Image.getInstance(imagePath);
		/*
		 * img.setAbsolutePosition( 200,200);
		 * writer.getDirectContent().addImage(img, true);
		 */

		writer.getDirectContent().addImage(img, 200, 0, 0, 130, 183, 48);
	}

	public static String formatDecimalPlaces(Object r) {
		String s = r.toString();
		if (!s.contains("E")) {
			return s;
		} else {
			int i = s.indexOf("E");
			int f = Integer.parseInt(s.substring(i + 1));
			String d = s.substring(0, 1) + s.substring(2, f + 2) + "."
					+ s.substring(f + 2, i);
			Double e = Double.parseDouble(d);
			return d;
		}
		/*
		 * String s=r.toString(); Double d=Double.parseDouble(s); long
		 * a=d.longValue(); return a;
		 */
	}

	public Object formatDecimal(Object r) {

		if (r instanceof String)
			return r;

		String s = r.toString();
		Double d = Double.parseDouble(s);
		long a = d.longValue();
		return a;

	}

	/*public static ClientResponse apiAcess(String url, String inputString, String method) {
		
		ClientResponse client_response = null;
		try {
			if (url != null && inputString != null) {

				SSLContext ssl_ctx = SSLContext.getInstance("TLS");
				TrustManager[] trust_mgr = get_trust_mgr();
				// ssl_ctx.init(null, // key manager
				// trust_mgr, // trust manager
				// new SecureRandom()); // random number generator
				ClientConfig config = new DefaultClientConfig();
				// SSLContext ctx = SSLContext.getInstance("TLS");
				ssl_ctx.init(null, trust_mgr, null);
				config.getProperties().put(
						HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
						new HTTPSProperties(new HostnameVerifier() {

							public boolean verify(String arg0, SSLSession arg1) {
								// TODO Auto-generated method stub
								return true;
							}

						}

						, ssl_ctx));

				Client client = Client.create(config);

				// Client client = Client.create();

				ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.database");

				String user = resourceBundle.getString("sap.new.user");

				String password = resourceBundle.getString("sap.new.password");

				// -----added sap username and password from properties
				// file-----//
				client.addFilter(new HTTPBasicAuthFilter(user, password));

				WebResource webResource = client.resource(url);

				if (method.equals("post")) {
					client_response = webResource.type("application/json")
							.post(ClientResponse.class, inputString);
				} else {
					client_response = webResource.type("application/json").get(
							ClientResponse.class);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);

		}
		return client_response;
	}
*/
	public static ClientResponse payconnectApiAcess(String url,
			String inputString, String method) {
		// byPassUrl(url);
		ClientResponse client_response = null;
		try {
			if (url != null && inputString != null) {

				SSLContext ssl_ctx = SSLContext.getInstance("TLS");
				TrustManager[] trust_mgr = get_trust_mgr();
				// ssl_ctx.init(null, // key manager
				// trust_mgr, // trust manager
				// new SecureRandom()); // random number generator
				ClientConfig config = new DefaultClientConfig();
				// SSLContext ctx = SSLContext.getInstance("TLS");
				ssl_ctx.init(null, trust_mgr, null);
				config.getProperties().put(
						HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
						new HTTPSProperties(new HostnameVerifier() {

							public boolean verify(String arg0, SSLSession arg1) {
								// TODO Auto-generated method stub
								return true;
							}

						}

						, ssl_ctx));

				Client client = Client.create(config);

				// Client client = Client.create();

				ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.database");

				String user = resourceBundle.getString("sap.new.user");

				String password = resourceBundle.getString("sap.new.password");
				
				logger.info("sap server user name "+user);
				
				logger.info("sap server password "+password);

				// -----added sap username and password from properties
				// file-----//
				client.addFilter(new HTTPBasicAuthFilter(user, password));

				WebResource webResource = client.resource(url);

				if (method.equals("post")) {
					client_response = webResource.type("application/json")
							.post(ClientResponse.class, inputString);
				} else {
					client_response = webResource.type("application/json").get(
							ClientResponse.class);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			logger.error("error message payconnectApiAcess inside common "+e.getMessage());
			logger.error("error message payconnectApiAcess inside common "+client_response.getStatus());
			

		}
		return client_response;
	}

	// get request headers
	public HashMap<String, String> getHeadersInfo(HttpServletRequest request) {

		HashMap<String, String> map = new HashMap<String, String>();

		Enumeration<String> headerNames = request.getHeaderNames();

		while (headerNames.hasMoreElements()) {

			String key = (String) headerNames.nextElement();

			String value = request.getHeader(key);

			map.put(key, value);
		}

		return map;
	}

	public static String encriptString(String strInputString) {
		String encryptedString = null;
		try {
			String key = "Bar12345Bar12345Bar12345Bar12345";
			SecretKey secretKey = new SecretKeySpec(key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			byte[] plainTextByte = strInputString.getBytes();
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] encryptedByte = cipher.doFinal(plainTextByte);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		return encryptedString;
	}

	@SuppressWarnings("deprecation")
	public static String generateSessionToken() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddMMss");
		String strSessionToken = null;
		Calendar cal = Calendar.getInstance();
		String strCurrTime = sdf.format(cal.getTime());
		Random random = new Random();
		strSessionToken = random.nextInt(999999) + "" + strCurrTime;
		return strSessionToken;
	}

	public static String generateOtac(int min, int max) {

		Random random = new Random();

		int rand_number = random.nextInt(max);
		String rand_str = "";
		if (rand_number >= min && rand_number <= max) {
			// System.out.println("otac  in common: " + rand_number);
			rand_str = String.valueOf(rand_number);

		} else {
			rand_str = generateOtac(min, max);
		}

		return rand_str;
	}

	// AES Encryption
	/**
	 * Encodes a String in AES-128 with a given key
	 * 
	 * @param context
	 * @param password
	 * @param text
	 * @return String Base64 and AES encoded String
	 * @throws NoPassGivenException
	 * @throws NoTextGivenException
	 */
	public static String encode(String password, String text) throws Exception {
		if (password.length() == 0 || password == null) {
			throw new NoPassGivenException("Please give Password");
		}

		if (text == null) {
			throw new NoTextGivenException("Please give text");
		}

		SecretKeySpec skeySpec = getKey(password);
		byte[] clearText = text.toString().getBytes("UTF8");

		final byte[] iv = new byte[16];
		Arrays.fill(iv, (byte) 0x00);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

		// Cipher is not thread safe
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);

		String encrypedValue = new Base64().encodeAsString(cipher
				.doFinal(clearText));

		return encrypedValue;

		
	}

	/**
	 * Decodes a String using AES-128 and Base64
	 * 
	 * @param context
	 * @param password
	 * @param text
	 * @return desoded String
	 * @throws NoPassGivenException
	 * @throws NoTextGivenException
	 */
	public static String decode(String password, String text) throws Exception {

		if (password.length() == 0 || password == null) {
			throw new NoPassGivenException("Please give Password");
		}

		if (text == null) {
			throw new NoTextGivenException("Please give text");
		}

		/* try { */
		SecretKey key = getKey(password);

		final byte[] iv = new byte[16];
		Arrays.fill(iv, (byte) 0x00);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

		byte[] encrypedPwdBytes = new Base64().decodeBase64(text);

		// cipher is not thread safe
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec);
		byte[] decrypedValueBytes = (cipher.doFinal(encrypedPwdBytes));

		String decrypedValue = new String(decrypedValueBytes);

		return decrypedValue;


	}

	/**
	 * Generates a SecretKeySpec for given password
	 * 
	 * @param password
	 * @return SecretKeySpec
	 * @throws UnsupportedEncodingException
	 */
	public static SecretKeySpec getKey(String password)
			throws UnsupportedEncodingException {

		int keyLength = 128;
		byte[] keyBytes = new byte[keyLength / 8];
		// explicitly fill with zeros
		Arrays.fill(keyBytes, (byte) 0x0);

		// if password is shorter then key length, it will be zero-padded
		// to key length
		byte[] passwordBytes = password.getBytes("UTF-8");
		int length = passwordBytes.length < keyBytes.length ? passwordBytes.length
				: keyBytes.length;
		System.arraycopy(passwordBytes, 0, keyBytes, 0, length);
		SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
		return key;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static JSONObject load(JSONObject jsonObject) throws Exception {
		for (Iterator iterator = jsonObject.keySet().iterator(); iterator
				.hasNext();) {
			String key = (String) iterator.next();

			if (key.equals("ResponseCode") || key.equals("ResponseDeatils")
					|| key.equals("error") || key.equals("Responsedetails")) {
				continue;
			}

			Object thing = jsonObject.get(key);
			// String classNameOfThing = thing.getClass().getName();
			// Object thing1 = null;

			if (thing instanceof Number) {
				Number doub = (Number) thing;
				Double dou = doub.doubleValue();

				if (dou == dou.longValue()) {
					if (dou.longValue() >= Integer.MIN_VALUE
							&& dou.longValue() <= Integer.MAX_VALUE) {
						thing = ((Number) thing).intValue();
						String encoded = encode(PRIVATE_KEY, thing.toString());

						jsonObject.put(key, encoded);
					} else {

						String encoded = encode(PRIVATE_KEY,
								((Number) dou.longValue()).toString());
						jsonObject.put(key, encoded);
					}

				} else {
					if (dou >= Float.MIN_VALUE && dou <= Float.MAX_VALUE) {

						thing = formatDecimalPlaces(thing);
						String encoded = encode(PRIVATE_KEY, thing.toString());

						jsonObject.put(key, encoded);
					} else {
						// thing=((Number) thing).doubleValue();
						thing = formatDecimalPlaces(thing);
						String encoded = encode(PRIVATE_KEY, thing.toString());
						jsonObject.put(key, encoded);
					}
				}

			}

			else if (thing instanceof JSONObject) {
				JSONObject jtemp = load((JSONObject) thing);
				jsonObject.put(key, jtemp);
			} else if (thing instanceof JSONArray) {
				JSONArray jarr = new JSONArray();
				// String[] arr = new String[((JSONArray) thing).size()];
				for (int i = 0; i < ((JSONArray) thing).size(); i++) {
					if (((JSONArray) thing).get(i) instanceof JSONObject) {
						JSONObject jtemp = load(((JSONObject) ((JSONArray) thing)
								.get(i)));
						jarr.add(jtemp);

						jsonObject.put(key, jarr);
					} else if (((JSONArray) thing).get(i) instanceof Number) {
						Number doub = (Number) ((JSONArray) thing).get(i);
						Double dou = doub.doubleValue();
						String s = dou.toString();
						if (s.contains("E")) {
							BigDecimal bb = new BigDecimal(dou);
							String encoded = encode(PRIVATE_KEY, bb.toString());
							jsonObject.put(key, encoded);
						}

						if (dou == dou.intValue()) {
							if (dou >= Integer.MIN_VALUE
									|| dou <= Integer.MAX_VALUE) {
								thing = ((Number) ((JSONArray) thing).get(i))
										.intValue();
								String encoded = encode(PRIVATE_KEY,
										thing.toString());
								jsonObject.put(key, encoded);
							} else {
								BigDecimal bd = new BigDecimal(
										((Number) thing).longValue());
								String encoded = encode(PRIVATE_KEY,
										bd.toString());
								jsonObject.put(key, encoded);
							}

						} else {
							if (dou >= Float.MIN_VALUE
									|| dou <= Float.MAX_VALUE) {
								thing = ((Number) ((JSONArray) thing).get(i))
										.floatValue();

								String encoded = encode(PRIVATE_KEY,
										thing.toString());
								jsonObject.put(key, encoded);
							} else {
								thing = ((Number) ((JSONArray) thing).get(i))
										.doubleValue();
								String encoded = encode(PRIVATE_KEY,
										thing.toString());
								jsonObject.put(key, encoded);
							}
						}

					}
					jsonObject.put(key, jarr);
				}
			} else {
				String temp = (String) jsonObject.get(key);
				String encoded = encode(PRIVATE_KEY, temp.toString());
				jsonObject.put(key, encoded);
			}
		}
		return jsonObject;
	}
	
	
	

	// Bypass certificates
	
	
	
	private static TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}

	private static void byPassUrl(String axisUrl) {
		String https_url = axisUrl;
		URL url;
		try {

			// Create a context that doesn't check certificates.
			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, // key manager
					trust_mgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx
					.getSocketFactory());

			url = new URL(https_url);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			// Guard against "bad hostname" errors during handshake.
			con.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					// if (host.equals("localhost")) return true;
					// else return false;
					// if (host.equals(checkHostName)) return true;
					// else return false;
					return true;
				}
			});

		} catch (MalformedURLException e) {
			e.printStackTrace();
			logger.error(e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			logger.error(e);
		} catch (KeyManagementException e) {
			e.printStackTrace();
			logger.error(e);
		}
	}

	public String getIp(HttpServletRequest request) {

		InetAddress ip = null;
		try {
			ip = InetAddress.getLocalHost();

			// System.out.println("machine ip is :"+ip);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.error(e);

		}

		return ip.getHostAddress();

	}

	public String generateCaptchHTML(String captchaString) {

		String captchaContain = "<html> <body> <div style='background-color: #a45756; border: medium solid; color: white; font-family: Comic Sans MS; font-size: x-large; font-weight: 400; height: 35px; text-align: center; width: 160px;'> ApgKeSPIoM </div> </body> </html>";
		return captchaContain.replace("ApgKeSPIoM", captchaString);
	}

	public static String generateEncryptedPassword(String Password) {

		byte[] defaultBytes = Password.getBytes();

		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String word = Integer.toHexString(0xFF & messageDigest[i]);
				if (word.length() == 1)
					word = "0" + word;
				hexString.append(word);
			}
			Password = hexString + "";
		} catch (Exception e) {

			e.printStackTrace();
		}
		return Password;

	}

	public static Date getDate(String modelDate) throws ParseException {

		String pattern = "yyyy-MM-dd HH:mm:ss";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String formattedDate = simpleDateFormat.format(new Date());

		Date formattedCreateDate = simpleDateFormat.parse(formattedDate);

		return formattedCreateDate;

	}
	
	
	public static String formatDateFromAxis(String entityDate) throws ParseException{
		
		
		DateFormat userDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		DateFormat dateFormatNeeded = new SimpleDateFormat("dd/MM/yyyy");
		
		Date date = userDateFormat.parse(entityDate);
		
		String convertedDate = dateFormatNeeded.format(date);
		
		return convertedDate;
		
		
	}
	
	
	
public static String formatDateForLastLogin(String entityDate) throws ParseException{
		
		

	 String pattern = "dd/MM/yyyy hh:mm:ss";
		
	 String pattern1 = "dd-MM-yyyy hh:mm:ss";
    
    SimpleDateFormat format = new SimpleDateFormat(pattern);
    
    SimpleDateFormat format1 = new SimpleDateFormat(pattern1);
    
      Date date = format.parse(entityDate);
    
      return format1.format(date);
		
		
	}





public static String formatDateForDownTime(Date entityDate) throws ParseException{
	
	
	 String pattern = "dd-MM-yyyy HH:mm:ss ";
	 
	 SimpleDateFormat format = new SimpleDateFormat(pattern);
   
     return format.format(entityDate);
		
		
	}
	
	
	
	
	
    


	
	 public static boolean isValidDate(String modeldate) {
		 
		   		    
		    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		    
		    if (modeldate.trim().length() != dateFormat.toPattern().length())
		      return false;
		 
		    dateFormat.setLenient(false);
		    
		    try {
		      //parse the inDate parameter
		      dateFormat.parse(modeldate.trim());
		    }
		    catch (ParseException pe) {
		      return false;
		    }
		    return true;
		  }
	
	
	
	

	@SuppressWarnings("unchecked")
	public static String getUserAccountReport(String api_version,
			String api_key, String session_token, String userid, String mPin,
			String system) {

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("ApiVersion", api_version);
		// jsonObject.put("api_key", api_key);
		// jsonObject.put("session_token", session_token);
		jsonObject.put("UserID", userid);
		// jsonObject.put("MPin", mPin);
		jsonObject.put("CorpID", "");
		jsonObject.put("System", system);

		return jsonObject.toString();
	}

	@SuppressWarnings("unchecked")
	public static String getUserDashboradReport(String api_version,
			String userid, String system, String corpId) {

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("ApiVersion", api_version);
		// jsonObject.put("api_key", api_key);
		// jsonObject.put("session_token", session_token);
		jsonObject.put("UserID", userid);
		// jsonObject.put("MPin", mPin);
		jsonObject.put("System", system);
		jsonObject.put("CorpID", corpId);
		
		//System.out.println(" axis api passes parameter is:  " + jsonObject.toString());

		return jsonObject.toString();
		
		
	}

	@SuppressWarnings("unchecked")
	public static String getPowerAccessLogout(String userid,
			String session_token, String mpin, String device_id) {

		JSONObject jsonObject = new JSONObject();

		//jsonObject.put("api_key", api_key);
		jsonObject.put("userid", userid);
		jsonObject.put("session_token", session_token);
		jsonObject.put("Mpin", "");
		jsonObject.put("device_id", device_id);

		return jsonObject.toString();
	}

	@SuppressWarnings("unchecked")
	public static String productListApiInputString(String apiVersion,String corpId,
			String userId,String productList,String system)
			throws Exception {

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("ApiVersion", apiVersion.trim());
		jsonObject.put("CorpID", corpId.trim());
		jsonObject.put("UserID", userId);
		jsonObject.put("System", system.trim());
		jsonObject.put("Product_list", productList.trim());
		
		

		//System.out.println("input parameter is:  " + jsonObject.toString());

		return jsonObject.toString();
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public static String accountBalnceApiInputString(String apiVersion,String corpId,
			String userId, String accountNo,String system)
			throws Exception {

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("ApiVersion", apiVersion.trim());
		jsonObject.put("CorpID", corpId.trim());
		jsonObject.put("UserID", userId);
		jsonObject.put("System", system.trim());
		jsonObject.put("AccNo", accountNo.trim());
		
		

		//System.out.println("input parameter is:  " + jsonObject.toString());

		return jsonObject.toString();
	}
	
	
	
	@SuppressWarnings("unchecked")
	public static String getAccountListApiInputString(String apiVersion,String corpId,
			String userId,String type,String system)
			throws Exception {

		JSONObject jsonObject = new JSONObject();
	

		jsonObject.put("ApiVersion", apiVersion.trim());
		jsonObject.put("CorpID", corpId.trim());
		jsonObject.put("UserID", userId);
		jsonObject.put("System", system.trim());
		jsonObject.put("Type", type.trim());
		
		
		//System.out.println("input parameter is:  " + jsonObject.toString());

		return jsonObject.toString();
	}
	
	
	
	
	

	@SuppressWarnings("unchecked")
	public static String getTransactionwiseAuthorizationAdvanceSearchApiInputString(
			String apiVersion, String corpId, String userId, String system,
			String pageNo, String pageSize, String batchId, String accountId,
			String isOnHold, String transactionAmountFrom,
			String transactionAmountTo, String bankTransID,
			String beneficiaryAccount, String beneficiaryName,
			String corporateRef, String corporateAccount, String productName)
			throws Exception {

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("ApiVersion", apiVersion.trim());
		jsonObject.put("CorpID", corpId.trim());
		jsonObject.put("UserID", userId.trim());
		jsonObject.put("System", system.trim());
		jsonObject.put("Pagesize", pageSize.trim());
		jsonObject.put("Pageno", pageNo.trim());
        jsonObject.put("BatchID", batchId.trim());
		jsonObject.put("AccountID", accountId.trim());
		jsonObject.put("IsOnHold", isOnHold.trim());
		jsonObject.put("TransactionAmountFrom", transactionAmountFrom.trim());
		jsonObject.put("TransactionAmountTo", transactionAmountTo.trim());
		jsonObject.put("BankTransID", bankTransID.trim());
		jsonObject.put("BeneficiaryAccount", beneficiaryAccount.trim());
		jsonObject.put("BeneficiaryName", beneficiaryName.trim());
		jsonObject.put("CorporateRef", corporateRef.trim());
		jsonObject.put("CorporateAccount", corporateAccount.trim());
		jsonObject.put("ProductName", productName.trim());

		//System.out.println("input parameter is:  " + jsonObject.toString());

		return jsonObject.toString();
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public static String getStatementEnquiry(
			String apiVersion, String corpId, String userId, String system,
			String accountName, String accountNo, String formdate, String toDate,
			String statementFormat,String pageSize,String pageNo,String fileType)throws Exception {

		JSONObject jsonObject = new JSONObject();
		
		

		
		

		jsonObject.put("ApiVersion", apiVersion.trim());
		jsonObject.put("CorpID", corpId.trim());
		jsonObject.put("UserID", userId.trim());
		jsonObject.put("System", system.trim());
		jsonObject.put("AccountName", accountName.trim());
		jsonObject.put("AccountNo", accountNo.trim());
        jsonObject.put("FromDate", formdate.trim());
		jsonObject.put("ToDate", toDate.trim());
		jsonObject.put("StatementFormat", statementFormat.trim());
		jsonObject.put("Pagesize", pageSize.trim());
		jsonObject.put("Pageno", pageNo.trim());
		jsonObject.put("Filetype", fileType.trim());
		
		
		//System.out.println("input parameter in common is:  " + jsonObject.toString());

		return jsonObject.toString();
	}
	
	
	
	
	

	@SuppressWarnings("unchecked")
	public static String getransactiondetails(
			String apiVersion, String corpId, String userId, String system,
			String pageNo, String pageSize, String batchId, String accountId,
			String isOnHold)throws Exception {

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("ApiVersion", apiVersion.trim());
		jsonObject.put("CorpID", corpId.trim());
		jsonObject.put("UserID", userId.trim());
		jsonObject.put("System", system.trim());
		jsonObject.put("Pagesize", pageSize.trim());
		jsonObject.put("Pageno", pageNo.trim());
        jsonObject.put("BatchID", batchId.trim());
		jsonObject.put("AccountID", accountId.trim());
		jsonObject.put("IsOnHold", isOnHold.trim());
		
		//System.out.println("input parameter is:  " + jsonObject.toString());

		return jsonObject.toString();
	}
	
	
	@SuppressWarnings("unchecked")
	 public static String getAuthHeaderJSONData(String apiVersion,String corpId,String userid,String system,String pageNo,String pageSize,String accountId,String isOnHold) {
	  
	   JSONObject jsonObject = new JSONObject();

	   jsonObject.put("ApiVersion", apiVersion);
	   jsonObject.put("CorpID", corpId);
	   jsonObject.put("UserID", userid);
	   jsonObject.put("System", system);
	   jsonObject.put("Pageno", pageNo);
	   jsonObject.put("Pagesize", pageSize);
	   jsonObject.put("AccountID", accountId);
	   jsonObject.put("IsOnHold", isOnHold);
	   
	   return jsonObject.toString();
	 }
	
	
	
	
	
	@SuppressWarnings("unchecked")
	 public static String getActionOnTransactionOrBatchJSONData(String apiVersion,String corpId,String userid,String system,String actionFor,String id,String actionType,String remarks) {
	  
	   JSONObject jsonObject = new JSONObject();

	   jsonObject.put("ApiVersion", apiVersion.trim());
	   jsonObject.put("CorpID", corpId.trim());
	   jsonObject.put("UserID", userid.trim());
	   jsonObject.put("System", system.trim());
	   jsonObject.put("ActionFor", actionFor.trim());
	   jsonObject.put("ID", id.trim());
	   jsonObject.put("Actiontype", actionType.trim());
	   jsonObject.put("Remarks", remarks.trim());
	   
	  // System.out.println("input parameter is:  " + jsonObject.toString());
	   
	   return jsonObject.toString();
	 }
	
	
	/**
	 * This method is use to return Balance Compare JSONData
	 * @param apiVersion
	 * @param corpId
	 * @param userid
	 * @param system
	 * @param id
	 * @param isBatch
	 * @return
	 */
	@SuppressWarnings("unchecked")
	 public static String getBalanceCompareJSONData(String apiVersion,String corpId,String userid,String system,String id,String isBatch) {
	  
	   JSONObject jsonObject = new JSONObject();

	   jsonObject.put("ApiVersion", apiVersion.trim());
	   jsonObject.put("CorpID", corpId.trim());
	   jsonObject.put("UserID", userid.trim());
	   jsonObject.put("System", system.trim());
	   jsonObject.put("Id", id.trim());
	   jsonObject.put("IsBatch", isBatch.trim());
	   
	   return jsonObject.toString();
	 }
	
	/**
	 * This method is use to return Corp batchid list JSONData
	 * @param apiVersion
	 * @param corpId
	 * @param userid
	 * @param system
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	 public static String getCorpbatchidlistJSONData(String apiVersion,String corpId,String userid,String system,String fromDate,String toDate) {
	  
	   JSONObject jsonObject = new JSONObject();

	   jsonObject.put("ApiVersion", apiVersion.trim());
	   jsonObject.put("CorpID", corpId.trim());
	   jsonObject.put("UserID", userid.trim());
	   jsonObject.put("System", system.trim());
	   jsonObject.put("FromDate", fromDate.trim());
	   jsonObject.put("ToDate", toDate.trim());
	   
	   return jsonObject.toString();
	 }
	
	
	
	@SuppressWarnings("unchecked")
	 public static String getBankBatchIdlistJSONData(String apiVersion,String corpId,String userid,String system,String fromDate,String toDate) {
		
	
	  
	   JSONObject jsonObject = new JSONObject();

	   jsonObject.put("ApiVersion", apiVersion.trim());
	   jsonObject.put("CorpID", corpId.trim());
	   jsonObject.put("UserID", userid.trim());
	   jsonObject.put("System", system.trim());
	   jsonObject.put("FromDate", fromDate.trim());
	   jsonObject.put("ToDate", toDate.trim());
	   
	   return jsonObject.toString();
	 }
	
	
	
	
	
	/**
	 * load method for static encription
	 * @param jsonObject
	 * @return
	 * @throws Exception 
	 */

	@SuppressWarnings("unchecked")
	public static JSONObject updateLoad(JSONObject jsonObject) throws Exception {
		for (Iterator iterator = jsonObject.keySet().iterator(); iterator
				.hasNext();) {
			String key = (String) iterator.next();

			if (key.equals("ResponseCode") || key.equals("ResponseDeatils")
					|| key.equals("error") || key.equals("Responsedetails")) {
				continue;
			}
			Object thing = jsonObject.get(key);
			String classNameOfThing = thing.getClass().getName();
			Object thing1 = null;
			if (thing instanceof Number) {
				Number doub = (Number) thing;
				Double dou = doub.doubleValue();

				if (dou == dou.longValue()) {
					if (dou.longValue() >= Integer.MIN_VALUE
							&& dou.longValue() <= Integer.MAX_VALUE) {
						thing = ((Number) thing).intValue();
						String encoded = encode(PRIVATE_KEY,
								thing.toString());
						jsonObject.put(key, encoded);
					} else {

						String encoded = encode(PRIVATE_KEY,
								((Number) dou.longValue()).toString());
						jsonObject.put(key, encoded);
					}

				} else {
					if (dou >= Float.MIN_VALUE && dou <= Float.MAX_VALUE) {

						// thing=((Number) thing).floatValue();

						thing = formatDecimalPlaces(thing);
						String encoded = encode(PRIVATE_KEY,
								thing.toString());
						jsonObject.put(key, encoded);
					} else {
						// thing=((Number) thing).doubleValue();
						thing = formatDecimalPlaces(thing);
						String encoded = encode(PRIVATE_KEY,
								thing.toString());
						jsonObject.put(key, encoded);
					}
				}

			}

			else if (thing instanceof JSONObject) {
				JSONObject jtemp = load((JSONObject) thing);
				jsonObject.put(key, jtemp);
			} else if (thing instanceof JSONArray) {
				JSONArray jarr = new JSONArray();
				String[] arr = new String[((JSONArray) thing).size()];
				for (int i = 0; i < ((JSONArray) thing).size(); i++) {
					if (((JSONArray) thing).get(i) instanceof JSONObject) {
						JSONObject jtemp = load(((JSONObject) ((JSONArray) thing)
								.get(i)));
						jarr.add(jtemp);

						jsonObject.put(key, jarr);
					} else if (((JSONArray) thing).get(i) instanceof Number) {
						Number doub = (Number) ((JSONArray) thing).get(i);
						Double dou = doub.doubleValue();
						String s = dou.toString();
						if (s.contains("E")) {
							BigDecimal bb = new BigDecimal(dou);
							String encoded = encode(PRIVATE_KEY,
									bb.toString());
							jsonObject.put(key, encoded);
						}

						if (dou == dou.intValue()) {
							if (dou >= Integer.MIN_VALUE
									|| dou <= Integer.MAX_VALUE) {
								thing = ((Number) ((JSONArray) thing).get(i))
										.intValue();
								String encoded = encode(
										PRIVATE_KEY,thing.toString());
								jsonObject.put(key, encoded);
							} else {
								BigDecimal bd = new BigDecimal(
										((Number) thing).longValue());
								String encoded = encode(PRIVATE_KEY,bd.toString());
								jsonObject.put(key, encoded);
							}

						} else {
							if (dou >= Float.MIN_VALUE
									|| dou <= Float.MAX_VALUE) {
								thing = ((Number) ((JSONArray) thing).get(i))
										.floatValue();

								String encoded = encode(PRIVATE_KEY,thing.toString());
								jsonObject.put(key, encoded);
							} else {
								thing = ((Number) ((JSONArray) thing).get(i))
										.doubleValue();
								String encoded = encode(PRIVATE_KEY,thing.toString());
								jsonObject.put(key, encoded);
							}
						}

					}
					jsonObject.put(key, jarr);
				}
			} else if (thing instanceof ArrayList) {
				
				ArrayList arr = new ArrayList();
				for (int i = 0; i < ((ArrayList) thing).size(); i++) {
					if (((ArrayList) thing).get(i) instanceof Map) {
						HashMap jtemp = (((HashMap) ((ArrayList) thing).get(i)));
						HashMap mapnew = new HashMap();
						for (Iterator it = jtemp.keySet().iterator(); it
								.hasNext();) {
							String mapkey = (String) it.next();
							mapnew.put(
									mapkey,
									encode(PRIVATE_KEY, jtemp.get(mapkey).toString()));

						}

						arr.add(mapnew);

						jsonObject.put(key, arr);
					}
				}
			}

			else {
				String temp = (String) jsonObject.get(key);
				String encoded = encode(PRIVATE_KEY, temp.toString());
				jsonObject.put(key, encoded);
			}
		}
		return jsonObject;
	}

}
	



