package com.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

@Service
public interface InvalidCredentials {

	Map<String, Integer> INVALID_CREDENTIAL = new ConcurrentHashMap<>();

	Map<String, Integer> CAPTCHA_COUNTER = new ConcurrentHashMap<>();
	
	Map<String, String> CAPTCHA_STRING = new ConcurrentHashMap<>();

}
