package com.common;

public class NoPassGivenException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NoPassGivenException(String message) {
		super(message);
	}

}

