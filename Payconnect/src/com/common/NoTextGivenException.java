package com.common;

public class NoTextGivenException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoTextGivenException(String message) {
		super(message);
	}

}