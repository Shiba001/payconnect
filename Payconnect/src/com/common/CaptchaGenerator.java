/**
 * 
 */
package com.common;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Avishek Seal
 *
 */
/**
 * display: inline-flex;
    margin-left: -8px;
    margin-top: -8px;
    padding-left: 16px;
    text-align: center;
    width: 100%
 * @author java
 *
 */
public final class CaptchaGenerator {
    
    //private static final String HTML = new String("<html><head></head><body><div style='display: inline-flex; margin-left: -8px; margin-top: -8px; background-color: rgb(105, 105, 105); width: 100%; text-align: center;'>@word</div></body></html>");
    
    //private static final String HTML = new String("<html><head></head><body><div style='display: inline-flex; margin-left: -8px; margin-top: -8px; background-color:white; width: 100%; text-align: center;'>@word</div></body></html>");
	
	private static final String HTML = new String("<html><head></head><body><div style='display: inline-flex; margin-left: 5px; margin-top: -8px; background-color:white; width: 100%; text-align: center;'>@word</div></body></html>");
    
    private static final Map<Integer, String> LETTER_STYLE = new HashMap<Integer, String>(){
	
	private static final long serialVersionUID = -2555731397513043450L;

	{
	    //put(1, "transform: rotate(15deg); text-align: center; height: 38px; color: midnightblue; font-size: 2.2em;");
	    //put(2, "transform: rotate(10deg); font-size: x-large; font-weight: 400; height: 35px; color: lightsalmon; font-family: fantasy; text-align: left; margin: 0px 8px;");
	    //put(3, "transform: rotate(-6deg); font-weight: 400; text-align: center; color: tan; height: auto; font-family: Georgia; font-size: 3em;");
	    //put(4, "transform: rotate(-16deg); color: red; font-weight: 400; height: 35px; text-align: center; font-size: 3em;");
	    //put(5, "text-align: center; height: 35px; font-family: Tahoma; font-size: 1.6em; color: rgb(161, 119, 255);");
	    //put(6, "transform: rotate(11deg); font-weight: 400; height: 35px; text-align: center; font-size: 2em; font-family: caption; color: tomato; text-shadow: 1px 1px turquoise;");
	   // put(7, "transform: rotate(6deg); height: 35px; text-align: center; font-family: Tahoma; font-size: 3em; font-weight: bold; color: rgb(185, 156, 166); text-shadow: 2px 2px windowframe;");
		
		
		
		
		// put(1, "transform: rotate(15deg); text-align: center; height: 38px; color: midnightblue; font-size: 2.2em;");
		   // put(2, "transform: rotate(10deg); font-size: x-large; font-weight: 400; height: 35px; color: lightsalmon; font-family: fantasy; text-align: left; margin: 0px 8px;");
		   // put(3, "transform: rotate(-6deg); font-weight: 400; text-align: center; color: tan; height: auto; font-family: Georgia; font-size: 3em;");
		   // put(4, "transform: rotate(-16deg); color: red; font-weight: 400; height: 35px; text-align: center; font-size: 3em;");
		   // put(5, "text-align: center; height: 35px; font-family: Tahoma; font-size: 1.6em; color: rgb(161, 119, 255);");
		   // put(6, "transform: rotate(11deg); font-weight: 400; height: 35px; text-align: center; font-size: 2em; font-family: caption; color: tomato; text-shadow: 1px 1px turquoise;");
		   // put(7, "transform: rotate(6deg); height: 35px; text-align: center; font-family: Tahoma; font-size: 3em; font-weight: bold; color: rgb(185, 156, 166); text-shadow: 2px 2px windowframe;");
		    
		    
		    
		    
		    put(1, "transform: rotate(15deg); text-align: center; float: left; height: 38px; color: midnightblue; font-size: 16vw;");
		    put(2, "transform: rotate(10deg); font-size: 16vw; float: left; font-weight: 400; height: 35px; color: lightsalmon; font-family: fantasy; text-align: left; margin: 0px 8px;");
		    put(3, "transform: rotate(-6deg); font-weight: 400; text-align: center; float: left; color: tan; height: auto; font-family: Georgia; font-size: 17vw;");
		    put(4, "transform: rotate(-16deg); color: red; font-weight: 400; height: 35px; text-align: center; float: left; font-size: 17vw;");
		    put(5, "text-align: center; float: left; height: 35px; font-family: Tahoma; font-size: 17vw; color: rgb(161, 119, 255);");
		    put(6, "transform: rotate(11deg); font-weight: 400; height: 35px; text-align: center; float: left; font-size: 17vw; font-family: caption; color: tomato; text-shadow: 1px 1px turquoise;");
		    put(7, "transform: rotate(6deg); height: 35px; text-align: center; float: left; font-family: Tahoma; font-size: 16vw; font-weight: bold; color: rgb(185, 156, 166); text-shadow: 2px 2px windowframe;");
		    
	}
    };
    
    /**
     * this method is used to generate captcha html for a given captcha string
     * @since 11-Jan-2016
     * @author Avishek Seal
     * @param captchaString
     * @return
     */
    public static final String getCaptchaHTML(String captchaString){
	final StringBuilder builder = new StringBuilder("");
	
	for (int i = 0; i < captchaString.length() ; i++) {
	    String divString = newDiv(String.valueOf(captchaString.charAt(i)), LETTER_STYLE.get(getKey()));
	    builder.append(divString);
	}
	
	String html = HTML.replace("@word", builder.toString());
	
	return html;
    }
    
    /**
     * this method is used to create new div of letter for a given letter and style
     * @since 11-Jan-2016
     * @author Avishek Seal
     * @param letter
     * @param style
     * @return
     */
    public static final String newDiv(String letter, String style){
	String span = new String("<div style= '@style'> @letter </div>");
	return span.replace("@style", style).replace("@letter", letter);
    }
    
    /**
     * this method is used to generate random key through which a random style can be fetched
     * @since 11-Jan-2016
     * @author Avishek Seal
     * @return
     */
    private final static int getKey(){
	int key = new BigDecimal(Math.random() * Math.random() * 500).intValue() % 8;
	
	if(key == 0) {
	    return getKey();
	}
	
	return key;
    }
}
