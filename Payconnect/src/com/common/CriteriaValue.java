package com.common;

import java.io.Serializable;

import com.enumeration.Operator;

/**
 * 
 * @author Avishek Seal
 *
 */

public class CriteriaValue implements Serializable{
	
	private static final long serialVersionUID = 3099412508097137572L;
	
	private Object valuObject;
	
	private Operator operator;

	public Object getValuObject() {
		return valuObject;
	}

	public void setValuObject(Object valuObject) {
		this.valuObject = valuObject;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}
	
}
