package com.enumeration;

public enum Status {

	INACTIVE(0),ACTIVE(1),DELETE(2);
	
	private int id;
	
	Status(int id){
		this.id = id;
	}
	
	public int getStatus(){
		return id; 
	}
}
