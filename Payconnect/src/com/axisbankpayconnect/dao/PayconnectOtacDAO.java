package com.axisbankpayconnect.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.poweraccess.dao.GenericDAO;
import com.poweraccess.model.OtacModel;

@Repository
public class PayconnectOtacDAO extends GenericDAO<OtacModel, Integer> {
	
	Logger logger=Logger.getLogger(PayconnectOtacDAO.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 5020146259373244828L;

	public PayconnectOtacDAO() {
		
		super(OtacModel.class);
		
	}
	
	
	
	/**
	 * This method is use to get session token details
	 * @param userId
	 * @return
	 */
	public OtacModel getOtacByUserId(String userId){
		
		System.out.println("inside getOtacByUserId method >>>>>>>>>>> ");
		
		if(logger.isInfoEnabled()){
			logger.info("getOtacByUserId start");
		}
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		
		criteria.add(Restrictions.eq("userID", userId));
		
		if(logger.isInfoEnabled()){
			logger.info("getPayProSessionToken End");
		}
		
		return (OtacModel) criteria.uniqueResult();
	}
	
	
	
	
	

	/**
	 * This method is use to get otac  details
	 * @param userId
	 * @param sytemvalue
	 */
	public OtacModel getOtacByUserIdAndSystemValue(String userId,String systemValue){
		
		System.out.println("inside getOtacByUserIdSystemValue method >>>>>>>>>>> ");
		
		if(logger.isInfoEnabled()){
			logger.info("getOtacByUserIdSystemValue start");
		}
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		
		criteria.add(Restrictions.eq("userID", userId));
		
		criteria.add(Restrictions.eq("system", systemValue));
		
		
		if(logger.isInfoEnabled()){
			logger.info("getOtacByUserIdSystemValue End");
		}
		
		return (OtacModel) criteria.uniqueResult();
	}
	
	
	
	
	
	
	

}
