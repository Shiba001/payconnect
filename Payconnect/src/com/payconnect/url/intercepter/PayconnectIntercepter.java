package com.payconnect.url.intercepter;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.WebContentInterceptor;

import com.common.Common;
import com.common.MessageUtil;

/**
 * 
 * @author Avishek Seal
 *
 */
@Component
public class PayconnectIntercepter extends WebContentInterceptor implements IPTracker{

	@Autowired
	public MessageUtil messageUtil;
	
	/**
	 * this method is used to intercept every request for IP adress checking
	 * @return boolean
	 * @author Avishek Seal
	 * @since 11-03-2016
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException {
		try {
			final String requestURL = request.getRequestURI();
			//System.out.println("request url in payconnect"+requestURL);
			
			if(!StringUtils.contains(requestURL, "login")) {
				
				HashMap<String, String> headerMap = (new Common()).getHeadersInfo(request);
				
				if(headerMap.get("session_token") != null) {
					
					String ipAddr = SESSION_IP_POOL.get(Common.decode(Common.PRIVATE_KEY,(String) headerMap.get("session_token")));
					
					if(StringUtils.isNotBlank(ipAddr) && StringUtils.equals(ipAddr, request.getRemoteAddr())){
						
						return true;
						
					} else {
						
						JSONObject jsonObject = new JSONObject();
					    
					    jsonObject.put("ResponseCode",  messageUtil.getBundle("error.invalid.sessiontoken.code"));
					    jsonObject.put("error",   messageUtil.getBundle("invalid.sessiontoken.message"));
					    //jsonObject.put("error",   "wrong device");
					    
					  
					    
					    
					    response.getWriter().write(jsonObject.toJSONString());
					    
					    return false;
					}
				}
			}
		} catch (Exception e) {
			return true;
		}
		
		return true;
	}
}
