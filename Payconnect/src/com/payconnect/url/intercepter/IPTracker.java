package com.payconnect.url.intercepter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface IPTracker {

	Map<String, String> SESSION_IP_POOL = new ConcurrentHashMap<String, String>();
}
