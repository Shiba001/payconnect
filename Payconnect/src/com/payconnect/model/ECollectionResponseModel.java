/**
 * 
 */
package com.payconnect.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;

import com.common.Common;

/**
 * @author Avishek Seal
 *
 */
public class ECollectionResponseModel extends AbstractResponseModel<ECollectionResponseModel> {

    private static final long serialVersionUID = -2741194857374328740L;
    
    @MapToField(name="ecoll.totalCount", nullAllowed=false)
    private Integer totalCount;
    
    @MapToField(name="ecoll.responseCode", nullAllowed=false, encrypt = false)
    private String responseCode;
    
    @MapToField(name="ecoll.responseDetails", nullAllowed=false, encrypt = false)
    private String responseDetails;
    
    @MapToField(name="ecoll.pdfDownloadLink", nullAllowed=false)
    private String pdfDownloadLink;
    
    @MapToField(name="ecoll.excelDownloadLink", nullAllowed=false)
    private String excelDownloadLink;
    
    @MapToField(name="ecoll.error", nullAllowed=false, encrypt = false)
    private String error;
    
    @MapToField(name="ecoll.collectionInformationResponseModels", nullAllowed=false, associateList = true, encrypt = false)
    private List<ECollectionInformationResponseModel> collectionInformationResponseModels;
    
    public ECollectionResponseModel() {
	super(ECollectionResponseModel.class);
    }

    /**
     * Avishek Seal
     * @return the totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }


    /**
     * Avishek Seal
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }


    /**
     * Avishek Seal
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }


    /**
     * Avishek Seal
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }


    /**
     * Avishek Seal
     * @return the responseDetails
     */
    public String getResponseDetails() {
        return responseDetails;
    }


    /**
     * Avishek Seal
     * @param responseDetails the responseDetails to set
     */
    public void setResponseDetails(String responseDetails) {
        this.responseDetails = responseDetails;
    }


    /**
     * Avishek Seal
     * @return the pdfDownloadLink
     */
    public String getPdfDownloadLink() {
        return pdfDownloadLink;
    }


    /**
     * Avishek Seal
     * @param pdfDownloadLink the pdfDownloadLink to set
     */
    public void setPdfDownloadLink(String pdfDownloadLink) {
        this.pdfDownloadLink = pdfDownloadLink;
    }


    /**
     * Avishek Seal
     * @return the excelDownloadLink
     */
    public String getExcelDownloadLink() {
        return excelDownloadLink;
    }


    /**
     * Avishek Seal
     * @param excelDownloadLink the excelDownloadLink to set
     */
    public void setExcelDownloadLink(String excelDownloadLink) {
        this.excelDownloadLink = excelDownloadLink;
    }


    /**
     * Avishek Seal
     * @return the error
     */
    public String getError() {
        return error;
    }


    /**
     * Avishek Seal
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }
    
    /**
     * Avishek Seal
     * @return the collectionInformationResponseModels
     */
    public List<ECollectionInformationResponseModel> getCollectionInformationResponseModels() {
        return collectionInformationResponseModels;
    }

    /**
     * Avishek Seal
     * @param collectionInformationResponseModels the collectionInformationResponseModels to set
     */
    public void setCollectionInformationResponseModels(List<ECollectionInformationResponseModel> collectionInformationResponseModels) {
        this.collectionInformationResponseModels = collectionInformationResponseModels;
    }

    public final boolean isSuccess() {
	return StringUtils.isNotBlank(responseCode) && StringUtils.equals(responseCode, "200");
    }
    /**
     * @since 17-Feb-2016
     * @author Avishek Seal
     * @return
     * @throws Exception
     */
   
    
    
    @Override
	public String toJsonString() throws Exception {
		Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.associateList()) {
						JSONArray array = new JSONArray(field.get(this)
								.getClass().cast(field.get(this)).toString());
						jsonObject.put(bundle.getString(field2.name()), array);
					} else {
						if (field2.encrypt()) {
							jsonObject.put(bundle.getString(field2.name()),
									Common.encode(Common.PRIVATE_KEY, field
											.get(this).toString()));
						} else {

							jsonObject.put(bundle.getString(field2.name()),
									field.get(this).toString());
						}
					}

				} else {
					if (field2.nullAllowed()) {
						if(field2.associateList()) {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, "[]"));
						} else {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
						}
					}
				}
			}
		}

		return jsonObject.toString();
	}
    
    
}
