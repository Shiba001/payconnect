package com.payconnect.model;

public class StatementInfo {

	public int slno;

	public String transctiondate;

	public String chequeNo;

	public String particulars;

	public String debit;

	public String credit;

	public String balanceammount;

	public String sol;

	public int getSlno() {
		return slno;
	}

	public void setSlno(int slno) {
		this.slno = slno;
	}

	public String getTransctiondate() {
		return transctiondate;
	}

	public void setTransctiondate(String transctiondate) {
		this.transctiondate = transctiondate;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getDebit() {
		return debit;
	}

	public void setDebit(String debit) {
		this.debit = debit;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getBalanceammount() {
		return balanceammount;
	}

	public void setBalanceammount(String balanceammount) {
		this.balanceammount = balanceammount;
	}

	public String getSol() {
		return sol;
	}

	public void setSol(String sol) {
		this.sol = sol;
	}

	@Override
	public String toString() {
		return "StatementInfo [slno=" + slno + ", transctiondate="
				+ transctiondate + ", chequeNo=" + chequeNo + ", particulars="
				+ particulars + ", debit=" + debit + ", credit=" + credit
				+ ", balanceammount=" + balanceammount + ", sol=" + sol + "]";
	}

}
