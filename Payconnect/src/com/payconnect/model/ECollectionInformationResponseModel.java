/**
 * 
 */
package com.payconnect.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.json.JSONException;
import org.json.JSONObject;

import com.common.Common;

/**
 * @author Avishek Seal
 *
 */
public class ECollectionInformationResponseModel implements Serializable{
    
  



	private static final long serialVersionUID = -3552768209083950219L;
	
	


    @MapToField(name="ecoll.serialNumber", nullAllowed = false)
    private String serialNumber;
    
    @MapToField(name="ecoll.transactionDate", nullAllowed = false)
    private String transactionDate;
    
    @MapToField(name="ecoll.transactionAmount", nullAllowed = false)
    private String transactionAmount;
    
    @MapToField(name="ecoll.senderName", nullAllowed = false)
    private String senderName;
    
    @MapToField(name="ecoll.utrNumber", nullAllowed = false)
    private String utrNumber;
    
    @MapToField(name="ecoll.corporateActualAccountNumber", nullAllowed = false)
    private String corporateActualAccountNumber;
    
    @MapToField(name="ecoll.senderInformation", nullAllowed = false)
    private String senderInformation;
    
    @MapToField(name="ecoll.paymentMode", nullAllowed = false)
    private String paymentMode;

    /**
     * Avishek Seal
     * @return the serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Avishek Seal
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Avishek Seal
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * Avishek Seal
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * Avishek Seal
     * @return the transactionAmount
     */
    public String getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * Avishek Seal
     * @param transactionAmount the transactionAmount to set
     */
    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    /**
     * Avishek Seal
     * @return the senderName
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * Avishek Seal
     * @param senderName the senderName to set
     */
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    /**
     * Avishek Seal
     * @return the utrNumber
     */
    public String getUtrNumber() {
        return utrNumber;
    }

    /**
     * Avishek Seal
     * @param utrNumber the utrNumber to set
     */
    public void setUtrNumber(String utrNumber) {
        this.utrNumber = utrNumber;
    }

    /**
     * Avishek Seal
     * @return the corporateActualAccountNumber
     */
    public String getCorporateActualAccountNumber() {
        return corporateActualAccountNumber;
    }

    /**
     * Avishek Seal
     * @param corporateActualAccountNumber the corporateActualAccountNumber to set
     */
    public void setCorporateActualAccountNumber(String corporateActualAccountNumber) {
        this.corporateActualAccountNumber = corporateActualAccountNumber;
    }

    /**
     * Avishek Seal
     * @return the senderInformation
     */
    public String getSenderInformation() {
        return senderInformation;
    }

    /**
     * Avishek Seal
     * @param senderInformation the senderInformation to set
     */
    public void setSenderInformation(String senderInformation) {
        this.senderInformation = senderInformation;
    }

    /**
     * Avishek Seal
     * @return the paymentMode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Avishek Seal
     * @param paymentMode the paymentMode to set
     */
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
    
    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @return
     * @throws Exception 
     * @throws JSONException 
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */

    
    
    
   
    private final String toJsonString() throws IllegalArgumentException, IllegalAccessException, JSONException, Exception {
	   	Field[] filelds = this.getClass().getDeclaredFields();
	   	
	   	ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

	   	JSONObject jsonObject = new JSONObject();
	   	
	   	for (Field field : filelds) {
	   		Annotation annotation = field.getAnnotation(MapToField.class);
	   		MapToField field2 = MapToField.class.cast(annotation);

	   		if (field2 != null) {
	   			if (field.get(this) != null) {
	   				if (field2.encrypt()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
	   				} else {
	   					jsonObject.put(bundle.getString(field2.name()), field.get(this));
	   				}

	   			} else {
	   				if (field2.nullAllowed()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
	   				}
	   			}
	   		}
	   	}
	           return jsonObject.toString();
   }
    
    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @return
     */
    @Override
    public String toString() {
        try {
	    return toJsonString();
	} catch (Exception e) {
	    return "{}";
	}
    }
}
