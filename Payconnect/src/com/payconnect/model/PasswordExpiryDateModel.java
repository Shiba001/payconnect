package com.payconnect.model;

import java.io.Serializable;
import java.util.Date;

public class PasswordExpiryDateModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5962940528472331032L;
	private String userId;
	private String system;
	private String pwdExpiryDate;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getPwdExpiryDate() {
		return pwdExpiryDate;
	}
	public void setPwdExpiryDate(String pwdExpiryDate) {
		this.pwdExpiryDate = pwdExpiryDate;
	}
	@Override
	public String toString() {
		return "PasswordExpiryDateModel [userId=" + userId + ", system="
				+ system + ", pwdExpiryDate=" + pwdExpiryDate + "]";
	}
	

	
	
	

}
