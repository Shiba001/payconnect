/**
 * 
 */
package com.payconnect.model;

import java.util.Date;

/**
 * @author Avishek Seal
 *
 */
public class ECollectionReportRequestModel extends AbstractRequestModel {

	private static final long serialVersionUID = 3814791650236919986L;

	private String version;

	private String corporateCode;

	private String logingID;

	private String productName;

	private Date startDate;

	private Date endDate;

	private Integer recordPerPage;

	private Integer pageNumber;

	private String corporateAccountNumber;

	private String ammountFrom;

	private String ammountTo;

	private String transactionalUTR;

	/**
	 * Avishek Seal
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the corporateCode
	 */
	public String getCorporateCode() {
		return corporateCode;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param corporateCode
	 *            the corporateCode to set
	 */
	public void setCorporateCode(String corporateCode) {
		this.corporateCode = corporateCode;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the logingID
	 */
	public String getLogingID() {
		return logingID;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param logingID
	 *            the logingID to set
	 */
	public void setLogingID(String logingID) {
		this.logingID = logingID;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	
	  public void setStartDate(String startDate) {
	    	if(startDate == null) {
	    		this.startDate = null;
	    	}
	    }
	
	

	/**
	 * Avishek Seal
	 * 
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param endDate
	 *  the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	  public void setEndDate(String endDate) {
	    	if(endDate == null) {
	    		this.endDate = null;
	    	}
	    }

	/**
	 * Avishek Seal
	 * 
	 * @return the recordPerPage
	 */
	public Integer getRecordPerPage() {
		return recordPerPage;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param recordPerPage
	 *            the recordPerPage to set
	 */
	public void setRecordPerPage(Integer recordPerPage) {
		this.recordPerPage = recordPerPage;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the pageNumber
	 */
	public Integer getPageNumber() {
		return pageNumber;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the corporateAccountNumber
	 */
	public String getCorporateAccountNumber() {
		return corporateAccountNumber;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param corporateAccountNumber
	 *            the corporateAccountNumber to set
	 */
	public void setCorporateAccountNumber(String corporateAccountNumber) {
		this.corporateAccountNumber = corporateAccountNumber;
	}

	public String getAmmountFrom() {
		return ammountFrom;
	}

	public void setAmmountFrom(String ammountFrom) {
		this.ammountFrom = ammountFrom;
	}

	public String getAmmountTo() {
		return ammountTo;
	}

	public void setAmmountTo(String ammountTo) {
		this.ammountTo = ammountTo;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the transactionalUTR
	 */
	public String getTransactionalUTR() {
		return transactionalUTR;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param transactionalUTR
	 *            the transactionalUTR to set
	 */
	public void setTransactionalUTR(String transactionalUTR) {
		this.transactionalUTR = transactionalUTR;
	}
}
