/**
 * 
 */
package com.payconnect.model;

import org.apache.commons.lang.StringUtils;

import com.common.Common;

/**
 * @author Avishek Seal
 *
 */
public class MPINRequestModel extends AbstractRequestModel {

    private static final long serialVersionUID = -7742635587420979185L;
    
    private String userID;
    
    private String mPin;
    
    private String deviiceID;
    
    private String deviceName;
    
    private String latitude;
    
    private String longitude;
    
    private String deviceToken;
    
    private String deviceType;
    
    private String flag;
    
    /**
     * Avishek Seal
     * @return the userID
     */
   
    public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

    /**
     * Avishek Seal
     * @return the mPin
     */
  
	
	

    /**
     * Avishek Seal
     * @return the deviiceID
     */
    public String getDeviiceID() {
        return deviiceID;
    }

    public String getmPin() {
		return mPin;
	}

	public void setmPin(String mPin) {
		this.mPin = mPin;
	}

	/**
     * Avishek Seal
     * @param deviiceID the deviiceID to set
     * @throws Exception 
     */
    public void setDeviiceID(String deviiceID) throws Exception {
	if (deviiceID == null) {
		this.deviiceID = "";
	} else {
		this.deviiceID = Common.decode(Common.PRIVATE_KEY, deviiceID);
	}
    }

    /**
     * Avishek Seal
     * @return the deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Avishek Seal
     * @param deviceName the deviceName to set
     * @throws Exception 
     */
    public void setDeviceName(String deviceName) throws Exception {
	if (deviceName == null) {
		this.deviceName = "";
	} else {
		this.deviceName = Common.decode(Common.PRIVATE_KEY, deviceName);
	}
    }

    /**
     * Avishek Seal
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Avishek Seal
     * @param latitude the latitude to set
     * @throws Exception 
     */
    public void setLatitude(String latitude) throws Exception {
	if (latitude == null) {
		this.latitude = "";
	} else {
		this.latitude = Common.decode(Common.PRIVATE_KEY, latitude);
	}
    }

    /**
     * Avishek Seal
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Avishek Seal
     * @param longitude the longitude to set
     * @throws Exception 
     */
    public void setLongitude(String longitude) throws Exception {
	if (longitude == null) {
		this.longitude = "";
	} else {
		this.longitude = Common.decode(Common.PRIVATE_KEY, longitude);
	}
    }

    /**
     * Avishek Seal
     * @return the deviceToken
     */
    public String getDeviceToken() {
        return deviceToken;
    }

    /**
     * Avishek Seal
     * @param deviceToken the deviceToken to set
     * @throws Exception 
     */
    public void setDeviceToken(String deviceToken) throws Exception {
        if (deviceToken == null) {
		this.deviceToken = "";
	} else {
		this.deviceToken = Common.decode(Common.PRIVATE_KEY, deviceToken);
	}
    }

    /**
     * Avishek Seal
     * @return the deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Avishek Seal
     * @param deviceType the deviceType to set
     * @throws Exception 
     */
    public void setDeviceType(String deviceType) throws Exception {
	if (deviceType == null) {
		this.deviceType = "";
	} else {
		this.deviceType = Common.decode(Common.PRIVATE_KEY, deviceType);
	}
    }

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	
    
    
	
	

	/*public boolean isFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		if (flag == null) {
			this.flag = false;
		} else {
			try {
				if(StringUtils.isNotBlank(flag) && flag.equals("1")){
					this.flag = Boolean.parseBoolean(Common.decode(Common.PRIVATE_KEY, flag));
				}
				
			} catch (Exception e) {
				this.flag = false;
			}
		}
	}
    */
    
    
}
