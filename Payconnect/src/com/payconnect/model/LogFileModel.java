/**
 * 
 */
package com.payconnect.model;

import java.io.Serializable;

/**
 * @author Avishek Seal
 *
 */
public class LogFileModel implements Serializable{

    private static final long serialVersionUID = -6635565395351936414L;
    
    private String fileName;
    
    private String dateModified;

    /**
     * Avishek Seal
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Avishek Seal
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Avishek Seal
     * @return the dateModified
     */
    public String getDateModified() {
        return dateModified;
    }

    /**
     * Avishek Seal
     * @param dateModified the dateModified to set
     */
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }
}
