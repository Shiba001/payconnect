/**
 * 
 */
package com.payconnect.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.json.JSONException;
import org.json.JSONObject;

import com.common.Common;

/**
 * @author Avishek Seal
 *
 */
public class TransactionInformationModel implements Serializable {

    private static final long serialVersionUID = -2645694864864877748L;

    @MapToField(name = "transaction.serial.number", nullAllowed = false)
    private String serialNumber;
    
    @MapToField(name = "transaction.id", nullAllowed = false)
    private String transactionId;
    
    @MapToField(name = "transaction.benificiary.name", nullAllowed = false)
    private String beneficiaryName;
    
    @MapToField(name = "transaction.payment.method", nullAllowed = false)
    private String paymentMethod;
    
    @MapToField(name = "transaction.reference.number", nullAllowed = false)
    private String customerReferenceNumber;

    @MapToField(name = "transaction.corporate.account", nullAllowed = false)
    private String corporateAccount;
    
    @MapToField(name = "transaction.payable.amount", nullAllowed = false)
    private String payableAmount;
   
    @MapToField(name = "transaction.description", nullAllowed = false)
    private String transactionDescription;
    
    @MapToField(name = "transaction.request.status", nullAllowed = false)
    private String systemRequestStatus;
    
    @MapToField(name = "transaction.utr", nullAllowed = false)
    private String transactionUTROrCheque;

    /**
     * Avishek Seal
     * @return the serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Avishek Seal
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Avishek Seal
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Avishek Seal
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Avishek Seal
     * @return the beneficiaryName
     */
    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    /**
     * Avishek Seal
     * @param beneficiaryName the beneficiaryName to set
     */
    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    /**
     * Avishek Seal
     * @return the paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Avishek Seal
     * @param paymentMethod the paymentMethod to set
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * Avishek Seal
     * @return the customerReferenceNumber
     */
    public String getCustomerReferenceNumber() {
        return customerReferenceNumber;
    }

    /**
     * Avishek Seal
     * @param customerReferenceNumber the customerReferenceNumber to set
     */
    public void setCustomerReferenceNumber(String customerReferenceNumber) {
        this.customerReferenceNumber = customerReferenceNumber;
    }

    /**
     * Avishek Seal
     * @return the corporateAccount
     */
    public String getCorporateAccount() {
        return corporateAccount;
    }

    /**
     * Avishek Seal
     * @param corporateAccount the corporateAccount to set
     */
    public void setCorporateAccount(String corporateAccount) {
        this.corporateAccount = corporateAccount;
    }

    /**
     * Avishek Seal
     * @return the payableAmount
     */
    public String getPayableAmount() {
        return payableAmount;
    }

    /**
     * Avishek Seal
     * @param payableAmount the payableAmount to set
     */
    public void setPayableAmount(String payableAmount) {
        this.payableAmount = payableAmount;
    }

    /**
     * Avishek Seal
     * @return the transactionDescription
     */
    public String getTransactionDescription() {
        return transactionDescription;
    }

    /**
     * Avishek Seal
     * @param transactionDescription the transactionDescription to set
     */
    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    /**
     * Avishek Seal
     * @return the systemRequestStatus
     */
    public String getSystemRequestStatus() {
        return systemRequestStatus;
    }

    /**
     * Avishek Seal
     * @param systemRequestStatus the systemRequestStatus to set
     */
    public void setSystemRequestStatus(String systemRequestStatus) {
        this.systemRequestStatus = systemRequestStatus;
    }
    
    
    /**
	 * @return the transactionUTROrCheque
	 */
	public String getTransactionUTROrCheque() {
		return transactionUTROrCheque;
	}

	/**
	 * @param transactionUTROrCheque the transactionUTROrCheque to set
	 */
	public void setTransactionUTROrCheque(String transactionUTROrCheque) {
		this.transactionUTROrCheque = transactionUTROrCheque;
	}

	/**
     * 
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws JSONException
     * @throws Exception
     */
    private final String toJsonString() throws Exception {
   	Field[] filelds = this.getClass().getDeclaredFields();
   	
   	ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

   	JSONObject jsonObject = new JSONObject();
   	
   	for (Field field : filelds) {
   		Annotation annotation = field.getAnnotation(MapToField.class);
   		MapToField field2 = MapToField.class.cast(annotation);

   		if (field2 != null) {
   			if (field.get(this) != null) {
   				if (field2.encrypt()) {
   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
   				} else {
   					jsonObject.put(bundle.getString(field2.name()), field.get(this));
   				}

   			} else {
   				if (field2.nullAllowed()) {
   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
   				}
   			}
   		}
   	}
 
   	return jsonObject.toString();
 }
    
    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @return
     */
    @Override
    public String toString() {
        try {
	    return toJsonString();
	} catch (Exception e) {
	    return "{}";
	}
    }
}