/**
 * 
 */
package com.payconnect.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.json.JSONException;
import org.json.JSONObject;

import com.common.Common;

/**
 * @author Avishek Seal
 *
 */
public class StatementInformationModel implements Serializable {

    private static final long serialVersionUID = 7537725014816737234L;

    @MapToField(name="stmt.serial.number", nullAllowed = false)
    private String serialiNumber;

    @MapToField(name="stmt.transaction.date", nullAllowed = false)
    private String transactionDate;
    
    @MapToField(name="stmt.cheque.number", nullAllowed = false)
    private String chequeNumber;
    
    @MapToField(name="stmt.particulars", nullAllowed = false)
    private String particulars;
    
    @MapToField(name="stmt.debit.balance", nullAllowed = false)
    private String debitBalance;
    
    @MapToField(name="stmt.credit.balance", nullAllowed = false)
    private String creditBalance;
    
    @MapToField(name="stmt.balance.amount", nullAllowed = false)
    private String balanceAmount;
    
    @MapToField(name="stmt.isol", nullAllowed = false)
    private String iSol;

    /**
     * Avishek Seal
     * @return the serialiNumber
     */
    public String getSerialiNumber() {
        return serialiNumber;
    }

    /**
     * Avishek Seal
     * @param serialiNumber the serialiNumber to set
     */
    public void setSerialiNumber(String serialiNumber) {
        this.serialiNumber = serialiNumber;
    }

    /**
     * Avishek Seal
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * Avishek Seal
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * Avishek Seal
     * @return the chequeNumber
     */
    public String getChequeNumber() {
        return chequeNumber;
    }

    /**
     * Avishek Seal
     * @param chequeNumber the chequeNumber to set
     */
    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    /**
     * Avishek Seal
     * @return the particulars
     */
    public String getParticulars() {
        return particulars;
    }

    /**
     * Avishek Seal
     * @param particulars the particulars to set
     */
    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    /**
     * Avishek Seal
     * @return the debitBalance
     */
    public String getDebitBalance() {
        return debitBalance;
    }

    /**
     * Avishek Seal
     * @param debitBalance the debitBalance to set
     */
    public void setDebitBalance(String debitBalance) {
        this.debitBalance = debitBalance;
    }

    /**
     * Avishek Seal
     * @return the creditBalance
     */
    public String getCreditBalance() {
        return creditBalance;
    }

    /**
     * Avishek Seal
     * @param creditBalance the creditBalance to set
     */
    public void setCreditBalance(String creditBalance) {
        this.creditBalance = creditBalance;
    }

    /**
     * Avishek Seal
     * @return the balanceAmount
     */
    public String getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Avishek Seal
     * @param balanceAmount the balanceAmount to set
     */
    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    /**
     * Avishek Seal
     * @return the iSol
     */
    public String getiSol() {
        return iSol;
    }

    /**
     * Avishek Seal
     * @param iSol the iSol to set
     */
    public void setiSol(String iSol) {
        this.iSol = iSol;
    }
    
    /**
     * 
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws JSONException
     * @throws Exception
     */
    private final String toJsonString() throws IllegalArgumentException, IllegalAccessException, JSONException, Exception {
	   	Field[] filelds = this.getClass().getDeclaredFields();
	   	
	   	ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

	   	JSONObject jsonObject = new JSONObject();
	   	
	   	for (Field field : filelds) {
	   		Annotation annotation = field.getAnnotation(MapToField.class);
	   		MapToField field2 = MapToField.class.cast(annotation);

	   		if (field2 != null) {
	   			if (field.get(this) != null) {
	   				if (field2.encrypt()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
	   				} else {
	   					jsonObject.put(bundle.getString(field2.name()), field.get(this));
	   				}

	   			} else {
	   				if (field2.nullAllowed()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
	   				}
	   			}
	   		}
	   	}
	           return jsonObject.toString();
   }
    
    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @return
     */
    @Override
    public String toString() {
        try {
	    return toJsonString();
	} catch (Exception e) {
	    return "{}";
	}
    }
}
