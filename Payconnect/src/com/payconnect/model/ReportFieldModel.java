package com.payconnect.model;

import java.io.Serializable;

/**
 * 
 * @author Avishek Seal
 *
 */
public class ReportFieldModel implements Serializable{

	private static final long serialVersionUID = 754757639499223479L;
	
	private int id;
	
	private String reportField;
	
	private String fieldLabel;
	
	private Integer menuID;
	
	private String menuName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReportField() {
		return reportField;
	}

	public void setReportField(String reportField) {
		this.reportField = reportField;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public Integer getMenuID() {
		return menuID;
	}

	public void setMenuID(Integer menuID) {
		this.menuID = menuID;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	@Override
	public String toString() {
		return "ReportFieldModel [id=" + id + ", reportField=" + reportField
				+ ", fieldLabel=" + fieldLabel + ", menuID=" + menuID
				+ ", menuName=" + menuName + "]";
	}
	
}
