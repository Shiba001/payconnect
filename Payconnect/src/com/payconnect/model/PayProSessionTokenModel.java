package com.payconnect.model;

public class PayProSessionTokenModel {

	private String userID;
	private String sessionToken;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	@Override
	public String toString() {
		return "PayProSessionTokenModel [userID=" + userID + ", sessionToken="
				+ sessionToken + "]";
	}

}
