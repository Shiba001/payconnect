package com.payconnect.model;

public class DeviceModel {
	
	private String deviceId;
	
	private String deviceStatus;
	
	private String unlockCode;
	
	private String createDate;
	
	private String updateDate;
	

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public String getUnlockCode() {
		return unlockCode;
	}

	public void setUnlockCode(String unlockCode) {
		this.unlockCode = unlockCode;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "DeviceModel [deviceId=" + deviceId + ", deviceStatus="
				+ deviceStatus + ", unlockCode=" + unlockCode + ", createDate="
				+ createDate + ", updateDate=" + updateDate + "]";
	}
	
	
	
	
	
	

}
