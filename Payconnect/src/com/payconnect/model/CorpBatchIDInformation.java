package com.payconnect.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.json.JSONException;
import org.json.JSONObject;

import com.common.Common;

public class CorpBatchIDInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1183482307754355049L;

	@MapToField(name = "corpbatch.batchnumber")
	private String corpBatchNumber;

	@MapToField(name = "corpbatch.creationdate")
	private String creationDate;

	public String getCorpBatchNumber() {
		return corpBatchNumber;
	}

	public void setCorpBatchNumber(String corpBatchNumber) {
		this.corpBatchNumber = corpBatchNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	
	private final String toJsonString() throws IllegalArgumentException, IllegalAccessException, JSONException, Exception {
	   	Field[] filelds = this.getClass().getDeclaredFields();
	   	
	   	ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

	   	JSONObject jsonObject = new JSONObject();
	   	
	   	for (Field field : filelds) {
	   		Annotation annotation = field.getAnnotation(MapToField.class);
	   		MapToField field2 = MapToField.class.cast(annotation);

	   		if (field2 != null) {
	   			if (field.get(this) != null) {
	   				if (field2.encrypt()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
	   				} else {
	   					jsonObject.put(bundle.getString(field2.name()), field.get(this));
	   				}

	   			} else {
	   				if (field2.nullAllowed()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
	   				}
	   			}
	   		}
	   	}
	           return jsonObject.toString();
	       }
	    
	    /**
	     * @since 18-Feb-2016
	     * @author Avishek Seal
	     * @return
	     */
	    @Override
	    public String toString() {
	        try {
		    return toJsonString();
		} catch (Exception e) {
		    return "{}";
		}
	    }

}
