package com.payconnect.model;

import java.util.Date;


public class CorpBatchIDListRequestModel extends AbstractRequestModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4287005858386085040L;

	private String apiVersion;
	private String corporateCode;
	private String userId;
	private Date startdate;
	private Date endDate;

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getCorporateCode() {
		return corporateCode;
	}

	public void setCorporateCode(String corporateCode) {
		this.corporateCode = corporateCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "CorpBatchIDListRequestModel [apiVersion=" + apiVersion
				+ ", corporateCode=" + corporateCode + ", userId=" + userId
				+ ", startdate=" + startdate + ", endDate=" + endDate + "]";
	}

	
	

}
