package com.payconnect.model;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.common.Common;

public class SetBiomatricModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7602326758457133555L;
	private String userId;
	private String deviceId;
	private String corpId;
	private String system;
	
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) throws Exception {
		if(StringUtils.isNotBlank(deviceId)){
			this.deviceId=Common.decode(Common.PRIVATE_KEY, deviceId);
		}
		else{
			this.deviceId = "";
		}
		
	}
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) throws Exception {
		if(StringUtils.isNotBlank(corpId)){
			this.corpId=Common.decode(Common.PRIVATE_KEY, corpId);
		}else{
			this.corpId = "";	
		}
		
	}
	
	
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	
	@Override
	public String toString() {
		return "SetBiomatricModel [userId=" + userId + ", deviceId=" + deviceId
				+ ", corpId=" + corpId + ", system=" + system + "]";
	}
	
	
	
	
	
	
	
	
	
	

}
