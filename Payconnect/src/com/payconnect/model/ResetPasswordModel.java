package com.payconnect.model;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;

import com.common.Common;

public class ResetPasswordModel extends AbstractRequestModel {

    private static final long serialVersionUID = 5532406353871811316L;
    private String corporateCode;
    private String userName;
    private String password;
    private String oldPassword;
    private String deviceID;
    private String apiVersion;

    private String sessionToken;

    public boolean isUserNameBlank() {
	return StringUtils.isBlank(userName);
    }

    public boolean isCoroporateCodeBlank() {
	return StringUtils.isBlank(corporateCode);
    }

    public boolean isPasswordBlank() {
	return StringUtils.isBlank(password);
    }

    public boolean isOldPasswordBlank() {
	return StringUtils.isBlank(oldPassword);
    }

    public boolean isDeviceIDBlank() {
	return StringUtils.isBlank(deviceID);
    }
    
    public boolean isSessionTokenBlank() {
	return StringUtils.isBlank(sessionToken);
    }

    public String getCorporateCode() {
	return corporateCode;
    }

    public void setCorporateCode(String corporateCode) throws Exception {
	if (StringUtils.isNotBlank(corporateCode)) {
	    this.corporateCode = Common.decode(Common.PRIVATE_KEY, corporateCode);
	} else {
	    this.corporateCode = "";
	}
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) throws Exception {
	if (StringUtils.isNotBlank(userName)) {
	    this.userName = Common.decode(Common.PRIVATE_KEY, userName);
	} else {
	    this.userName = "";
	}
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) throws Exception {
	if (StringUtils.isNotBlank(password)) {
	    this.password = Common.decode(Common.PRIVATE_KEY, password);
	} else {
	    this.password = "";
	}
    }

    public String getOldPassword() {
	return oldPassword;
    }

    public void setOldPassword(String oldPassword) throws Exception {
	if (StringUtils.isNotBlank(oldPassword)) {
	    this.oldPassword = Common.decode(Common.PRIVATE_KEY, oldPassword);
	} else {
	    this.oldPassword = "";
	}
    }

    public String getDeviceID() {
	return deviceID;
    }

    public void setDeviceID(String deviceID) throws Exception {
	if (StringUtils.isNotBlank(deviceID)) {
	    this.deviceID = Common.decode(Common.PRIVATE_KEY, deviceID);
	} else {
	    this.deviceID = "";
	}
    }

    public String getSessionToken() {
	return sessionToken;
    }

    public void setSessionToken(String sessionToken) throws Exception {
    if(StringUtils.isNotBlank(sessionToken)){
    	this.sessionToken=Common.decode(Common.PRIVATE_KEY, sessionToken);
    }
    else{
    	this.sessionToken = "";
    }
	
    }

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) throws Exception {
		if(StringUtils.isNotBlank(apiVersion)){
			this.apiVersion=Common.decode(Common.PRIVATE_KEY, apiVersion);
		}else{
			this.apiVersion = "";
		}
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String toApiInputString() throws Exception {

		JSONObject jsonObject = null;

		if (userName != null && password != null && oldPassword!=null) {

			jsonObject = new JSONObject();
			
			
		

			jsonObject.put("ApiVersion", apiVersion.trim());
			jsonObject.put("CorpID", corporateCode.trim());
			jsonObject.put("UserID", userName.trim());
			jsonObject.put("OldPwd", oldPassword.trim());
			jsonObject.put("NewPwd", password.trim());
			jsonObject.put("DeviceID", deviceID.trim());
			jsonObject.put("System",  getSystemValueFromApi());
			
			
			System.out.println("input parameter for reset password is:  " + jsonObject.toString());

		}

		return jsonObject.toString();
	}

	

	@Override
	public String toString() {
		return "ResetPasswordModel [corporateCode=" + corporateCode
				+ ", userName=" + userName + ", password=" + password
				+ ", oldPassword=" + oldPassword + ", deviceID=" + deviceID
				+ ", apiVersion=" + apiVersion + ", sessionToken="
				+ sessionToken + "]";
	}
	
	
    
    
}
