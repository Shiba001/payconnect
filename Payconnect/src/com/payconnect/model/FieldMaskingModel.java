package com.payconnect.model;

import java.io.Serializable;
import java.util.List;

public class FieldMaskingModel implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3805888008668446833L;
	public int CustomerID;
	public String CustomerName;
	public String CustomerAddress;
	public String SchmDesc;
	public String Currency;
	public int Totalcount;
	public int StatementFormat;
	public int Filetype;
	public int ResponseCode;
	public String ResponseDeatils;
	public String Statementdownloadlink;
	public String Message;
	public List<StatementInfo> StatementInfo;
	
	
	public int getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(int customerID) {
		CustomerID = customerID;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustomerAddress() {
		return CustomerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}
	public String getSchmDesc() {
		return SchmDesc;
	}
	public void setSchmDesc(String schmDesc) {
		SchmDesc = schmDesc;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public int getTotalcount() {
		return Totalcount;
	}
	public void setTotalcount(int totalcount) {
		Totalcount = totalcount;
	}
	public int getStatementFormat() {
		return StatementFormat;
	}
	public void setStatementFormat(int statementFormat) {
		StatementFormat = statementFormat;
	}
	public int getFiletype() {
		return Filetype;
	}
	public void setFiletype(int filetype) {
		Filetype = filetype;
	}
	public int getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(int responseCode) {
		ResponseCode = responseCode;
	}
	public String getResponseDeatils() {
		return ResponseDeatils;
	}
	public void setResponseDeatils(String responseDeatils) {
		ResponseDeatils = responseDeatils;
	}
	public String getStatementdownloadlink() {
		return Statementdownloadlink;
	}
	public void setStatementdownloadlink(String statementdownloadlink) {
		Statementdownloadlink = statementdownloadlink;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public List<StatementInfo> getStatementInfo() {
		return StatementInfo;
	}
	public void setStatementInfo(List<StatementInfo> statementInfo) {
		StatementInfo = statementInfo;
	}
	
	
	@Override
	public String toString() {
		return "FieldMaskingModel [CustomerID=" + CustomerID
				+ ", CustomerName=" + CustomerName + ", CustomerAddress="
				+ CustomerAddress + ", SchmDesc=" + SchmDesc + ", Currency="
				+ Currency + ", Totalcount=" + Totalcount
				+ ", StatementFormat=" + StatementFormat + ", Filetype="
				+ Filetype + ", ResponseCode=" + ResponseCode
				+ ", ResponseDeatils=" + ResponseDeatils
				+ ", Statementdownloadlink=" + Statementdownloadlink
				+ ", Message=" + Message + ", StatementInfo=" + StatementInfo
				+ "]";
	}
	
	
	
	
	
	

}
