package com.payconnect.model;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.common.Common;

/**
 * 
 * @author Avishek Seal
 *
 */
public abstract class AbstractRequestModel implements Serializable {

    private static final long serialVersionUID = 1633035481826425897L;

    private String systemValueFromApi;
    
    

    public boolean isSystemValueFromAPINotBlank() {
	return StringUtils.isNotBlank(systemValueFromApi);
    }

    public Integer getSystemValueFromApiInteger() {
	return Integer.parseInt(systemValueFromApi);
    }

    public String getSystemValueFromApi() {
	return systemValueFromApi;
    }

    public void setSystemValueFromApi(String systemValueFromApi)
	    throws Exception {
	if (systemValueFromApi == null) {
	    this.systemValueFromApi = "";
	} else {
	    this.systemValueFromApi = Common.decode(Common.PRIVATE_KEY,
		    systemValueFromApi);
	}
    }
    
    public void setSystem(String system){
    	this.systemValueFromApi = system;
    }

}
