package com.payconnect.model;

import java.io.Serializable;

public class RMInformationModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7314560766288967564L;
	
	
	
	private String supportDesk;
	private String rmName;
	private String email;
	private String phoneNo;
	
	
	public String getSupportDesk() {
		return supportDesk;
	}
	public void setSupportDesk(String supportDesk) {
		this.supportDesk = supportDesk;
	}
	public String getRmName() {
		return rmName;
	}
	public void setRmName(String rmName) {
		this.rmName = rmName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		return "RMInformationModel [supportDesk=" + supportDesk + ", rmName="
				+ rmName + ", email=" + email + ", phoneNo=" + phoneNo + "]";
	}
	
	
	
	
	
	

}
