package com.payconnect.model;

import java.util.Date;


public class PayconnectOtacModel {

	private String userId;

	private String otac;

	private String registeredPhone;

	private String status;

	private Date createDate;

	private String isMatch;

	private Date smsReceiveDate;

	private String encriptStr;

	private Date verifyDate;

	private String mobileNo;

	private String decriptStr;

	private String deviceId;

	private String isValidated;

	private String system;
	
	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOtac() {
		return otac;
	}

	public void setOtac(String otac) {
		this.otac = otac;
	}

	public String getRegisteredPhone() {
		return registeredPhone;
	}

	public void setRegisteredPhone(String registeredPhone) {
		this.registeredPhone = registeredPhone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getIsMatch() {
		return isMatch;
	}

	public void setIsMatch(String isMatch) {
		this.isMatch = isMatch;
	}

	public Date getSmsReceiveDate() {
		return smsReceiveDate;
	}

	public void setSmsReceiveDate(Date smsReceiveDate) {
		this.smsReceiveDate = smsReceiveDate;
	}

	public String getEncriptStr() {
		return encriptStr;
	}

	public void setEncriptStr(String encriptStr) {
		this.encriptStr = encriptStr;
	}

	public Date getVerifyDate() {
		return verifyDate;
	}

	public void setVerifyDate(Date verifyDate) {
		this.verifyDate = verifyDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getDecriptStr() {
		return decriptStr;
	}

	public void setDecriptStr(String decriptStr) {
		this.decriptStr = decriptStr;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getIsValidated() {
		return isValidated;
	}

	public void setIsValidated(String isValidated) {
		this.isValidated = isValidated;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}
	

	@Override
	public String toString() {
		return "PowerAccessOtacModel [userId=" + userId + ", otac=" + otac
				+ ", registeredPhone=" + registeredPhone + ", status=" + status
				+ ", createDate=" + createDate + ", isMatch=" + isMatch
				+ ", smsReceiveDate=" + smsReceiveDate + ", encriptStr="
				+ encriptStr + ", verifyDate=" + verifyDate + ", mobileNo="
				+ mobileNo + ", decriptStr=" + decriptStr + ", deviceId="
				+ deviceId + ", isValidated=" + isValidated + ", system="
				+ system + "]";
	}

	
	
	
}
