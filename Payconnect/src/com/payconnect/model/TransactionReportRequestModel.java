package com.payconnect.model;

/**
 * 
 * @author Satya Karanam
 * 18-Feb-2016
 */
public class TransactionReportRequestModel extends AbstractRequestModel {

	
	private static final long serialVersionUID = 7278274403444043167L;

	private String version;
	
	private String userId;
	
	private String corporateID;
	
	private String loginID;
	
	private String pageNo;
	
	private String recPerPage;
	
	private String startDate;
	
	private String endDate;
	
	private String productCode;
	
	private String statusCode;
	
	private String fileName;
	
	private String batchId;
	
	private String corpAccountNumber;
	
	private String amountFrom;
	
	private String amountTo;
	
	private String vendorCode;
	
	private String benificiaryName;
	
	private String benificiaryAccountNumber;
	
	private String benificiaryIfscCode;
	
	private String transactionId;
	
	private String transactionUtr;
	
	private String payDocNumber;
	
	private String chequeNumber;
	
	private String corporateBatchID;
	
	private String benificiaryCode;
	
	private String corporateRef;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	/**
	 * Avishek Seal
	 * @return the loginID
	 */
	public String getLoginID() {
	    return loginID;
	}

	/**
	 * Avishek Seal
	 * @param loginID the loginID to set
	 */
	public void setLoginID(String loginID) {
	    this.loginID = loginID;
	}

	/**
	 * Avishek Seal
	 * @return the corporateID
	 */
	public String getCorporateID() {
	    return corporateID;
	}

	/**
	 * Avishek Seal
	 * @param corporateID the corporateID to set
	 */
	public void setCorporateID(String corporateID) {
	    this.corporateID = corporateID;
	}

	public String getPageNo() {
		return pageNo;
	}

	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	public String getRecPerPage() {
		return recPerPage;
	}

	public void setRecPerPage(String recPerPage) {
		this.recPerPage = recPerPage;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getCorpAccountNumber() {
		return corpAccountNumber;
	}

	public void setCorpAccountNumber(String corpAccountNumber) {
		this.corpAccountNumber = corpAccountNumber;
	}

	public String getAmountFrom() {
		return amountFrom;
	}

	public void setAmountFrom(String amountFrom) {
		this.amountFrom = amountFrom;
	}

	public String getAmountTo() {
		return amountTo;
	}

	public void setAmountTo(String amountTo) {
		this.amountTo = amountTo;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getBenificiaryName() {
		return benificiaryName;
	}

	public void setBenificiaryName(String benificiaryName) {
		this.benificiaryName = benificiaryName;
	}

	public String getBenificiaryAccountNumber() {
		return benificiaryAccountNumber;
	}

	public void setBenificiaryAccountNumber(String benificiaryAccountNumber) {
		this.benificiaryAccountNumber = benificiaryAccountNumber;
	}

	public String getBenificiaryIfscCode() {
		return benificiaryIfscCode;
	}

	public void setBenificiaryIfscCode(String benificiaryIfscCode) {
		this.benificiaryIfscCode = benificiaryIfscCode;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionUtr() {
		return transactionUtr;
	}

	public void setTransactionUtr(String transactionUtr) {
		this.transactionUtr = transactionUtr;
	}

	public String getPayDocNumber() {
		return payDocNumber;
	}

	public void setPayDocNumber(String payDocNumber) {
		this.payDocNumber = payDocNumber;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	/**
	 * Avishek Seal
	 * @return the corporateBatchID
	 */
	public String getCorporateBatchID() {
	    return corporateBatchID;
	}

	/**
	 * Avishek Seal
	 * @param corporateBatchID the corporateBatchID to set
	 */
	public void setCorporateBatchID(String corporateBatchID) {
	    this.corporateBatchID = corporateBatchID;
	}

	/**
	 * Avishek Seal
	 * @return the benificiaryCode
	 */
	public String getBenificiaryCode() {
	    return benificiaryCode;
	}

	/**
	 * Avishek Seal
	 * @param benificiaryCode the benificiaryCode to set
	 */
	public void setBenificiaryCode(String benificiaryCode) {
	    this.benificiaryCode = benificiaryCode;
	}

	/**
	 * Avishek Seal
	 * @return the corporateRef
	 */
	public String getCorporateRef() {
	    return corporateRef;
	}

	/**
	 * Avishek Seal
	 * @param corporateRef the corporateRef to set
	 */
	public void setCorporateRef(String corporateRef) {
	    this.corporateRef = corporateRef;
	}
	
}
