package com.payconnect.model;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;

import com.common.Common;

/**
 * 
 * @author Avishek Seal
 *
 */
public class LoginRequestModel extends AbstractRequestModel {

	private static final long serialVersionUID = -3291729907034905397L;

	private String userName;
	private String userPassward;
	private String deviceID;
	private String corporateID;
    private String captcha;
	private String apiVersion;
	private String latitude;
	private String longitude;
	private String clientIp;

	public String getUserName() {
		return userName;
	}

	public boolean isUserNameNotBlank() {
		return StringUtils.isNotBlank(userName);
	}

	public boolean isPasswordNotBlank() {
		return StringUtils.isNotBlank(userPassward);
	}

	public boolean isDeviceIDNotBlank() {
		return StringUtils.isNotBlank(deviceID);
	}

	public boolean isCorporateIDNotBlank() {
		return StringUtils.isNotBlank(corporateID);
	}

	public boolean isApiVersionNotBlank() {
		return StringUtils.isNotBlank(apiVersion);
	}

	public boolean isCaptchaBlank() {
		return StringUtils.isNotBlank(captcha);
	}

	public String getCorporateID() {
		return corporateID;
	}

	public void setCorporateID(String corporateID) throws Exception {
		if (corporateID == null) {
			this.corporateID = "";
		} else {
			this.corporateID = Common.decode(Common.PRIVATE_KEY, corporateID);
		}
	}

	public void setUserPassward(String userPassward) throws Exception {
		if (userPassward == null) {
			this.userPassward = "";
		} else {
			this.userPassward = Common.decode(Common.PRIVATE_KEY, userPassward);
		}
	}

	public String getUserPassward() {
		return userPassward;
	}

	public void setUserName(String userName) throws Exception {
		if (userName == null) {
			this.userName = "";
		} else {
			this.userName = Common.decode(Common.PRIVATE_KEY, userName);
		}
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) throws Exception {
		if (deviceID == null) {
			this.deviceID = "";
		} else {
			this.deviceID = Common.decode(Common.PRIVATE_KEY, deviceID);
		}
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) throws Exception {
		if (captcha == null) {
			this.captcha = "";
		} else {
			this.captcha = Common.decode(Common.PRIVATE_KEY, captcha);
		}
	}
	
	


	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) throws Exception {
		
		if (apiVersion == null) {
			this.apiVersion = "";
		} 
		else {
			this.apiVersion = Common.decode(Common.PRIVATE_KEY, apiVersion);
		}
	}
	
	
	

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) throws Exception {
		
		if(latitude==null){
			this.latitude="";
		}
		else{
			this.latitude = Common.decode(Common.PRIVATE_KEY, latitude);
		}
		
	}
	

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) throws Exception {
		
		if(longitude==null){
			this.longitude="";
		}
		else{
			this.longitude =Common.decode(Common.PRIVATE_KEY, longitude);
		}
		
	}
	
	
	
	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		
		if(clientIp==null){
			this.clientIp="";
		}
		else{
			this.clientIp = clientIp;
		}
		
	}

	@SuppressWarnings("unchecked")
	public String toApiInputString() throws Exception {

		JSONObject jsonObject = null;

		if (userName != null && userPassward != null) {

			jsonObject = new JSONObject();

			jsonObject.put("UserID", userName.trim());
			jsonObject.put("Password", userPassward.trim());
			jsonObject.put("DeviceID", deviceID);
			jsonObject.put("CorpID", corporateID.trim());
			jsonObject.put("ApiVersion",  apiVersion.trim());
			jsonObject.put("System",  getSystemValueFromApi());
			jsonObject.put("IPAddress",  clientIp.trim());
			jsonObject.put("Latitude", latitude.trim());
			jsonObject.put("Longitude", longitude.trim());
			
			
			

			System.out.println(" power access input parameter is:  " + jsonObject.toString());

		}

		return jsonObject.toString();
	}

	@Override
	public String toString() {
		return "LoginRequestModel [userName=" + userName + ", userPassward="
				+ userPassward + ", deviceID=" + deviceID + ", corporateID="
				+ corporateID + ", captcha=" + captcha + ", apiVersion="
				+ apiVersion + ", latitude=" + latitude + ", longitude="
				+ longitude + ", clientIp=" + clientIp + "]";
	}

	
	

	
	
	
	
}
