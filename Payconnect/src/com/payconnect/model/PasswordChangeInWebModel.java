/**
 * 
 */
package com.payconnect.model;

import java.io.Serializable;

/**
 * @author Avishek Seal
 *
 */
public class PasswordChangeInWebModel implements Serializable{

    private static final long serialVersionUID = -6567949215347238405L;

    private String userID;
    
    private String system;
    
    private String value;

    /**
     * Avishek Seal
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Avishek Seal
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * Avishek Seal
     * @return the system
     */
    public String getSystem() {
        return system;
    }

    /**
     * Avishek Seal
     * @param system the system to set
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * Avishek Seal
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Avishek Seal
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
