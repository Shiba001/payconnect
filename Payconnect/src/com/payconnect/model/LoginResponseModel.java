package com.payconnect.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

import com.common.Common;

public class LoginResponseModel extends
		AbstractResponseModel<LoginResponseModel> {

	public LoginResponseModel() {
		super(LoginResponseModel.class);
	}

	private static transient final long serialVersionUID = 6286020454018074983L;

	@MapToField(name = "login.response_code", encrypt = false)
	private String responseCode;

	@MapToField(name = "login.responseDetails", encrypt = false)
	private String responseDetails;

	@MapToField(name = "login.error", nullAllowed = false, encrypt = false)
	private String error;

	@MapToField(name = "login.userstatus", nullAllowed = false)
	private String userStatus;
	
	@MapToField(name = "login.userid", nullAllowed = false)
	private String userID;

	@MapToField(name = "login.username", nullAllowed = false)
	private String userName;

	@MapToField(name = "login.mobileNumber", nullAllowed = false)
	private String mobileNumber;

	@MapToField(name = "login.sessiontoken", nullAllowed = false)
	private String sessionToken;

	@MapToField(name = "login.senderShortCode", nullAllowed = false)
	private String senderShortCode;

	@MapToField(name = "login.senderID", nullAllowed = false)
	private String senderID;

	@MapToField(name = "login.emailid", nullAllowed = false)
	private String emailID;

	@MapToField(name = "login.lastpwdchgdate", nullAllowed = false)
	private String lastChangePaswordDate;

	@MapToField(name = "login.pwdexpirydate", nullAllowed = false)
	private String passwordExpiryDate;

	@MapToField(name = "login.lastlogondate", nullAllowed = false)
	private String lastLogin;

	@MapToField(name = "login.captchaString", nullAllowed = false)
	private String captchaString;

	@MapToField(name = "login.pwdchangeinweb", nullAllowed = false)
	private String passwordChangeinWeb;
	
	@MapToField(name = "login.pwdchangeneeded", nullAllowed = false)
	private String passwordChangeNeed;

	@MapToField(name = "login.corpId", nullAllowed = false)
	private String corpID;
	
	@MapToField(name = "login.currentdate", nullAllowed = false)
	private String currentDateTime;
	
	@MapToField(name = "login.userType", nullAllowed = false)
	private String userType;
	
	@MapToField(name = "login.firstname", nullAllowed = false)
	private String firstName;
	
	@MapToField(name = "login.lastname", nullAllowed = false)
	private String lastName;
	
	private String system;
	
	@MapToField(name = "login.attempt.remains", nullAllowed = false)
	private String loginAttemptRemaining;
	
	@MapToField(name = "login.captcha.attempt.remains", nullAllowed = false)
	private String captchaAttemptRemaining;
	
	@MapToField(name="resetpassword.attempt.remains", nullAllowed=false)
	private String ResetPasswordAttemptRemaining;
	
	@MapToField(name = "login.unlockCode", nullAllowed = false)
	private String unlockCode;//newly added
	
	

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDetails() {
		return responseDetails;
	}

	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public String getUserID() {
	    return userID;
	}

	public void setUserID(String userID) {
	    this.userID = userID;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getSenderShortCode() {
		return senderShortCode;
	}

	public void setSenderShortCode(String senderShortCode) {
		System.out.println(senderShortCode+" in setter");
		this.senderShortCode = senderShortCode;
	}

	public String getSenderID() {
		return senderID;
	}

	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getLastChangePaswordDate() {
		return lastChangePaswordDate;
	}

	public void setLastChangePaswordDate(String lastChangePaswordDate) {
		this.lastChangePaswordDate = lastChangePaswordDate;
	}

	public String getPasswordExpiryDate() {
		return passwordExpiryDate;
	}

	public void setPasswordExpiryDate(String passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

	public String getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getCaptchaString() {
		return captchaString;
	}

	public void setCaptchaString(String captchaString) {
		this.captchaString = captchaString;
	}

	public String getPasswordChangeinWeb() {
		return passwordChangeinWeb;
	}

	public void setPasswordChangeinWeb(String passwordChangeinWeb) {
		this.passwordChangeinWeb = passwordChangeinWeb;
	}
	
	
 
	
	public String getPasswordChangeNeed() {
		return passwordChangeNeed;
	}

	public void setPasswordChangeNeed(String passwordChangeNeed) {
		this.passwordChangeNeed = passwordChangeNeed;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	/**
	 * @return the loginAttemptRemaining
	 */
	public String getLoginAttemptRemaining() {
		return loginAttemptRemaining;
	}

	/**
	 * @param loginAttemptRemaining the loginAttemptRemaining to set
	 */
	public void setLoginAttemptRemaining(String loginAttemptRemaining) {
		this.loginAttemptRemaining = loginAttemptRemaining;
	}

	/**
	 * @return the captchaAttemptRemaining
	 */
	public String getCaptchaAttemptRemaining() {
		return captchaAttemptRemaining;
	}

	/**
	 * @param captchaAttemptRemaining the captchaAttemptRemaining to set
	 */
	public void setCaptchaAttemptRemaining(String captchaAttemptRemaining) {
		this.captchaAttemptRemaining = captchaAttemptRemaining;
	}
	
	

	public String getResetPasswordAttemptRemaining() {
		return ResetPasswordAttemptRemaining;
	}

	public void setResetPasswordAttemptRemaining(
			String resetPasswordAttemptRemaining) {
		ResetPasswordAttemptRemaining = resetPasswordAttemptRemaining;
	}

	public String getUnlockCode() {
		return unlockCode;
	}

	public void setUnlockCode(String unlockCode) {
		this.unlockCode = unlockCode;
	}

	public final boolean isSuccess() {
		return StringUtils.isNotBlank(responseCode)
				&& StringUtils.equals(responseCode, "200");
	}

	public final boolean isInvalidCredential() {
		return StringUtils.isNotBlank(responseCode)
				&& StringUtils.equals(responseCode, "202");
	}

	/**
	 * Avishek Seal
	 * @return the userType
	 */
	public String getUserType() {
	    return userType;
	}

	/**
	 * Avishek Seal
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
	    this.userType = userType;
	}
	
	

	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Avishek Seal
	 * @return the currentDateTime
	 */
	public String getCurrentDateTime() {
	    return currentDateTime;
	}

	/**
	 * Avishek Seal
	 * @param currentDateTime the currentDateTime to set
	 */
	public void setCurrentDateTime(String currentDateTime) {
	    this.currentDateTime = currentDateTime;
	}

	/**
	 * Avishek Seal
	 * @return the system
	 */
	public String getSystem() {
	    return system;
	}

	/**
	 * Avishek Seal
	 * @param system the system to set
	 */
	public void setSystem(String system) {
	    this.system = system;
	}

	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.encrypt()) {
						System.out.println(field2.name()+"  "+field.get(this).toString());
						jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
					} else {
						jsonObject.put(bundle.getString(field2.name()), field.get(this));
					}

				} else {
					if (field2.nullAllowed()) {
						jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
					}
				}
			}
		}

		return jsonObject.toString();
	}
}
