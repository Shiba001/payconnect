package com.payconnect.model;

import java.io.Serializable;
import java.util.Date;

public class BankBatchIdListRequestModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2952244626625495146L;
	/**
	 * 
	 */
	

	private String apiVersion;
	private String corporateCode;
	private String loginId;
	private Date startdate;
	private Date endDate;

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getCorporateCode() {
		return corporateCode;
	}

	public void setCorporateCode(String corporateCode) {
		this.corporateCode = corporateCode;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "BankBatchIdListRequestModel [apiVersion=" + apiVersion
				+ ", corporateCode=" + corporateCode + ", loginId=" + loginId
				+ ", startdate=" + startdate + ", endDate=" + endDate + "]";
	}

}
