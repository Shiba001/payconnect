package com.payconnect.model;



public class UnlockDeviceModel {
	
	private String deviceId;
	private String unlockCode;
	private String deviceStatus;
	private String createDate;
	private String updateDate;
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getUnlockCode() {
		return unlockCode;
	}
	public void setUnlockCode(String unlockCode) {
		this.unlockCode = unlockCode;
	}
	public String getDeviceStatus() {
		return deviceStatus;
	}
	public void setDeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	@Override
	public String toString() {
		return "UnlockDeviceModel [deviceId=" + deviceId + ", unlockCode="
				+ unlockCode + ", deviceStatus=" + deviceStatus
				+ ", createDate=" + createDate + ", updateDate=" + updateDate
				+ "]";
	}
	
	
	
	}
	
	
	
	

	
	
	

	
	
	
	
	
	
	


