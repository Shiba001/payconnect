package com.payconnect.model;

import java.io.Serializable;

public class FinalMaskingModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5052325545155305171L;
	
	
	public int slno;

	public String transctiondate;

	public String chequeNo;

	public String particulars;

	public String debit;

	public String credit;

	public String balanceammount;

	public String sol;

	public int getSlno() {
		return slno;
	}

	public void setSlno(int slno) {
		this.slno = slno;
	}

	public String getTransctiondate() {
		return transctiondate;
	}

	public void setTransctiondate(String transctiondate) {
		this.transctiondate = transctiondate;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getDebit() {
		return debit;
	}

	public void setDebit(String debit) {
		this.debit = debit;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getBalanceammount() {
		return balanceammount;
	}

	public void setBalanceammount(String balanceammount) {
		this.balanceammount = balanceammount;
	}

	public String getSol() {
		return sol;
	}

	public void setSol(String sol) {
		this.sol = sol;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
