package com.payconnect.model;

import java.io.Serializable;

import org.json.JSONObject;

public abstract class AbstractResponseModel<T> implements Serializable{

	protected Class<T> thisClass;
	
	private static transient final long serialVersionUID = -2493419493107218231L;
	
	protected final transient JSONObject jsonObject = new JSONObject();;
	
	public AbstractResponseModel(Class<T> thisClass) {
		this.thisClass = thisClass;
	}
	public abstract String toJsonString() throws Exception;
	
}
