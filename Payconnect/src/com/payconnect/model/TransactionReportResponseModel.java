package com.payconnect.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;

import com.common.Common;

/**
 * 
 * @author Satya Karanam 18-Feb-2016
 */

public class TransactionReportResponseModel extends AbstractResponseModel<TransactionReportResponseModel> {

	private static final long serialVersionUID = -3195073466824226099L;

	@MapToField(name = "transaction.total.count", nullAllowed = false)
	private Integer totalCount;

	@MapToField(name = "transaction.response.code", nullAllowed = false, encrypt = false)
	private String responseCode;

	@MapToField(name = "transaction.response.details", nullAllowed = false, encrypt = false)
	private String responseDetails;

	@MapToField(name = "transaction.pdf.download.link", nullAllowed = false)
	private String pdfDownloadLink;

	@MapToField(name = "transaction.excel.download.link", nullAllowed = false)
	private String excelDownloadLink;

	@MapToField(name = "transaction.error", nullAllowed = false, encrypt = false)
	private String error;

	@MapToField(name = "transaction.transaction.information.response.model", nullAllowed = false, associateList = true, encrypt = false)
	private List<TransactionInformationModel> transactionInformationModels;

	public TransactionReportResponseModel() {
		super(TransactionReportResponseModel.class);
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDetails() {
		return responseDetails;
	}

	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}

	public String getPdfDownloadLink() {
		return pdfDownloadLink;
	}

	public void setPdfDownloadLink(String pdfDownloadLink) {
		this.pdfDownloadLink = pdfDownloadLink;
	}

	public String getExcelDownloadLink() {
		return excelDownloadLink;
	}

	public void setExcelDownloadLink(String excelDownloadLink) {
		this.excelDownloadLink = excelDownloadLink;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	/**
	 * Avishek Seal
	 * @return the transactionInformationModels
	 */
	public List<TransactionInformationModel> getTransactionInformationModels() {
	    return transactionInformationModels;
	}

	/**
	 * Avishek Seal
	 * @param transactionInformationModels the transactionInformationModels to set
	 */
	public void setTransactionInformationModels(
		List<TransactionInformationModel> transactionInformationModels) {
	    this.transactionInformationModels = transactionInformationModels;
	}

	public final boolean isSuccess() {
		return StringUtils.isNotBlank(responseCode) && StringUtils.equals(responseCode, "200");
	}
	/**
	 * this method will return the json string of the object
	 * @since 18-Feb-2016
	 * @author Avishek Seal
	 * @return
	 * @throws Exception
	 */
	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.associateList()) {
						JSONArray array = new JSONArray(field.get(this)
								.getClass().cast(field.get(this)).toString());
						jsonObject.put(bundle.getString(field2.name()), array);
					} else {
						if (field2.encrypt()) {
							jsonObject.put(bundle.getString(field2.name()),
									Common.encode(Common.PRIVATE_KEY, field
											.get(this).toString()));
						} else {

							jsonObject.put(bundle.getString(field2.name()),
									field.get(this).toString());
						}
					}

				} else {
					if (field2.nullAllowed()) {
						if(field2.associateList()) {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, "[]"));
						} else {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
						}
					}
				}
			}
		}

		return jsonObject.toString();
	}

}
