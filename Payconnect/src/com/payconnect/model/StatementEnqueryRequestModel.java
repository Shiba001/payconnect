/**
 * 
 */
package com.payconnect.model;

import java.util.Date;

/**
 * @author Avishek Seal
 *
 */
public class StatementEnqueryRequestModel{

    private static final long serialVersionUID = 3718596992033990710L;

    private String corporateCode;

    private String userID;

    private String corporateAccountNumber;
    
    private String accountName;

    private Date startDate;

    private Date endDate;

    private String statementType;

    private String recordPerPage;

    private String pageNumber;
    
    private String fileType;
    
    private String apiVersion;
    
    private String system;

    /**
     * Avishek Seal
     * @return the corporateCode
     */
    public String getCorporateCode() {
        return corporateCode;
    }

    /**
     * Avishek Seal
     * @param corporateCode the corporateCode to set
     */
    public void setCorporateCode(String corporateCode) {
        this.corporateCode = corporateCode;
    }

    /**
     * Avishek Seal
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Avishek Seal
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * Avishek Seal
     * @return the corporateAccountNumber
     */
    public String getCorporateAccountNumber() {
        return corporateAccountNumber;
    }

    /**
     * Avishek Seal
     * @param corporateAccountNumber the corporateAccountNumber to set
     */
    public void setCorporateAccountNumber(String corporateAccountNumber) {
        this.corporateAccountNumber = corporateAccountNumber;
    }

    /**
     * Avishek Seal
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Avishek Seal
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public void setStartDate(String startDate) {
    	if(startDate == null) {
    		this.startDate = null;
    	}
    }

    /**
     * Avishek Seal
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Avishek Seal
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public void setEndDate(String endDate) {
    	if(endDate == null) {
    		this.endDate = null;
    	}
       
    }

    /**
     * Avishek Seal
     * @return the statementType
     */
    public String getStatementType() {
        return statementType;
    }

    /**
     * Avishek Seal
     * @param statementType the statementType to set
     */
    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }

    /**
     * Avishek Seal
     * @return the recordPerPage
     */
    public String getRecordPerPage() {
        return recordPerPage;
    }

    /**
     * Avishek Seal
     * @param recordPerPage the recordPerPage to set
     */
    public void setRecordPerPage(String recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    /**
     * Avishek Seal
     * @return the pageNumber
     */
    public String getPageNumber() {
        return pageNumber;
    }

    /**
     * Avishek Seal
     * @param pageNumber the pageNumber to set
     */
    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}
	
	
	

    
}
