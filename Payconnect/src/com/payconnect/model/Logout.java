package com.payconnect.model;

import java.io.Serializable;
 
public class Logout implements Serializable{

	private static final long serialVersionUID = -7788542253276944994L;
	
	private String system;
	
	private String sessionToken;
	
	private String userId;
	
	private String deviceId;
	
	private String mpin;
	
	/**
	 * Avishek Seal
	 * @return the mpin
	 */
	public String getMpin() {
	    return mpin;
	}

	/**
	 * Avishek Seal
	 * @param mpin the mpin to set
	 */
	public void setMpin(String mpin) {
	    this.mpin = mpin;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String toString() {
		return "Logout [system=" + system + ", sessionToken=" + sessionToken
				+ ", userId=" + userId + ", deviceId=" + deviceId + "]";
	}
}
