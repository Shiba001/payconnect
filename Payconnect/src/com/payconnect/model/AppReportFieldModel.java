package com.payconnect.model;

import java.io.Serializable;


public class AppReportFieldModel implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1351808770359752604L;
	private int id;
	private String reportField;
    private String fieldLabel;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getReportField() {
		return reportField;
	}
	public void setReportField(String reportField) {
		this.reportField = reportField;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	@Override
	public String toString() {
		return "AppReportFieldModel [id=" + id + ", reportField=" + reportField
				+ ", fieldLabel=" + fieldLabel + "]";
	}
	
	
    
    
    

}
