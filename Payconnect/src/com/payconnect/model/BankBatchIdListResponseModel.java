package com.payconnect.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;

import com.common.Common;

public class BankBatchIdListResponseModel extends
		AbstractResponseModel<BankBatchIdListResponseModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8576075775780355304L;

	public BankBatchIdListResponseModel() {
		super(BankBatchIdListResponseModel.class);
	}

	@MapToField(name = "response.code", encrypt = false)
	private String responseCode;

	@MapToField(name = "response.details", encrypt = false)
	private String responseDetails;
	
	
	@MapToField(name = "error", encrypt = false, nullAllowed = false)
	private String error;

	@MapToField(name = "bankbatch.list", nullAllowed = false, associateList = true, encrypt = false)
	private List<BankBatchIDInformationModel> bankBatchIDInformationModels;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDetails() {
		return responseDetails;
	}

	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<BankBatchIDInformationModel> getBankBatchIDInformationModels() {
		return bankBatchIDInformationModels;
	}

	public void setBankBatchIDInformationModels(
			List<BankBatchIDInformationModel> bankBatchIDInformationModels) {
		this.bankBatchIDInformationModels = bankBatchIDInformationModels;
	}

	public final boolean isSuccess() {
		return StringUtils.isNotBlank(responseCode)
				&& StringUtils.equals(responseCode, "200");
	}

	
	@Override
	public String toJsonString() throws Exception {
	    Field[] filelds = thisClass.getDeclaredFields();
	    ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

	    for (Field field : filelds) {
		Annotation annotation = field.getAnnotation(MapToField.class);
		MapToField field2 = MapToField.class.cast(annotation);

		if (field2 != null) {
		    if (field.get(this) != null) {
    			if(field2.associateList()) {
    			    JSONArray array = new JSONArray(field.get(this).getClass().cast(field.get(this)).toString());
    			    jsonObject.put(bundle.getString(field2.name()), array);
    			} else {
				if (field2.encrypt()) {
					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
				} else {
				    
					jsonObject.put(bundle.getString(field2.name()), field.get(this).toString());
				}
			    }

		    } else {
			if (field2.nullAllowed()) {
			    jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
			}
		    }
		}
	    }

	return jsonObject.toString();
	}
}
