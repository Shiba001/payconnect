package com.payconnect.model;

public class DeRegisterDevice {

	private String userId;
	private String sessionToken;
	private String system;
	private String deviceId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String toString() {
		return "DeRegisterDevice [userId=" + userId + ", sessionToken="
				+ sessionToken + ", system=" + system + ", deviceId="
				+ deviceId + ", getUserId()=" + getUserId()
				+ ", getSessionToken()=" + getSessionToken() + ", getSystem()="
				+ getSystem() + ", getDeviceId()=" + getDeviceId()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
}
