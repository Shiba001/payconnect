package com.payconnect.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;

import com.common.Common;

/**
 * 
 * @author Satya Karanam 18-Feb-2016
 */
public class StatementEnqueryResponseModel extends
		AbstractResponseModel<StatementEnqueryResponseModel> {

	private static final long serialVersionUID = -6291798872056351298L;

	@MapToField(name = "stmt.customer.id", nullAllowed = false)
	private Number customerID;

	@MapToField(name = "stmt.customer.name", nullAllowed = false)
	private String customerName;

	@MapToField(name = "stmt.customer.address", nullAllowed = false)
	private String customerAddress;

	@MapToField(name = "stmt.scheme.description", nullAllowed = false)
	private String schemeDescription;

	@MapToField(name = "stmt.currency", nullAllowed = false)
	private String currency;

	@MapToField(name = "stmt.total.record.count", nullAllowed = false)
	private Integer totalRecordCount;

	@MapToField(name = "stmt.response.code", encrypt = false, nullAllowed=false)
	private String responseCode;

	@MapToField(name = "stmt.response.deatils", encrypt= false, nullAllowed=false)
	private String responseDeatils;

	@MapToField(name = "stmt.download.link", nullAllowed = false)
	private String downloadLink;

	@MapToField(name = "stmt.message", nullAllowed = false)
	private String message;

	@MapToField(name = "stmt.error", nullAllowed = false)
	private String error;

	@MapToField(name = "stmt.file.type", nullAllowed = false)
	private String fileType;

	@MapToField(name = "stmt.statement.type", nullAllowed = false)
	private String statementType;

	@MapToField(name = "stmt.information", nullAllowed = true, associateList = true, encrypt = false)
	private List<StatementInformationModel> statementInformationModels;

	public StatementEnqueryResponseModel() {
		super(StatementEnqueryResponseModel.class);
	}

	public Number getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Number customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getSchemeDescription() {
		return schemeDescription;
	}

	public void setSchemeDescription(String schemeDescription) {
		this.schemeDescription = schemeDescription;
	}

	public int getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(int totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDeatils() {
		return responseDeatils;
	}

	public void setResponseDeatils(String responseDeatils) {
		this.responseDeatils = responseDeatils;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getStatementType() {
		return statementType;
	}

	public void setStatementType(String statementType) {
		this.statementType = statementType;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the statementInformationModels
	 */
	public List<StatementInformationModel> getStatementInformationModels() {
		return statementInformationModels;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param statementInformationModels
	 *            the statementInformationModels to set
	 */
	public void setStatementInformationModels(
			List<StatementInformationModel> statementInformationModels) {
		this.statementInformationModels = statementInformationModels;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public final boolean isSuccess() {
		return StringUtils.isNotBlank(responseCode)
				&& StringUtils.equals(responseCode, "200");
	}

	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.associateList()) {
						JSONArray array = new JSONArray(field.get(this)
								.getClass().cast(field.get(this)).toString());
						jsonObject.put(bundle.getString(field2.name()), array);
					} else {
						if (field2.encrypt()) {
							jsonObject.put(bundle.getString(field2.name()),
									Common.encode(Common.PRIVATE_KEY, field
											.get(this).toString()));
						} else {

							jsonObject.put(bundle.getString(field2.name()),
									field.get(this).toString());
						}
					}

				} else {
					if (field2.nullAllowed()) {
						if(field2.associateList()) {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, "[]"));
						} else {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
						}
					}
				}
			}
		}

		return jsonObject.toString();
	}

}
