package com.payconnect.model;

import org.apache.commons.lang.StringUtils;

import com.common.Common;

public class VerifyDeviceModel {
	
	private String deviceId;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) throws Exception {
		if(StringUtils.isNotBlank(deviceId)){
			this.deviceId=Common.decode(Common.PRIVATE_KEY, deviceId);
		}
		else{
			this.deviceId = "";
		}
		
	}

	@Override
	public String toString() {
		return "VerifyDeviceModel [deviceId=" + deviceId + "]";
	}
	
	

}
