package com.axisbank.dao;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.common.Common;
import com.payconnect.model.LoginRequestModel;
import com.payconnect.model.LoginResponseModel;
import com.sun.jersey.api.client.ClientResponse;

@Repository
public class PowerAccessResponseDAO {
	
	private Logger logger = Logger.getLogger(PowerAccessResponseDAO.class);
	
	
	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;

	@Value("${axis.payconnect.api.path}")
	private String apiUrl;

	@Value("${error.400}")
	private String error400;

	@Value("${error.401}")
	private String error401;

	@Value("${error.402}")
	private String error402;

	@Value("${error.403}")
	private String error403;

	@Value("${error.404}")
	private String error404;

	@Value("${error.405}")
	private String error405;

	@Value("${error.408}")
	private String error408;

	@Value("${error.500}")
	private String error500;

	@Value("${error.502}")
	private String error502;

	@Value("${error.503}")
	private String error503;

	@Value("${error.504}")
	private String error504;

	@Value("${error.other}")
	private String errorOther;

	@Value("${oatc.sms.api.path}")
	private String otacSmsApiPath;

	@Value("${oatc.sms.api.username}")
	private String otacSmsApiUsername;

	@Value("${oatc.sms.api.password}")
	private String otacSmsApiPassword;

	@Value("${oatc.sms.api.ctype}")
	private String otacSmsApiCtype;

	@Value("${oatc.sms.api.alert}")
	private String otacSmsApiAlert;

	@Value("${oatc.sms.api.msgtype}")
	private String otacSmsApiMsgType;

	@Value("${oatc.sms.api.priority}")
	private String otacSmsApiPriority;

	@Value("${oatc.sms.api.sender}")
	private String otacSmsApiSender;

	@Value("${oatc.sms.api.msg}")
	private String otacSmsApiMsg;

	@Value("${oatc.sms.api.decode}")
	private String otacSmsApiDecode;

	@Value("${otac.sms.gateway.server}")
	private String smsServer;

	/**
	 * @author Arup Paul
	 * @param loginRequestModel
	 * @return
	 * @throws Exception
	 */
	public LoginResponseModel getResponseFromAxisApi(LoginRequestModel loginRequestModel) throws Exception {
		
		LoginResponseModel loginResponseModel = new LoginResponseModel();

		String URL = apiUrl+"login";
		
		String inputString = loginRequestModel.toApiInputString();
		
		System.out.println("power access input string "+inputString);
		
		logger.info("power access api input string  "+inputString);

		ClientResponse client_response = Common.payconnectApiAcess(URL, inputString, "post");

		String responseStatus = String.valueOf(client_response.getStatus());
		
		logger.info(" power access  login api response status form axis for login "+responseStatus);
		
		System.out.println(" power access  login api response status form axis for login "+responseStatus);
		
		
		
		

		JSONParser parser = new JSONParser();

		//JSONObject jsonObject = JSONObject.class.cast(parser.parse(jsonString));

		if (StringUtils.equals(String.valueOf(client_response.getStatus()), "200")) {
			
			 String jsonString = client_response.getEntity(String.class);
			
			JSONObject jsonObject = JSONObject.class.cast(parser.parse(jsonString));
			
			System.out.println("login response from axis end "+jsonObject.toString());
			
			logger.info("power access login api response from axis "+jsonObject.toString());
			
			
			
			if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("ResponseCode")))) {
				loginResponseModel.setResponseCode(String.valueOf(jsonObject.get("ResponseCode")));
			}

			if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("ResponseDetails")))) {
				loginResponseModel.setResponseDetails(String.valueOf(jsonObject.get("ResponseDetails")));
			}

			if (loginResponseModel.isSuccess()) {
				
				  String username=String.valueOf(jsonObject.get("UserName"));
				
				  String[]userNames=username.split("~");
				
				  String firstName=userNames[0];
				  
				  String lastName=userNames[1];
				  
				 
				
							
				loginResponseModel.setCorpID(String.valueOf(jsonObject.get("CorpID")));
				loginResponseModel.setEmailID(String.valueOf(jsonObject.get("EmailID")));
				loginResponseModel.setLastChangePaswordDate(String.valueOf(jsonObject.get("LastPswChangeDate")));
				loginResponseModel.setPasswordExpiryDate(String.valueOf(jsonObject.get("PwdExpiryDate")));
				loginResponseModel.setLastLogin(String.valueOf(jsonObject.get("LastLogonDate")));
				
				loginResponseModel.setPasswordChangeinWeb(String.valueOf(jsonObject.get("PwdChangeInWeb")));
				
			    loginResponseModel.setPasswordChangeNeed(String.valueOf(jsonObject.get("PwdChangeNeed")));
				
				
				loginResponseModel.setUserStatus(String.valueOf(jsonObject.get("UserStatus")));
				loginResponseModel.setUserName(firstName+" "+lastName);
				loginResponseModel.setFirstName(firstName);
				loginResponseModel.setLastName(lastName);
				loginResponseModel.setUserType(String.valueOf(jsonObject.get("UserType")));
				loginResponseModel.setSenderShortCode(jsonObject.get("SenderShortCode").toString());
				loginResponseModel.setSenderID("");

				if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("UserID")))) {
					loginResponseModel.setUserID(String.valueOf(jsonObject.get("UserID")));
				}

				if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("MobileNo")))) {
					loginResponseModel.setMobileNumber(String.valueOf(jsonObject.get("MobileNo")));
				}
			}

			return loginResponseModel;
		}

		else {
			
			
			loginResponseModel = new LoginResponseModel();
			String errorMsg = this.getServerErrorMsg(responseStatus);
			System.out.println("power access login api error message " + errorMsg);
			logger.info("power access login api error message " + errorMsg);
			loginResponseModel.setError(errorMsg);
			return loginResponseModel;
		}

	}
	
	
	

	public String getResponseFromSmsApi(String mobile, String otac) {

		String sms_url = "";
		
		String successMessage="OTP SEND SUCCESSFULLY";

		if (smsServer.equals("axis")) {

			sms_url = otacSmsApiPath + "?dcode=" + otacSmsApiDecode
					+ "&userid=" + otacSmsApiUsername + "&pwd="
					+ otacSmsApiPassword + "&ctype=" + otacSmsApiCtype
					+ "&alert=" + otacSmsApiAlert + "&msgtype="
					+ otacSmsApiMsgType + "&priority=" + otacSmsApiPriority
					+ "&sender=" + otacSmsApiSender + "&pno=" + mobile
					+ "&msgtxt=" + otac + otacSmsApiMsg;
		} else {

			sms_url = "http://alerts.solutionsinfini.com/api/v3/index.php?method=sms"
					+ "&api_key=A6f8b45980b586dd9a2fc8281e9dc5b34&to="
					+ mobile
					+ "&sender=MYRTAX"
					+ "&message=Hello,%20Welcome%20to%20MakeYourTax.com.%20The%20OTP%20for%20registration%20is%20"
					+ otac + "&format=json&custom=1,2&flash=0";
		}
		

		ClientResponse sms_response = Common.payconnectApiAcess(sms_url, "", "get");

		String sms_str = sms_response.getEntity(String.class);
		
		logger.info("SMS gateway URL for Login" + sms_url);

		String smsResponseStatus = String.valueOf(sms_response.getStatus());
		
		logger.info("SMS gateway URL Login" + smsResponseStatus);

		if (!smsResponseStatus.equals("200")) {
			String errorMsg = this.getServerErrorMsg(smsResponseStatus);
			logger.info(errorMsg);
			return errorMsg;
		}

		return successMessage;
	}

	// To generate server error msg

	public String getServerErrorMsg(String errorCode) {
		HashMap<String, String> errorMap = new HashMap<String, String>();

		errorMap.put("400", error400);
		errorMap.put("401", error401);
		errorMap.put("402", error402);
		errorMap.put("403", error403);
		errorMap.put("404", error404);
		errorMap.put("405", error405);
		errorMap.put("408", error408);
		errorMap.put("500", error500);
		errorMap.put("502", error502);
		errorMap.put("503", error503);
		errorMap.put("504", error504);

		if (errorMap.get(errorCode) != null) {
			return (String) errorMap.get(errorCode);
		} else {
			return errorOther;
		}

	}
}
