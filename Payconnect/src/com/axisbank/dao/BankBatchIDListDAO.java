/**
 * 
 */
package com.axisbank.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;

import com.axisbank.connection.PayproProcedure11GDBConnection;
import com.axisbank.entity.BankBatchIDRequest;
import com.axisbank.entity.BankBatchIDInformation;
import com.axisbank.entity.BankBatchIDList;


/**
 * @author Avishek Seal
 *
 */
@Repository
public class BankBatchIDListDAO extends BankBatchIDList{
	
	private final Logger logger = Logger
			.getLogger(BankBatchIDListDAO.class);

    private static final long serialVersionUID = -3658148569903414268L;

 
    private static final String SQL = "Call axis_payconnect_pkg.axis_bank_batchid_list(:p_version,"
    	+ ":P_CORP_cODE,"
    	+ ":p_user_id,"
    	+ ":p_start_date,"
    	+ ":p_end_date,"
    	+ ":cur_batch_id,"
    	+ ":p_response_code,"
    	+ ":p_response_details,"
    	+ ":p_error)";
    
    /**
     * this method is used to fetch bank batch ID list executing procedure
     * @since 17-Feb-2016
     * @author Avishek Seal
     * @param request
     * @return
     * @throws SQLException
     */
    public BankBatchIDList getBankBatchIDList(BankBatchIDRequest request) throws SQLException{
	@SuppressWarnings("deprecation")
	//Connection connection = getFactory().getCurrentSession().connection();
	
	Connection connection=PayproProcedure11GDBConnection.getConnection();
	
	
	CallableStatement callableStatement = connection.prepareCall(SQL);
	
	callableStatement.setObject("p_version", request.getVersion());
	callableStatement.setObject("P_CORP_cODE", request.getCorporateCode());
	callableStatement.setObject("p_user_id", request.getLoginID());
	callableStatement.setObject("p_start_date", request.getStartDate());
	callableStatement.setObject("p_end_date", request.getEndDate());
	
	
	callableStatement.registerOutParameter("cur_batch_id", OracleTypes.CURSOR);
	callableStatement.registerOutParameter("p_response_code", Types.VARCHAR);
	callableStatement.registerOutParameter("p_response_details", Types.VARCHAR);
	callableStatement.registerOutParameter("p_error", Types.VARCHAR);
	
	callableStatement.executeQuery();
	
	ResultSet resultSet = (ResultSet) callableStatement.getObject("cur_batch_id");
	
	logger.info(" Bank batch procedure response code "+callableStatement.getString("p_response_code"));
	
	logger.info(" Bank batch procedure response details "+callableStatement.getString("p_response_details"));
	
	logger.info(" Bank batch procedure error is "+ callableStatement.getString("p_error"));
	
	final List<BankBatchIDInformation> bankBatchIDInformations = new ArrayList<>();
	
	while (resultSet.next()) {
		try{
			
		
			   final BankBatchIDInformation bankBatchIDInformation = new BankBatchIDInformation();
			   
			   logger.info("BANKBATCHID "+resultSet.getString("BANKBATCHID"));
			   
			   logger.info(" CORPBATCHID "+resultSet.getString("CORPBATCHID"));
			   
			    bankBatchIDInformation.setBankBatchNumber(resultSet.getString("BANKBATCHID"));
			    
			    bankBatchIDInformation.setCorpBatchNumber(resultSet.getString("CORPBATCHID"));
			    
			    bankBatchIDInformations.add(bankBatchIDInformation);
			    
		}
		
		catch(Exception e){
			
			e.printStackTrace();
			
			logger.info("inside Bank batch procedure exception"+e);
			logger.info("inside Bank batch procedure exception "+e.getMessage());
		}
		
	   
	}
	
	final BankBatchIDList bankBatchIDList = new BankBatchIDList();
	
	bankBatchIDList.setBankBatchIDInformations(bankBatchIDInformations);
	bankBatchIDList.setError(callableStatement.getString("p_error"));
	bankBatchIDList.setResponseCode(callableStatement.getString("p_response_code"));
	bankBatchIDList.setResponseDetails(callableStatement.getString("p_response_details"));
	
	return bankBatchIDList;
    }
}
