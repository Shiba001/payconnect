/**
 * 
 */
package com.axisbank.dao;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;

import com.axisbank.connection.PayproProcedure11GDBConnection;
import com.axisbank.entity.ECollectionInformation;
import com.axisbank.entity.ECollectionReport;
import com.axisbank.entity.ECollectionRequest;
import com.common.Common;
import com.sun.jersey.api.client.ClientResponse;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class ECollectionReportDAO extends ECollectionReport {

    private static final long serialVersionUID = -8090193768012207497L;
    
    private final Logger logger = Logger
			.getLogger(ECollectionReportDAO.class);
    
    @Value("${axis.payconnect.api.path}")
    private String apiUrl;
    @Value("${error.400}")
  	private String error400;

  	@Value("${error.401}")
  	private String error401;

  	@Value("${error.402}")
  	private String error402;

  	@Value("${error.403}")
  	private String error403;

  	@Value("${error.404}")
  	private String error404;

  	@Value("${error.405}")
  	private String error405;

  	@Value("${error.408}")
  	private String error408;

  	@Value("${error.500}")
  	private String error500;

  	@Value("${error.502}")
  	private String error502;

  	@Value("${error.503}")
  	private String error503;

  	@Value("${error.504}")
  	private String error504;

  	@Value("${error.other}")
  	private String errorOther;
    
    private static final String SQL = "Call axis_PAYCONNECT_pkg.axis_mob_apps_ecollection("
                                	+ ":P_VERSION,"
                                	+ ":p_corp_code,"
                                	+ ":p_loginid,"
                                	+ ":p_product_name,"
                                	+ ":p_start_date,"
                                	+ ":p_end_date,"
                                	+ ":p_rec_per_page,"
                                	+ ":p_page_num,"
                                	+ ":p_corp_acc_num,"
                                	+ ":p_amount_from,"
                                	+ ":p_amount_to,"
                                	+ ":p_transactional_utr,"
                                	+ ":p_total_count,"
                                	+ ":rec_iwd_txn_details,"
                                	+ ":p_response_code,"
                                	+ ":p_response_details,"
                                	+ ":p_pdf_download_link,"
                                	+ ":p_excel_download_link,"
                                	+ ":p_error)";
    
 
    
    /**
     * this method is used to get E Collection report from execution of procedure
     * @since 17-Feb-2016
     * @author Avishek Seal
     * @param request
     * @return
     * @throws SQLException
     */
    public ECollectionReport getECollectionReport(ECollectionRequest request) throws SQLException{
	
    Connection connection=PayproProcedure11GDBConnection.getConnection();
    	
	CallableStatement callableStatement = connection.prepareCall(SQL);
	callableStatement.setObject("P_VERSION", "");
	callableStatement.setObject("p_corp_code", request.getCorporateCode());
	callableStatement.setObject("p_loginid", request.getLogingID());
	callableStatement.setObject("p_product_name", request.getProductName());
	callableStatement.setObject("p_start_date", request.getStartDate());
	callableStatement.setObject("p_end_date", request.getEndDate());
	callableStatement.setObject("p_rec_per_page", request.getRecordPerPage());
	callableStatement.setObject("p_page_num", request.getPageNumber());
	
	callableStatement.setObject("p_corp_acc_num", request.getCorporateAccountNumber());
	callableStatement.setObject("p_amount_from", request.getAmmountFrom());
	callableStatement.setObject("p_amount_to", request.getAmmountTo());
	callableStatement.setObject("p_transactional_utr", request.getTransactionalUTR());
	
	callableStatement.registerOutParameter("p_total_count", OracleTypes.NUMBER);
	callableStatement.registerOutParameter("rec_iwd_txn_details", OracleTypes.CURSOR);
	callableStatement.registerOutParameter("p_response_code", Types.VARCHAR);
	callableStatement.registerOutParameter("p_response_details", Types.VARCHAR);
	callableStatement.registerOutParameter("p_pdf_download_link", Types.VARCHAR);
	callableStatement.registerOutParameter("p_excel_download_link", Types.VARCHAR);
	callableStatement.registerOutParameter("p_error", Types.VARCHAR);
	
	callableStatement.executeQuery();
	
	logger.info("axis_mob_apps_ecollection procedure response code "+callableStatement.getString("p_response_code"));
	
	logger.info(" axis_mob_apps_ecollection response details "+callableStatement.getString("p_response_details"));
	
	logger.info("axis_mob_apps_ecollection p_error "+ callableStatement.getString("p_error"));
	
	logger.info("axis_mob_apps_ecollection p_excel_download_link "+callableStatement.getString("p_excel_download_link"));
	
	logger.info(" axis_mob_apps_ecollection p_pdf_download_link "+callableStatement.getString("p_pdf_download_link"));
	
	logger.info(" axis_mob_apps_ecollection p_total_count "+callableStatement.getString("p_total_count"));
	
	
	
	ResultSet resultSet = (ResultSet) callableStatement.getObject("rec_iwd_txn_details");
	
	final List<ECollectionInformation> eCollectionInformations = new ArrayList<>();
	
	while(resultSet.next()) {
	    final ECollectionInformation collectionInformation = new ECollectionInformation();
	    collectionInformation.setCorporateActualAccountNumber(resultSet.getString("CORP_ACTUAL_ACCOUNT_NO"));
	    collectionInformation.setSenderInformation(resultSet.getString("SENDER_INFORMATION"));
	    collectionInformation.setSenderName(resultSet.getString("SENDER_NAME"));
	    collectionInformation.setSerialNumber(resultSet.getString("RN"));
	    collectionInformation.setTransactionAmount(resultSet.getString("TRANSACTION_AMOUNT"));
	    collectionInformation.setTransactionDate(resultSet.getString("TRANSACTION_DATE"));
	    collectionInformation.setUtrNumber(resultSet.getString("UTR_NO"));
	    collectionInformation.setPaymentMode(resultSet.getString("PAYMENT_MODE"));
	    
	    eCollectionInformations.add(collectionInformation);
	}
	
	final ECollectionReport collectionReport = new ECollectionReport();
	
	collectionReport.seteCollectionInformations(eCollectionInformations);
	collectionReport.setError(callableStatement.getString("p_error"));
	collectionReport.setExcelDownloadLink(callableStatement.getString("p_excel_download_link"));
	collectionReport.setPdfDownloadLink(callableStatement.getString("p_pdf_download_link"));
	collectionReport.setResponseCode(callableStatement.getString("p_response_code"));
	collectionReport.setResponseDetails(callableStatement.getString("p_response_details"));
	collectionReport.setTotalCount(callableStatement.getInt("p_total_count"));
	
	return collectionReport;
    }
    
    
    
    /**
     * this method is used to fetch eCollectionReport for power access application request
     * @author Satya Karanam
     * @since 24-Feb-2016
     * @param eCollectionRequest
     * @return
     * @throws JSONException
     * @throws ParseException
     */
    public ECollectionReport getECollectionReportForPowerAccess(ECollectionRequest eCollectionRequest) throws JSONException, ParseException {
    	
        final String URL = apiUrl+"getEcollectionReport";
		
		final String inputString = eCollectionRequest.toJsonString();
		
		logger.info("input passes in axis getEcollectionReport api "+inputString);
		
		
		final ClientResponse clientResponse = Common.payconnectApiAcess(URL, inputString, "post");
		
		final String responseStatus = String.valueOf(clientResponse.getStatus());
		
		logger.info("getEcollectionReport api response from axis "+responseStatus);
		
		
		final JSONParser jsonParser = new JSONParser();
		
		System.out.println("axis end response status "+responseStatus );
		
		
		
		if(StringUtils.equals(responseStatus, "200")) {
			
			final String jsonString = clientResponse.getEntity(String.class);
			
			final JSONObject jsonObject = JSONObject.class.cast(jsonParser.parse(jsonString));
			
			System.out.println("axis end json is "+jsonObject.toString());
			
			logger.info("getEcollectionReport api response from axis end"+jsonObject.toString());
			
			String ecollectionReportList=jsonObject.get("ECollectionReports").toString();
			
			if(StringUtils.isNotBlank(ecollectionReportList)){
				
				final ECollectionReport eCollectionReport = new ECollectionReport();
				
				
				eCollectionReport.setResponseCode(jsonObject.get("ResponseCode").toString());
				eCollectionReport.setResponseDetails(jsonObject.get("ResponseDeatils").toString());
				eCollectionReport.setTotalCount(Integer.parseInt(jsonObject.get("TotalCount").toString()));
				eCollectionReport.setPdfDownloadLink(jsonObject.get("PdfDownloadLink").toString());
				eCollectionReport.setExcelDownloadLink(jsonObject.get("ExcelDownloadLink").toString());
				
				final JSONArray array = JSONArray.class.cast(jsonObject.get("ECollectionReports"));
				
				final List<ECollectionInformation> eCollectionInformations = new ArrayList<>();
				
				if(array.size() > 0) {
				    for(int i = 0; i < array.size(); i++) {
					final JSONObject object = JSONObject.class.cast(array.get(i)) ;

					final ECollectionInformation eCollectionInformation = new ECollectionInformation();
					
					eCollectionInformation.setSerialNumber(object.get("slno").toString());
					eCollectionInformation.setPaymentMode(object.get("paymentmode").toString());
					eCollectionInformation.setTransactionDate(object.get("transactiondate").toString());
					eCollectionInformation.setTransactionAmount(object.get("transactionamount").toString());
					eCollectionInformation.setSenderName(object.get("sendername").toString());
					eCollectionInformation.setUtrNumber(object.get("UTRno").toString());
					eCollectionInformation.setCorporateActualAccountNumber(object.get("corparateaccountno").toString());
					eCollectionInformation.setSenderInformation(object.get("senderinfo").toString());
						
					eCollectionInformations.add(eCollectionInformation);
				    }
				}
				
				eCollectionReport.seteCollectionInformations(eCollectionInformations);
				
				return eCollectionReport;
			}
			
			
			else{
				
				final ECollectionReport collectionReport = new ECollectionReport();
				
				final List<ECollectionInformation> collectionInformations = new ArrayList<>();
				
				collectionReport.setResponseCode(jsonObject.get("ResponseCode").toString());
				collectionReport.setResponseDetails(jsonObject.get("ResponseDeatils").toString());
				collectionReport.setTotalCount(Integer.parseInt(jsonObject.get("TotalCount").toString()));
				collectionReport.setPdfDownloadLink(jsonObject.get("PdfDownloadLink").toString());
				collectionReport.setExcelDownloadLink(jsonObject.get("ExcelDownloadLink").toString());
				
				collectionReport.seteCollectionInformations(collectionInformations);
				
				
				return collectionReport;
			}
			
				
			}
		
			
		
		
		else {
			
			final ECollectionReport eCollectionReport = new ECollectionReport();
			
			String errorMsg = this.getServerErrorMsg(responseStatus);
			
			logger.info("getEcollectionReport error response from axis api"+errorMsg);
			
			eCollectionReport.setError(errorMsg);
			
			return eCollectionReport;
		}
    }
    
    
    
    
    // To generate server error msg

    	public String getServerErrorMsg(String errorCode) {
    		HashMap<String, String> errorMap = new HashMap<String, String>();

    		errorMap.put("400", error400);
    		errorMap.put("401", error401);
    		errorMap.put("402", error402);
    		errorMap.put("403", error403);
    		errorMap.put("404", error404);
    		errorMap.put("405", error405);
    		errorMap.put("408", error408);
    		errorMap.put("500", error500);
    		errorMap.put("502", error502);
    		errorMap.put("503", error503);
    		errorMap.put("504", error504);

    		if (errorMap.get(errorCode) != null) {
    			return (String) errorMap.get(errorCode);
    		} else {
    			return errorOther;
    		}

    	}

    
}
