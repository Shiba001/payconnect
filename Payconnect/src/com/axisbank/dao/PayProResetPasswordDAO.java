package com.axisbank.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.axisbank.connection.PayproProcedure11GDBConnection;
import com.axisbank.entity.Request;
import com.axisbank.entity.Response;
import com.axisbank.entity.UserLoginDetails;
import com.common.Common;

@Repository
public class PayProResetPasswordDAO extends Response{

	/**
	 * 
	 */
	
	private final Logger logger = Logger.getLogger(PayProResetPasswordDAO.class);
	
	private static final long serialVersionUID = 3733637746356314187L;

   private static final String RESET_PASSWORD_SQL = "Call AXIS_PAYCONNECT_PKG.PWD_CHANGE_REQUEST(:P_API_VERSION,"
   		    + ":p_corp_code,"
			+ " :p_user_name," 
			+ " :p_old_password,"
			+ " :p_new_password,"
			+ " :p_device_id,"
			+ " :p_response_code,"
			+ " :p_response_details,"
			+ " :cur_user_login_details,"
			+ " :P_ERROR)";
   
   
   	/*public PayProResetPasswordDAO() {
		super(Response.class);
	}
*/
   /**
    * this method is used to update password
    * @since 14-Jan-2016
    * @author Avishek Seal
    * @param request
    * @return
    * @throws SQLException
    */
   public Response getResetPasswordResponse(Request request) throws Exception{
		@SuppressWarnings("deprecation")
		
		//Connection connection = getFactory().getCurrentSession().connection();
		
		Connection connection=PayproProcedure11GDBConnection.getConnection();
		
		CallableStatement callableStatement = connection.prepareCall(RESET_PASSWORD_SQL);
		callableStatement.setString("P_API_VERSION", request.getApiVersion());
		callableStatement.setString("p_corp_code", request.getCorporateCode());
		callableStatement.setString("p_user_name", request.getUserName());
		callableStatement.setString("p_old_password ", request.getOldPassword());
		callableStatement.setString("p_new_password", request.getPassword());
		callableStatement.setString("p_device_id", request.getDeviceId());
		
		callableStatement.registerOutParameter("p_response_code", OracleTypes.VARCHAR);
		callableStatement.registerOutParameter("p_response_details", OracleTypes.VARCHAR);
		callableStatement.registerOutParameter("cur_user_login_details",  OracleTypes.CURSOR);
		callableStatement.registerOutParameter("P_ERROR", OracleTypes.VARCHAR);
		//callableStatement.registerOutParameter("P_USER_STATUS", oracle.jdbc.driver.OracleTypes.VARCHAR);
		
		callableStatement.executeQuery();
		
		final Response response = new Response();
		
		response.setResponseCode(callableStatement.getString("p_response_code"));
		response.setResponseDetails(callableStatement.getString("p_response_details"));
		//response.setUserStatus(callableStatement.getString("P_USER_STATUS"));
		response.setError(callableStatement.getString("P_ERROR"));
		
		
		logger.info(" PayProResetPassword procedure response code "+callableStatement.getString("p_response_code"));
		
		logger.info(" PayProResetPassword response details "+callableStatement.getString("p_response_details"));
		
		logger.info(" PayProResetPassword procedure error is "+ callableStatement.getString("P_ERROR"));
		
		
		ResultSet resultSet = null;
		
		try {
			resultSet = (ResultSet) callableStatement.getObject("CUR_USER_LOGIN_DETAILS");
		} catch (Exception e) {
			
		}
		
		final List<UserLoginDetails> userLoginDetails = new ArrayList<UserLoginDetails>();
		
		if(resultSet != null) {
			while(resultSet.next()){
			    final UserLoginDetails loginDetails = new UserLoginDetails();
			    
			    try{
			    	String username=resultSet.getString("USERNAME");
			    	
			    	  String[]userNames=username.split("~");
					  
						
					  String firstName=userNames[0];
					  
					  String lastName=userNames[1];
					  
					  
					  
					  
	               String passwordExpDate=resultSet.getString("PWDEXPIRYDATE");
					  
					  
					  String[]splitLastPasswordExpDate=passwordExpDate.split(" ");
					  
					  String passwordExpairyDate=splitLastPasswordExpDate[0];
					  
					 
					  
					  
					  
					  String lastPasswordChangeDate=resultSet.getString("LASTCHANGEPASSWORDDATE");
					  
					  
					  String[]splitLastPasswordChangeDate=lastPasswordChangeDate.split(" ");
					  
					  String lastPwdChangeDate=splitLastPasswordChangeDate[0];
					  
					 
					  
					  
	                 String lastLoginDate=resultSet.getString("lastlogin");
	                 
	                 System.out.println("last login date "+lastLoginDate);
	                
	                 
					  
					  
					  String[]splitLastLoginDate=lastLoginDate.split(" ");
					  
					  String lastLogonDate=splitLastLoginDate[0];
					  
					  
					  loginDetails.setFirstName(firstName);
					  
					  loginDetails.setLastname(lastName);
					  
				
					  
					  loginDetails.setMobileNumber(resultSet.getString("MOBILENO"));
					  loginDetails.setSessionToken(resultSet.getString("SESSIONTOKEN"));
					  loginDetails.setSenderShortCode(resultSet.getString("SENDERSHORTCODE"));
					  loginDetails.setSenderID(resultSet.getString("SENDERID"));
					  loginDetails.setEmailID(resultSet.getString("EMAILID"));
					  loginDetails.setLastChangePaswordDate(Common.formatDateFromAxis(lastPwdChangeDate));
					  loginDetails.setPasswordExpiryDate(Common.formatDateFromAxis(passwordExpairyDate));
					  loginDetails.setLastLogin(Common.formatDateFromAxis(lastLogonDate));
					  loginDetails.setUserStatus(resultSet.getString("USER_STATUS"));
					  loginDetails.setCorporateID(resultSet.getString("CORPID"));
					  loginDetails.setCorporateID(resultSet.getString("USERID"));
				      loginDetails.setUserId(resultSet.getString("PWDCHANGEINWEB"));
				      loginDetails.setUserType(resultSet.getString("USERTYPE"));
				
				userLoginDetails.add(loginDetails);
			    }catch(Exception e){
				  e.printStackTrace();
			    }
			}
		}
		
		
		
		response.setUserLoginDetails(userLoginDetails);
		
		return response;
   }

}
