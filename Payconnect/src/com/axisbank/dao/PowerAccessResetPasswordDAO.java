package com.axisbank.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.axisbank.entity.Response;
import com.axisbank.entity.UserLoginDetails;
import com.common.Common;
import com.payconnect.model.ResetPasswordModel;
import com.sun.jersey.api.client.ClientResponse;

@Repository
public class PowerAccessResetPasswordDAO {
	
	private Logger logger = Logger.getLogger(PowerAccessResetPasswordDAO.class);
	
	
	@Value("${axis.payconnect.api.path}")
	private String apiUrlPayconnect;

	@Value("${error.400}")
	private String error400;

	@Value("${error.401}")
	private String error401;

	@Value("${error.402}")
	private String error402;

	@Value("${error.403}")
	private String error403;

	@Value("${error.404}")
	private String error404;

	@Value("${error.405}")
	private String error405;

	@Value("${error.408}")
	private String error408;

	@Value("${error.500}")
	private String error500;

	@Value("${error.502}")
	private String error502;

	@Value("${error.503}")
	private String error503;

	@Value("${error.504}")
	private String error504;

	@Value("${error.other}")
	private String errorOther;
	
	
	
	
	public Response getResetPasswordResponse(ResetPasswordModel resetPasswordModel) throws Exception{
		
		final Response response=new Response();

		String URL = apiUrlPayconnect+"changepassword";
		
		String inputString = resetPasswordModel.toApiInputString();
		
		logger.info("input string passes to axis api changepassword " + inputString);

		ClientResponse client_response = Common.payconnectApiAcess(URL, inputString, "post");

		String responseStatus = String.valueOf(client_response.getStatus());
		
		logger.info("api response status form axis for login " + responseStatus);
		
		System.out.println("api response status form axis for login "+responseStatus);
		
		
		

		JSONParser parser = new JSONParser();

		

		if (StringUtils.equals(String.valueOf(client_response.getStatus()), "200")) {
			
			final UserLoginDetails userLoginDetails=new UserLoginDetails();
			
			final List<UserLoginDetails>loginDetails=new ArrayList<UserLoginDetails>();
			
			String jsonString = client_response.getEntity(String.class);
			
			JSONObject jsonObject = JSONObject.class.cast(parser.parse(jsonString));
			
			System.out.println("api response status form axis for login"+jsonObject.toString() );
			
			logger.info("api response status form axis for login " + jsonObject.toString());
			
			
			
			if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("ResponseCode")))) {
				response.setResponseCode(String.valueOf(jsonObject.get("ResponseCode")));
			}

			if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("ResponseDetails")))) {
				response.setResponseDetails(String.valueOf(jsonObject.get("ResponseDetails")));
			}
			
			if(StringUtils.equals(response.getResponseCode(), "200")){
				
				
				
				if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("UserStatus")))) {
					 response.setUserStatus(String.valueOf(jsonObject.get("UserStatus")));
				}
				
				
				if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("UserID")))) {
					userLoginDetails.setUserId(String.valueOf(jsonObject.get("UserID")));
				}

				if (StringUtils.isNotBlank(String.valueOf(jsonObject.get("MobileNo")))) {
					userLoginDetails.setMobileNumber(String.valueOf(jsonObject.get("MobileNo")));
				}
			
				
				
				
				
					
					  String username=String.valueOf(jsonObject.get("UserName"));
					  
					
					  String[]userNames=username.split("~");
					  
					   String firstName=userNames[0];
					  
					  String lastName=userNames[1];
					  
						userLoginDetails.setUserName(firstName+" "+lastName);
						
						userLoginDetails.setFirstName(firstName);
						
						userLoginDetails.setLastname(lastName);
					  
					 
				   userLoginDetails.setCorporateID(String.valueOf(jsonObject.get("CorpID")));
								
						
						
				   userLoginDetails.setEmailID(String.valueOf(jsonObject.get("EmailID")));
				   
				   userLoginDetails.setLastChangePaswordDate(String.valueOf(jsonObject.get("LastPswChangeDate")));
				   
				   userLoginDetails.setPasswordExpiryDate(String.valueOf(jsonObject.get("PwdExpiryDate")));
				   
				   userLoginDetails.setLastLogin(String.valueOf(jsonObject.get("LastLogonDate")));
				   
				   
				   
				   userLoginDetails.setUserType(String.valueOf(jsonObject.get("UserType")));
					
				   userLoginDetails.setSenderShortCode(jsonObject.get("SenderShortCode").toString());
					
				   userLoginDetails.setSenderID("");
				   
				   loginDetails.add(userLoginDetails);
				   
				   
				   response.setUserLoginDetails(loginDetails);
				
			}
			
		

				

			return response;
		}

		else {
			
			System.out.println("inside else part !!!!");
			final Response errorResponse=new Response();
			String errorMsg = this.getServerErrorMsg(responseStatus);
			response.setError(errorMsg);
			return errorResponse;
		}

		
	}
	
	
	
	
	// To generate server error msg

		public String getServerErrorMsg(String errorCode) {
			HashMap<String, String> errorMap = new HashMap<String, String>();

			errorMap.put("400", error400);
			errorMap.put("401", error401);
			errorMap.put("402", error402);
			errorMap.put("403", error403);
			errorMap.put("404", error404);
			errorMap.put("405", error405);
			errorMap.put("408", error408);
			errorMap.put("500", error500);
			errorMap.put("502", error502);
			errorMap.put("503", error503);
			errorMap.put("504", error504);

			if (errorMap.get(errorCode) != null) {
				return (String) errorMap.get(errorCode);
			} else {
				return errorOther;
			}

		}
	
	

}
