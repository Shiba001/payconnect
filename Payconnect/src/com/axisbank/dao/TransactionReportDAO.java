package com.axisbank.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.axisbank.connection.PayproProcedure11GDBConnection;
import com.axisbank.entity.TransactionInformation;
import com.axisbank.entity.TransactionReport;
import com.axisbank.entity.TransactionReportRequest;
import com.common.Common;
import com.sun.jersey.api.client.ClientResponse;

/**
 * 
 * @author Satya Karanam
 * 17-Feb-2016
 */

@Repository
public class TransactionReportDAO extends TransactionReport{

	/**
	 * 
	 */
	
	private final Logger logger = Logger
			.getLogger(TransactionReportDAO.class);
	
	private static final long serialVersionUID = 4661395344063167716L;
	
    @Value("${axis.payconnect.api.path}")
    private String apiUrl;
    
    @Value("${error.400}")
  	private String error400;

  	@Value("${error.401}")
  	private String error401;

  	@Value("${error.402}")
  	private String error402;

  	@Value("${error.403}")
  	private String error403;

  	@Value("${error.404}")
  	private String error404;

  	@Value("${error.405}")
  	private String error405;

  	@Value("${error.408}")
  	private String error408;

  	@Value("${error.500}")
  	private String error500;

  	@Value("${error.502}")
  	private String error502;

  	@Value("${error.503}")
  	private String error503;

  	@Value("${error.504}")
  	private String error504;

  	@Value("${error.other}")
  	private String errorOther;
	
    private static final String SQL = "Call axis_PAYCONNECT_pkg.axis_mob_transaction_report("
						+ ":p_version,"
                    	+ ":p_user_id,"
                    	+ ":p_loginid,"
                    	+ ":p_page_no,"
                    	+ ":p_rec_per_page,"
                    	+ ":p_start_date,"
                    	+ ":p_end_date,"
                    	+ ":p_product_code,"
                    	+ ":p_status_code,"
                    	+ ":p_file_name,"
                    	+ ":p_batch_id,"
                    	+ ":p_corp_acc_num,"
                    	+ ":p_amount_from,"
                    	+ ":p_amount_to,"
                    	+ ":p_vendor_code,"
                    	+ ":p_benefi_name,"
                    	+ ":p_benefi_acc_num,"
                    	+ ":p_benefi_ifsc_code,"
                    	+ ":p_transaction_id,"
                    	+ ":p_transaction_utr,"
                    	+ ":p_pay_doc_number,"
                    	+ ":p_cheque_no,"
                    	+ ":p_response_code,"
                    	+ ":p_response_details,"
                    	+ ":p_total_count,"
                    	+ ":p_pdf_download_link,"
                    	+ ":p_excel_download_link,"
                    	+ ":p_error,"
                    	+ ":rec_bank_rep)";


    /**
     * @param entityClass
     */


	public TransactionReport getTransactionList(TransactionReportRequest transactionReportRequest) throws SQLException
	{
	
		@SuppressWarnings("deprecation")
		//Connection connection = getFactory().getCurrentSession().connection();
		
		Connection connection=PayproProcedure11GDBConnection.getConnection();
		CallableStatement callableStatement = connection.prepareCall(SQL);
		callableStatement.setObject("p_version", transactionReportRequest.getVersion());
		callableStatement.setObject("p_user_id", transactionReportRequest.getCorporateID());
		callableStatement.setObject("p_loginid", transactionReportRequest.getLoginID());
		callableStatement.setObject("p_page_no", transactionReportRequest.getPageNo());
		callableStatement.setObject("p_rec_per_page", transactionReportRequest.getRecPerPage());
		callableStatement.setObject("p_start_date", transactionReportRequest.getStartDate());
		callableStatement.setObject("p_end_date", transactionReportRequest.getEndDate());
		callableStatement.setObject("p_product_code", transactionReportRequest.getProductCode());
		callableStatement.setObject("p_status_code", transactionReportRequest.getStatusCode());
		callableStatement.setObject("p_file_name", transactionReportRequest.getFileName());
		callableStatement.setObject("p_batch_id", transactionReportRequest.getBatchId());
		callableStatement.setObject("p_corp_acc_num", transactionReportRequest.getCorpAccountNumber());
		callableStatement.setObject("p_amount_from", transactionReportRequest.getAmountFrom());
		callableStatement.setObject("p_amount_to", transactionReportRequest.getAmountTo());
		callableStatement.setObject("p_vendor_code", transactionReportRequest.getVendorCode());
		callableStatement.setObject("p_benefi_name", transactionReportRequest.getBenificiaryName());
		callableStatement.setObject("p_benefi_acc_num", transactionReportRequest.getBenificiaryAccountNumber());
		callableStatement.setObject("p_benefi_ifsc_code", transactionReportRequest.getBenificiaryIfscCode());
		callableStatement.setObject("p_transaction_id", transactionReportRequest.getTransactionId());
		callableStatement.setObject("p_transaction_utr", transactionReportRequest.getTransactionUtr());
		callableStatement.setObject("p_pay_doc_number", transactionReportRequest.getPayDocNumber());
		callableStatement.setObject("p_cheque_no", transactionReportRequest.getChequeNumber());
	
		callableStatement.registerOutParameter("p_response_code", java.sql.Types.VARCHAR);
		callableStatement.registerOutParameter("p_response_details", java.sql.Types.VARCHAR);
		callableStatement.registerOutParameter("p_total_count", OracleTypes.NUMBER);
		callableStatement.registerOutParameter("p_pdf_download_link", java.sql.Types.VARCHAR);
		callableStatement.registerOutParameter("p_excel_download_link", java.sql.Types.VARCHAR);
		callableStatement.registerOutParameter("p_error", java.sql.Types.VARCHAR);
		callableStatement.registerOutParameter("rec_bank_rep", OracleTypes.CURSOR);
		
		callableStatement.executeQuery();
		
		ResultSet resultSet = (ResultSet) callableStatement.getObject("rec_bank_rep");
		
		final List<TransactionInformation> transactions = new ArrayList<>();
		
		while(resultSet.next()) {
		    final TransactionInformation transaction = new TransactionInformation();
		   
		    transaction.setBeneficiaryName(resultSet.getString("BENEFICIARY_NAME"));
		    transaction.setCorporateAccount(resultSet.getString("CORPORATE_ACCOUNT"));
		    transaction.setCustomerReferenceNumber(resultSet.getString("CUSTOMER_REFERENCE_NUMBER"));
		    transaction.setPayableAmount(resultSet.getString("PAYABLEAMOUNT"));
		    transaction.setPaymentMethod(resultSet.getString("PAYMENT_METHOD"));
		    transaction.setSerialNumber(resultSet.getString("SRLNUM"));
		    transaction.setSystemRequestStatus(resultSet.getString("SYSTEM_REQUEST_STATUS"));
		    transaction.setTransactionDescription(resultSet.getString("TRANSACTION_DESCRIPTION"));
		    transaction.setTransactionId(resultSet.getString("TRANSACTIONID"));
		    transaction.setTransactionUTROrCheque(resultSet.getString("TRANSACTION_UTR"));

		    transactions.add(transaction);
		}

		final TransactionReport transactreport = new TransactionReport();
		
		transactreport.setTransactionInformation(transactions);
		transactreport.setExcelDownloadLink(callableStatement.getString("p_excel_download_link"));
		transactreport.setPdfDownloadLink(callableStatement.getString("p_pdf_download_link"));
		transactreport.setResponseCode(callableStatement.getString("p_response_code"));
		transactreport.setResponseDetails(callableStatement.getString("p_response_details"));
		transactreport.setTotalCount(callableStatement.getInt("p_total_count"));
		
		
        logger.info(" axis_mob_transaction_report procedure p_excel_download_link "+callableStatement.getString("p_excel_download_link"));
		
		logger.info(" axis_mob_transaction_report procedure p_pdf_download_link "+callableStatement.getString("p_pdf_download_link"));
		
		logger.info(" axis_mob_transaction_report procedure p_response_code "+callableStatement.getString("p_response_code"));
		
		logger.info(" axis_mob_transaction_report procedure response details "+callableStatement.getString("p_response_details"));
		
		logger.info(" axis_mob_transaction_report procedure p_total_count is "+ callableStatement.getString("p_total_count"));
		
		
		
		return transactreport;
	}
	
	/**
	 * this method is used to fetch transaction report for power access application request
	 * @author Satya Karanam, Avishek Seal
	 * @since 24-02-2016
	 * @param transactionReportRequest
	 * @return
	 * @throws JSONException
	 * @throws ParseException
	 */
	public TransactionReport getTransactionReportForPowerAccess(TransactionReportRequest transactionReportRequest) throws JSONException, ParseException {
		
		final String URL = apiUrl+"getTransactionReportList";
		
		final String inputString = transactionReportRequest.toJsonString();
		
		System.out.println("axis input string "+ inputString);
		
		logger.info("input string passes to  getTransactionReportList api " + inputString);
		
		final ClientResponse clientResponse = Common.payconnectApiAcess(URL, inputString, "post");
		
		final String responseStatus = String.valueOf(clientResponse.getStatus());
		
		logger.info( "responseStatus from axis getTransactionReportList api " + responseStatus);
		
		
		final String jsonString = clientResponse.getEntity(String.class);
		
		final JSONParser jsonParser = new JSONParser();
		
		if(StringUtils.equals(responseStatus, "200")) {
			
			final JSONObject jsonObject = JSONObject.class.cast(jsonParser.parse(jsonString));
			
			System.out.println("from axis end "+jsonObject.toString());
			
			logger.info("axis getTransactionReportList api json response " + jsonObject.toString());
		
			String transactionReportListDb=jsonObject.get("CUR_Filter_Transaction_Report_list_DB").toString();
			
			if(StringUtils.isNotEmpty(transactionReportListDb)){
				
				final TransactionReport transactionReport = new TransactionReport();
				
				transactionReport.setResponseCode(jsonObject.get("ResponseCode").toString());
				transactionReport.setResponseDetails(jsonObject.get("Responsedetails").toString());
				transactionReport.setTotalCount(Integer.parseInt(jsonObject.get("TotalCount").toString()));
				transactionReport.setPdfDownloadLink(jsonObject.get("PdfDownloadLink").toString());
				transactionReport.setExcelDownloadLink(jsonObject.get("ExcelDownloadLink").toString());
							
				final JSONArray array = JSONArray.class.cast(jsonObject.get("CUR_Filter_Transaction_Report_list_DB"));
				
				final List<TransactionInformation> transactionInformations = new ArrayList<>();
				
				  if(array.size() > 0) {
				      for(int i = 0; i < array.size(); i++) {
					  final JSONObject object = JSONObject.class.cast(array.get(i));
					  
					  final TransactionInformation transactionInformation = new TransactionInformation();
						    
					  transactionInformation.setSerialNumber(object.get("SlNo").toString());
					  transactionInformation.setTransactionId(object.get("TransactionId").toString());
					  transactionInformation.setBeneficiaryName(object.get("BenificiaryName").toString());
					  transactionInformation.setPaymentMethod(object.get("ProductName").toString());
					  transactionInformation.setCustomerReferenceNumber(object.get("CorporateRef").toString());
					  transactionInformation.setCorporateAccount(object.get("CorporateAccount").toString());
					  transactionInformation.setPayableAmount(object.get("TransactionAmount").toString());
					  transactionInformation.setTransactionDescription(object.get("TransactionDescription").toString());
					  transactionInformation.setSystemRequestStatus(object.get("TransactionStatus").toString());
					  transactionInformation.setTransactionUTROrCheque(object.get("TransactionUTROrCheque").toString());//key name remove from axis api
					  
					  transactionInformations.add(transactionInformation);
					  
				      }
				  }
				
				transactionReport.setTransactionInformation(transactionInformations);
				
				return transactionReport;
				
				
				
			}
			
			else{
				
				final TransactionReport report = new TransactionReport();
				
				final List<TransactionInformation> informations = new ArrayList<>();
				
				
				report.setResponseCode(jsonObject.get("ResponseCode").toString());
				report.setResponseDetails(jsonObject.get("Responsedetails").toString());
				report.setTotalCount(Integer.parseInt(jsonObject.get("TotalCount").toString()));
				report.setPdfDownloadLink(jsonObject.get("PdfDownloadLink").toString());
				report.setExcelDownloadLink(jsonObject.get("ExcelDownloadLink").toString());
				
				report.setTransactionInformation(informations);
				
				
				
				return report;
				
			}
			
			
		}
		
		
		
		
		
		else {
			final TransactionReport transactionReport = new TransactionReport();
			
			String errorMsg = this.getServerErrorMsg(responseStatus);
			
			logger.info("axis getTransactionReportList api error message from Axis " + errorMsg);
			
			transactionReport.setError(errorMsg);
			
			return transactionReport;
		}
		
	}
	
	
	
	   
	 // To generate server error msg

	 	public String getServerErrorMsg(String errorCode) {
	 		HashMap<String, String> errorMap = new HashMap<String, String>();

	 		errorMap.put("400", error400);
	 		errorMap.put("401", error401);
	 		errorMap.put("402", error402);
	 		errorMap.put("403", error403);
	 		errorMap.put("404", error404);
	 		errorMap.put("405", error405);
	 		errorMap.put("408", error408);
	 		errorMap.put("500", error500);
	 		errorMap.put("502", error502);
	 		errorMap.put("503", error503);
	 		errorMap.put("504", error504);

	 		if (errorMap.get(errorCode) != null) {
	 			return (String) errorMap.get(errorCode);
	 		} else {
	 			return errorOther;
	 		}

	 	}

	
}