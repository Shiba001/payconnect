package com.axisbank.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.axisbank.connection.PayproProcedure11GDBConnection;
import com.axisbank.entity.Request;
import com.axisbank.entity.Response;
import com.axisbank.entity.UserLoginDetails;
import com.common.Common;

@Repository
public class ResponseDAO extends Response {
	
	private final Logger logger = Logger
			.getLogger(ResponseDAO.class);

	private static final long serialVersionUID = -4976198051613661398L;
	
	private static final String LOGIN_SQL = "Call axis_payconnect_pkg.axis_user_login(:P_api_version,"
			+ ":p_corp_code,"
			+ ":p_user_name,"
			+ ":p_password,"
			+ ":p_device_id,"
			+ ":p_response_code,"
			+ ":p_response_details,"
			+ ":cur_user_login_details,"
			+ ":P_ERROR,"+":P_ip_address,"+":P_LATTITUDE,"+":P_longitude)";

	
	
	public Response getLoginResponse(Request request) throws SQLException{
		
		Connection connection=PayproProcedure11GDBConnection.getConnection();
		
		System.out.println("inside login paypro db connection from axis connection "+connection);
		
		logger.info("inside login paypro db connection from axis connection   "+connection);
		
		CallableStatement callableStatement = connection.prepareCall(LOGIN_SQL);
		callableStatement.setObject("P_api_version", request.getApiVersion());
		callableStatement.setObject("p_corp_code", request.getCorporateCode());
		callableStatement.setObject("p_user_name", request.getUserName());
		callableStatement.setObject("p_password", request.getPassword());
		callableStatement.setObject("p_device_id", request.getDeviceId());
		
		
		callableStatement.registerOutParameter("p_response_code", java.sql.Types.VARCHAR);
		callableStatement.registerOutParameter("p_response_details", java.sql.Types.VARCHAR);
		callableStatement.registerOutParameter("cur_user_login_details", OracleTypes.CURSOR);
		callableStatement.registerOutParameter("P_ERROR", java.sql.Types.VARCHAR);
		
		callableStatement.setObject("P_ip_address", request.getIpAddress());
		callableStatement.setObject("P_LATTITUDE", request.getLatitude());
		callableStatement.setObject("P_longitude", request.getLongitude());
		
		callableStatement.executeQuery();
		
		ResultSet resultSet = (ResultSet) callableStatement.getObject("CUR_USER_LOGIN_DETAILS");
		
		
		final Response response = new Response();
		
		final List<UserLoginDetails> userLoginDetails = new ArrayList<UserLoginDetails>();
		
		
		System.out.println(" response code "+callableStatement.getString("p_response_code"));
		
		System.out.println("response details "+callableStatement.getString("p_response_details"));
		
		System.out.println(" error is "+ callableStatement.getString("p_error"));
		
		
		logger.info(" Paypro login procedure response code from axis procedure"+callableStatement.getString("p_response_code"));
		
		logger.info(" Paypro login procedure response details from axis procedure "+callableStatement.getString("p_response_details"));
		
		logger.info("Paypro login procedure error is from axis procedure "+ callableStatement.getString("p_error"));
		
		
		
		response.setResponseCode(callableStatement.getString("p_response_code"));
		
			
		response.setResponseDetails(callableStatement.getString("p_response_details"));
		
		
		response.setError(callableStatement.getString("p_error"));
			
		while(resultSet.next()){
			
			logger.info("inside paypro login procedure  CUR_USER_LOGIN_DETAILS cursor");
			
			System.out.println("inside paypro login procedure  CUR_USER_LOGIN_DETAILS cursor");
			
			final UserLoginDetails loginDetails = new UserLoginDetails();
			
			try{
				
				 String username=resultSet.getString("username");
				 
				logger.info("paypro login procedure username "+ username);
					
				  String[]userNames=username.split("~");
				  
				
				  String firstName=userNames[0];
				  
				  logger.info("paypro login procedure first name "+ firstName);
				  
				  String lastName=userNames[1];
				  
				  logger.info("paypro login procedure lastName "+ lastName);
				  
				  
				  String passwordExpDate=resultSet.getString("PwdExpiryDate");
				  
				  logger.info("paypro login procedure passwordExpDate "+ passwordExpDate);
				  
				  
				  String[]splitLastPasswordExpDate=passwordExpDate.split(" ");
				  
				  String passwordExpairyDate=splitLastPasswordExpDate[0];
				  
				  logger.info("paypro login procedure formated passwordExpairyDate "+ passwordExpairyDate);
				  
				  String lastPasswordChangeDate=resultSet.getString("LASTCHANGEPASSWORDDATE");
				  
				  logger.info("paypro login procedure lastPasswordChangeDate "+ lastPasswordChangeDate);
				  
				
				  String[]splitLastPasswordChangeDate=lastPasswordChangeDate.split(" ");
				  
				  String lastPwdChangeDate=splitLastPasswordChangeDate[0];
				  
				  
				  logger.info("paypro login procedure formated lastPwdChangeDate "+ lastPwdChangeDate);
				  
				  
                 String lastLoginDate=resultSet.getString("lastlogin");
                 
                 System.out.println("paypro login procedure formated lastLoginDate "+ lastLoginDate);
                 
                 logger.info("paypro login procedure formated lastLoginDate "+ lastLoginDate);
                 
                 logger.info("paypro login procedure emailid "+ resultSet.getString("emailid"));
                 
                 logger.info("paypro login procedure  mobileno "+ resultSet.getString("mobileno"));
                 
                 logger.info("paypro login procedure  SenderId "+ resultSet.getString("SenderId"));
                 
                 logger.info("paypro login procedure  SENDERSHORTCODE "+ resultSet.getString("SENDERSHORTCODE"));
                 
                 logger.info("paypro login procedure  USER_STATUS "+ resultSet.getString("USER_STATUS"));
                 
                 logger.info("paypro login procedure  USERTYPE "+ resultSet.getString("USERTYPE"));
                 
                 logger.info("paypro login procedure  USERID "+ resultSet.getString("USERID"));
                 
                 logger.info("paypro login procedure  CORPID "+ resultSet.getString("CORPID"));
                 
                 logger.info("paypro login procedure  PWDCHANGEINWEB "+ resultSet.getString("PWDCHANGEINWEB"));
                 
                 logger.info("paypro login procedure  PWDCHANGENEED "+ resultSet.getString("PWDCHANGENEED"));
                 
               
                 
                
				loginDetails.setEmailID(resultSet.getString("emailid"));
				loginDetails.setLastChangePaswordDate(Common.formatDateFromAxis(lastPwdChangeDate));
				loginDetails.setLastLogin(lastLoginDate);
				loginDetails.setMobileNumber(resultSet.getString("mobileno"));
				loginDetails.setPasswordExpiryDate(Common.formatDateFromAxis(passwordExpairyDate));
				loginDetails.setSenderID(resultSet.getString("SenderId"));
				loginDetails.setSenderShortCode(resultSet.getString("SENDERSHORTCODE"));
				loginDetails.setSessionToken(resultSet.getString("sessiontoken"));
				loginDetails.setUserName(firstName +" "+lastName);
				loginDetails.setFirstName(firstName);
				loginDetails.setLastname(lastName);
				loginDetails.setUserStatus(resultSet.getString("USER_STATUS"));
				loginDetails.setUserId(resultSet.getString("USERID"));
			    loginDetails.setUserType(resultSet.getString("USERTYPE"));
				loginDetails.setCorporateID(resultSet.getString("CORPID"));
				loginDetails.setPasswordChangeInWeb(resultSet.getString("PWDCHANGEINWEB"));
				loginDetails.setPasswordChangeNeed(resultSet.getString("PWDCHANGENEED"));
				
				
				
				userLoginDetails.add(loginDetails);
			}catch(Exception e){
				e.printStackTrace();
				logger.info("paypro login procedure exception"+e);
				logger.info("paypro login procedure exception "+e.getMessage());
			}
		}
		
		response.setUserLoginDetails(userLoginDetails);
		
		return response;
	}
}
