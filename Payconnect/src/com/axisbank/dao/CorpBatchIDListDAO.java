package com.axisbank.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.axisbank.connection.PayproProcedure11GDBConnection;
import com.axisbank.entity.CorpBatchIDInformation;
import com.axisbank.entity.CorpBatchIDList;
import com.axisbank.entity.CorpBatchIDRequest;


@Repository
public class CorpBatchIDListDAO extends CorpBatchIDList {
	
	private final Logger logger = Logger
			.getLogger(CorpBatchIDListDAO.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 5721906618311803941L;

	
	
	
	 private static final String SQL = "Call axis_payconnect_pkg.axis_corp_batchid_list(:p_version,"
		    	+ ":P_CORP_cODE,"
		    	+ ":p_user_id,"
		    	+ ":p_start_date,"
		    	+ ":p_end_date,"
		    	+ ":cur_file_name,"
		    	+ ":p_response_code,"
		    	+ ":p_response_details,"
		    	+ ":p_error)";
		    
		    /**
		     * this method is used to fetch corp batch ID list executing procedure
		     * @since 17-Feb-2016
		     * @param request
		     * @return
		     * @throws SQLException
		     */
		    public CorpBatchIDList getCorpBatchIDList(CorpBatchIDRequest request) throws SQLException{
	
			//Connection connection = getFactory().getCurrentSession().connection();
			
			Connection connection=PayproProcedure11GDBConnection.getConnection();
			
			CallableStatement callableStatement = connection.prepareCall(SQL);
			
			callableStatement.setObject("p_version", request.getVersion());
			callableStatement.setObject("P_CORP_cODE", request.getCorporateCode());
			callableStatement.setObject("p_user_id", request.getUserId());
			callableStatement.setObject("p_start_date", request.getStartDate());
			callableStatement.setObject("p_end_date", request.getEndDate());
			
			callableStatement.registerOutParameter("cur_file_name", OracleTypes.CURSOR);
			callableStatement.registerOutParameter("p_response_code", Types.VARCHAR);
			callableStatement.registerOutParameter("p_response_details", Types.VARCHAR);
			callableStatement.registerOutParameter("p_error", Types.VARCHAR);
			
			callableStatement.executeQuery();
			
			

			logger.info(" Corp batch procedure response code "+callableStatement.getString("p_response_code"));
			
			logger.info(" Corp batch procedure response details "+callableStatement.getString("p_response_details"));
			
			logger.info(" Corp batch procedure error is "+ callableStatement.getString("p_error"));
			
			
			
			
			ResultSet resultSet = (ResultSet) callableStatement.getObject("cur_file_name");
			
			final List<CorpBatchIDInformation> corpBatchIDInformations = new ArrayList<>();
			
			while (resultSet.next()) {
				try{
					  final CorpBatchIDInformation corpBatchIDInformation = new CorpBatchIDInformation();
					  
					  logger.info(" Corp batch procedure CORPBATCHID "+resultSet.getString("CORPBATCHID"));
					  
					  logger.info(" Corp batch procedure CREATIONDATE "+resultSet.getString("CREATIONDATE"));
					  
					    corpBatchIDInformation.setCorpBatchNumber(resultSet.getString("CORPBATCHID"));
					    corpBatchIDInformation.setCreationDate(resultSet.getString("CREATIONDATE"));
					    corpBatchIDInformations.add(corpBatchIDInformation);
				}
				catch(Exception e){
					e.printStackTrace();
					logger.error("Corp batch error is"+e);
				}
			  
			 
			}
			
			final CorpBatchIDList corpBatchIDList = new CorpBatchIDList();
			
			corpBatchIDList.setBatchIDInformations(corpBatchIDInformations);
			corpBatchIDList.setError(callableStatement.getString("p_error"));
			corpBatchIDList.setResponseCode(callableStatement.getString("p_response_code"));
			corpBatchIDList.setResponseDetails(callableStatement.getString("p_response_details"));
			
			return corpBatchIDList;
			
		    }
	

}
