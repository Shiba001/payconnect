/**
 * 
 */
package com.axisbank.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.axisbank.connection.PayproProcedure11GDBConnection;
import com.axisbank.entity.StatementEnquery;
import com.axisbank.entity.StatementEnqueryRequest;
import com.axisbank.entity.StatementInformation;
import com.common.Common;
import com.sun.jersey.api.client.ClientResponse;

/**
 * @author Avishek Seal
 *
 */

@Repository
public class StatementEnqueryDAO extends StatementEnquery {

    private static final long serialVersionUID = 1976735693007399615L;
    
    private final Logger logger = Logger
			.getLogger(StatementEnqueryDAO.class);
    
    @Value("${axis.payconnect.api.path}")
    private String apiUrl;
    
    @Value("${error.400}")
	private String error400;

	@Value("${error.401}")
	private String error401;

	@Value("${error.402}")
	private String error402;

	@Value("${error.403}")
	private String error403;

	@Value("${error.404}")
	private String error404;

	@Value("${error.405}")
	private String error405;

	@Value("${error.408}")
	private String error408;

	@Value("${error.500}")
	private String error500;

	@Value("${error.502}")
	private String error502;

	@Value("${error.503}")
	private String error503;

	@Value("${error.504}")
	private String error504;

	@Value("${error.other}")
	private String errorOther;
    
    private static final String SQL = "Call axis_payconnect_pkg.axis_statement_enquiry("
    					+ ":p_version,"
                                	+ ":p_corp_code,"
                                	+ ":p_user_id,"
                                	+ ":p_corp_acc_num,"
                                	+ ":p_start_date,"
                                	+ ":p_end_date,"
                                	+ ":p_stmnt_type,"
                                	+ ":p_rec_per_page,"
                                	+ ":p_page_num,"
                                	+ ":p_file_type,"
                                	+ ":rec_stmnt_enqry,"
                                	+ ":p_cust_id,"
                                	+ ":p_cust_name,"
                                	+ ":p_cust_address,"
                                	+ ":p_schm_desc,"
                                	+ ":p_currency,"
                                	+ ":p_total_rec_cnt,"
                                	+ ":p_response_code,"
                                	+ ":p_response_details,"
                                	+ ":p_download_link,"
                                	+ ":p_message,"
                                	+ ":p_error)";
    
    /**
     * @param entityClass
     */
   
    /**
     * this method is used to fetch list of statement inquiry 
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param statementEnqueryRequest
     * @return
     * @throws SQLException
     */
    public StatementEnquery getListOfStatementEnquery(StatementEnqueryRequest statementEnqueryRequest) throws Exception{

	//Connection connection = getFactory().getCurrentSession().connection();
	
	Connection connection=PayproProcedure11GDBConnection.getConnection();
	
	CallableStatement callableStatement = connection.prepareCall(SQL);
	
	callableStatement.setObject("p_version", "");
	callableStatement.setObject("p_corp_code", statementEnqueryRequest.getCorporateCode());
	callableStatement.setObject("p_user_id", statementEnqueryRequest.getUserID());
	callableStatement.setObject("p_corp_acc_num", statementEnqueryRequest.getCorporateAccountNumber());
	callableStatement.setObject("p_start_date", statementEnqueryRequest.getStartDate());
	callableStatement.setObject("p_end_date", statementEnqueryRequest.getEndDate());
	callableStatement.registerOutParameter("p_stmnt_type", Types.VARCHAR);
	callableStatement.setObject("p_stmnt_type", statementEnqueryRequest.getStatementType());
	callableStatement.setObject("p_rec_per_page", statementEnqueryRequest.getRecordPerPage());
	callableStatement.setObject("p_page_num", statementEnqueryRequest.getPageNumber());
	callableStatement.registerOutParameter("p_file_type", OracleTypes.VARCHAR);
	callableStatement.setObject("p_file_type", "");
	
	callableStatement.registerOutParameter("rec_stmnt_enqry", OracleTypes.CURSOR);
	callableStatement.registerOutParameter("p_cust_id", OracleTypes.NUMBER);
	callableStatement.registerOutParameter("p_cust_name", Types.VARCHAR);
	callableStatement.registerOutParameter("p_cust_address", Types.VARCHAR);
	callableStatement.registerOutParameter("p_schm_desc", OracleTypes.VARCHAR);
	callableStatement.registerOutParameter("p_currency", Types.VARCHAR);
	callableStatement.registerOutParameter("p_total_rec_cnt", OracleTypes.NUMBER);
	callableStatement.registerOutParameter("p_response_code", Types.VARCHAR);
	callableStatement.registerOutParameter("p_response_details", Types.VARCHAR);
	callableStatement.registerOutParameter("p_download_link", Types.VARCHAR);
	callableStatement.registerOutParameter("p_message", Types.VARCHAR);
	callableStatement.registerOutParameter("p_error", Types.VARCHAR);

	
	callableStatement.executeQuery();
	
	final StatementEnquery enquery = new StatementEnquery();
	
	enquery.setCurrency(callableStatement.getString("p_currency"));
	enquery.setCustomerAddress(callableStatement.getString("p_cust_address"));
	enquery.setCustomerID(callableStatement.getInt("p_cust_id"));
	enquery.setCustomerName(callableStatement.getString("p_cust_name"));
	enquery.setDownloadLink(callableStatement.getString("p_download_link"));
	enquery.setError(callableStatement.getString("p_error"));
	enquery.setFileType(callableStatement.getString("p_file_type"));
	enquery.setMessage(callableStatement.getString("p_message"));
	enquery.setResponseCode(callableStatement.getString("p_response_code"));
	enquery.setResponseDeatils(callableStatement.getString("p_response_details"));
	enquery.setSchemeDescription(callableStatement.getString("p_schm_desc"));
	enquery.setStatementType(callableStatement.getString("p_stmnt_type"));
	enquery.setTotalRecordCount(callableStatement.getInt("p_total_rec_cnt"));
	
	
     logger.info(" paypro StatementEnquery procedure p_currency "+callableStatement.getString("p_currency"));
	
	logger.info(" paypro StatementEnquery procedure p_cust_address "+callableStatement.getString("p_cust_address"));
	
	logger.info(" paypro StatementEnquery procedure p_cust_id is "+ callableStatement.getString("p_cust_id"));
	
    logger.info(" paypro StatementEnquery procedure p_cust_name  "+callableStatement.getString("p_cust_name"));
	
	logger.info(" paypro StatementEnquery procedure p_download_link "+callableStatement.getString("p_download_link"));
	
	logger.info(" paypro StatementEnquery procedure p_file_type "+ callableStatement.getString("p_file_type"));
	
    logger.info(" paypro StatementEnquery procedure p_message "+callableStatement.getString("p_message"));
	
	logger.info(" paypro StatementEnquery procedure p_schm_desc "+callableStatement.getString("p_schm_desc"));
	
	logger.info(" paypro StatementEnquery procedure p_stmnt_type "+ callableStatement.getString("p_stmnt_type"));
	
    logger.info(" paypro StatementEnquery procedure p_total_rec_cnt "+callableStatement.getString("p_total_rec_cnt"));
	
	logger.info(" paypro StatementEnquery procedure p_response_details "+callableStatement.getString("p_response_details"));
	
	logger.info(" paypro StatementEnquery procedure p_response_code "+ callableStatement.getString("p_response_code"));
	
	logger.info(" paypro StatementEnquery procedure p_error "+ callableStatement.getString("p_error"));
	
	
	final List<StatementInformation> informations = new ArrayList<>();
	
	try{
		if(callableStatement.getObject("rec_stmnt_enqry") != null) {
			ResultSet resultSet = (ResultSet) callableStatement.getObject("rec_stmnt_enqry");

			while(resultSet.next()) {
			    final StatementInformation information = new StatementInformation();
			    
			    information.setBalanceAmount(resultSet.getString("BALANCE"));
			    information.setChequeNumber(resultSet.getString("CHQNO"));
			    information.setCreditBalance(resultSet.getString("CRTXN"));
			    information.setDebitBalance(resultSet.getString("DRTXN"));
			    information.setiSol(resultSet.getString("ISOL"));
			    information.setParticulars(resultSet.getString("PARTICULARS"));
			    information.setSerialiNumber(resultSet.getString("SRLNUM"));
			    information.setTransactionDate(resultSet.getString("TDT"));
			    
			    informations.add(information);
			}
		}
	}catch(Exception e){
		logger.info("paypro StatementEnquery procedure exception "+e);
		logger.info("paypro StatementEnquery procedure exception message "+e.getMessage());
	}
	
	enquery.setStatementInformations(informations);
	
	return enquery;
    }
    
    /**
     * this method is used to fetch statement enquery report for power access applicationo for a particular request
     * @since 24-Feb-2016
     * @author Avishek Seal
     * @param statementEnqueryRequest
     * @return
     * @throws JSONException
     * @throws ParseException
     */
    public StatementEnquery getStatementEnqueryForPowerAccess(StatementEnqueryRequest statementEnqueryRequest) throws JSONException, ParseException{
    	
	final String URL = apiUrl+"getStatementEnquiry";
	
	final String inputString = statementEnqueryRequest.toJsonString();
	
	logger.info("power access getStatementEnquiry input passes to axis api "+ inputString);
	
	System.out.println("power access getStatementEnquiry input passes to axis api "+ inputString);
	
	final ClientResponse clientResponse = Common.payconnectApiAcess(URL, inputString, "post");
	
	final String responseStatus = String.valueOf(clientResponse.getStatus());
	
	System.out.println("response status from axis end "+responseStatus);
	
	logger.info("power access getStatementEnquiry api response status from axis  "+ responseStatus);
	
	
	final JSONParser jsonParser = new JSONParser();
	
	
	 if(StringUtils.equals(responseStatus, "200")) {
		
		final String jsonString = clientResponse.getEntity(String.class);
		
		
		final JSONObject jsonObject = JSONObject.class.cast(jsonParser.parse(jsonString));
		
		System.out.println("axis end json response is" +jsonObject.toString());
		
		logger.info("power access getStatementEnquiry api json response from axis"+ jsonObject.toString());
		
		String statementInfoList=jsonObject.get("StatementInfo").toString();
		
		if(StringUtils.isNotEmpty(statementInfoList)){
			
			
			   final StatementEnquery enquery = new StatementEnquery();
			    
			    enquery.setCurrency(jsonObject.get("Currency"));
			    enquery.setCustomerAddress(jsonObject.get("CustomerAddress"));
			    enquery.setCustomerID(jsonObject.get("CustomerID"));
			    enquery.setCustomerName(jsonObject.get("CustomerName"));
			    enquery.setFileType(jsonObject.get("Filetype"));
			    enquery.setResponseCode(jsonObject.get("ResponseCode"));
			    enquery.setResponseDeatils( jsonObject.get("ResponseDeatils").toString());
			    enquery.setSchemeDescription(jsonObject.get("SchmDesc"));
			    enquery.setStatementType(jsonObject.get("StatementFormat"));
			    enquery.setTotalRecordCount(jsonObject.get("Totalcount"));
			    enquery.setMessage(jsonObject.get("Message").toString());
			    
			    
			    
			    final List<StatementInformation> informations = new ArrayList<>();
			    
			   
			    	
			    	final JSONArray array = JSONArray.class.cast(jsonObject.get("StatementInfo"));
				    
				    if(array.size() > 0) {
						for(int i = 0; i < array.size(); i++) {
						    final JSONObject object = JSONObject.class.cast(array.get(i));
						    final StatementInformation statementInformation = new StatementInformation();
						    
						    statementInformation.setBalanceAmount(object.get("balanceammount"));
						    statementInformation.setChequeNumber(object.get("chequeNo"));
						    statementInformation.setCreditBalance(object.get("credit"));
						    statementInformation.setDebitBalance(object.get("debit"));
						    statementInformation.setiSol(object.get("sol"));
						    statementInformation.setParticulars(object.get("particulars"));
						    statementInformation.setSerialiNumber(object.get("slno"));
						    statementInformation.setTransactionDate(object.get("transctiondate"));
						    
						    informations.add(statementInformation);
						}
				    }
			    
			    
			    enquery.setStatementInformations(informations);
			    
			    return enquery;
			
			
		}
		
		
		else{
			
			 final StatementEnquery statementEnquery = new StatementEnquery();
			 
			 final List<StatementInformation> statementInformations = new ArrayList<>();
			 
			
			  
			 statementEnquery.setFileType(jsonObject.get("Filetype"));
			 statementEnquery.setResponseCode(jsonObject.get("ResponseCode"));
			 statementEnquery.setResponseDeatils( jsonObject.get("ResponseDeatils").toString());
			 statementEnquery.setSchemeDescription(jsonObject.get("SchmDesc"));
			 statementEnquery.setStatementType(jsonObject.get("StatementFormat"));
			 statementEnquery.setMessage(jsonObject.get("Message").toString());
			
			 statementEnquery.setStatementInformations(statementInformations);  
			 
		
			
			
			return statementEnquery;
			
		}
		

	}
	
	

	
	
	else {
		
		
		 final StatementEnquery enquery = new StatementEnquery();
		 
		 String errorMsg = this.getServerErrorMsg(responseStatus);
		 
		 logger.info("power access getStatementEnquiry api error message from axis api"+errorMsg);
		 
		 enquery.setError(errorMsg);
		 
		 return enquery;
	}
	
	
    }
    
    
    
 // To generate server error msg

 	public String getServerErrorMsg(String errorCode) {
 		HashMap<String, String> errorMap = new HashMap<String, String>();

 		errorMap.put("400", error400);
 		errorMap.put("401", error401);
 		errorMap.put("402", error402);
 		errorMap.put("403", error403);
 		errorMap.put("404", error404);
 		errorMap.put("405", error405);
 		errorMap.put("408", error408);
 		errorMap.put("500", error500);
 		errorMap.put("502", error502);
 		errorMap.put("503", error503);
 		errorMap.put("504", error504);

 		if (errorMap.get(errorCode) != null) {
 			return (String) errorMap.get(errorCode);
 		} else {
 			return errorOther;
 		}

 	}

}