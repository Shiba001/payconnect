/**
 * 
 */
package com.axisbank.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author Avishek Seal
 *
 */
public interface PayproSQLDateFormat {
    String SE_DATE_PATTERN = "dd/MMM/yyyy";
    DateFormat SE_DATE_FORMAT = new SimpleDateFormat(SE_DATE_PATTERN);
    
    String EC_DATE_PATTERN = "dd-MM-yyyy";
    DateFormat EC_DATE_FORMAT = new SimpleDateFormat(EC_DATE_PATTERN);
    
    String BB_DATE_PATTERN = "dd/MM/yyyy";
    DateFormat BB_DATE_FORMAT = new SimpleDateFormat(EC_DATE_PATTERN);
}
