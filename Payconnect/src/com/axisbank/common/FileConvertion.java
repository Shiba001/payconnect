/**
 * 
 */
package com.axisbank.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author Avishek Seal
 *
 */
@Component
public class FileConvertion {

    /**
     * this method is used to convert list of objects to excel file
     * 
     * @since 22-Feb-2016
     * @author Avishek Seal
     * @param data
     * @param fileName
     */
    public void toExcel(List<List<Object>> data, String fileName) {
	final XSSFWorkbook workbook = new XSSFWorkbook();

	final XSSFSheet sheet = workbook.createSheet("Employee Data");

	int rownum = 0;
	for (List<Object> rowData : data) {
	    Row row = sheet.createRow(rownum++);

	    int cellnum = 0;
	    for (Object obj : rowData) {
		Cell cell = row.createCell(cellnum++);

		if (obj instanceof String) {
		    cell.setCellValue((String) obj);
		} else if (obj instanceof Integer) {
		    cell.setCellValue((Integer) obj);
		} else if (obj instanceof Double) {
		    cell.setCellValue((Double) obj);
		}

	    }
	}

	try (FileOutputStream out = new FileOutputStream(new File(fileName))) {
	    workbook.write(out);
	    out.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * this method is used to create PDF table from list of objects
     * @since 22-Feb-2016
     * @author Avishek Seal
     * @param data
     * @param fileName
     * @param reportName
     */
    public void toPDF(List<List<Object>> data, String fileName, String reportName) {
	if(!CollectionUtils.isEmpty(data) && !CollectionUtils.isEmpty(data.get(0))) {
	    final Integer columnNumber = data.get(0).size();
		
		final PdfPTable pdfPTable = new PdfPTable(columnNumber);  
		
		pdfPTable.setWidthPercentage(100);
		
		final Font font = new Font(FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.WHITE);
		
		final PdfPCell cell = new PdfPCell(new Phrase(reportName, font));
	        
		cell.setColspan(columnNumber);
	        headerCellStyle(cell);
	        pdfPTable.addCell(cell);
	        
	        final List<Object> tableHeader = data.get(0);
	        
	        for(Object celldata : tableHeader) {
	            pdfPTable.addCell(createTableHeaderCell(celldata.toString()));
	        }
	        
	        data.remove(0);
	        
	        int i = 0;
	        
	        for(List<Object> rowData : data) {
	            for(Object celldata : rowData) {
		        	if(i%2 == 0) {
		        	    pdfPTable.addCell(createLabelCell(String.valueOf(celldata)));
		        	} else {
		        	    pdfPTable.addCell(createValueCell(String.valueOf(celldata)));
		        	}
	            }
	            
	            i++;
	        }
	        
	        try(OutputStream outputStream = new FileOutputStream(new File(fileName))) {
	            final Document document = new Document();
	            document.setPageSize(PageSize.A4);
	     
	            PdfWriter.getInstance(document, outputStream);

	            document.open();
	            document.add(pdfPTable);
	            document.close();
	        } catch (Exception e) {
		    e.printStackTrace();
		}
	}
    }

    private PdfPCell createLabelCell(String text) {
	Font font = new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	PdfPCell cell = new PdfPCell(new Phrase(text, font));
	labelCellStyle(cell);
	
	return cell;
    }

    private PdfPCell createTableHeaderCell(String text) {
	Font font = new Font(FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.WHITE);
	PdfPCell cell = new PdfPCell(new Phrase(text, font));
	headerLabelCellStyle(cell);
	
	return cell;
    }
    
    private PdfPCell createValueCell(String text) {
	Font font = new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	PdfPCell cell = new PdfPCell(new Phrase(text, font));
	valueCellStyle(cell);
	
	return cell;
    }

    private void headerCellStyle(PdfPCell cell) {
	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	cell.setPaddingTop(0f);
	cell.setPaddingBottom(7f);
	cell.setBackgroundColor(new BaseColor(0, 121, 182));
	cell.setBorder(0);
	cell.setBorderWidthBottom(2f);
    }

    private void headerLabelCellStyle(PdfPCell cell) {
	cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	cell.setPaddingLeft(3f);
	cell.setPaddingTop(0f);
	cell.setBackgroundColor(BaseColor.DARK_GRAY);
	cell.setBorder(0);
	cell.setBorderWidthBottom(1);
	cell.setBorderColorBottom(BaseColor.GRAY);
	cell.setMinimumHeight(18f);
    }
    
    private void labelCellStyle(PdfPCell cell) {
	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	cell.setPaddingLeft(3f);
	cell.setPaddingTop(0f);
	cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
	cell.setBorder(0);
	cell.setBorderWidthBottom(1);
	cell.setBorderColorBottom(BaseColor.GRAY);
	cell.setMinimumHeight(18f);
    }

    public void valueCellStyle(PdfPCell cell) {
	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	cell.setPaddingTop(0f);
	cell.setPaddingBottom(5f);
	cell.setBorder(0);
	cell.setBorderWidthBottom(0.5f);
	cell.setMinimumHeight(18f);
    }
}
