/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.axisbank.common.PayproSQLDateFormat;

/**
 * @author Avishek Seal
 *
 */
public class ECollectionRequest implements Serializable, PayproSQLDateFormat {

	private static final long serialVersionUID = 4902760692160517235L;

	private String version;

	private String corporateCode;

	private String logingID;

	private String productName;

	private String startDate="";

	private String endDate="";

	private Integer recordPerPage;

	private Integer pageNumber;

	private String corporateAccountNumber;

	private String ammountFrom;

	private String ammountTo;

	private String transactionalUTR;

	/**
	 * Avishek Seal
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the corporateCode
	 */
	public String getCorporateCode() {
		return corporateCode;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param corporateCode
	 *            the corporateCode to set
	 */
	public void setCorporateCode(String corporateCode) {
		this.corporateCode = corporateCode;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the logingID
	 */
	public String getLogingID() {
		return logingID;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param logingID
	 *            the logingID to set
	 */
	public void setLogingID(String logingID) {
		this.logingID = logingID;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	


	/**
	 * Avishek Seal
	 * 
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		if(startDate!=null){
			this.startDate = EC_DATE_FORMAT.format(startDate);
		}
		
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate=endDate;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		
		if(endDate!=null){
			this.endDate=EC_DATE_FORMAT.format(endDate);
		}
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the recordPerPage
	 */
	public Integer getRecordPerPage() {
		return recordPerPage;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param recordPerPage
	 *            the recordPerPage to set
	 */
	public void setRecordPerPage(Integer recordPerPage) {
		this.recordPerPage = recordPerPage;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the pageNumber
	 */
	public Integer getPageNumber() {
		return pageNumber;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the corporateAccountNumber
	 */
	public String getCorporateAccountNumber() {
		return corporateAccountNumber;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param corporateAccountNumber
	 *            the corporateAccountNumber to set
	 */
	public void setCorporateAccountNumber(String corporateAccountNumber) {
		this.corporateAccountNumber = corporateAccountNumber;
	}

	/**
	 * Avishek Seal
	 * 
	 * @return the transactionalUTR
	 */
	
	
	
	
	public String getTransactionalUTR() {
		return transactionalUTR;
	}

	public String getAmmountFrom() {
		return ammountFrom;
	}

	public void setAmmountFrom(String ammountFrom) {
		this.ammountFrom = ammountFrom;
	}

	public String getAmmountTo() {
		return ammountTo;
	}

	public void setAmmountTo(String ammountTo) {
		this.ammountTo = ammountTo;
	}

	/**
	 * Avishek Seal
	 * 
	 * @param transactionalUTR
	 *            the transactionalUTR to set
	 */
	public void setTransactionalUTR(String transactionalUTR) {
		this.transactionalUTR = transactionalUTR;
	}

	public String toJsonString() throws JSONException {
		final JSONObject jsonObject = new JSONObject();

		jsonObject.put("ApiVersion", getVersion());
		jsonObject.put("CorpID", getCorporateCode());
		jsonObject.put("UserID", getLogingID());
		jsonObject.put("pagesize", getRecordPerPage());
		jsonObject.put("pageno", getPageNumber());
		jsonObject.put("startdate", getStartDate());
		jsonObject.put("enddate", getEndDate());
		jsonObject.put("UTRno", getTransactionalUTR());
		jsonObject.put("paymentmode", getProductName());
		jsonObject.put("transactionamountfrom", getAmmountFrom());
		jsonObject.put("startdate", getStartDate());
		jsonObject.put("transactionamountto", getAmmountTo());
		jsonObject.put("creditaccountno", getCorporateAccountNumber());
		jsonObject.put("System", 1);

		return jsonObject.toString();
	}
}