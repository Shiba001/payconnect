package com.axisbank.entity;

import java.io.Serializable;
import java.util.List;



import org.springframework.util.CollectionUtils;

public class CorpBatchIDList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 956137714441121119L;
	
	
	    private String responseCode;
	    
	    private String responseDetails;
	    
	    private String error;
	    
	    private List<CorpBatchIDInformation>batchIDInformations;
	    

		public String getResponseCode() {
			return responseCode;
		}

		public void setResponseCode(String responseCode) {
			this.responseCode = responseCode;
		}

		public String getResponseDetails() {
			return responseDetails;
		}

		public void setResponseDetails(String responseDetails) {
			this.responseDetails = responseDetails;
		}

		public String getError() {
			return error;
		}

		public void setError(String error) {
			this.error = error;
		}

		public List<CorpBatchIDInformation> getBatchIDInformations() {
			return batchIDInformations;
		}

		public void setBatchIDInformations(
				List<CorpBatchIDInformation> batchIDInformations) {
			this.batchIDInformations = batchIDInformations;
		}

		
		
	    
	
}
