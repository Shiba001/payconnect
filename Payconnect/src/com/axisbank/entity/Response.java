package com.axisbank.entity;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.CollectionUtils;

public class Response implements Serializable{

	private static final long serialVersionUID = 3302209848025526690L;

	private String responseCode;
	
	private String responseDetails;

	private String error;
	
	private String userStatus;
	
	private List<UserLoginDetails> userLoginDetails;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDetails() {
		return responseDetails;
	}

	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public UserLoginDetails getUserLoginDetails() {
		return CollectionUtils.isEmpty(userLoginDetails) ? null : userLoginDetails.get(0);
	}

	public void setUserLoginDetails(List<UserLoginDetails> userLoginDetails) {
		this.userLoginDetails = userLoginDetails;
	}

	@Override
	public String toString() {
		return "Response [responseCode=" + responseCode + ", responseDetails="
				+ responseDetails + ", error=" + error + ", userStatus="
				+ userStatus + ", userLoginDetails=" + userLoginDetails + "]";
	}
	
}
