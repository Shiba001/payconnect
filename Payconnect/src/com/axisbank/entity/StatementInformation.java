/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;

/**
 * @author Avishek Seal
 *
 */
public class StatementInformation implements Serializable{

    private static final long serialVersionUID = 5606996397254262133L;
    
    private String serialiNumber;
    
    private String transactionDate;
    
    private String chequeNumber;
    
    private String particulars;
    
    private String debitBalance;
    
    private String creditBalance;
    
    private String balanceAmount;
    
    private String iSol;

    public String getSerialiNumber() {
        return serialiNumber;
    }

    public void setSerialiNumber(Object serialiNumber) {
	if(serialiNumber != null) {
	    this.serialiNumber = serialiNumber.toString();
	}
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Object transactionDate) {
        if(transactionDate != null) {
            this.transactionDate = transactionDate.toString();
        }
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(Object chequeNumber) {
        if(chequeNumber != null) {
            this.chequeNumber = chequeNumber.toString();
        }
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(Object particulars) {
        if(particulars !=  null) {
            this.particulars = particulars.toString();
        }
    }

    public String getDebitBalance() {
        return debitBalance;
    }

    public void setDebitBalance(Object debitBalance) {
        if(debitBalance != null) {
            this.debitBalance = debitBalance.toString();
        }
    }

    public String getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(Object creditBalance) {
        if(creditBalance != null) {
            this.creditBalance = creditBalance.toString();
        }
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Object balanceAmount) {
        if(balanceAmount != null) {
            this.balanceAmount = balanceAmount.toString();
        }
    }

    public String getiSol() {
        return iSol;
    }

    public void setiSol(Object iSol) {
        if(iSol != null) {
            this.iSol = iSol.toString();
        }
    }

    /**
     * Avishek Seal
     * @param serialiNumber the serialiNumber to set
     */
    public void setSerialiNumber(String serialiNumber) {
        this.serialiNumber = serialiNumber;
    }

    /**
     * Avishek Seal
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * Avishek Seal
     * @param chequeNumber the chequeNumber to set
     */
    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    /**
     * Avishek Seal
     * @param particulars the particulars to set
     */
    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    /**
     * Avishek Seal
     * @param debitBalance the debitBalance to set
     */
    public void setDebitBalance(String debitBalance) {
        this.debitBalance = debitBalance;
    }

    /**
     * Avishek Seal
     * @param creditBalance the creditBalance to set
     */
    public void setCreditBalance(String creditBalance) {
        this.creditBalance = creditBalance;
    }

    /**
     * Avishek Seal
     * @param balanceAmount the balanceAmount to set
     */
    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    /**
     * Avishek Seal
     * @param iSol the iSol to set
     */
    public void setiSol(String iSol) {
        this.iSol = iSol;
    }
}
