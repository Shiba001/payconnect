package com.axisbank.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Satya Karanam 17-Feb-2016
 */
public class TransactionReport implements Serializable {

	private static final long serialVersionUID = -6946571355263154259L;

	private Integer totalCount;
	private String responseCode;
	private String responseDetails;
	private String pdfDownloadLink;
	private String excelDownloadLink;
	private String error;
	private List<TransactionInformation> transactionInformation;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDetails() {
		return responseDetails;
	}

	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}

	public String getPdfDownloadLink() {
		return pdfDownloadLink;
	}

	public void setPdfDownloadLink(String pdfDownloadLink) {
		this.pdfDownloadLink = pdfDownloadLink;
	}

	public String getExcelDownloadLink() {
		return excelDownloadLink;
	}

	public void setExcelDownloadLink(String excelDownloadLink) {
		this.excelDownloadLink = excelDownloadLink;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<TransactionInformation> getTransactionInformation() {
		return transactionInformation;
	}

	public void setTransactionInformation(
			List<TransactionInformation> transactionInformation) {
		this.transactionInformation = transactionInformation;
	}

}
