/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @author Avishek Seal
 *
 */
public class ECollectionReport implements Serializable {

    private static final long serialVersionUID = -2694172893429727555L;
    
    private Integer totalCount;
    
    private String responseCode;
    
    private String responseDetails;
    
    private String pdfDownloadLink;
    
    private String excelDownloadLink;
    
    private String error;
    
    private List<ECollectionInformation> eCollectionInformations;

    /**
     * Avishek Seal
     * @return the totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * Avishek Seal
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * Avishek Seal
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Avishek Seal
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Avishek Seal
     * @return the responseDetails
     */
    public String getResponseDetails() {
        return responseDetails;
    }

    /**
     * Avishek Seal
     * @param responseDetails the responseDetails to set
     */
    public void setResponseDetails(String responseDetails) {
        this.responseDetails = responseDetails;
    }

    /**
     * Avishek Seal
     * @return the pdfDownloadLink
     */
    public String getPdfDownloadLink() {
        return pdfDownloadLink;
    }

    /**
     * Avishek Seal
     * @param pdfDownloadLink the pdfDownloadLink to set
     */
    public void setPdfDownloadLink(String pdfDownloadLink) {
        this.pdfDownloadLink = pdfDownloadLink;
    }

    /**
     * Avishek Seal
     * @return the excelDownloadLink
     */
    public String getExcelDownloadLink() {
        return excelDownloadLink;
    }

    /**
     * Avishek Seal
     * @param excelDownloadLink the excelDownloadLink to set
     */
    public void setExcelDownloadLink(String excelDownloadLink) {
        this.excelDownloadLink = excelDownloadLink;
    }

    /**
     * Avishek Seal
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * Avishek Seal
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Avishek Seal
     * @return the eCollectionInformations
     */
    public List<ECollectionInformation> geteCollectionInformations() {
        return eCollectionInformations;
    }

    /**
     * Avishek Seal
     * @param eCollectionInformations the eCollectionInformations to set
     */
    public void seteCollectionInformations(List<ECollectionInformation> eCollectionInformations) {
        this.eCollectionInformations = eCollectionInformations;
    }
    
}
