/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;


public class BankBatchIDInformation implements Serializable{

    private static final long serialVersionUID = 544358657753917405L;
    
    private String bankBatchNumber;
    
    private String corpBatchNumber;
    
	public String getBankBatchNumber() {
		return bankBatchNumber;
	}
	public void setBankBatchNumber(String bankBatchNumber) {
		this.bankBatchNumber = bankBatchNumber;
	}
	public String getCorpBatchNumber() {
		return corpBatchNumber;
	}
	public void setCorpBatchNumber(String corpBatchNumber) {
		this.corpBatchNumber = corpBatchNumber;
	}
    
   
}
