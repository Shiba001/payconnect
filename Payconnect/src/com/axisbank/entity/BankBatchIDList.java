/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.CollectionUtils;

/**
 * @author Avishek Seal
 *
 */
public class BankBatchIDList implements Serializable{

    private static final long serialVersionUID = -1416561094417035835L;
    
    private String responseCode;
    
    private String responseDetails;
    
    private String error;
    
    private List<BankBatchIDInformation> bankBatchIDInformations;

    /**
     * Avishek Seal
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Avishek Seal
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Avishek Seal
     * @return the responseDetails
     */
    public String getResponseDetails() {
        return responseDetails;
    }

    /**
     * Avishek Seal
     * @param responseDetails the responseDetails to set
     */
    public void setResponseDetails(String responseDetails) {
        this.responseDetails = responseDetails;
    }

    /**
     * Avishek Seal
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * Avishek Seal
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

	public List<BankBatchIDInformation> getBankBatchIDInformations() {
		return bankBatchIDInformations;
	}

	public void setBankBatchIDInformations(
			List<BankBatchIDInformation> bankBatchIDInformations) {
		this.bankBatchIDInformations = bankBatchIDInformations;
	}

    /**
     * Avishek Seal
     * @return the bankBatchIDInformations
     */
   
    
    
    
  
}
