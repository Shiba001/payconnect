package com.axisbank.entity;

import java.io.Serializable;

/**
 * 
 * @author Satya Karanam 17-Feb-2016
 */
public class TransactionInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 679196293381713560L;
	private String serialNumber;
	private String transactionId;
	private String beneficiaryName;
	private String paymentMethod;
	private String customerReferenceNumber;
	private String corporateAccount;
	private String payableAmount;
	private String transactionDescription;
	private String systemRequestStatus;
	private String transactionUTROrCheque;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getCustomerReferenceNumber() {
		return customerReferenceNumber;
	}

	public void setCustomerReferenceNumber(String customerReferenceNumber) {
		this.customerReferenceNumber = customerReferenceNumber;
	}

	public String getCorporateAccount() {
		return corporateAccount;
	}

	public void setCorporateAccount(String corporateAccount) {
		this.corporateAccount = corporateAccount;
	}

	public String getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}

	public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public String getSystemRequestStatus() {
		return systemRequestStatus;
	}

	public void setSystemRequestStatus(String systemRequestStatus) {
		this.systemRequestStatus = systemRequestStatus;
	}

	/**
	 * @return the transactionUTROrCheque
	 */
	public String getTransactionUTROrCheque() {
		return transactionUTROrCheque;
	}

	/**
	 * @param transactionUTROrCheque the transactionUTROrCheque to set
	 */
	public void setTransactionUTROrCheque(String transactionUTROrCheque) {
		this.transactionUTROrCheque = transactionUTROrCheque;
	}
}
