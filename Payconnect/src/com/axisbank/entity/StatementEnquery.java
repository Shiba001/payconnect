/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;
import java.util.List;


/**
 * @author Avishek Seal
 *
 */
public class StatementEnquery implements Serializable {

    private static final long serialVersionUID = 5606996397254262133L;
    
    private Number customerID;
    
    private String customerName;
    
    private String customerAddress;
    
    private String schemeDescription;
    
    private String currency;
    
    private Integer totalRecordCount;
    
    private String responseCode;
    
    private String responseDeatils;
    
    private String downloadLink;
    
    private String message;
    
    private String error;
    
    private String fileType;
    
    private String statementType;
    
    private List<StatementInformation> statementInformations;

    /**
     * Avishek Seal
     * @return the customerID
     */
    public Number getCustomerID() {
        return customerID;
    }

    /**
     * Avishek Seal
     * @param customerID the customerID to set
     */
    public void setCustomerID(Object customerID) {
	if(customerID != null) {
	    this.customerID = Number.class.cast(customerID); 
	}
    }

    /**
     * Avishek Seal
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Avishek Seal
     * @param customerName the customerName to set
     */
    public void setCustomerName(Object customerName) {
	if(customerName != null) {
	    this.customerName = customerName.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the customerAddress
     */
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Avishek Seal
     * @param customerAddress the customerAddress to set
     */
    public void setCustomerAddress(Object customerAddress) {
	if(customerAddress != null) {
	    this.customerAddress = customerAddress.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the schemeDescription
     */
    public String getSchemeDescription() {
        return schemeDescription;
    }

    /**
     * Avishek Seal
     * @param schemeDescription the schemeDescription to set
     */
    public void setSchemeDescription(Object schemeDescription) {
	if(schemeDescription != null) {
	    this.schemeDescription = schemeDescription.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Avishek Seal
     * @param currency the currency to set
     */
    public void setCurrency(Object currency) {
	if(currency != null) {
	    this.currency = currency.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the totalRecordCount
     */
    public Integer getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * Avishek Seal
     * @param totalRecordCount the totalRecordCount to set
     */
    public void setTotalRecordCount(Object totalRecordCount) {
	if(totalRecordCount != null) {
	    this.totalRecordCount = Integer.parseInt(totalRecordCount.toString());
	}
    }

    /**
     * Avishek Seal
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Avishek Seal
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(Object responseCode) {
	if(responseCode != null) {
	    this.responseCode = responseCode.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the responseDeatils
     */
    public String getResponseDeatils() {
        return responseDeatils;
    }

    /**
     * Avishek Seal
     * @param responseDeatils the responseDeatils to set
     */
    public void setResponseDeatils(Object responseDeatils) {
	if(responseDeatils != null) {
	    this.responseDeatils = responseDeatils.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the downloadLink
     */
    public String getDownloadLink() {
        return downloadLink;
    }

    /**
     * Avishek Seal
     * @param downloadLink the downloadLink to set
     */
    public void setDownloadLink(Object downloadLink) {
	if(downloadLink != null) {
	    this.downloadLink = downloadLink.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Avishek Seal
     * @param message the message to set
     */
    public void setMessage(Object message) {
	if(message != null) {
	    this.message = message.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * Avishek Seal
     * @param error the error to set
     */
    public void setError(Object error) {
	if(error != null) {
	    this.error = error.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * Avishek Seal
     * @param fileType the fileType to set
     */
    public void setFileType(Object fileType) {
	if(fileType != null) {
	    this.fileType = fileType.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the statementType
     */
    public String getStatementType() {
        return statementType;
    }

    /**
     * Avishek Seal
     * @param statementType the statementType to set
     */
    public void setStatementType(Object statementType) {
	if(statementType != null) {
	    this.statementType = statementType.toString();
	}
    }

    /**
     * Avishek Seal
     * @return the statementInformations
     */
    public List<StatementInformation> getStatementInformations() {
        return statementInformations;
    }

    /**
     * Avishek Seal
     * @param statementInformations the statementInformations to set
     */
    public void setStatementInformations(List<StatementInformation> statementInformations) {
        this.statementInformations = statementInformations;
    }

    /**
     * Avishek Seal
     * @param customerID the customerID to set
     */
    public void setCustomerID(Number customerID) {
        this.customerID = customerID;
    }

    /**
     * Avishek Seal
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * Avishek Seal
     * @param customerAddress the customerAddress to set
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * Avishek Seal
     * @param schemeDescription the schemeDescription to set
     */
    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    /**
     * Avishek Seal
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Avishek Seal
     * @param totalRecordCount the totalRecordCount to set
     */
    public void setTotalRecordCount(Integer totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    /**
     * Avishek Seal
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Avishek Seal
     * @param responseDeatils the responseDeatils to set
     */
    public void setResponseDeatils(String responseDeatils) {
        this.responseDeatils = responseDeatils;
    }

    /**
     * Avishek Seal
     * @param downloadLink the downloadLink to set
     */
    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    /**
     * Avishek Seal
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Avishek Seal
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * Avishek Seal
     * @param fileType the fileType to set
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * Avishek Seal
     * @param statementType the statementType to set
     */
    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }

	@Override
	public String toString() {
		return "StatementEnquery [customerID=" + customerID + ", customerName="
				+ customerName + ", customerAddress=" + customerAddress
				+ ", schemeDescription=" + schemeDescription + ", currency="
				+ currency + ", totalRecordCount=" + totalRecordCount
				+ ", responseCode=" + responseCode + ", responseDeatils="
				+ responseDeatils + ", downloadLink=" + downloadLink
				+ ", message=" + message + ", error=" + error + ", fileType="
				+ fileType + ", statementType=" + statementType
				+ ", statementInformations=" + statementInformations + "]";
	}
}