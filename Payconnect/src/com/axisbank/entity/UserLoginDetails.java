package com.axisbank.entity;

import java.io.Serializable;


public class UserLoginDetails implements Serializable{

	private static final long serialVersionUID = -8744356532394770968L;

	private String userName;
	
	private String mobileNumber;
	
	private String sessionToken;
	
	private String senderShortCode;
	
	private String senderID;
	
	private String emailID;
	
	private String lastChangePaswordDate;
	
	private String passwordExpiryDate;
	
	private String lastLogin;
	
	private String userStatus;
	
	private String userId; 
	
	private String userType;
	
	private String firstName;//newly added in login method
	
	private String lastname;//newly added in login method
	
	private String corporateID;
	
	private String PasswordChangeInWeb;//newly added
	
	private String PasswordChangeNeed;//newly added
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getSenderShortCode() {
		return senderShortCode;
	}

	public void setSenderShortCode(String senderShortCode) {
		this.senderShortCode = senderShortCode;
	}

	public String getSenderID() {
		return senderID;
	}

	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getLastChangePaswordDate() {
		return lastChangePaswordDate;
	}

	public void setLastChangePaswordDate(String lastChangePaswordDate) {
		this.lastChangePaswordDate = lastChangePaswordDate;
	}

	public String getPasswordExpiryDate() {
		return passwordExpiryDate;
	}

	public void setPasswordExpiryDate(String passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

	public String getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Avishek Seal
	 * @return the userType
	 */
	public String getUserType() {
	    return userType;
	}

	/**
	 * Avishek Seal
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
	    this.userType = userType;
	}
	
	public String getCorporateID() {
		return corporateID;
	}

	public void setCorporateID(String corporateID) {
		this.corporateID = corporateID;
	}
	
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	

	public String getPasswordChangeInWeb() {
		return PasswordChangeInWeb;
	}

	public void setPasswordChangeInWeb(String passwordChangeInWeb) {
		PasswordChangeInWeb = passwordChangeInWeb;
	}
	
	
	public String getPasswordChangeNeed() {
		return PasswordChangeNeed;
	}

	public void setPasswordChangeNeed(String passwordChangeNeed) {
		PasswordChangeNeed = passwordChangeNeed;
	}

	@Override
	public String toString() {
		return "UserLoginDetails [userName=" + userName + ", mobileNumber="
				+ mobileNumber + ", sessionToken=" + sessionToken
				+ ", senderShortCode=" + senderShortCode + ", senderID="
				+ senderID + ", emailID=" + emailID
				+ ", lastChangePaswordDate=" + lastChangePaswordDate
				+ ", passwordExpiryDate=" + passwordExpiryDate + ", lastLogin="
				+ lastLogin + ", userStatus=" + userStatus + ", userId="
				+ userId + ", userType=" + userType + ", firstName="
				+ firstName + ", lastname=" + lastname + ", corporateID="
				+ corporateID + ", PasswordChangeInWeb=" + PasswordChangeInWeb
				+ ", PasswordChangeNeed=" + PasswordChangeNeed + "]";
	}

	
	
	
	
	
	
	
	

	
	
	
	
	
	

}
