package com.axisbank.entity;

import java.io.Serializable;
import java.util.Date;

import com.axisbank.common.PayproSQLDateFormat;

public class CorpBatchIDRequest implements Serializable, PayproSQLDateFormat {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6252548706254830971L;

	private String version;

	private String corporateCode;

	private String userId;

	private String startDate;

	private String endDate;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCorporateCode() {
		return corporateCode;
	}

	public void setCorporateCode(String corporateCode) {
		this.corporateCode = corporateCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = BB_DATE_FORMAT.format(startDate);
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = BB_DATE_FORMAT.format(endDate);
	}
}
