/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;
import java.util.Date;

import com.axisbank.common.PayproSQLDateFormat;

/**
 * @author Avishek Seal
 *
 */
public class BankBatchIDRequest implements Serializable, PayproSQLDateFormat{

    private static final long serialVersionUID = 7107488149211932022L;
    
    private String version;
    
    private String corporateCode;
    
    private String loginID;
    
    private String startDate;
    
    private String endDate;
    

    /**
     * Avishek Seal
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Avishek Seal
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Avishek Seal
     * @return the corporateCode
     */
    public String getCorporateCode() {
        return corporateCode;
    }

    /**
     * Avishek Seal
     * @param corporateCode the corporateCode to set
     */
    public void setCorporateCode(String corporateCode) {
        this.corporateCode = corporateCode;
    }

    /**
     * Avishek Seal
     * @return the loginID
     */
    public String getLoginID() {
        return loginID;
    }

    /**
     * Avishek Seal
     * @param loginID the loginID to set
     */
    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    /**
     * Avishek Seal
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Avishek Seal
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = BB_DATE_FORMAT.format(startDate);
    }

    /**
     * Avishek Seal
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Avishek Seal
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    /**
     * Avishek Seal
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = BB_DATE_FORMAT.format(endDate);
    }
}
