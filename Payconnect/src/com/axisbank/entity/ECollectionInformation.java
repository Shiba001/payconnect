/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;

/**
 * @author Avishek Seal
 *
 */
public class ECollectionInformation implements Serializable {

    private static final long serialVersionUID = -1919566930223466891L;
    
    private String serialNumber;
    
    private String transactionDate;
    
    private String transactionAmount;
    
    private String senderName;
    
    private String utrNumber;
    
    private String corporateActualAccountNumber;
    
    private String senderInformation;
    
    private String paymentMode;

    /**
     * Avishek Seal
     * @return the serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Avishek Seal
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Avishek Seal
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * Avishek Seal
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * Avishek Seal
     * @return the transactionAmount
     */
    public String getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * Avishek Seal
     * @param transactionAmount the transactionAmount to set
     */
    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    /**
     * Avishek Seal
     * @return the senderName
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * Avishek Seal
     * @param senderName the senderName to set
     */
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    /**
     * Avishek Seal
     * @return the utrNumber
     */
    public String getUtrNumber() {
        return utrNumber;
    }

    /**
     * Avishek Seal
     * @param utrNumber the utrNumber to set
     */
    public void setUtrNumber(String utrNumber) {
        this.utrNumber = utrNumber;
    }

    /**
     * Avishek Seal
     * @return the corporateActualAccountNumber
     */
    public String getCorporateActualAccountNumber() {
        return corporateActualAccountNumber;
    }

    /**
     * Avishek Seal
     * @param corporateActualAccountNumber the corporateActualAccountNumber to set
     */
    public void setCorporateActualAccountNumber(String corporateActualAccountNumber) {
        this.corporateActualAccountNumber = corporateActualAccountNumber;
    }

    /**
     * Avishek Seal
     * @return the senderInformation
     */
    public String getSenderInformation() {
        return senderInformation;
    }

    /**
     * Avishek Seal
     * @param senderInformation the senderInformation to set
     */
    public void setSenderInformation(String senderInformation) {
        this.senderInformation = senderInformation;
    }

    /**
     * Avishek Seal
     * @return the paymentMode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Avishek Seal
     * @param paymentMode the paymentMode to set
     */
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
}
