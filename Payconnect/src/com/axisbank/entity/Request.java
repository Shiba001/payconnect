package com.axisbank.entity;

import java.io.Serializable;


public class Request implements Serializable{

	private static final long serialVersionUID = -519121213493777499L;

	private String corporateCode;
	
	private String userName;
	
	private String password;
	
	private String oldPassword;
	
	private String deviceId;
	
	private String IpAddress;
	
	private String latitude;
	
	private String longitude;
	
	private String apiVersion;

	public String getCorporateCode() {
		return corporateCode;
	}

	public void setCorporateCode(String corporateCode) {
		this.corporateCode = corporateCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	

	public String getIpAddress() {
		return IpAddress;
	}

	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Override
	public String toString() {
		return "Request [corporateCode=" + corporateCode + ", userName="
				+ userName + ", password=" + password + ", oldPassword="
				+ oldPassword + ", deviceId=" + deviceId + ", IpAddress="
				+ IpAddress + ", latitude=" + latitude + ", longitude="
				+ longitude + ", apiVersion=" + apiVersion + "]";
	}

	
	

	
	

	
	
	

}
