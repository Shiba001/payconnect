/**
 * 
 */
package com.axisbank.entity;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.axisbank.common.PayproSQLDateFormat;

/**
 * @author Avishek Seal
 *
 */
public class StatementEnqueryRequest implements Serializable, PayproSQLDateFormat{

    private static final long serialVersionUID = 2966867591840111665L;
    
    private String corporateCode;
    
    private String userID;
    
    private String corporateAccountNumber;
    
    private String accountname;
    
    private String startDate = "";
    
    private String endDate = "";
    
    private String statementType;
    
    private String recordPerPage;
    
    private String pageNumber;
    
    private String fileType;
    
    private String apiVersion;
    
    private String system;

    public String getCorporateCode() {
        return corporateCode;
    }

    public void setCorporateCode(String corporateCode) {
        this.corporateCode = corporateCode;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCorporateAccountNumber() {
        return corporateAccountNumber;
    }

    public void setCorporateAccountNumber(String corporateAccountNumber) {
        this.corporateAccountNumber = corporateAccountNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setStartDate(Date startDate) {
    	if(startDate != null) {
    		this.startDate = SE_DATE_FORMAT.format(startDate);
    	}
    }
    
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    public void setEndDate(Date endDate) {
    	if(endDate != null) {
    		this.endDate = SE_DATE_FORMAT.format(endDate);
    	}
    }

    public String getStatementType() {
        return statementType;
    }

    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }

    public String getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(String recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }
    
    
    public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
	
	

	public String getAccountname() {
		return accountname;
	}

	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}
	
	
	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String toJsonString() throws JSONException{
    	
	final JSONObject jsonObject = new JSONObject();
	
	
	jsonObject.put("ApiVersion", getApiVersion());
	jsonObject.put("CorpID", getCorporateCode());
	jsonObject.put("UserID", getUserID());
	jsonObject.put("AccountNo", getCorporateAccountNumber());
	jsonObject.put("AccountName", getAccountname());
	jsonObject.put("FromDate", getStartDate());
	jsonObject.put("ToDate", getEndDate());
	jsonObject.put("StatementFormat", getStatementType());
	jsonObject.put("Pagesize", getRecordPerPage());
	jsonObject.put("Pageno", getPageNumber());
	jsonObject.put("Filetype", getFileType());
	jsonObject.put("System", getSystem());
	
	
	System.out.println("json from client side "+jsonObject.toString() );
	
	return jsonObject.toString();
	
	
	
    }
    
    
    

    
}