package com.axisbank.entity;

import java.io.Serializable;

public class CorpBatchIDInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8857776006389442065L;
	
	
	  private String corpBatchNumber;
    
      private String creationDate;
       

	public String getCorpBatchNumber() {
		return corpBatchNumber;
	}

	public void setCorpBatchNumber(String corpBatchNumber) {
		this.corpBatchNumber = corpBatchNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
       
  
}
