/**
 * 
 */
package com.axisbank.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axisbankcmspaypro.comon.AxisbankDateFormat;
import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.payconnect.model.LogFileModel;

/**
 * @author Avishek Seal
 *
 */
@Service
public class LogFileService{

    @Autowired
    private DirectoryUtil directoryUtil;
    
    /**
     * this method is used to fetch list of log file from the server
     * @since 09-Mar-2016
     * @author Avishek Seal
     * @return
     */
    public List<LogFileModel> getListOfLogFile(){
	final File file = new File(directoryUtil.getLogFilePath());
	final List<LogFileModel> fileModels = new ArrayList<LogFileModel>();
	
	if(file.exists() && file.isDirectory()) {
	    File[] files = file.listFiles();
	    
	    for(File log : files) {
		final LogFileModel logFileModel = new LogFileModel();
		
		logFileModel.setDateModified(AxisbankDateFormat.DATE_TIME_FORMAT.format(new Date(log.lastModified())));
		logFileModel.setFileName(log.getName());
		
		fileModels.add(logFileModel);
	    }
	}
	
	return fileModels;
    }
    
    /**
     * this method
     * @since 09-Mar-2016
     * @author Avishek Seal
     * @param fileName
     * @return
     */
    public String getLogFilePath(String fileName){
	return directoryUtil.getLogFilePath().concat("\\"+fileName);
    }
}
