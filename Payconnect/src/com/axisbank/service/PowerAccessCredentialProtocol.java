package com.axisbank.service;

import org.springframework.stereotype.Service;

import com.common.InvalidCredentials;
import com.payconnect.model.LoginRequestModel;
import com.payconnect.model.LoginResponseModel;

@Service
public interface PowerAccessCredentialProtocol extends InvalidCredentials{
	
LoginResponseModel login(LoginRequestModel loginRequestModel) throws Exception;
	
	boolean isInvalidCredentialPresent(String userName);
	
	boolean isInvalidCredentialLimitExceeded(String userName);
	
	boolean isInvalidCaptcha(String userName);
	
	boolean isInvalidCaptchaLimitExceeded(String userName);
	
	boolean isCaptchValid(String userName, String captchString);
	
	void clearInvalidCaptcha(String userName);
	
	void clearCaptchaCounter(String userName);
	
	void clearInvalidCreedentialCounter(String userName);
	
	void incrementInvalidCredential(String userName);
	
	void incrementInvalidCaptchaCounter(String userName);
	
	void setCaptchString(String userName, String captchaString);
	
	String loginAttemptAvailable(String userName);
	
	String captchAttemptAvailable(String userName);

}
