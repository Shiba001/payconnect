package com.axisbank.service;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbank.converter.RequestConverter;
import com.axisbank.converter.ResponseConverter;
import com.axisbank.dao.ResponseDAO;
import com.axisbank.entity.Response;
import com.axisbankcmspaypro.business.DeviceBusiness;
import com.axisbankcmspaypro.business.PayConnectUserBusiness;
import com.axisbankcmspaypro.dao.CmsConfigurationDAO;
import com.axisbankcmspaypro.entity.CMSConfiguration;
import com.common.CaptchaGenerator;
import com.common.CapthaRandomNumber;
import com.payconnect.model.LoginRequestModel;
import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.UnlockDeviceModel;

@Service
@Transactional
public class PayProCredentialService implements PayProCredentialProtocol{
	
	private final Logger logger = Logger
			.getLogger(PayProCredentialService.class);
	
	@Autowired
	private ResponseDAO responseDAO;
	
	@Autowired
	private ResponseConverter responseConverter;
	
	@Autowired
	private RequestConverter requestConverter;
	
	@Autowired
	private PayConnectUserBusiness connectUserBusiness;
	
	@Autowired
	private CmsConfigurationDAO cmsConfigurationDAO;
	
	@Autowired
	private DeviceBusiness deviceBusiness;
	
	/**
	 * this method is used to get logged in
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param loginRequestModel
	 * @return
	 * @throws SQLException
	 */
	@Override
	public LoginResponseModel login(LoginRequestModel loginRequestModel) throws Exception{
		
		
    UnlockDeviceModel unlockDeviceModel=deviceBusiness.isUnlockCodeGenerated(loginRequestModel);
		
		
		if(StringUtils.isNotBlank(unlockDeviceModel.getUnlockCode())&& StringUtils.equals(unlockDeviceModel.getDeviceStatus(), "D")){
			
			logger.info("inside paypro Your device is blocked and but unlock code is generated for this device ID");
			
			System.out.println("inside paypro Your device is blocked and but unlock code is generated for this device ID");

			LoginResponseModel  loginResponseModel = new LoginResponseModel();
			loginResponseModel.setResponseCode("206");
			loginResponseModel.setResponseDetails("Your device is blocked and but unlock code is generated for this device ID");
			loginResponseModel.setUnlockCode("1");
			
			return loginResponseModel;
		}
		
		else if(StringUtils.isBlank(unlockDeviceModel.getUnlockCode()) && StringUtils.equals(unlockDeviceModel.getDeviceStatus(), "D")){
			
			logger.info("inside paypro Your device is blocked and still now unlock code is not generate for this device ID");
			
			System.out.println("inside paypro Your device is blocked and still now unlock code is not generate for this device ID");
			
			LoginResponseModel  loginResponseModel = new LoginResponseModel();
			loginResponseModel.setResponseCode("206");
			loginResponseModel.setResponseDetails("Your device is blocked and still now unlock code is not generate for this device ID");
			loginResponseModel.setUnlockCode("0");
			
			return loginResponseModel;
			
		}
		
		else{
			
		    if(connectUserBusiness.isUserblocked(loginRequestModel)){
		    	
		    	logger.info("inside paypro isUserblocked");
		    	
		    	System.out.println("inside paypro isUserblocked");
		    	
				LoginResponseModel  loginResponseModel = new LoginResponseModel();
				loginResponseModel.setResponseCode("205");
				loginResponseModel.setResponseDetails("user Blocked from CMS");
				
				return loginResponseModel;
			    } else {
				if(isInvalidCredentialPresent(loginRequestModel.getUserName())){
					if(isInvalidCaptcha(loginRequestModel.getUserName())){
						if(isInvalidCaptchaLimitExceeded(loginRequestModel.getUserName())){
							LoginResponseModel  loginResponseModel = new LoginResponseModel();
							loginResponseModel.setResponseCode("205");
							loginResponseModel.setResponseDetails("app Blocked");
							
							//connectUserBusiness.blockPayConnectUser(loginRequestModel);//block payconnect user
							
							
							deviceBusiness.blockDevice(loginRequestModel);//block device
							
							clearCaptchaCounter(loginRequestModel.getUserName());
							clearInvalidCaptcha(loginRequestModel.getUserName());
							clearInvalidCreedentialCounter(loginRequestModel.getUserName());
							
							return loginResponseModel;
						} else {
							if(isCaptchValid(loginRequestModel.getUserName(), loginRequestModel.getCaptcha())) {
								
								logger.info("inside paypro isCaptchValid");
								System.out.println("inside paypro isCaptchValid");
							    
								Response response = responseDAO.getLoginResponse(requestConverter.modelToEntity(loginRequestModel));
								LoginResponseModel loginResponseModel = responseConverter.entityToModel(response);
								if(loginResponseModel.isSuccess()){
									clearCaptchaCounter(loginRequestModel.getUserName());
									clearInvalidCaptcha(loginRequestModel.getUserName());
									clearInvalidCreedentialCounter(loginRequestModel.getUserName());
								} else if(loginResponseModel.isInvalidCredential()) {
									
									incrementInvalidCaptchaCounter(loginRequestModel.getUserName());
									String newCaptchaString = CapthaRandomNumber.generateRandomCaptcha();
									String html = CaptchaGenerator.getCaptchaHTML(newCaptchaString);
									setCaptchString(loginRequestModel.getUserName(), newCaptchaString);
									//for captcha response code 203
									loginResponseModel.setResponseCode("203");
									loginResponseModel.setResponseDetails("Invalid credential");
									loginResponseModel.setCaptchaString(html);
									loginResponseModel.setCaptchaAttemptRemaining(captchAttemptAvailable(loginRequestModel.getUserName()));
									loginResponseModel.setLoginAttemptRemaining(loginAttemptAvailable(loginRequestModel.getUserName()));
								} else {
									//something went wrong
								}
								
								return loginResponseModel;
							} else {
								incrementInvalidCaptchaCounter(loginRequestModel.getUserName());
								//for invalid captcha response code 204
								LoginResponseModel  loginResponseModel = new LoginResponseModel();
								loginResponseModel.setResponseCode("204");
								loginResponseModel.setResponseDetails("Invalid captcha");
								
								String newCaptchaString = CapthaRandomNumber.generateRandomCaptcha();
								String html = CaptchaGenerator.getCaptchaHTML(newCaptchaString);
								setCaptchString(loginRequestModel.getUserName(), newCaptchaString);
								
								loginResponseModel.setCaptchaString(html);
								loginResponseModel.setCaptchaAttemptRemaining(captchAttemptAvailable(loginRequestModel.getUserName()));
								loginResponseModel.setLoginAttemptRemaining(loginAttemptAvailable(loginRequestModel.getUserName()));
								return loginResponseModel;
							}
						}
					} else {
						if(isInvalidCredentialLimitExceeded(loginRequestModel.getUserName())){
							
							logger.info("inside paypro isInvalidCredentialLimitExceeded");
							System.out.println("inside paypro isInvalidCredentialLimitExceeded");
							
							Response response = responseDAO.getLoginResponse(requestConverter.modelToEntity(loginRequestModel));
							LoginResponseModel loginResponseModel = responseConverter.entityToModel(response);
							
							if(loginResponseModel.isSuccess()){
								clearCaptchaCounter(loginRequestModel.getUserName());
								clearInvalidCaptcha(loginRequestModel.getUserName());
								clearInvalidCreedentialCounter(loginRequestModel.getUserName());
							} else if(loginResponseModel.isInvalidCredential()){
								incrementInvalidCaptchaCounter(loginRequestModel.getUserName());
								
								loginResponseModel.setResponseCode("203");
								loginResponseModel.setResponseDetails("Invalid credential");
								
								String newCaptchaString = CapthaRandomNumber.generateRandomCaptcha();
								String html = CaptchaGenerator.getCaptchaHTML(newCaptchaString);
								setCaptchString(loginRequestModel.getUserName(), newCaptchaString);
								
								loginResponseModel.setCaptchaString(html);
								loginResponseModel.setCaptchaAttemptRemaining(captchAttemptAvailable(loginRequestModel.getUserName()));
								loginResponseModel.setLoginAttemptRemaining(loginAttemptAvailable(loginRequestModel.getUserName()));
							} else {
								//something went wrong
							}
							
							return loginResponseModel;
							
						} else {
							try{
								logger.info("inside paypro isInvalidCredentialLimitExceeded");
								System.out.println("inside paypro isInvalidCredentialLimitExceeded");
							    Response response = responseDAO.getLoginResponse(requestConverter.modelToEntity(loginRequestModel));
								LoginResponseModel loginResponseModel = responseConverter.entityToModel(response);
								
								if(loginResponseModel.isSuccess()){
									clearCaptchaCounter(loginRequestModel.getUserName());
									clearInvalidCaptcha(loginRequestModel.getUserName());
									clearInvalidCreedentialCounter(loginRequestModel.getUserName());
								} else if(loginResponseModel.isInvalidCredential()){
									incrementInvalidCredential(loginRequestModel.getUserName());
									
									loginResponseModel.setLoginAttemptRemaining(loginAttemptAvailable(loginRequestModel.getUserName()));
								}
								
								return loginResponseModel;
							}catch (Exception e) {
							    e.printStackTrace();
							    return null;
							}
						}
					}
				} else {
					
					logger.info("inside paypro right credentials");
					
					System.out.println("inside paypro right credentials");
					
					Response response = responseDAO.getLoginResponse(requestConverter.modelToEntity(loginRequestModel));//encription logic here
					
					LoginResponseModel loginResponseModel = responseConverter.entityToModel(response);
					
					if(loginResponseModel.isSuccess()){
						clearCaptchaCounter(loginRequestModel.getUserName());
						clearInvalidCaptcha(loginRequestModel.getUserName());
						clearInvalidCreedentialCounter(loginRequestModel.getUserName());
						connectUserBusiness.saveOrUpdateUser(loginRequestModel, loginResponseModel);
						
					} else if(loginResponseModel.isInvalidCredential()) {
						incrementInvalidCredential(loginRequestModel.getUserName());
						
						loginResponseModel.setLoginAttemptRemaining(loginAttemptAvailable(loginRequestModel.getUserName()));
					} else {
						//something went wrong
					}
					return loginResponseModel;
				}
			    }
			
		}
	    

	}
	
	/**
	 * this method is used to check whether invalid credential limit exceeded or not
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 * @return
	 */
	@Override
	public boolean isInvalidCredentialLimitExceeded(String userName) {
		
		Integer count = INVALID_CREDENTIAL.get(userName);
		
		List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)){
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
        int credentialLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			credentialLimitFromDb = 1;
		} else {
			credentialLimitFromDb = cmsConfiguration.getLoginAttemptCounter();
		}
		
		return count != null && count == credentialLimitFromDb;//change here for invalid credentials attempt
	}
	
	/**
	 * this method is used to check whether invalid credential already attempted by the user or not
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 * @return
	 */
	@Override
	public boolean isInvalidCredentialPresent(String userName) {
		Integer count = INVALID_CREDENTIAL.get(userName);
		return count != null && count > 0;
	}
	
	/**
	 * this method is used to check whether invalid captch already set for an user or not
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 * @return
	 */
	@Override
	public boolean isInvalidCaptcha(String userName) {
		Integer count = CAPTCHA_COUNTER.get(userName);
		return count != null && count > 0;
	}
	
	/**
	 * this method is used to check whether invalid captcha limit exceeded
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 * @return
	 */
	@Override
	public boolean isInvalidCaptchaLimitExceeded(String userName) {
		Integer count = CAPTCHA_COUNTER.get(userName);
		List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)) {
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
		int captchaLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			captchaLimitFromDb = 1;
		} else {
			captchaLimitFromDb = cmsConfiguration.getCapchaAttemptCounter();
		}
		
		return count != null && count == captchaLimitFromDb;//change here for dynamic captcha
		
		
	}
	
	/**
	 * this method is used to check whether captcha string is valid
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 * @param captchString
	 * @return
	 */
	@Override
	public boolean isCaptchValid(String userName, String captchString) {
		return StringUtils.isNotBlank(CAPTCHA_STRING.get(userName)) && StringUtils.equals(CAPTCHA_STRING.get(userName), captchString);
	}
	
	/**
	 * this method is used to clear invalid captcha counter counter buffer
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 */
	@Override
	public void clearCaptchaCounter(String userName) {
		CAPTCHA_COUNTER.remove(userName);
	}
	
	/**
	 * this method is used to clear invalid captcha counter buffer
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 */
	@Override
	public void clearInvalidCaptcha(String userName) {
		CAPTCHA_STRING.remove(userName);
	}
	
	/**
	 * this method is used to clear invalid credential counter buffer
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 */
	@Override
	public void clearInvalidCreedentialCounter(String userName) {
		INVALID_CREDENTIAL.remove(userName);
	}
	
	/**
	 * this method is used to increase invalid credential counter
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 */
	@Override
	public void incrementInvalidCredential(String userName) {
		Integer invalidCredentialCount = INVALID_CREDENTIAL.get(userName);
		
		if(invalidCredentialCount == null){
			invalidCredentialCount = 0;
		}
		
		invalidCredentialCount++;
		
		INVALID_CREDENTIAL.put(userName, invalidCredentialCount);
	}
	
	/**
	 * this method is used to increase captcha counter
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 */
	@Override
	public void incrementInvalidCaptchaCounter(String userName) {
		Integer invalidCaptchaCount = CAPTCHA_COUNTER.get(userName);
		
		if(invalidCaptchaCount == null){
			invalidCaptchaCount = 0;
		}
		
		invalidCaptchaCount++;
		
		CAPTCHA_COUNTER.put(userName, invalidCaptchaCount);
	}
	
	/**
	 * this method is used to set captcha string for a specific user
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userName
	 * @param captchaString
	 */
	@Override
	public void setCaptchString(String userName, String captchaString) {
		CAPTCHA_STRING.put(userName, captchaString);
	}
	
	@Override
	public String captchAttemptAvailable(String userName) {
		Integer invalidCaptchaCount = CAPTCHA_COUNTER.get(userName);
		
		if(invalidCaptchaCount == null) {
			invalidCaptchaCount = 0;
		}
		
		final List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)) {
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
		Integer captchaLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			captchaLimitFromDb = 1;
		} else {
			captchaLimitFromDb = cmsConfiguration.getCapchaAttemptCounter();
		}
		
		int remaining = captchaLimitFromDb - invalidCaptchaCount;
		
		return String.valueOf(remaining);
	}
	
	@Override
	public String loginAttemptAvailable(String userName) {
		Integer invalidCredentialCount = INVALID_CREDENTIAL.get(userName);
		
		if(invalidCredentialCount == null) {
			invalidCredentialCount = 0;
		}
		
		final List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)){
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
        int credentialLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			credentialLimitFromDb = 1;
		} else {
			credentialLimitFromDb = cmsConfiguration.getLoginAttemptCounter();
		}
		
		int remaining = credentialLimitFromDb - invalidCredentialCount;
		
		return String.valueOf(remaining);
	}
}
