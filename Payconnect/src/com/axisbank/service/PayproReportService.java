/**
 * 
 */
package com.axisbank.service;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.axisbank.common.FileConvertion;
import com.axisbank.converter.ECollectionReportConverter;
import com.axisbank.converter.ECollectionReportRequestConverter;
import com.axisbank.converter.StatementEnqueryConverter;
import com.axisbank.converter.StatementEnqueryRequestConverter;
import com.axisbank.converter.TransactionReportConverter;
import com.axisbank.converter.TransactionReportRequestConverter;
import com.axisbank.dao.ECollectionReportDAO;
import com.axisbank.dao.StatementEnqueryDAO;
import com.axisbank.dao.TransactionReportDAO;
import com.axisbankcmspaypro.comon.AxisbankDateFormat;
import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.payconnect.model.ECollectionInformationResponseModel;
import com.payconnect.model.ECollectionReportRequestModel;
import com.payconnect.model.ECollectionResponseModel;
import com.payconnect.model.ReportFieldModel;
import com.payconnect.model.StatementEnqueryRequestModel;
import com.payconnect.model.StatementEnqueryResponseModel;
import com.payconnect.model.StatementInformationModel;
import com.payconnect.model.TransactionInformationModel;
import com.payconnect.model.TransactionReportRequestModel;
import com.payconnect.model.TransactionReportResponseModel;

/**
 * @author Avishek Seal
 *
 */
@Transactional
@Service
public class PayproReportService {
    
    private static final String ECOLLECTION_REPORT_UNIQUE_VALUE = "6";
    
    private static final String STATEMENT_ENQUIRY_REPORT_UNIQUE_VALUE = "4";
    
    private static final String TRANSACTION_REPORT_UNIQUE_VALUE = "5";
    
    private static final String SYSTEM_VALUE = "2";
    
    private static final String PDF_FORMAT = "pdf";
    
    private static final String EXCEL_FORMAT = "excel";
    
    private static final String E_COLLECTION_REPORT = "E Collection Report for ";
    
    private static final String STATEMENT_ENQUERY_REPORT = "Statement Enquery Report for";
    
    private static final String Transaction_Report = "Transaction Report for";
    
    @Autowired
    private ECollectionReportDAO eCollectionReportDAO;
    
    @Autowired
    private StatementEnqueryDAO statementEnqueryDAO;
    
    @Autowired
    private TransactionReportDAO transactionReportDAO;
    
    @Autowired
    private AppFieldMaskingService appFieldMaskingService;
    
    @Autowired
    private ECollectionReportConverter eCollectionReportConverter;
    
    @Autowired
    private ECollectionReportRequestConverter eCollectionReportRequestConverter;
    
    @Autowired
    private StatementEnqueryConverter statementEnqueryConverter;
    
    @Autowired
    private StatementEnqueryRequestConverter statementEnqueryRequestConverter;
    
    @Autowired
    private TransactionReportConverter transactionReportConverter;
    
    @Autowired
    private TransactionReportRequestConverter transactionReportRequestConverter;
    
    @Autowired
    private FileConvertion fileConvertion;
    
    @Autowired
    private DirectoryUtil directoryUtil;
    
    /**
     * this method is used to get E Collection report depending on request value
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param collectionReportRequestModel
     * @return
     * @throws SQLException
     */
    public ECollectionResponseModel getECollectionResponseModel(ECollectionReportRequestModel collectionReportRequestModel, String appURL) throws SQLException{
	final List<ReportFieldModel> permittedFields = appFieldMaskingService.listOfReportMenuByApplicationUser(collectionReportRequestModel.getLogingID(), ECOLLECTION_REPORT_UNIQUE_VALUE, SYSTEM_VALUE);
	
	final ECollectionResponseModel collectionResponseModel = eCollectionReportConverter.convert(eCollectionReportDAO.getECollectionReport(eCollectionReportRequestConverter.modelToEntity(collectionReportRequestModel)), permittedFields);

	
	final String excelPath = prepareExcelECollection(collectionResponseModel.getCollectionInformationResponseModels(), permittedFields, collectionReportRequestModel.getLogingID(), ECOLLECTION_REPORT_UNIQUE_VALUE, appURL);
	final String pdfPath = preparePDFECollection(collectionResponseModel.getCollectionInformationResponseModels(), permittedFields, collectionReportRequestModel.getLogingID(), ECOLLECTION_REPORT_UNIQUE_VALUE, E_COLLECTION_REPORT, appURL);
	
	collectionResponseModel.setExcelDownloadLink(excelPath);
	collectionResponseModel.setPdfDownloadLink(pdfPath);
	
	return collectionResponseModel;
    }
    
    /**
     * this method is used to get StatementEnquery Report
     * @author Satya Karanam
     * @param statementEnqueryRequestModel
     * @return
     * @throws SQLException
     */
    public StatementEnqueryResponseModel getStatementEnqueryResponseModel(StatementEnqueryRequestModel statementEnqueryRequestModel, String appURL) throws Exception{
    final List<ReportFieldModel> permittedFields = appFieldMaskingService.listOfReportMenuByApplicationUser(statementEnqueryRequestModel.getUserID(), STATEMENT_ENQUIRY_REPORT_UNIQUE_VALUE, SYSTEM_VALUE);
    
    final StatementEnqueryResponseModel enqueryResponseModel = statementEnqueryConverter.convert(statementEnqueryDAO.getListOfStatementEnquery(statementEnqueryRequestConverter.modelToEntity(statementEnqueryRequestModel)), permittedFields);
    
		if (enqueryResponseModel.getStatementType().equals("2")) {
			final String pdfPath = preparePDFStatementEnquery(enqueryResponseModel.getStatementInformationModels(), permittedFields, statementEnqueryRequestModel.getUserID(), STATEMENT_ENQUIRY_REPORT_UNIQUE_VALUE, STATEMENT_ENQUERY_REPORT, appURL);
			enqueryResponseModel.setDownloadLink(pdfPath);
		} else if (enqueryResponseModel.getStatementType().equals("3")) {
			final String excelPath = prepareExcelStatementEnquery(enqueryResponseModel.getStatementInformationModels(), permittedFields, statementEnqueryRequestModel.getUserID(), STATEMENT_ENQUIRY_REPORT_UNIQUE_VALUE, appURL);
			enqueryResponseModel.setDownloadLink(excelPath);
		}
		return enqueryResponseModel;
    }
    


	/**
     * this method is used to get TransactionReport
     * @author Satya Karanam
     * @param transactionReportRequestModel
     * @return
     * @throws SQLException
     */
    
    public TransactionReportResponseModel getTransactionReportResponseModel(TransactionReportRequestModel transactionReportRequestModel, String appURL)throws SQLException{
    final List<ReportFieldModel> permittedFields = appFieldMaskingService.listOfReportMenuByApplicationUser(transactionReportRequestModel.getLoginID(), TRANSACTION_REPORT_UNIQUE_VALUE, SYSTEM_VALUE);
    
    final TransactionReportResponseModel transactionReportResponseModel = transactionReportConverter.convert(transactionReportDAO.getTransactionList(transactionReportRequestConverter.modelToEntity(transactionReportRequestModel)), permittedFields);
     
    final String excelPath = prepareExcelTransactionReport(transactionReportResponseModel.getTransactionInformationModels(), permittedFields, transactionReportRequestModel.getLoginID(), TRANSACTION_REPORT_UNIQUE_VALUE, appURL);
    transactionReportResponseModel.setExcelDownloadLink(excelPath);
    
    final String pdfPath = preparePDFTransactionReport(transactionReportResponseModel.getTransactionInformationModels(), permittedFields, transactionReportRequestModel.getLoginID(), TRANSACTION_REPORT_UNIQUE_VALUE, Transaction_Report, appURL);
    transactionReportResponseModel.setPdfDownloadLink(pdfPath);
    
    return transactionReportResponseModel; 
    }
    
    
    /**
     * this method is used E Collection PDF Report
     * @param collectionInformationResponseModels
     * @param permittedFields
     * @param userID
     * @param reportType
     * @param reportName
     * @return
     */
    
    private String preparePDFECollection(List<ECollectionInformationResponseModel> collectionInformationResponseModels, List<ReportFieldModel> permittedFields,String userID, String reportType, String reportName, String appURL) {
	String fileName = userID+"_"+String.valueOf(new Date().getTime())+".pdf";
	String[] path = reportDumpingPath(userID, reportType, fileName, PDF_FORMAT, appURL);
	
	final List<List<Object>> data = new ArrayList<>();
	
	if(!CollectionUtils.isEmpty(permittedFields)) {
	    final List<Object> labels = new ArrayList<>();
	    
	    labels.add("Serial Number");
	    
	    for(ReportFieldModel fieldModel : permittedFields) {
	    	labels.add(fieldModel.getFieldLabel());
	    }
	    
	    data.add(labels);
	}
	
	if(!CollectionUtils.isEmpty(collectionInformationResponseModels)) {
	    for(ECollectionInformationResponseModel model : collectionInformationResponseModels) {
	    	final List<Object> rowData = new ArrayList<>();

	    	if(Objects.nonNull(model.getSerialNumber())){
	    		rowData.add(model.getSerialNumber());
	    	}
		
	    	for(ReportFieldModel fieldModel : permittedFields) {
	    		switch (fieldModel.getReportField()) {
		    	
	    		case "paymentmode":
			    		if(Objects.nonNull(model.getPaymentMode())){
			    		    rowData.add(model.getPaymentMode());
			    		}
		    	    	break;
	    		case "transactiondate":
                        	if(Objects.nonNull(model.getTransactionDate())){
                    		    rowData.add(model.getTransactionDate());
                    		}
                        	break;
	    		case "transactionamount":
                        	if(Objects.nonNull(model.getTransactionAmount())){
                    		    rowData.add(model.getTransactionAmount());
                    		}
                        	break;
	    		case "sendername":
                        	if(Objects.nonNull(model.getSenderName())){
                    		    rowData.add(model.getSenderName());
                    		}
                        	break;
	    		case "UTRno":
                        	if(Objects.nonNull(model.getUtrNumber())){
                    		    rowData.add(model.getUtrNumber());
                    		}
                        	break;
	    		case "corparateaccountno":
                        	if(Objects.nonNull(model.getCorporateActualAccountNumber())){
                    		    rowData.add(model.getCorporateActualAccountNumber());
                    		}
                        	break;
	    		case "senderinfo":
                        	if(Objects.nonNull(model.getSenderInformation())){
                    		    rowData.add(model.getSenderInformation());
                    		}
                        	break;
	    		}
	    	}

	    	data.add(rowData);
	    }
	}
	
	fileConvertion.toPDF(data, path[0], reportName+userID);
	
	return path[1];
    }
    
    
    /**
     * this method is used E Collection Excel Report
     * @param collectionInformationResponseModels
     * @param permittedFields
     * @param userID
     * @param reportType
     * @return
     */
    
    private String prepareExcelECollection(List<ECollectionInformationResponseModel> collectionInformationResponseModels, List<ReportFieldModel> permittedFields,String userID, String reportType, String appURL){
	String fileName = userID+"_"+String.valueOf(new Date().getTime())+".xls";
	String[] path = reportDumpingPath(userID, reportType, fileName, EXCEL_FORMAT, appURL);
	
final List<List<Object>> data = new ArrayList<>();
	
	if(!CollectionUtils.isEmpty(permittedFields)) {
	    final List<Object> labels = new ArrayList<>();
	    
	    labels.add("Serial Number");
	    
	    for(ReportFieldModel fieldModel : permittedFields) {
	    	labels.add(fieldModel.getFieldLabel());
	    }
	    
	    data.add(labels);
	}
	
	if(!CollectionUtils.isEmpty(collectionInformationResponseModels)) {
	    for(ECollectionInformationResponseModel model : collectionInformationResponseModels) {
	    	final List<Object> rowData = new ArrayList<>();

	    	if(Objects.nonNull(model.getSerialNumber())){
	    		rowData.add(model.getSerialNumber());
	    	}
		
	    	for(ReportFieldModel fieldModel : permittedFields) {
	    		switch (fieldModel.getReportField()) {
		    	
	    		case "paymentmode":
			    		if(Objects.nonNull(model.getPaymentMode())){
			    		    rowData.add(model.getPaymentMode());
			    		}
		    	    	break;
	    		case "transactiondate":
                        	if(Objects.nonNull(model.getTransactionDate())){
                    		    rowData.add(model.getTransactionDate());
                    		}
                        	break;
	    		case "transactionamount":
                        	if(Objects.nonNull(model.getTransactionAmount())){
                    		    rowData.add(model.getTransactionAmount());
                    		}
                        	break;
	    		case "sendername":
                        	if(Objects.nonNull(model.getSenderName())){
                    		    rowData.add(model.getSenderName());
                    		}
                        	break;
	    		case "UTRno":
                        	if(Objects.nonNull(model.getUtrNumber())){
                    		    rowData.add(model.getUtrNumber());
                    		}
                        	break;
	    		case "corparateaccountno":
                        	if(Objects.nonNull(model.getCorporateActualAccountNumber())){
                    		    rowData.add(model.getCorporateActualAccountNumber());
                    		}
                        	break;
	    		case "senderinfo":
                        	if(Objects.nonNull(model.getSenderInformation())){
                    		    rowData.add(model.getSenderInformation());
                    		}
                        	break;
	    		}
	    	}

	    	data.add(rowData);
	    }
	}
	
	fileConvertion.toExcel(data, path[0]);
	
	return path[1];
    }
    
    
    /**
     * this method is used Statement Enquery PDF Report
     * @author Satya Karanam
     * @param statementInformationModels
     * @param permittedFields
     * @param userID
     * @param reportType
     * @param reportName
     * @return
     */
    private String preparePDFStatementEnquery(List<StatementInformationModel> statementInformationModels, List<ReportFieldModel> permittedFields,String userID, String reportType, String reportName, String appURL) {
    	String fileName = userID+"_"+String.valueOf(new Date().getTime())+".pdf";
    	String[] path = reportDumpingPath(userID, reportType, fileName, PDF_FORMAT, appURL);
    	
final List<List<Object>> data = new ArrayList<>();
    	
    	if(!CollectionUtils.isEmpty(permittedFields)) {
    	    final List<Object> labels = new ArrayList<>();
    	    
    	    labels.add("Serial Number");
    	    
    	    for(ReportFieldModel fieldModel : permittedFields) {
    		labels.add(fieldModel.getFieldLabel());
    	    }
    	    
    	    data.add(labels);
    	}
    	
    	
    	if(!CollectionUtils.isEmpty(statementInformationModels)) {
    	    for(StatementInformationModel model : statementInformationModels) {
    		final List<Object> rowData = new ArrayList<>();
    		
    		if(StringUtils.isNotBlank(model.getSerialiNumber())){
    		    rowData.add(model.getSerialiNumber());
    		} else {
    		    rowData.add("");
    		}
    		
    		for(ReportFieldModel fieldModel : permittedFields) {
    		
    		    switch (fieldModel.getReportField()) {
            		case "transctiondate":
                		if(StringUtils.isNotBlank(model.getTransactionDate())){
                		    rowData.add(model.getTransactionDate());
                		} else {
                		    rowData.add("");
                		}
            			
                		break;
            		case "chequeNo":
                		if(StringUtils.isNotBlank(model.getChequeNumber())){
                		    rowData.add(model.getChequeNumber());
                		} else {
                		    rowData.add("");
                		}
                		
            			break;
            		case "particulars":
                		if(StringUtils.isNotBlank(model.getParticulars())){
                		    rowData.add(model.getParticulars());
                		} else {
                		    rowData.add("");
                		}
                		
            			break;
            		case "debit":
                		if(StringUtils.isNotBlank(model.getDebitBalance())){
                		    rowData.add(model.getDebitBalance());
                		} else {
                		    rowData.add("");
                		}
                		
            			break;
            		case "credit":
                		if(StringUtils.isNotBlank(model.getCreditBalance())){
                		    rowData.add(model.getCreditBalance());
                		} else {
                		    rowData.add("");
                		}
                		
            			break;
            		case "balanceammount":
                		if(StringUtils.isNotBlank(model.getBalanceAmount())){
                		    rowData.add(model.getBalanceAmount());
                		} else {
                		    rowData.add("");
                		}
                		
            			break;
            		case "sol":
                		if(StringUtils.isNotBlank(model.getiSol())){
                		    rowData.add(model.getiSol());
                		} else {
                		    rowData.add("");
                		}
            			break;
            		}
        	}
    		
    		data.add(rowData);
    	    }
    	}
    	
    	fileConvertion.toPDF(data, path[0], reportName+userID);
    	
    	
    	return path[1];
    }
    

    /**
     * this method is used Statement Enquery Excel Report
     * @author Satya Karanam
     * @param statementInformationModels
     * @param permittedFields
     * @param userID
     * @param reportType
     * @return
     */
    private String prepareExcelStatementEnquery(List<StatementInformationModel> statementInformationModels,List<ReportFieldModel> permittedFields, String userID, String reportType, String appURL){
	String fileName = userID+"_"+String.valueOf(new Date().getTime())+".xls";
	String[] path = reportDumpingPath(userID, reportType, fileName, EXCEL_FORMAT, appURL);
	
	final List<List<Object>> data = new ArrayList<>();
	
	if(!CollectionUtils.isEmpty(permittedFields)) {
	    final List<Object> labels = new ArrayList<>();
	    
	    labels.add("Serial Number");
	    
	    for(ReportFieldModel fieldModel : permittedFields) {
		labels.add(fieldModel.getFieldLabel());
	    }
	    
	    data.add(labels);
	}
	
	
	if(!CollectionUtils.isEmpty(statementInformationModels)) {
	    for(StatementInformationModel model : statementInformationModels) {
		final List<Object> rowData = new ArrayList<>();
		
		if(StringUtils.isNotBlank(model.getSerialiNumber())){
		    rowData.add(model.getSerialiNumber());
		} else {
		    rowData.add("");
		}
		
		for(ReportFieldModel fieldModel : permittedFields) {
		
		    switch (fieldModel.getReportField()) {
        		case "transctiondate":
            		if(StringUtils.isNotBlank(model.getTransactionDate())){
            		    rowData.add(model.getTransactionDate());
            		} else {
            		    rowData.add("");
            		}
        			
            		break;
        		case "chequeNo":
            		if(StringUtils.isNotBlank(model.getChequeNumber())){
            		    rowData.add(model.getChequeNumber());
            		} else {
            		    rowData.add("");
            		}
            		
        			break;
        		case "particulars":
            		if(StringUtils.isNotBlank(model.getParticulars())){
            		    rowData.add(model.getParticulars());
            		} else {
            		    rowData.add("");
            		}
            		
        			break;
        		case "debit":
            		if(StringUtils.isNotBlank(model.getDebitBalance())){
            		    rowData.add(model.getDebitBalance());
            		} else {
            		    rowData.add("");
            		}
            		
        			break;
        		case "credit":
            		if(StringUtils.isNotBlank(model.getCreditBalance())){
            		    rowData.add(model.getCreditBalance());
            		} else {
            		    rowData.add("");
            		}
            		
        			break;
        		case "balanceammount":
            		if(StringUtils.isNotBlank(model.getBalanceAmount())){
            		    rowData.add(model.getBalanceAmount());
            		} else {
            		    rowData.add("");
            		}
            		
        			break;
        		case "sol":
            		if(StringUtils.isNotBlank(model.getiSol())){
            		    rowData.add(model.getiSol());
            		} else {
            		    rowData.add("");
            		}
        			break;
        		}
    	}
		
		data.add(rowData);
	    }
	}
	
	fileConvertion.toExcel(data, path[0]);
	
	return path[1];
    }
    
    
    /**
     * this method is used Transaction Report PDF Report
     * @author Satya Karanam
     * @param transactionInformationModels
     * @param permittedFields
     * @param userID
     * @param reportType
     * @param reportName
     * @return
     */
    private String preparePDFTransactionReport(List<TransactionInformationModel> transactionInformationModels, List<ReportFieldModel> permittedFields,String userID, String reportType, String reportName, String appURL) {
    	String fileName = userID+"_"+String.valueOf(new Date().getTime())+".pdf";
    	String[] path = reportDumpingPath(userID, reportType, fileName, PDF_FORMAT, appURL);
    	
    	final List<List<Object>> data = new ArrayList<>();
    	
    	if(!CollectionUtils.isEmpty(permittedFields)) {
    	    final List<Object> labels = new ArrayList<>();
            	    
    	    labels.add("Serial Number");
    	    
    	    for(ReportFieldModel fieldModel : permittedFields) {
    	    	labels.add(fieldModel.getFieldLabel());
    	    }
    	    
    	    data.add(labels);
    	}
    	
    	for(TransactionInformationModel model : transactionInformationModels) {
    	    final List<Object> rowData = new ArrayList<>();
    		
    	    if(StringUtils.isNotBlank(model.getSerialNumber())){
    		    rowData.add(model.getSerialNumber());
    	    } else {
    		rowData.add(" ");
    	    }
    		
    	    for (ReportFieldModel fieldModel : permittedFields) {
    		switch (fieldModel.getReportField()) {

    		case "TransactionId":
    		    if(StringUtils.isNotBlank(model.getTransactionId())){
    			rowData.add(model.getTransactionId());
    		    } else {
    			rowData.add(" ");
    		    }
            			    
    		    break;
    		case "BenificiaryName":
    		    if(StringUtils.isNotBlank(model.getBeneficiaryName())){
    			rowData.add(model.getBeneficiaryName());
    		    } else {
    			rowData.add(" ");
    		    }

    		    break;
    		case "ProductName":
            	
    		    if(StringUtils.isNotBlank(model.getPaymentMethod())){
    			rowData.add(model.getPaymentMethod());
    		    } else {
    			rowData.add(" ");
    		    }

    		    break;
    		case "CorporateRef":
            	
    		    if(StringUtils.isNotBlank(model.getCustomerReferenceNumber())){
    			rowData.add(model.getCustomerReferenceNumber());
    		    } else {
    			rowData.add(" ");
    		    }

    		    break;
    		case "CorporateAccount":
            	
    		    if(StringUtils.isNotBlank(model.getCorporateAccount())){
    			rowData.add(model.getCorporateAccount());
    		    } else {
    			rowData.add(" ");
    		    }

    		    break;
    		case "TransactionAmount":
            	
    		    if(StringUtils.isNotBlank(model.getPayableAmount())){
    			rowData.add(model.getPayableAmount());
    		    } else {
    			rowData.add(" ");
    		    }
            			    
    		    break;
    		case "TransactionDescription":
    		    if(StringUtils.isNotBlank(model.getTransactionDescription())){
    			rowData.add(model.getTransactionDescription());
    		    } else {
    			rowData.add(" ");
    		    }
            			    
    		    break;
    		case "TransactionStatus":
    		    if(StringUtils.isNotBlank(model.getSystemRequestStatus())){
    			rowData.add(model.getSystemRequestStatus());
    		    } else {
    			rowData.add(" ");
            		
    		    }
            		
    		    break;
    		    
    		case "TransactionUTROrCheque":
    			if(StringUtils.isNotBlank(model.getSystemRequestStatus())){
    				rowData.add(model.getTransactionUTROrCheque());
    		    } else {
    				rowData.add(" ");
    		    }
    			break;
    		}
    	    }
    	    
    	    data.add(rowData);
    	}
    	
    	fileConvertion.toPDF(data, path[0], reportName+" "+userID);
    	
    	return path[1];
    }
    
    
    /**
     * this method is used Transaction Report Excel Report
     * @author Satya Karanam
     * @param transactionInformationModels
     * @param permittedFields
     * @param userID
     * @param reportType
     * @return
     */
    private String prepareExcelTransactionReport(List<TransactionInformationModel> transactionInformationModels,List<ReportFieldModel> permittedFields, String userID, String reportType, String appURL){
	String fileName = userID+"_"+String.valueOf(new Date().getTime())+".xls";
	String[] path = reportDumpingPath(userID, reportType, fileName, EXCEL_FORMAT, appURL);
	
	final List<List<Object>> data = new ArrayList<>();
	
	if(!CollectionUtils.isEmpty(permittedFields)) {
	    final List<Object> labels = new ArrayList<>();
        	    
	    labels.add("Serial Number");
	    
	    for(ReportFieldModel fieldModel : permittedFields) {
		labels.add(fieldModel.getFieldLabel());
	    }

	    data.add(labels);
	}
	
	for(TransactionInformationModel model : transactionInformationModels) {
	    final List<Object> rowData = new ArrayList<>();
		
	    if(StringUtils.isNotBlank(model.getSerialNumber())){
		    rowData.add(model.getSerialNumber());
	    } else {
		rowData.add("");
	    }
		
	    for (ReportFieldModel fieldModel : permittedFields) {
		switch (fieldModel.getReportField()) {

		case "TransactionId":
		    if(StringUtils.isNotBlank(model.getTransactionId())){
			rowData.add(model.getTransactionId());
		    } else {
			rowData.add("");
		    }
        			    
		    break;
		case "BenificiaryName":
		    if(StringUtils.isNotBlank(model.getBeneficiaryName())){
			rowData.add(model.getBeneficiaryName());
		    } else {
			rowData.add("");
		    }

		    break;
		case "ProductName":
        	
		    if(StringUtils.isNotBlank(model.getPaymentMethod())){
			rowData.add(model.getPaymentMethod());
		    } else {
			rowData.add("");
		    }

		    break;
		case "CorporateRef":
        	
		    if(StringUtils.isNotBlank(model.getCustomerReferenceNumber())){
			rowData.add(model.getCustomerReferenceNumber());
		    } else {
			rowData.add("");
		    }

		    break;
		case "CorporateAccount":
        	
		    if(StringUtils.isNotBlank(model.getCorporateAccount())){
			rowData.add(model.getCorporateAccount());
		    } else {
			rowData.add("");
		    }

		    break;
		case "TransactionAmount":
        	
		    if(StringUtils.isNotBlank(model.getPayableAmount())){
			rowData.add(model.getPayableAmount());
		    } else {
			rowData.add("");
		    }
        			    
		    break;
		case "TransactionDescription":
		    if(StringUtils.isNotBlank(model.getTransactionDescription())){
			rowData.add(model.getTransactionDescription());
		    } else {
			rowData.add("");
		    }
        			    
		    break;
		case "TransactionStatus":
		    if(StringUtils.isNotBlank(model.getSystemRequestStatus())){
			rowData.add(model.getSystemRequestStatus());
		    } else {
			rowData.add("");
        		
		    }
        		
		    break;
		case "TransactionUTROrCheque":
			if(StringUtils.isNotBlank(model.getSystemRequestStatus())){
				rowData.add(model.getTransactionUTROrCheque());
		    } else {
				rowData.add(" ");
		    }
			break;
		}
	    }
		
	    data.add(rowData);
	}
	
	fileConvertion.toExcel(data, path[0]);
	
	return path[1];
    }
    
    private String[] reportDumpingPath(String userID, String reportType, String fileName, String fileFormat, String appURL){
	final StringBuilder actualPath = new StringBuilder();
	final StringBuilder relativePath = new StringBuilder();
	
	final Date date = new Date();
	
	actualPath.append(directoryUtil.getReportDumpingPath());
	actualPath.append("/"+userID);
	actualPath.append("/"+reportType);
	actualPath.append("/"+fileFormat);
	actualPath.append("/"+AxisbankDateFormat.DATE_TIME_FORMAT.format(date).replace(" ", "_").replace(":", "_"));
	
	final File file = new File(actualPath.toString());
	
	if(!file.exists()) {
	    file.mkdirs();
	}
	
	actualPath.append("/"+fileName);
	
	relativePath.append(appURL);
	relativePath.append("/api/download/?path=");
	relativePath.append("/"+userID);
	relativePath.append("/"+reportType);
	relativePath.append("/"+fileFormat);
	relativePath.append("/"+AxisbankDateFormat.DATE_TIME_FORMAT.format(date).replace(" ", "_").replace(":", "_"));
	relativePath.append("/"+fileName);
	
	return new String[]{actualPath.toString(), relativePath.toString()};
    }
}
