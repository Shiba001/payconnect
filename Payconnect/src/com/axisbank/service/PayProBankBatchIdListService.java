package com.axisbank.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbank.converter.BankBatchIdListRequestConveter;
import com.axisbank.converter.BankBatchIdListResponseConveter;
import com.axisbank.dao.BankBatchIDListDAO;
import com.axisbank.entity.BankBatchIDList;
import com.payconnect.model.BankBatchIdListRequestModel;
import com.payconnect.model.BankBatchIdListResponseModel;

@Service
@Transactional
public class PayProBankBatchIdListService {
	
 @Autowired
 private BankBatchIDListDAO bankBatchIDListDAO;	
 
 @Autowired
 private BankBatchIdListRequestConveter bankBatchIdListRequestConveter;
 
 
 @Autowired
 private BankBatchIdListResponseConveter bankBatchIdListResponseConveter;
 
 
 /**
  * 
  * @param batchIdListRequestModel
  * @return
  * @throws SQLException
  */

  public BankBatchIdListResponseModel getBankBatchIdListResponseModel(BankBatchIdListRequestModel batchIdListRequestModel) throws SQLException{
	  
	  
	  BankBatchIDList bankBatchIDList = bankBatchIDListDAO.getBankBatchIDList(bankBatchIdListRequestConveter.modelToEntity(batchIdListRequestModel));
	  
	  
	  return bankBatchIdListResponseConveter.entityToModel(bankBatchIDList);
	  
	
  }
	

}
