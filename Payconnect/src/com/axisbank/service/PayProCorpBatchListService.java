package com.axisbank.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbank.converter.CorpBatchIdListRequestConveter;
import com.axisbank.converter.CorpBatchIdListResponseConveter;
import com.axisbank.dao.CorpBatchIDListDAO;
import com.axisbank.entity.CorpBatchIDList;
import com.payconnect.model.CorpBatchIDListRequestModel;
import com.payconnect.model.CorpBatchIDListResponseModel;

@Service
@Transactional
public class PayProCorpBatchListService {
	
	@Autowired
	private CorpBatchIDListDAO corpBatchIDListDAO;
	
	@Autowired
	private CorpBatchIdListRequestConveter corpBatchIdListRequestConveter;
	
	@Autowired
	private CorpBatchIdListResponseConveter corpBatchIdListResponseConveter;
	
	
	public CorpBatchIDListResponseModel getCorpBatchIdListResponseModel(CorpBatchIDListRequestModel corpBatchIDListRequestModel) throws SQLException{
	    CorpBatchIDList corpBatchIDList = corpBatchIDListDAO.getCorpBatchIDList(corpBatchIdListRequestConveter.modelToEntity(corpBatchIDListRequestModel));

	    return corpBatchIdListResponseConveter.entityToModel(corpBatchIDList);
		  
		
	  }
	
	
	

}
