package com.axisbank.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.axisbank.converter.ApplicationReportFieldConverter;
import com.axisbankcmspaypro.dao.AppReportFieldDAO;
import com.axisbankcmspaypro.dao.PayConnectReportFieldDAO;
import com.axisbankcmspaypro.entity.PayConnectUserReportField;
import com.payconnect.model.ReportFieldModel;

@Service
@Transactional
public class AppFieldMaskingService {

	Logger logger = Logger.getLogger(AppFieldMaskingService.class);

	@Autowired
	private AppReportFieldDAO appReportFieldDAO;
	
	@Autowired
	private ApplicationReportFieldConverter applicationReportFieldConverter ;
	
	@Autowired
	private PayConnectReportFieldDAO connectReportFieldDAO; 
	
	
	
	/**
	 * 
	 * @param appUserID
	 * @param reportMenuID
	 * @return
	 */
	
	public List<ReportFieldModel> listOfReportMenuByApplicationUser(Integer appUserID, Integer reportMenuID){
		final List<ReportFieldModel> reportFieldModels = new ArrayList<>();
		
		final List<PayConnectUserReportField> connectUserReportFields = connectReportFieldDAO.getListOfPermittedField(appUserID, reportMenuID);
		
		if(!CollectionUtils.isEmpty(connectUserReportFields)){
			for(PayConnectUserReportField connectUserReportField : connectUserReportFields){
				reportFieldModels.add(applicationReportFieldConverter.entityToModel(connectUserReportField.getAppReportField()));
			}
		}
		
		return reportFieldModels;
	}
	
	/**
	 * this method is used to get list of report field for a particular report (menu ID) an user (user ID) can access
	 * @since 18-Feb-2016
	 * @author Avishek Seal
	 * @param userID
	 * @param reportMenuID
	 * @return
	 */
	public List<ReportFieldModel> listOfReportMenuByApplicationUser(String userID, Integer reportMenuID){
		final List<ReportFieldModel> reportFieldModels = new ArrayList<>();
		
		final List<PayConnectUserReportField> connectUserReportFields = connectReportFieldDAO.getListOfPermittedField(userID, reportMenuID);
		
		if(!CollectionUtils.isEmpty(connectUserReportFields)){
			for(PayConnectUserReportField connectUserReportField : connectUserReportFields){
				reportFieldModels.add(applicationReportFieldConverter.entityToModel(connectUserReportField.getAppReportField()));
			}
		}
		
		return reportFieldModels;
	}

	/**
	 * this method is used to get list of report field for a particular report (report unique key) an user (user ID) can access
	 * @since 18-Feb-2016
	 * @author Avishek Seal
	 * @param userID
	 * @param reportMenuUniqueKey
	 * @return
	 */
	public List<ReportFieldModel> listOfReportMenuByApplicationUser(String userID, String reportMenuUniqueKey, String system){
		final List<ReportFieldModel> reportFieldModels = new ArrayList<>();
		
		final List<PayConnectUserReportField> connectUserReportFields = connectReportFieldDAO.getListOfPermittedField(userID, reportMenuUniqueKey, system);
		
		if(!CollectionUtils.isEmpty(connectUserReportFields)){
			for(PayConnectUserReportField connectUserReportField : connectUserReportFields){
				reportFieldModels.add(applicationReportFieldConverter.entityToModel(connectUserReportField.getAppReportField()));
			}
		}
		
		return reportFieldModels;
	}
}
