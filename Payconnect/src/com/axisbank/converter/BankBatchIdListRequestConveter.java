package com.axisbank.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.BankBatchIDRequest;
import com.payconnect.model.BankBatchIdListRequestModel;

@Component
public class BankBatchIdListRequestConveter implements Converter<BankBatchIDRequest,BankBatchIdListRequestModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5665091393271724682L;

	/**
	 * 
	 */
	
	@Override
	public BankBatchIDRequest modelToEntity(BankBatchIdListRequestModel m) {
		
		if(m!=null){
			
			final BankBatchIDRequest bankBatchIDRequest=new BankBatchIDRequest();
			bankBatchIDRequest.setVersion(m.getApiVersion());
			bankBatchIDRequest.setLoginID(m.getLoginId());
			bankBatchIDRequest.setCorporateCode(m.getCorporateCode());
	        bankBatchIDRequest.setEndDate(m.getEndDate());
	        bankBatchIDRequest.setStartDate(m.getStartdate());
	        
	        return bankBatchIDRequest;
		}
		
		return null;
		
		
	}

	@Override
	public BankBatchIdListRequestModel entityToModel(BankBatchIDRequest m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BankBatchIdListRequestModel> entityToModel(
			List<BankBatchIDRequest> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BankBatchIDRequest> modelToEntity(
			List<BankBatchIdListRequestModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

	
	

	
	
	

}
