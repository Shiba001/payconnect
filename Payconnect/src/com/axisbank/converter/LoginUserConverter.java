/**
 * 
 */
package com.axisbank.converter;

import java.io.Serializable;

/**
 * @author Avishek Seal
 *
 */
public interface LoginUserConverter<R, P, M> extends Serializable{

    M toUser(R r, P p);
}
