/**
 * 
 */
package com.axisbank.converter;

import java.io.Serializable;
import java.util.List;

import com.payconnect.model.ReportFieldModel;

/**
 * @author Avishek Seal
 *
 */
public interface ReportConverter<E, M> extends Serializable{
    
    M convert(E e, List<ReportFieldModel> fieldModels);
}
