package com.axisbank.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.BankBatchIDInformation;
import com.axisbank.entity.BankBatchIDList;
import com.payconnect.model.BankBatchIDInformationModel;
import com.payconnect.model.BankBatchIdListResponseModel;

@Component
public class BankBatchIdListResponseConveter implements Converter<BankBatchIDList, BankBatchIdListResponseModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2846365790560413858L;

	@Override
	public BankBatchIDList modelToEntity(BankBatchIdListResponseModel m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BankBatchIdListResponseModel entityToModel(BankBatchIDList bankBatchIDList) {
		
		if(bankBatchIDList!=null){
			
			final BankBatchIdListResponseModel bankBatchIdListResponseModel=new BankBatchIdListResponseModel();
			
			final List<BankBatchIDInformationModel>bankBatchIDInformationModels=new ArrayList<BankBatchIDInformationModel>();
			
			bankBatchIdListResponseModel.setResponseCode(bankBatchIDList.getResponseCode());
			
			bankBatchIdListResponseModel.setError(bankBatchIDList.getError());
			
			bankBatchIdListResponseModel.setResponseDetails(bankBatchIDList.getResponseDetails());
			
			for(BankBatchIDInformation bankBatchIDInformation:bankBatchIDList.getBankBatchIDInformations()){
				
				BankBatchIDInformationModel bankBatchIDInformationModel=new BankBatchIDInformationModel();
				
				bankBatchIDInformationModel.setBankBatchNumber(bankBatchIDInformation.getBankBatchNumber());
				
				bankBatchIDInformationModel.setCorpBatchNumber(bankBatchIDInformation.getCorpBatchNumber());
				
			    bankBatchIDInformationModels.add(bankBatchIDInformationModel);
				
			
			}
			
			
			bankBatchIdListResponseModel.setBankBatchIDInformationModels(bankBatchIDInformationModels);
			
			
			return bankBatchIdListResponseModel;
			
		}
		
		
		
		
		return null;

	}

	@Override
	public List<BankBatchIdListResponseModel> entityToModel(
			List<BankBatchIDList> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BankBatchIDList> modelToEntity(
			List<BankBatchIdListResponseModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
