package com.axisbank.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.Request;
import com.common.Common;
import com.payconnect.model.LoginRequestModel;

@Component
public class RequestConverter implements Converter<Request, LoginRequestModel> {

	private static final long serialVersionUID = -1311045921451730633L;

	@Override
	public Request modelToEntity(LoginRequestModel m) {
		final Request request = new Request();
		
		request.setUserName(m.getUserName());
		request.setPassword(Common.generateEncryptedPassword(m.getUserPassward()));//encription logic here
		request.setCorporateCode(m.getCorporateID());
		request.setLatitude(m.getLatitude());
		request.setLongitude(m.getLongitude());
		request.setIpAddress(m.getClientIp());
		request.setDeviceId(m.getDeviceID());
		request.setApiVersion(m.getApiVersion());
		
		return request;
	}
	
	@Override
	public LoginRequestModel entityToModel(Request m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LoginRequestModel> entityToModel(List<Request> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Request> modelToEntity(List<LoginRequestModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
