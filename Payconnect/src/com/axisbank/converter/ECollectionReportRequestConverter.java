/**
 * 
 */
package com.axisbank.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.ECollectionRequest;
import com.payconnect.model.ECollectionReportRequestModel;

/**
 * @author Avishek Seal
 *
 */
@Component
public class ECollectionReportRequestConverter implements Converter<ECollectionRequest, ECollectionReportRequestModel>{

    private static final long serialVersionUID = -4139223242162357888L;

    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param m
     * @return
     */
    @Override
    public ECollectionRequest modelToEntity(ECollectionReportRequestModel m) {
	if(m != null) {
	    final ECollectionRequest eCollectionRequest = new ECollectionRequest();
	    
	    eCollectionRequest.setAmmountFrom(m.getAmmountFrom());
	    eCollectionRequest.setAmmountTo(m.getAmmountTo());
	    eCollectionRequest.setCorporateAccountNumber(m.getCorporateAccountNumber());
	    eCollectionRequest.setCorporateCode(m.getCorporateCode());
	    eCollectionRequest.setEndDate(m.getEndDate());
	    eCollectionRequest.setLogingID(m.getLogingID());
	    eCollectionRequest.setPageNumber(m.getPageNumber());
	    eCollectionRequest.setProductName(m.getProductName());
	    eCollectionRequest.setRecordPerPage(m.getRecordPerPage());
	    eCollectionRequest.setStartDate(m.getStartDate());
	    eCollectionRequest.setTransactionalUTR(m.getTransactionalUTR());
	    eCollectionRequest.setVersion(m.getVersion());
	    
	    return eCollectionRequest;
	}
	return null;
    }

    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param m
     * @return
     */
    @Override
    public ECollectionReportRequestModel entityToModel(ECollectionRequest m) {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param es
     * @return
     */
    @Override
    public List<ECollectionReportRequestModel> entityToModel(
	    List<ECollectionRequest> es) {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param ms
     * @return
     */
    @Override
    public List<ECollectionRequest> modelToEntity(
	    List<ECollectionReportRequestModel> ms) {
	// TODO Auto-generated method stub
	return null;
    }
    
    
}
