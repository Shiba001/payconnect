/**
 * 
 */
package com.axisbank.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.payconnect.model.LoginRequestModel;
import com.payconnect.model.LoginResponseModel;

/**
 * @author Avishek Seal
 * 
 */
@Component
public class RequestToPayConnectConverter implements LoginUserConverter<LoginRequestModel, LoginResponseModel, PayConnectUser>{

	private static final long serialVersionUID = -1739943989006514526L;

	@Autowired
	private ApplicationDAO applicationDAO;
	
	/**
	* @since 02-Mar-2016
	* @author Avishek Seal
	* @param r
	* @param p
	* @return
	*/
	@Override
	public PayConnectUser toUser(LoginRequestModel loginRequestModel, LoginResponseModel loginResponseModel) {
	    final PayConnectUser connectUser = new PayConnectUser();
		
	    connectUser.setUserID(loginResponseModel.getUserID());
	    connectUser.setDeviceID(loginRequestModel.getDeviceID());
	    connectUser.setApplication(applicationDAO.getApplicationBySystem(loginRequestModel.getSystemValueFromApi()));
		
	    return connectUser;
	}

}
