package com.axisbank.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.TransactionReportRequest;
import com.payconnect.model.TransactionReportRequestModel;

/**
 * 
 * @author Satya Karanam 18-Feb-2016
 */
@Component
public class TransactionReportRequestConverter implements
		Converter<TransactionReportRequest, TransactionReportRequestModel> {

	private static final long serialVersionUID = -1854573562581943561L;

	@Override
	public TransactionReportRequest modelToEntity(
			TransactionReportRequestModel m) {
		if (m != null) {
			final TransactionReportRequest transactionReportRequest = new TransactionReportRequest();

			transactionReportRequest.setAmountFrom(m.getAmountFrom());
			transactionReportRequest.setAmountTo(m.getAmountTo());
			transactionReportRequest.setBatchId(m.getBatchId());
			transactionReportRequest.setBenificiaryAccountNumber(m.getBenificiaryAccountNumber());
			transactionReportRequest.setBenificiaryIfscCode(m.getBenificiaryIfscCode());
			transactionReportRequest.setBenificiaryName(m.getBenificiaryName());
			transactionReportRequest.setChequeNumber(m.getChequeNumber());
			transactionReportRequest.setCorpAccountNumber(m.getCorpAccountNumber());
			transactionReportRequest.setEndDate(m.getEndDate());
			transactionReportRequest.setFileName(m.getFileName());
			transactionReportRequest.setCorporateID(m.getCorporateID());
			transactionReportRequest.setLoginID(m.getLoginID());
			transactionReportRequest.setPageNo(m.getPageNo());
			transactionReportRequest.setPayDocNumber(m.getPayDocNumber());
			transactionReportRequest.setProductCode(m.getProductCode());
			transactionReportRequest.setRecPerPage(m.getRecPerPage());
			transactionReportRequest.setStartDate(m.getStartDate());
			transactionReportRequest.setStatusCode(m.getStatusCode());
			transactionReportRequest.setTransactionId(m.getTransactionId());
			transactionReportRequest.setTransactionUtr(m.getTransactionUtr());
			transactionReportRequest.setUserId(m.getUserId());
			transactionReportRequest.setVendorCode(m.getVendorCode());
			transactionReportRequest.setVersion(m.getVersion());
			transactionReportRequest.setCorporateRef(m.getCorporateRef());
			transactionReportRequest.setBenificiaryCode(m.getBenificiaryCode());
			transactionReportRequest.setCorporateBatchID(m.getCorporateBatchID());

			return transactionReportRequest;
		}
		return null;
	}

	@Override
	public TransactionReportRequestModel entityToModel(
			TransactionReportRequest m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TransactionReportRequestModel> entityToModel(
			List<TransactionReportRequest> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TransactionReportRequest> modelToEntity(
			List<TransactionReportRequestModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
