package com.axisbank.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.TransactionInformation;
import com.axisbank.entity.TransactionReport;
import com.payconnect.model.ReportFieldModel;
import com.payconnect.model.TransactionInformationModel;
import com.payconnect.model.TransactionReportResponseModel;

/**
 * 
 * @author Satya Karanam 18-Feb-2016
 */
@Component
public class TransactionReportConverter implements
		ReportConverter<TransactionReport, TransactionReportResponseModel> {

	private static final long serialVersionUID = 686129888968912244L;

	@Override
	public TransactionReportResponseModel convert(TransactionReport e, List<ReportFieldModel> fieldModels) {

		if (e != null) {
			final TransactionReportResponseModel transactionReportResponseModel = new TransactionReportResponseModel();

			transactionReportResponseModel.setError(e.getError());
			transactionReportResponseModel.setExcelDownloadLink(e.getExcelDownloadLink());
			transactionReportResponseModel.setPdfDownloadLink(e.getPdfDownloadLink());
			transactionReportResponseModel.setResponseCode(e.getResponseCode());
			transactionReportResponseModel.setResponseDetails(e.getResponseDetails());
			transactionReportResponseModel.setTotalCount(e.getTotalCount());

			final List<TransactionInformationModel> transactionInformationModels = new ArrayList<>();
			
			for (TransactionInformation transactionInformation : e.getTransactionInformation()) {
				final TransactionInformationModel transactionInformationModel = new TransactionInformationModel();

				transactionInformationModel.setSerialNumber(transactionInformation.getSerialNumber());

				for (ReportFieldModel fieldModel : fieldModels) {

					switch (fieldModel.getReportField()) {
					case "TransactionId":
						transactionInformationModel.setTransactionId(transactionInformation.getTransactionId());
						break;

					case "BenificiaryName":
						transactionInformationModel.setBeneficiaryName(transactionInformation.getBeneficiaryName());
						break;

					case "ProductName":
						transactionInformationModel.setPaymentMethod(transactionInformation.getPaymentMethod());
						break;

					case "CorporateRef":
						transactionInformationModel.setCustomerReferenceNumber(transactionInformation.getCustomerReferenceNumber());
						break;

					case "CorporateAccount":
						transactionInformationModel.setCorporateAccount(transactionInformation.getCorporateAccount());
						break;

					case "TransactionAmount":
						transactionInformationModel.setPayableAmount(transactionInformation.getPayableAmount());
						break;

					case "TransactionDescription":
						transactionInformationModel.setTransactionDescription(transactionInformation.getTransactionDescription());
						break;

					case "TransactionStatus":
						transactionInformationModel.setSystemRequestStatus(transactionInformation.getSystemRequestStatus());
						break;
					
					case "TransactionUTROrCheque":
						transactionInformationModel.setTransactionUTROrCheque(transactionInformation.getTransactionUTROrCheque());
						break;
					}

				}
				transactionInformationModels.add(transactionInformationModel);

			}
			
			transactionReportResponseModel.setTransactionInformationModels(transactionInformationModels);
			
			return transactionReportResponseModel;

		}
		return null;
	}

}
