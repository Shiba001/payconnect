package com.axisbank.converter;

import java.io.Serializable;
import java.util.List;

public interface Converter<E, M> extends Serializable{
	
	E modelToEntity(M m);
	
	M entityToModel(E m);//
	
   public  List<M> entityToModel(List<E> es);
	
   public  List<E> modelToEntity(List<M> ms);
}
