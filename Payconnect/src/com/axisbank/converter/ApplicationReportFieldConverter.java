package com.axisbank.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axisbankcmspaypro.dao.AppReportFieldDAO;
import com.axisbankcmspaypro.entity.AppReportField;
import com.payconnect.model.ReportFieldModel;

@Component
public class ApplicationReportFieldConverter implements Converter<AppReportField, ReportFieldModel> {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private AppReportFieldDAO appReportFieldDAO;
	

	@Override
	public AppReportField modelToEntity(ReportFieldModel m) {
		AppReportField appReportField = appReportFieldDAO.find(m.getId());
		
		return appReportField;
	}

	@Override
	public ReportFieldModel entityToModel(AppReportField e) {
		final ReportFieldModel fieldModel = new ReportFieldModel();
		
		fieldModel.setId(e.getId());
		fieldModel.setFieldLabel(e.getFieldLabel());
		fieldModel.setReportField(e.getReportField());
		fieldModel.setMenuID(e.getAppMenu().getId());
		fieldModel.setMenuName(e.getAppMenu().getManuName());

		return fieldModel;
	}

	@Override
	public List<ReportFieldModel> entityToModel(List<AppReportField> es) {
		final List<ReportFieldModel> fieldModels = new ArrayList<>();
		
		for(AppReportField appReportField : es){
			fieldModels.add(entityToModel(appReportField));
		}
		
		return fieldModels;
	}

	@Override
	public List<AppReportField> modelToEntity(List<ReportFieldModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
