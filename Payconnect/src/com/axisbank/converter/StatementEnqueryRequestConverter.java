package com.axisbank.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.StatementEnqueryRequest;
import com.payconnect.model.StatementEnqueryRequestModel;

/**
 * 
 * @author Satya Karanam 18-Feb-2016
 */
@Component
public class StatementEnqueryRequestConverter implements
		Converter<StatementEnqueryRequest, StatementEnqueryRequestModel> {

	private static final long serialVersionUID = 4340539921333983678L;

	@Override
	public StatementEnqueryRequest modelToEntity(StatementEnqueryRequestModel m) {
		if (m != null) {
			final StatementEnqueryRequest statementEnqueryRequest = new StatementEnqueryRequest();

			statementEnqueryRequest.setCorporateAccountNumber(m.getCorporateAccountNumber());
			statementEnqueryRequest.setCorporateCode(m.getCorporateCode());
			statementEnqueryRequest.setEndDate(m.getEndDate());
			statementEnqueryRequest.setPageNumber(m.getPageNumber());
			statementEnqueryRequest.setRecordPerPage(m.getRecordPerPage());
			statementEnqueryRequest.setStartDate(m.getStartDate());
			statementEnqueryRequest.setStatementType(m.getStatementType());
			statementEnqueryRequest.setUserID(m.getUserID());
			statementEnqueryRequest.setApiVersion(m.getApiVersion());
			statementEnqueryRequest.setFileType(m.getFileType());
			statementEnqueryRequest.setAccountname(m.getAccountName());
			
			statementEnqueryRequest.setSystem(m.getSystem());
			

			return statementEnqueryRequest;
		}
		return null;
	}

	@Override
	public StatementEnqueryRequestModel entityToModel(StatementEnqueryRequest m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StatementEnqueryRequestModel> entityToModel(
			List<StatementEnqueryRequest> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StatementEnqueryRequest> modelToEntity(
			List<StatementEnqueryRequestModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
