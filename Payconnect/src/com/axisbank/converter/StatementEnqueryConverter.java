package com.axisbank.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.StatementEnquery;
import com.axisbank.entity.StatementInformation;
import com.payconnect.model.ReportFieldModel;
import com.payconnect.model.StatementEnqueryResponseModel;
import com.payconnect.model.StatementInformationModel;

/**
 * 
 * @author Satya Karanam 18-Feb-2016
 */
@Component
public class StatementEnqueryConverter implements
		ReportConverter<StatementEnquery, StatementEnqueryResponseModel> {

	private static final long serialVersionUID = 7843010674656531771L;

	@Override
	public StatementEnqueryResponseModel convert(StatementEnquery e, List<ReportFieldModel> fieldModels) {
		if (e != null) {
			final StatementEnqueryResponseModel statementEnqueryResponseModel = new StatementEnqueryResponseModel();
			statementEnqueryResponseModel.setCurrency(e.getCurrency());
			statementEnqueryResponseModel.setCustomerAddress(e.getCustomerAddress());
			statementEnqueryResponseModel.setCustomerID(e.getCustomerID());
			statementEnqueryResponseModel.setCustomerName(e.getCustomerName());
			statementEnqueryResponseModel.setDownloadLink(e.getDownloadLink());
			statementEnqueryResponseModel.setError(e.getError());
			statementEnqueryResponseModel.setFileType(e.getFileType());
			statementEnqueryResponseModel.setMessage(e.getMessage());
			statementEnqueryResponseModel.setResponseCode(e.getResponseCode());
			statementEnqueryResponseModel.setResponseDeatils(e.getResponseDeatils());
			statementEnqueryResponseModel.setSchemeDescription(e.getSchemeDescription());
			statementEnqueryResponseModel.setStatementType(e.getStatementType());
			statementEnqueryResponseModel.setTotalRecordCount(e.getTotalRecordCount());

			final List<StatementInformationModel> statementInformationModels = new ArrayList<>();

			for (StatementInformation statementInformation : e.getStatementInformations()) {

				final StatementInformationModel statementInformationModel = new StatementInformationModel();

				statementInformationModel.setSerialiNumber(statementInformation
						.getSerialiNumber());

				for (ReportFieldModel fieldModel : fieldModels) {

					switch (fieldModel.getReportField()) {
					case "transctiondate":
						statementInformationModel.setTransactionDate(statementInformation.getTransactionDate());
						break;
						
					case "chequeNo":
						statementInformationModel.setChequeNumber(statementInformation.getChequeNumber());
						break;
						
					case "particulars":
						statementInformationModel.setParticulars(statementInformation.getParticulars());
						break;
						
					case "debit":
						statementInformationModel.setDebitBalance(statementInformation.getDebitBalance());
						break;
						
					case "credit":
						statementInformationModel.setCreditBalance(statementInformation.getCreditBalance());
						break;
						
					case "balanceammount":
						statementInformationModel.setBalanceAmount(statementInformation.getBalanceAmount());
						break;
						
					case "sol":
						statementInformationModel.setiSol(statementInformation.getiSol());
						break;
					}
				}

				statementInformationModels.add(statementInformationModel);
			}

			statementEnqueryResponseModel.setStatementInformationModels(statementInformationModels);
			
			return statementEnqueryResponseModel;
		}

		return null;
	}

}
