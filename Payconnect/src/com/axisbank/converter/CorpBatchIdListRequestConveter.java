package com.axisbank.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbank.entity.CorpBatchIDRequest;
import com.payconnect.model.CorpBatchIDListRequestModel;

@Component
public class CorpBatchIdListRequestConveter implements Converter<CorpBatchIDRequest, CorpBatchIDListRequestModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2977437133411722689L;

	@Override
	public CorpBatchIDRequest modelToEntity(CorpBatchIDListRequestModel corpBatchIDListRequestModel) {
		
		if(corpBatchIDListRequestModel!=null){
			
			final CorpBatchIDRequest corpBatchIDRequest=new CorpBatchIDRequest();
			corpBatchIDRequest.setVersion(corpBatchIDListRequestModel.getApiVersion());
			corpBatchIDRequest.setCorporateCode(corpBatchIDListRequestModel.getCorporateCode());
			corpBatchIDRequest.setUserId(corpBatchIDListRequestModel.getUserId());
			corpBatchIDRequest.setStartDate(corpBatchIDListRequestModel.getStartdate());
			corpBatchIDRequest.setEndDate(corpBatchIDListRequestModel.getEndDate());
			
			return corpBatchIDRequest;
			
		}
		return null;
		
	}

	@Override
	public CorpBatchIDListRequestModel entityToModel(CorpBatchIDRequest m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CorpBatchIDListRequestModel> entityToModel(List<CorpBatchIDRequest> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CorpBatchIDRequest> modelToEntity(
			List<CorpBatchIDListRequestModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
