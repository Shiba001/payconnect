/**
 * 
 */
package com.axisbank.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axisbank.common.FileConvertion;
import com.axisbank.entity.ECollectionInformation;
import com.axisbank.entity.ECollectionReport;
import com.axisbankcmspaypro.comon.AxisbankDateFormat;
import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.payconnect.model.ECollectionInformationResponseModel;
import com.payconnect.model.ECollectionResponseModel;
import com.payconnect.model.ReportFieldModel;

/**
 * @author Avishek Seal
 *
 */
@Component
public class ECollectionReportConverter implements ReportConverter<ECollectionReport, ECollectionResponseModel> {

    private static final long serialVersionUID = 6226778298738209178L;

    /**
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param e
     * @param fieldModels
     * @return
     */
    @Override
    public ECollectionResponseModel convert(ECollectionReport e, List<ReportFieldModel> fieldModels) {
	if(e != null) {
	    final ECollectionResponseModel eCollectionResponseModel = new ECollectionResponseModel();
	    
	    eCollectionResponseModel.setError(e.getError());
	    eCollectionResponseModel.setExcelDownloadLink(e.getExcelDownloadLink());
	    eCollectionResponseModel.setPdfDownloadLink(e.getPdfDownloadLink());
	    eCollectionResponseModel.setResponseCode(e.getResponseCode());
	    eCollectionResponseModel.setResponseDetails(e.getResponseDetails());
	    eCollectionResponseModel.setTotalCount(e.getTotalCount());
	    
	    final List<ECollectionInformationResponseModel> collectionInformationResponseModels = new ArrayList<>();
	    
	    for(ECollectionInformation collectionInformation : e.geteCollectionInformations()) {
		final ECollectionInformationResponseModel collectionInformationResponseModel = new ECollectionInformationResponseModel();
		
		collectionInformationResponseModel.setSerialNumber(collectionInformation.getSerialNumber());
		
		for(ReportFieldModel fieldModel : fieldModels) {
		    
		    switch (fieldModel.getReportField()) {
		    	case "paymentmode":
		    	    	collectionInformationResponseModel.setPaymentMode(collectionInformation.getPaymentMode());
		    	    	break;
                        case "transactiondate":
                            	collectionInformationResponseModel.setTransactionDate(collectionInformation.getTransactionDate());
                        	break;
                        case "transactionamount":
                            	collectionInformationResponseModel.setTransactionAmount(collectionInformation.getTransactionAmount());
                        	break;
                        case "sendername":
                            	collectionInformationResponseModel.setSenderName(collectionInformation.getSenderName());
                        	break;
                        case "UTRno":
                            	collectionInformationResponseModel.setUtrNumber(collectionInformation.getUtrNumber());
                        	break;
                        case "corparateaccountno":
                            	collectionInformationResponseModel.setCorporateActualAccountNumber(collectionInformation.getCorporateActualAccountNumber());
                        	break;
                        case "senderinfo":
                            	collectionInformationResponseModel.setSenderInformation(collectionInformation.getSenderInformation());
                        	break;
		    }
		}
		
		collectionInformationResponseModels.add(collectionInformationResponseModel);
	    }
	    
	    eCollectionResponseModel.setCollectionInformationResponseModels(collectionInformationResponseModels);
	    
	    return eCollectionResponseModel; 
	}
	
	return null;
    }
    
}
