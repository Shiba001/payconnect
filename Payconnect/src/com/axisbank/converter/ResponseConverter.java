package com.axisbank.converter;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.axisbank.entity.Response;
import com.payconnect.model.LoginResponseModel;

@Component
public class ResponseConverter implements
	Converter<Response, LoginResponseModel> {

    private static final long serialVersionUID = -6926267592928098280L;

    @Override
    public Response modelToEntity(LoginResponseModel m) {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     * @author Avishek Seal
     */
    @Override
    public LoginResponseModel entityToModel(Response m) {
	final LoginResponseModel loginResponseModel = new LoginResponseModel();

	if (!Objects.equals(m.getUserLoginDetails(), null)) {

	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getEmailID())) {
	    	loginResponseModel.setEmailID(m.getUserLoginDetails().getEmailID());
	    }

	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getLastChangePaswordDate())) {
	    	loginResponseModel.setLastChangePaswordDate(m.getUserLoginDetails().getLastChangePaswordDate());
	    }

	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getMobileNumber())) {
	    	loginResponseModel.setMobileNumber(m.getUserLoginDetails().getMobileNumber());
	    }

	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getPasswordExpiryDate())) {
	    	loginResponseModel.setPasswordExpiryDate(m.getUserLoginDetails().getPasswordExpiryDate());
	    }
	    
	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getUserName())) {
	    	loginResponseModel.setUserName(m.getUserLoginDetails().getUserName());
	    }
	    
	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getFirstName())) {
	    	loginResponseModel.setFirstName(m.getUserLoginDetails().getFirstName());
	    }
	    
	    
	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getLastname())) {
	    	loginResponseModel.setLastName(m.getUserLoginDetails().getLastname());
	    }
	    
	    

	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getLastLogin())) {
	    	loginResponseModel.setLastLogin(m.getUserLoginDetails().getLastLogin());
	    }
	    
	    
	   if (StringUtils.isNotBlank(m.getUserLoginDetails().getUserId())) {
		loginResponseModel.setUserID(m.getUserLoginDetails().getUserId());
	    }
	    

	   if (StringUtils.isNotBlank(m.getUserLoginDetails().getCorporateID())) {
		loginResponseModel.setCorpID(m.getUserLoginDetails().getCorporateID());
	   }
	   
	    loginResponseModel.setUserType(m.getUserLoginDetails().getUserType());
	   
	    loginResponseModel.setSenderShortCode(m.getUserLoginDetails().getSenderShortCode());
	    
	    loginResponseModel.setSenderID("");
	    
	    loginResponseModel.setPasswordChangeinWeb(m.getUserLoginDetails().getPasswordChangeInWeb());
	    
	    loginResponseModel.setPasswordChangeNeed(m.getUserLoginDetails().getPasswordChangeNeed());
	    
	    
	    if (StringUtils.isNotBlank(m.getUserLoginDetails().getUserStatus())) {
		    loginResponseModel.setUserStatus(m.getUserLoginDetails().getUserStatus());
		} else {
		    loginResponseModel.setUserStatus("");
		}
	}

	if (StringUtils.isNotBlank(m.getError())) {
	    loginResponseModel.setError(m.getError());
	}

	if (StringUtils.isNotBlank(m.getResponseCode())) {
	    loginResponseModel.setResponseCode(m.getResponseCode());
	}

	if (StringUtils.isNotBlank(m.getResponseDetails())) {
	    loginResponseModel.setResponseDetails(m.getResponseDetails());
	} else {
	    loginResponseModel.setResponseDetails("");
	}

	
	

	return loginResponseModel;
    }

	@Override
	public List<LoginResponseModel> entityToModel(List<Response> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Response> modelToEntity(List<LoginResponseModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
