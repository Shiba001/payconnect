package com.axisbank.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.axisbank.entity.CorpBatchIDList;
import com.payconnect.model.CorpBatchIDInformation;
import com.payconnect.model.CorpBatchIDListResponseModel;
import com.payconnect.model.TransactionInformationModel;

@Component
public class CorpBatchIdListResponseConveter implements
		Converter<CorpBatchIDList, CorpBatchIDListResponseModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3335150523062100553L;

	@Override
	public CorpBatchIDList modelToEntity(CorpBatchIDListResponseModel m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CorpBatchIDListResponseModel entityToModel(
			CorpBatchIDList corpBatchIDList) {

		if (corpBatchIDList != null) {
			final CorpBatchIDListResponseModel corpBatchIDListResponseModel = new CorpBatchIDListResponseModel();

			final List<CorpBatchIDInformation> corpBatchIDInformation = new ArrayList<>();
			
			corpBatchIDListResponseModel.setResponseCode(corpBatchIDList.getResponseCode());
			
			corpBatchIDListResponseModel.setResponseDetails(corpBatchIDList.getResponseDetails());
			
			for (com.axisbank.entity.CorpBatchIDInformation batchIDInformation : corpBatchIDList.getBatchIDInformations()) {
				CorpBatchIDInformation corpBatchIDInformationModel = new CorpBatchIDInformation();

				corpBatchIDInformationModel.setCorpBatchNumber(batchIDInformation.getCorpBatchNumber());
				corpBatchIDInformationModel.setCreationDate(batchIDInformation.getCreationDate());

				corpBatchIDInformation.add(corpBatchIDInformationModel);
			}
			
			corpBatchIDListResponseModel.setCorpBatchIDInformations(corpBatchIDInformation);
			
			return corpBatchIDListResponseModel;
		}
		
		return null;
	}

	@Override
	public List<CorpBatchIDListResponseModel> entityToModel(
			List<CorpBatchIDList> batchIDLists) {
		return null;

	}

	@Override
	public List<CorpBatchIDList> modelToEntity(
			List<CorpBatchIDListResponseModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
