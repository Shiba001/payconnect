package com.axisbank.converter;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.axisbank.entity.Request;
import com.common.Common;
import com.payconnect.model.ResetPasswordModel;

@Component
public class ResetPasswordConverter implements Converter<Request, ResetPasswordModel>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5363296198998844372L;

	@Override
	public Request modelToEntity(ResetPasswordModel m) {
		// TODO Auto-generated method stub
		
		Request request = new Request();
		request.setCorporateCode(m.getCorporateCode());
		request.setUserName(m.getUserName());
		request.setDeviceId(m.getDeviceID());
		request.setOldPassword(Common.generateEncryptedPassword(m.getOldPassword())); //encription logic here
		request.setPassword(Common.generateEncryptedPassword(m.getPassword()));  //encription logic here
		
		return request;
	}

	@Override
	public ResetPasswordModel entityToModel(Request m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ResetPasswordModel> entityToModel(List<Request> es) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Request> modelToEntity(List<ResetPasswordModel> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
