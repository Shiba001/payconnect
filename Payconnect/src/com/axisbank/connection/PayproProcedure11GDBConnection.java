package com.axisbank.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class PayproProcedure11GDBConnection {
	
	private static final Logger logger=Logger.getLogger(PayproProcedure11GDBConnection.class);

	public static Connection getConnection() {
		
		

		Connection connection = null;

		try {
			
			
			
			ResourceBundle resourceBundle=ResourceBundle.getBundle("resources.database");
			
			String oracleDriver=resourceBundle.getString("procedure.database.driver");
			
			logger.info("driver is "+oracleDriver);
			
			String URL=resourceBundle.getString("procedure.database.url");
			
			logger.info("Paypro db URL "+ URL );
			
			String user=resourceBundle.getString("procedure.database.user");
			
			logger.info("Paypro db user "+ user);
			
			String password=resourceBundle.getString("procedure.database.password");
			
			logger.info("Paypro db password "+ password);
			
			Class.forName(oracleDriver);

			 connection = DriverManager.getConnection(URL, user, password);
			
			logger.info("paypro db connection successfull >>>" +connection);
			
			return connection;
			
		} catch (ClassNotFoundException | SQLException e) {
			
			logger.error("paypro db connection "+e);
			
			logger.error("paypro db connection error message "+e.getMessage());

			
			e.printStackTrace();
			
			return null;

		}

	}
	

	



}


