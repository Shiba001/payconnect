package com.axisbankcmspaypro.comon;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.Thumbnails.Builder;

import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.event.IVideoPictureEvent;
import com.xuggle.xuggler.Global;

public class ImageSnapListener extends MediaListenerAdapter {
	
	public static final double SECONDS_BETWEEN_FRAMES = 1;

	private int mVideoStreamIndex = -1;

	private long mLastPtsWrite = Global.NO_PTS;

	public static final long MICRO_SECONDS_BETWEEN_FRAMES = (long) (Global.DEFAULT_PTS_PER_SECOND * SECONDS_BETWEEN_FRAMES);
	
	private boolean imageGrabbed = false;
	
	private String location;
	
	/**
	 * @author Avishek Seal
	 * @return
	 */
	public boolean isImageGrabbed(){
		return imageGrabbed;
	}
	
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}



	@Override
	public void onVideoPicture(IVideoPictureEvent event) {

		if (event.getStreamIndex() != mVideoStreamIndex) {
			if (mVideoStreamIndex == -1) {
				mVideoStreamIndex = event.getStreamIndex();
			} else {
				return;
			}
		}

		if (mLastPtsWrite == Global.NO_PTS) {
			mLastPtsWrite = event.getTimeStamp() - MICRO_SECONDS_BETWEEN_FRAMES;
		} 

		if (event.getTimeStamp() - mLastPtsWrite >= MICRO_SECONDS_BETWEEN_FRAMES * 4) {
			try {
				scaleImage(event.getImage(), location);
				this.imageGrabbed = true;
				mLastPtsWrite += MICRO_SECONDS_BETWEEN_FRAMES;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private File scaleImage(BufferedImage image, String fileLocation) throws IOException {
		Builder<BufferedImage> thumbnail = Thumbnails.of(image).height(150).width(200);
		
		final File dir = new File(DirectoryMaker.convertToDirectory(fileLocation));
		
		dir.mkdirs();
		
		final File thumbnailFile =  new File(dir.getAbsolutePath()+"/"+System.currentTimeMillis() + ".jpg");
		
		ImageIO.write(thumbnail.asBufferedImage(), "jpg", thumbnailFile);
		
		return thumbnailFile;
	}
}
