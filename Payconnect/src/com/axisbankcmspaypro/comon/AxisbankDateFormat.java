package com.axisbankcmspaypro.comon;

import java.text.SimpleDateFormat;

public interface AxisbankDateFormat {
	public static final String DATE_PATTERN = "dd-MMM-yyyy";
	public static final String DATE_TIME_PATTERN = "dd-MMM-yyyy hh:mm a";
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN);
	public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat(DATE_TIME_PATTERN);
}
