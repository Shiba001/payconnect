package com.axisbankcmspaypro.comon;

public enum DeliveryStatus {
	Sent, Failed, Waiting;
}
