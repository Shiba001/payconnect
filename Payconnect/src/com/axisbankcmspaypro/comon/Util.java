/**
 * @author Supratim Sarkar
 * This class is use for common utill purpose
 * 
 * */

package com.axisbankcmspaypro.comon;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class Util {

	public static boolean isEmpty(String value) {
		return value == null || value.isEmpty();
	}

	public static boolean isEmpty(Date date) {
		return date == null;
	}

	public static boolean isEmpty(Object object) {
		return object == null;
	}

	public static boolean stringIsEmpty(String value) {

		return value.equalsIgnoreCase("") || value == null;
	}

	public static boolean isNumeric(String value) {
		Pattern pattern = Pattern.compile("\\d+");

		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}

	public static String formatDateFromEntity(Date value) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		String formatDate = simpleDateFormat.format(value);

		return formatDate;

	}

	public static Date formatDateFromModel(String value) throws ParseException {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Date formatDate = simpleDateFormat.parse(value);

		return formatDate;

	}

	/**
	 * @author Supratim Sarkar
	 * @param h
	 * @return
	 */
	public static String stringcut(String h) {

		h = h.replace(" ", "").trim().toUpperCase();

		return h;
	}

	public String getAppLoginUrl(HttpServletRequest request) {

		String projectNameFromXml = request.getServletContext()
				.getInitParameter("projectName");

		return request.getScheme() + "://" + request.getServerName() + ":"
				+ request.getServerPort() + projectNameFromXml + "/login";

	}

	public String getAppForgotPasswordUrl(HttpServletRequest request) {

		String projectNameFromXml = request.getServletContext()
				.getInitParameter("projectName");

		return request.getScheme() + "://" + request.getServerName() + ":"
				+ request.getServerPort() + projectNameFromXml
				+ "/update-forgot-user-password?MTE3ODAxNw=";

	}

	/**
	 * this method is used to determine if a string is an integer
	 * @author Avishek Seal
	 * @param str
	 * @return
	 */
	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}
	
	public static final int endOfMonth(int year, int month){
		if(month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11){
			return 31;
		} else if(month == 3 || month == 5 || month == 8 || month == 10){
			return 30;
		} else if(month == 1){
			if(year % 4 == 0) {
				return 29;
			} else {
				return 28;
			}
		}
		
		return 0;
	}

}
