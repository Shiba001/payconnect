package com.axisbankcmspaypro.comon;


public class DirectoryMaker {
	
	public static final String convertToDirectory(String fileName){
		final String[] slashSeparation = fileName.split("\\\\");
		
		final String[] dotSeparation = slashSeparation[slashSeparation.length - 1].split("\\.");
		
		return dotSeparation.length > 1 ? fileName.replace("."+dotSeparation[1], "") : fileName;
	}
}
