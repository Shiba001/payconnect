package com.axisbankcmspaypro.comon;

import java.awt.image.BufferedImage;

import org.springframework.stereotype.Component;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.ToolFactory;

@Component
public class ThumbnailMaker {
	
	public void getThumbnailImage(String filePath){
		
		
		
		final IMediaReader mediaReader = ToolFactory.makeReader(filePath);

		try {
			final ImageSnapListener isListener = new ImageSnapListener();
			
			isListener.setLocation(filePath);
			
			mediaReader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
			mediaReader.addListener(isListener);
			
			while (!isListener.isImageGrabbed()) {
				mediaReader.readPacket();
			}
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}
	
}
