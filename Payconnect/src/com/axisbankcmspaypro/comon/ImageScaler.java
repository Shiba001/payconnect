package com.axisbankcmspaypro.comon;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.Thumbnails.Builder;

import org.springframework.stereotype.Component;

@Component
public class ImageScaler {

	
	public void scaleImageAndToBase64String(File image) throws IOException {
		Builder<File> thumbnail = Thumbnails.of(image).height(150).width(200);
		
		File dir = new File(DirectoryMaker.convertToDirectory(image.getAbsolutePath()));
		
		dir.mkdir();

		final File thumbnailFile =  new File(dir.getAbsoluteFile()+"\\"+System.currentTimeMillis() + ".jpg");
		
		ImageIO.write(thumbnail.asBufferedImage(), "jpg", thumbnailFile);
	}
}
