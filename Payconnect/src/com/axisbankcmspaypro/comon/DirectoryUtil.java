package com.axisbankcmspaypro.comon;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
public class DirectoryUtil {

	@Autowired
	private WebApplicationContext applicationContext;
	
	//private static final String TICKET_LOCATION = "/vault/tickets";
	
	private static final String TICKET_LOCATION = "/vault/raised-tickets";
	
	private static final String COACH_MARK_LOCATION = "/vault/coach-mark";
	
	private static final String REPORT_LOCATION = "/vault/reports";
	
	private static final String LOG_LOCATION = "\\AxisBankPayConnect\\logs";
	
	/**
	 * this method is used to get the ticket files storing directory location
	 * @return
	 */
	public String getTicketDirectoryLocation(){
		return rootPath()+TICKET_LOCATION;
	}
	
	/**
	 * this method is used to get the coach mark image storing directory location
	 * @return
	 */
	public String getCoachMarkDirectoryLocation(){
		return rootPath()+COACH_MARK_LOCATION;
	}
	
	public String getReportDumpingPath(){
	    return rootPath()+REPORT_LOCATION;
	}
	
	public String getLogFilePath(){
	    return webAppsPath() + LOG_LOCATION;
	}
	
	private final String rootPath(){
	    final File file = new File(applicationContext.getServletContext().getRealPath("/"));
		
	    final String root = file.getParentFile().getParentFile().getAbsolutePath();// for tomcat path
	    
	    System.out.println("root path "+root);

	    return root;
	}
	
	private final String webAppsPath(){
	    final File file = new File(applicationContext.getServletContext().getRealPath("/"));
	    
	    final String root = file.getParentFile().getAbsolutePath();
	    
	    return root;
	}
}
