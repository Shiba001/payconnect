package com.axisbankcmspaypro.comon;

import java.io.File;
//import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;







import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
//import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import com.axisbankcmspaypro.model.TicketFile;

@Component
public class FileHandler implements Serializable{
	
	private static final long serialVersionUID = 8962387623269646093L;
	
	private static final Tika DETECTOR = new Tika();
	
	@Autowired
	private ThumbnailMaker thumbnailMaker;
	
	@Autowired
	private ImageScaler imageScaler;
	
	private final Logger logger = Logger.getLogger(FileHandler.class);

	public void saveFile(byte[] content, String location, String fileName, String fileType) throws IOException{	
		
		File directory = new File(location);
		
		if(!directory.exists()){
			directory.mkdirs();
		}
		
		//File dumpFile = new File(location + "/" + fileName);
		
		File dumpFile = new File(location + File.separator + fileName);
		
		logger.info("File string path "+dumpFile.getAbsolutePath());
		
		System.out.println("arup string path "+dumpFile.getAbsolutePath());
		
		
		
		try(OutputStream outputStream = new FileOutputStream(dumpFile)) {
			outputStream.write(content);
		} catch (IOException e) {
			throw e;
		}
		
		saveThumbnail(fileType, dumpFile.getAbsolutePath());
	}
	
	
	
	
	
  /* public void saveThumbnailFile(byte[] content, String location, String fileName, String fileType) throws IOException{	
		
		File directory = new File(location);
		
		if(!directory.exists()){
			directory.mkdirs();
		}
		
		//File dumpFile = new File(location + "/" + fileName);
		
		File dumpFile = new File(location + File.separator + fileName);
		
		System.out.println("arup string path "+dumpFile.getAbsolutePath());
		
		//C:\apache-tomcat-8.0.36/vault/newtickets\264925
		
		
		try(OutputStream outputStream = new FileOutputStream(dumpFile)) {
			outputStream.write(content);
		} catch (IOException e) {
			throw e;
		}
		
		saveThumbnail(fileType, dumpFile.getAbsolutePath());
	}
	*/
	
	
	
	private final void saveThumbnail(String fileType, String filePath){
		
		try{
			
			if(StringUtils.containsIgnoreCase(fileType, "video"))  {
				thumbnailMaker.getThumbnailImage(filePath);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}
	
	
/*private final void saveThumbnail(String fileType, String filePath){
		
		if(StringUtils.containsIgnoreCase(fileType, "video"))  {
			thumbnailMaker.getThumbnailImage(filePath);
		}
		
		else if(StringUtils.containsIgnoreCase(fileType, "image")) {
			try {
				imageScaler.scaleImageAndToBase64String(new File(filePath));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}*/
	
	
	
	public void saveSingleFile(byte[] content, String location, String fileName) throws IOException{		
		File directory = new File(location);
		
		if(!directory.exists()){
			directory.mkdirs();
		}
		
		File[] files = directory.listFiles();
		
		for(File file : files){
			file.delete();
		}
		
		//File dumpFile = new File(location + "/" + fileName);
		File dumpFile = new File(location + File.separator + fileName);
		try(OutputStream outputStream = new FileOutputStream(dumpFile)) {
			outputStream.write(content);
		} catch (IOException e) {
			throw e;
		}
	}
	
	public String retrieveCoachMarkImageFile(String location, String coachMarkID,String serverAddress){
		final File directory = new File(location);
		File[] files = directory.listFiles();
		
		if(ArrayUtils.isNotEmpty(files)) {
			StringBuilder builder = new StringBuilder();
			
			builder.append(serverAddress);
			builder.append("/download-coachmark?path=");
			builder.append("/"+coachMarkID);
			builder.append("/"+files[0].getName());
			
			logger.info("retrieveCoachMarkImageFile download link from Axis"+ builder.toString());
			
			return builder.toString();
		}
		
		return null;
	}
	
	public List<TicketFile> retrieveFiles(String location, String id,String serverAddress){
		final List<TicketFile> ticketFiles = new ArrayList<>();
		final File directory = new File(location);
		final File[] files = directory.listFiles();
		
		StringBuilder builder = new StringBuilder();
		builder.append(serverAddress);
		builder.append("/download-ticket?path=");
		builder.append(id);
		
		
		StringBuilder thumbnail = new StringBuilder();
		
		thumbnail.append(serverAddress);
		thumbnail.append("/download-ticket-thumbnail?path=");
		thumbnail.append(id);
		
		if(files != null){
			
			for(File file : files){
				
				System.out.println("local file directory is: >>>>> "+file.isDirectory());
				
				logger.info("file directory is: >>>>> "+file.isDirectory());
				
				if(!file.isDirectory() && file.isFile()) {
					
					logger.info("inside is directory");
					
					System.out.println("inside directory");
					
					
					TicketFile file2 = converter(file);
					
					System.out.println("link is"+file.getName());

					//file2.setFileLink(builder.toString()+"/"+file.getName());
					
					//file2.setFileThumbnail(thumbnail.toString()+"/"+file.getName());
					
					logger.info(" file download link in axis "+builder.toString()+File.separator+file.getName());
					
					logger.info(" thumbnail download link in axis "+thumbnail.toString()+File.separator+file.getName());
					
					System.out.println(" file download link in axis "+builder.toString()+File.separator+file.getName());
					
					System.out.println("thumbnail download link in axis "+thumbnail.toString()+File.separator+file.getName());
					
					
					
                    file2.setFileLink(builder.toString()+File.separator+file.getName());
					
				   file2.setFileThumbnail(thumbnail.toString()+File.separator+file.getName());
					
					
					ticketFiles.add(file2);
				}
			}
		}
		
		return ticketFiles;
	}
	
	private final TicketFile converter(File file){
		final TicketFile ticketFile = new TicketFile();

		ticketFile.setFileName(file.getName());
		try {
			ticketFile.setFileType(DETECTOR.detect(file));
		} catch (IOException e) {
			ticketFile.setFileType("application/octet-stream");
		}
		
		return ticketFile;
	}
	
	@PostConstruct
	public void init(){
		System.out.println(getClass() + " is initiated");
	}
	
	@PreDestroy
	public void delete(){
		System.out.println(getClass() + " is destroyed");
	}
}
