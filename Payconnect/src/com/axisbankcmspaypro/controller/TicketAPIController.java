/**
 * 
 */
package com.axisbankcmspaypro.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.business.TicketBusiness;
import com.axisbankcmspaypro.comon.DirectoryMaker;
import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.axisbankcmspaypro.model.Ticket;
import com.axisbankcmspaypro.model.TicketCategoryModel;
import com.axisbankcmspaypro.model.TicketFile;
import com.axisbankcmspaypro.model.TicketResponseModel;
import com.common.Common;
import com.google.gson.Gson;

/**
 * @author Avishek Seal
 *
 */
@Controller
public class TicketAPIController {
    
    private final Logger logger = Logger.getLogger(TicketAPIController.class);
    
    @Autowired
    private TicketBusiness ticketBusiness;
    
    @Autowired
    private PayProSessionTokenBusiness payProSessionTokenBusiness;

    @Autowired
    private PowerAccessTokenBusiness powerAccessTokenBusiness;
    
	@Value("${server.url}")
	private String serverURL;
    
    private static final String TICKET_TITLE = "tickettitle";
    private static final String TICKET_REASON = "ticketdescription";
    private static final String TICKET_CATEGORY = "ticketcategory";
    private static final String TICKET_DATE = "ticketdate";
    
    @Autowired
	private DirectoryUtil directoryUtil;

	private static final int BUFFER_SIZE = 4096;
    
    /**
     * this URL is used to create ticket for a specific application user and specific application
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param httpServletRequest
     * @param httpServletResponse
     * @param files
     * @throws Exception 
     */
   /* @RequestMapping(value = "/api/create-ticket", method = RequestMethod.POST)
    public void createTicket(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @RequestParam("file") MultipartFile[] files) throws Exception{
	try {
	    if(logger.isInfoEnabled()) {
		logger.info("createTicket() - start");
	    }
	   final Common common = new Common();
	  
	   Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
	   
	   String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
	   String userID = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
	   String sessionToken = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("session_token"));
	   
	   String presentSessionToken = null;
	   
	   if(StringUtils.equals(system, "1")) {
		   presentSessionToken = powerAccessTokenBusiness.getPowerAccessSessionToken(userID).getSessionToken();
	   } else if(StringUtils.equals(system, "2")) {
		   presentSessionToken = payProSessionTokenBusiness.getPayproSessionTokenByUser(userID).getSessionToken();
	   }
	   
	   if(StringUtils.equals(sessionToken, presentSessionToken)){
	       List<TicketFile> ticketFiles = new ArrayList<>();
	       
	       
	       
	       for(MultipartFile file : files) {
	    	   
	    	 
	    	   
	    	   TicketFile ticketFile = new TicketFile();
	    	   
	    	  if(StringUtils.isNotBlank(file.getContentType())){
	    		  ticketFile.setFileType(file.getContentType());  
	    	  }
	    	  
	    	  if(StringUtils.isNotBlank(file.getContentType())){
	    		  ticketFile.setFileName(file.getOriginalFilename());
	    	  }
	    	  
	    	    byte[] fileArr= file.getBytes();
	    	    
	    	    if(fileArr.length>0){
	    	    	
	    	    	 ticketFile.setFileContent(file.getBytes());
	    	    }
	    	  
	    	   
	    	  
	    	  if(StringUtils.isNotBlank(ticketFile.getFileType()) && StringUtils.isNotBlank(ticketFile.getFileName()) && ticketFile.getFileContent().length>0){
	    		  
	    		  ticketFiles.add(ticketFile);
	    	  }
		   
	    	   
	       }
	       
	       final Ticket ticket = new Ticket();
	       
	       if (ticketFiles == null || ticketFiles.isEmpty() || ticketFiles.size() == 0) {
	    	   
	    	   System.out.println("inside if ticket else part ");
	    	   
	    	   ticket.setFiles(ticketFiles);
	       }
	       
	       else{
	    	   
	    	   System.out.println("inside if ticket else part ");
	    	  
	    	   httpServletResponse.getWriter().write(jsonResponse("400", "Please upload the file again due to poor network connectivity file content not added successfully", true));
	       }
	       
	      
	       ticket.setReason(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_REASON)));
	       ticket.setTicketCategory(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_CATEGORY)));
	       ticket.setTicketDate(new Date(Long.parseLong(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_DATE)))));
	       ticket.setApplicationUserID(userID);
	       ticket.setTicketTitle(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_TITLE)));
	       
	       ticketBusiness.createTicket(ticket, system);
	       
	       httpServletResponse.getWriter().write(jsonResponse("200", "Success", false));
	   } else {
	       if(logger.isInfoEnabled()) {
		    logger.info("498 : Invalid SessionToken.");
		}
	       
	       httpServletResponse.getWriter().write(jsonResponse("498", "Invalid Session Token", true));
	   }
	   
	    httpServletResponse.setStatus(HttpServletResponse.SC_OK);
	} catch (Exception exception) {
	    if(logger.isInfoEnabled()) {
		logger.info("createTicket() - Exception "+exception.getMessage());
	    }
	    
	    exception.printStackTrace();
	    
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(jsonResponse("498", exception.getMessage(), true));
	} finally {
	    if(logger.isInfoEnabled()) {
		logger.info("createTicket() - end");
	    }
	}
    }
    
    */
    
    
    
    
    /**
     * this URL is used to create ticket for a specific application user and specific application
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param httpServletRequest
     * @param httpServletResponse
     * @param files
     * @throws Exception 
     */
    @RequestMapping(value = "/api/create-ticket", method = RequestMethod.POST)
    public void createTicket(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @RequestParam("file") MultipartFile[] files) throws Exception{
	try {
	    if(logger.isInfoEnabled()) {
		logger.info("createTicket() - start");
	    }
	   final Common common = new Common();
	  
	   Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
	   
	   String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
	   String userID = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
	   String sessionToken = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("session_token"));
	   
	   String presentSessionToken = null;
	   
	   if(StringUtils.equals(system, "1")) {
		   presentSessionToken = powerAccessTokenBusiness.getPowerAccessSessionToken(userID).getSessionToken();
	   } else if(StringUtils.equals(system, "2")) {
		   presentSessionToken = payProSessionTokenBusiness.getPayproSessionTokenByUser(userID).getSessionToken();
	   }
	   
	   if(StringUtils.equals(sessionToken, presentSessionToken)){
	       List<TicketFile> ticketFiles = new ArrayList<>();
	       
	       for(MultipartFile file : files) {
	    	   TicketFile ticketFile = new TicketFile();
	    	   
	    	   ticketFile.setFileType(file.getContentType());
	    	   ticketFile.setFileContent(file.getBytes());
	    	   ticketFile.setFileName(file.getOriginalFilename());
		   
	    	   ticketFiles.add(ticketFile);
	       }
	       
	       final Ticket ticket = new Ticket();
	       
	       ticket.setFiles(ticketFiles);
	       ticket.setReason(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_REASON)));
	       ticket.setTicketCategory(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_CATEGORY)));
	       ticket.setTicketDate(new Date(Long.parseLong(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_DATE)))));
	       ticket.setApplicationUserID(userID);
	       ticket.setTicketTitle(Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter(TICKET_TITLE)));
	       
	       ticketBusiness.createTicket(ticket, system);
	       
	       httpServletResponse.getWriter().write(jsonResponse("200", "Success", false));
	   } else {
	       if(logger.isInfoEnabled()) {
		    logger.info("498 : Invalid SessionToken.");
		}
	       
	       httpServletResponse.getWriter().write(jsonResponse("498", "Invalid Session Token", true));
	   }
	   
	    httpServletResponse.setStatus(HttpServletResponse.SC_OK);
	} catch (Exception exception) {
	    if(logger.isInfoEnabled()) {
		logger.info("createTicket() - Exception "+exception.getMessage());
	    }
	    
	    exception.printStackTrace();
	    
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(jsonResponse("498", exception.getMessage(), true));
	} finally {
	    if(logger.isInfoEnabled()) {
		logger.info("createTicket() - end");
	    }
	}
    }
	
    
    
	
	
   /**
     * this URL is used to get the list of ticket category
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param httpServletResponse
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/api/ticket-category-list")
    public void getListOfCategory(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception{
	try {
	    if(logger.isInfoEnabled()) {
		logger.info("getListOfCategory() - start");
	    }
	    
	    final Common common = new Common();
		  
	    Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
		   
	    String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
	    String userID = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
	    String sessionToken = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("session_token"));
	    
	    String presentSessionToken = null;
	    
	    if(StringUtils.equals(system, "1")) {
	    	presentSessionToken = powerAccessTokenBusiness.getPowerAccessSessionToken(userID).getSessionToken();
	    } else if(StringUtils.equals(system, "2")) {
	    	presentSessionToken = payProSessionTokenBusiness.getPayproSessionTokenByUser(userID).getSessionToken();
	    }
	    
	    if(StringUtils.equals(sessionToken, presentSessionToken)) {
	    	final TicketCategoryModel model = new TicketCategoryModel();
	    	
	    	model.setResponseCode("200");
	    	model.setResponseDetails("Success");
	    	model.setTicketCategories(ticketBusiness.getAvailableListOfTicketCategory());
	    	
	    	
	    	
	    	httpServletResponse.getWriter().write(model.toJsonString());
	    } else {
		if(logger.isInfoEnabled()) {
		    logger.info("498 : Invalid SessionToken.");
		}
	       
	       httpServletResponse.getWriter().write(jsonResponse("498", "Invalid Session Token", true));
	    }
	} catch (Exception exception) {
	    if(logger.isInfoEnabled()) {
		logger.info("getListOfCategory() - Exception "+exception.getMessage());
	    }
	    
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(jsonResponse("498", exception.getMessage(), true));
	} finally {
	    if(logger.isInfoEnabled()) {
		logger.info("getListOfCategory() - end");
	    }
	}
    }
    
    /**
     * this URL is used to get list of tickets which has been raised by a specific application user
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param httpServletRequest
     * @param httpServletResponse
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/api/application-user-raised-tickets", method = RequestMethod.POST)
    public void getTicketListOfApplicationUser(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException{
	try {
	    if(logger.isInfoEnabled()) {
		logger.info("getListOfCategory() - start");
	    }
	    
	    final Common common = new Common();
		  
	    Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
		   
	    String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
	    String userID = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
	    String sessionToken = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("session_token"));
	    
	    String presentSessionToken = null;
	    
	    if(StringUtils.equals(system, "1")) {
		presentSessionToken = powerAccessTokenBusiness.getPowerAccessSessionToken(userID).getSessionToken();
	    } else if(StringUtils.equals(system, "2")) {
		presentSessionToken = payProSessionTokenBusiness.getPayproSessionTokenByUser(userID).getSessionToken();
	    }
	    
	    if(StringUtils.equals(sessionToken, presentSessionToken)) {
	    	final TicketResponseModel model = new TicketResponseModel();
	    	
	       List<Ticket>tickets=ticketBusiness.getUserTickets(userID, system, getApplicationURL(httpServletRequest));
	       
	       
	
	       model.setResponseCode("200");
	       model.setResponseDetails("Success");
	       model.setTickets(tickets);
	       
	       
	       System.out.println("raised ticket json is >>>>>>>>> "+model.toJsonString());

	       httpServletResponse.getWriter().write(model.toJsonString());
	       
	       
	       
	    } else {
		if(logger.isInfoEnabled()) {
		    logger.info("498 : Invalid SessionToken.");
		}
		
		httpServletResponse.getWriter().write(jsonResponse("498", "Invalid Session Token", true));
	    }
	} catch (Exception exception) {
	    if(logger.isInfoEnabled()) {
		logger.info("getListOfCategory() - Exception "+exception.getMessage());
	    }
	    
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(exception.getMessage());
	    exception.printStackTrace();
	} finally {
	    if(logger.isInfoEnabled()) {
		logger.info("getListOfCategory() - end");
	    }
	}
    }
    
    /**
     * this URL is used to download ticket files
     * @author Avishek Seal
     * @param path
     * @param httpServletRequest
     * @param httpServletResponse
     */
    @RequestMapping(value = "/download-ticket")
    public void downloadTicketFile(@RequestParam(value = "path") String path, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
    	final File filePath = new File(actualPath(path));
    	
    	System.out.println("file path in tomcat "+filePath);
	    
	    try(FileInputStream inputStream = new FileInputStream(filePath); ServletOutputStream outStream = httpServletResponse.getOutputStream();){

		String mimeType = httpServletRequest.getServletContext().getMimeType(filePath.getAbsolutePath());
		
		System.out.println("file mime type in downloadTicketFile "+ mimeType);
		
		logger.info("file mime type in downloadTicketFile "+ mimeType);
		
		if (mimeType == null) {
		    mimeType = "application/octet-stream";
		}

		httpServletResponse.setContentType(mimeType);
		
		httpServletResponse.setContentLength((int) filePath.length());

		//String headerKey = "Content-Disposition";
		
		//String headerValue = String.format("attachment; filename=\"%s\"", filePath.getName());
		
		//httpServletResponse.setHeader(headerKey, headerValue);
		
		byte[] buffer = new byte[BUFFER_SIZE];
	
		int bytesRead = -1;

		while ((bytesRead = inputStream.read(buffer)) != -1) {
		    outStream.write(buffer, 0, bytesRead);
		}

		inputStream.close();
		outStream.close();
	    } catch(Exception exception) {
	    	logger.error(" download-ticket error "+exception.getMessage());
		exception.printStackTrace();
	    }
    }
    
    /**
     * this URL is used to view the thumbnail image of ticket image and video files
     * @param path
     * @param httpServletRequest
     * @param httpServletResponse
     */
    @RequestMapping(value = "/download-ticket-thumbnail")
    public void downloadTicketFileThumbanil(@RequestParam(value = "path") String path, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
    	final File filePath = new File(actualPath(DirectoryMaker.convertToDirectory(path)));
	    
    	if(filePath.isDirectory()) {
    		File[] listOfFile = filePath.listFiles();
    		
    		try(ServletOutputStream outStream = httpServletResponse.getOutputStream()){

    			String mimeType = httpServletRequest.getServletContext().getMimeType(listOfFile[0].getAbsolutePath());
    			
    			if (mimeType == null) {
    			    mimeType = "application/octet-stream";
    			}

    			BufferedImage bi = ImageIO.read(listOfFile[0]);
    			ImageIO.write(bi, "jpg", outStream);
    			
    			outStream.close();
    		} catch(Exception exception) {
    			exception.printStackTrace();
    		}
    	}
    }
    
    private final String actualPath(String relativePath){
	    final StringBuilder actualPath = new StringBuilder();
	   
	    actualPath.append(directoryUtil.getTicketDirectoryLocation());
	    actualPath.append("/"+relativePath);
	    
	    return actualPath.toString();
	}
    
    private String jsonResponse(String responseCode, String responseDetails, boolean error) throws Exception{
	Map<String, String> map = new HashMap<String, String>();
	
	map.put("ResponseCode", responseCode);
	
	if(error) {
		map.put("error", responseDetails);
	} else {
		map.put("ResponseDetails", responseDetails);
	}
	
	final Gson gson = new Gson();
	
	String jsonErrorMessage = gson.toJson(map);
	
	final JSONParser parser = new JSONParser();

	Object obj = parser.parse(jsonErrorMessage);
	JSONObject jsonObject = (JSONObject) obj;
	
	return jsonObject.toString();
    }
    
    private final String getApplicationURL(HttpServletRequest request){
    	
    	//System.out.println("server path is "+request.getScheme()+"://"+request.getServerName()+":"+request.getLocalPort()+request.getServletContext().getContextPath());
    	
    	//logger.info("server address is :"+ request.getScheme()+"://"+request.getServerName()+":"+request.getLocalPort()+request.getServletContext().getContextPath());
    	
    	//System.out.println("server address "+request.getScheme()+"://"+request.getServerName()+":"+request.getLocalPort()+request.getServletContext().getContextPath());
    	
		//return request.getScheme()+"://"+request.getServerName()+":"+request.getLocalPort()+request.getServletContext().getContextPath();
		
		
    	
    	System.out.println("server URL is "+ serverURL);
    	
    	logger.info("Ticket API server URL "+serverURL);
		
		return serverURL;
		
		
	 }
}
