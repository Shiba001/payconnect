package com.axisbankcmspaypro.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisbankcmspaypro.business.ApplicationActivityBusiness;
import com.axisbankcmspaypro.enumeration.BatchStatus;
import com.common.Common;
import com.google.gson.Gson;

/**
 * 
 * @author Avishek Seal
 *
 */
@Controller
public class ActivityController {
	
	@Autowired
	private ApplicationActivityBusiness applicationActivityBusiness;
	
	private final Logger logger = Logger.getLogger(VersionController.class);
	
	/**
	 * This URL is called when an activity transition occurs at mobile end
	 * @author Avishek Seal
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @throws Exception 
	 * @throws IOException 
	 */
	@RequestMapping(value = "/api/activity-transition", method = RequestMethod.POST)
	public void callBeforeTransition(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, Exception{
		try {
			logger.info("inside /api/activity-transition ");
			
			final Common common = new Common();
			  
			Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
			
			String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
			String userID = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
			String menu = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("menu"));
			
			applicationActivityBusiness.executeBeforeActivityTransition(userID, system, menu);
			
			httpServletResponse.getWriter().write(jsonResponse("200", "Success", false));
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("inside /api/activity-transition error "+e);
			httpServletResponse.getWriter().write(jsonResponse("500", e.getMessage(), true));
		}
	}
	
	/**
	 * This URL is called when logout occurs at mobile end
	 * @author Avishek Seal
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @throws Exception 
	 * @throws IOException 
	 */
	@RequestMapping(value = "/api/logout-transition", method = RequestMethod.POST)
	public void callBeforeLogout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, Exception{
		try {
			
			logger.info("/api/logout-transition");
			
			final Common common = new Common();
			  
			Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
			
			String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
			
			String userID = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
			
			String menu = Common.decode(Common.PRIVATE_KEY, (String) httpServletRequest.getParameter("menu"));
			
			applicationActivityBusiness.executeBeforeLogout(userID, system, menu);
			
			httpServletResponse.getWriter().write(jsonResponse("200", "Success", false));
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("/api/logout-transition error "+e);
			httpServletResponse.getWriter().write(jsonResponse("500", e.getMessage(), true));
		}
	}
	
	/**
	 * This URL is used to call when an user accept or reject any batch from the batch authorization
	 * @author Avishek Seal
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @throws IOException
	 * @throws Exception
	 */
	@RequestMapping(value = "/api/batch-authorization-status", method = RequestMethod.POST)
	public void saveBatchhAuthorizationStatus(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, Exception{
		try {
			logger.info("/api/batch-authorization-status");
			
			final Common common = new Common();
			  
			Map<String, String> headerMap = common.getHeadersInfo(httpServletRequest);
			
			String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
			String userID = Common.decode(Common.PRIVATE_KEY, (String) headerMap.get("user_id"));
			String batchstatus = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("batchstatus"));
			String batchNumber = Common.decode(Common.PRIVATE_KEY,  httpServletRequest.getParameter("batchnumber"));
			
			applicationActivityBusiness.saveStatusOfBatchAuthorization(userID, system, batchNumber, batchstatus);
			
			httpServletResponse.getWriter().write(jsonResponse("200", "Success", false));
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("/api/batch-authorization-status error "+e);
			httpServletResponse.getWriter().write(jsonResponse("500", e.getMessage(), true));
		}
	}
	
	/**
	 * This URL is called to get list of batch status
	 * @author Avishek Seal
	 * @return
	 */
	@RequestMapping(value = "/api/batch-status-list", method = RequestMethod.GET)
	public @ResponseBody String[] getBatchActivityStatuses(){
		final String[] statuses = new String[]{BatchStatus.Approved.toString(), BatchStatus.Rejected.toString()};
		
		return statuses;
	}
	
	//this method is used to create json response string
	private String jsonResponse(String responseCode, String responseDetails, boolean error) throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("ResponseCode", responseCode);
		
		if(error) {
			map.put("error", responseDetails);
		} else {
			map.put("ResponseDetails", responseDetails);
		}
		
		final Gson gson = new Gson();
		
		String jsonErrorMessage = gson.toJson(map);
		
		final JSONParser parser = new JSONParser();

		Object obj = parser.parse(jsonErrorMessage);
		JSONObject jsonObject = (JSONObject) obj;
		
		return jsonObject.toString();
	    }
}
