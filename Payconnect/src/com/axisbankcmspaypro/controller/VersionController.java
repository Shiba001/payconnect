package com.axisbankcmspaypro.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisbankcmspaypro.business.VersionBusiness;
import com.axisbankcmspaypro.model.VersionModel;
import com.google.gson.Gson;

/**
 * 
 * @author Avishek Seal
 *
 */
@Controller
public class VersionController {

	@Autowired
	private VersionBusiness business;
	
	 private final Logger logger = Logger.getLogger(VersionController.class);
	
	/**
	 * this URL is used to get application's current version with respect to device type
	 * @author Avishek Seal
	 * @param deviceType
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @return
	 * @throws Exception 
	 * @throws IOException 
	 */
	@RequestMapping(value = "/api/appplication-current-version")
	public void getCurrentVersionDetails(@RequestParam(value = "device") String deviceType, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, Exception{
		
		logger.info("inside appplication-current-version api ");
		
		try {
			VersionModel versionModel = business.getCurrentVersion(deviceType);
			
			if(versionModel != null) {
				
				versionModel.setResponseCode("200");
				versionModel.setResponseDetials("Success");
				
				httpServletResponse.getWriter().write(versionModel.toJsonString());
			} else {
				httpServletResponse.getWriter().write(jsonResponse("201", "No Version Available", false));
			}
		} catch (Exception e) {
			logger.info("inside appplication-current-version api error " +e);
			httpServletResponse.getWriter().write(jsonResponse("500", "Internal Server Error", true));
		}
	}
	
	private String jsonResponse(String responseCode, String responseDetails, boolean error) throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("ResponseCode", responseCode);
		
		if(error) {
			map.put("error", responseDetails);
		} else {
			map.put("ResponseDetails", responseDetails);
		}
		
		final Gson gson = new Gson();
		
		String jsonErrorMessage = gson.toJson(map);
		
		final JSONParser parser = new JSONParser();

		Object obj = parser.parse(jsonErrorMessage);
		JSONObject jsonObject = (JSONObject) obj;
		
		return jsonObject.toString();
	    }
}
