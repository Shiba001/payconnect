package com.axisbankcmspaypro.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axisbankcmspaypro.business.DowntimeBusiness;
import com.axisbankcmspaypro.model.DowntimeModel;
import com.common.Common;
import com.google.gson.Gson;

@Controller
public class DownTimeController {
	
	
	private final Logger logger=Logger.getLogger(DownTimeController.class);
	
	@Autowired
	private DowntimeBusiness downtimeBusiness;
	
	
	/**
	 * this URL is used to get downtime from CMS
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @throws Exception
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value ="/api/check-downtime", method = RequestMethod.GET)
	@ResponseBody
	public JSONObject getDownTime(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception{
		
		logger.info("inside down-time api start");
		
		try {
			
			 DowntimeModel downtimeModel= downtimeBusiness.getUpcomingDowntimes();
			 logger.info("in controller downtime start date "+downtimeModel.getStartDate());
			 logger.info("in controller downtime end date "+downtimeModel.getEndDate());
			 logger.info("in controller downtime reason is "+downtimeModel.getReason());
             //System.out.println("in controller start date "+downtimeModel.getStartDate());
             //System.out.println("in controller end date "+downtimeModel.getEndDate());
             //System.out.println("in controller reason "+downtimeModel.getReason());
             
             final JSONObject jsonObject = new JSONObject();
              if(!downtimeModel.getStartDate().equalsIgnoreCase("")){
            	    jsonObject.put("start_time", downtimeModel.getStartDate());
					jsonObject.put("end_time", downtimeModel.getEndDate());
					jsonObject.put("downtime_message", downtimeModel.getReason());
					jsonObject.put("ResponseCode", "200");
					jsonObject.put("Responsedetails", "Success");
					return Common.updateLoad(jsonObject);
            	 
             }else{
            	  jsonObject.put("start_time", Common.encode(Common.PRIVATE_KEY, ""));
				 jsonObject.put("end_time", Common.encode(Common.PRIVATE_KEY, ""));
				 jsonObject.put("downtime_message", Common.encode(Common.PRIVATE_KEY, ""));
				 jsonObject.put("ResponseCode", "200");
				 jsonObject.put("Responsedetails", "Success");
				 return jsonObject;
             }
             
         
		} catch (Exception e) {
			e.printStackTrace();
 			logger.error("down-time api error "+e);
			httpServletResponse.getWriter().write(jsonResponse("500", "Internal Server Error", true));
			 return null;
		}
		
	}
	

	
	private String jsonResponse(String responseCode, String responseDetails, boolean error) throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		map.put("ResponseCode", responseCode);
		
		if(error) {
			map.put("error", responseDetails);
		} else {
			map.put("ResponseDetails", responseDetails);
		}
		
		final Gson gson = new Gson();
		String jsonErrorMessage = gson.toJson(map);
		final JSONParser parser = new JSONParser();
        Object obj = parser.parse(jsonErrorMessage);
		JSONObject jsonObject = (JSONObject) obj;
		
		return jsonObject.toString();
	    }
	
	

}
