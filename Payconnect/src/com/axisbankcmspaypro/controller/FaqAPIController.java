/**
 * 
 */
package com.axisbankcmspaypro.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.axisbankcmspaypro.business.FAQBusiness;
import com.axisbankcmspaypro.business.PayProSessionTokenBusiness;
import com.axisbankcmspaypro.business.PowerAccessTokenBusiness;
import com.axisbankcmspaypro.model.Faq;

/**
 * @author Avishek Seal
 *
 */
@Controller
public class FaqAPIController {
    
    private final Logger logger = Logger.getLogger(FaqAPIController.class);
    
    @Autowired
    private FAQBusiness faqBusiness;
    
    @Autowired
    private PayProSessionTokenBusiness payProSessionTokenBusiness;

    @Autowired
    private PowerAccessTokenBusiness powerAccessTokenBusiness;
    
    @RequestMapping(value = "/faq-list-api/{system}")
    public @ResponseBody List<Faq> getFAQs(@PathVariable(value = "system") String system, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException{
	try {
	    if(logger.isInfoEnabled()) {
	    	logger.info("getFAQs() - start");
	    }
		
	    return faqBusiness.getActiveFAQs(system);
	} catch (Exception exception) {
	    if(logger.isInfoEnabled()) {
		logger.info("getFAQs() - Exception "+exception.getMessage());
	    }
	    
	    httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    httpServletResponse.getWriter().write(exception.getMessage());
	    
	    return null;
	} finally {
	    if(logger.isInfoEnabled()) {
		logger.info("getFAQs() - end");
	    }
	}
    }
    
    @RequestMapping(value = "/faq-details")
    public ModelAndView redirectToFAQPage(@RequestParam(value = "system") String system){
    	return new ModelAndView("FAQ").addObject("system", system);//js populated  by faq.js 
    }
}
