package com.axisbankcmspaypro.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axisbankcmspaypro.business.CoachMarkBusiness;
import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.axisbankcmspaypro.model.CoachMarkModel;
import com.common.Common;
import com.google.gson.Gson;

@Controller
public class CoachMarkController {
	
	@Autowired
	private CoachMarkBusiness coachMarkBusiness;
	
	@Autowired
	private DirectoryUtil directoryUtil;
	
	@Value("${server.url}")
	private String serverURL;
	
	private static final int BUFFER_SIZE = 4096;
	

	 private final Logger logger = Logger.getLogger(CoachMarkController.class);
	
	/**
	 * this URL is used to get Coach Mark on today's date
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @throws Exception
	 */
	@RequestMapping(value = "/api/coach-mark-today", method = RequestMethod.POST)
	public void getTodaysCoachMark(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception{
		
		logger.info("inside coach-mark-today api start");
		
		try {
			String system = Common.decode(Common.PRIVATE_KEY, httpServletRequest.getParameter("system"));
			
			final CoachMarkModel coachMarkModel = coachMarkBusiness.getTodaysCoachMarkModel(system, getApplicationURL(httpServletRequest));
			
			if(coachMarkModel != null) {
				coachMarkModel.setResponseCode("200");
				coachMarkModel.setResponseDetails("Success");
				
				httpServletResponse.getWriter().write(coachMarkModel.toJsonString());
			} else {
				httpServletResponse.getWriter().write(jsonResponse("201", "No Coachmark", false));
			}
			
		} catch (Exception e) {
			logger.error("coach-mark-today api error "+e);
			httpServletResponse.getWriter().write(jsonResponse("500", "Internal Server Error", true));
		}
	}
	
	@RequestMapping(value = "/download-coachmark", method = RequestMethod.GET)
	public void downloadCoachMarkImages(@RequestParam(value = "path") String image, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
		final File filePath = new File(actualPath(image));
	    
	    try(FileInputStream inputStream = new FileInputStream(filePath); ServletOutputStream outStream = httpServletResponse.getOutputStream();){

	    	String mimeType = httpServletRequest.getServletContext().getMimeType(filePath.getAbsolutePath());
		
	    	if (mimeType == null) {
	    		mimeType = "application/octet-stream";
	    	}

			httpServletResponse.setContentType(mimeType);
			httpServletResponse.setContentLength((int) filePath.length());

			//String headerKey = "Content-Disposition";
		
			//String headerValue = String.format("attachment; filename=\"%s\"", filePath.getName());
		
			//httpServletResponse.setHeader(headerKey, headerValue);
		
			byte[] buffer = new byte[BUFFER_SIZE];
	
			int bytesRead = -1;

			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			inputStream.close();
			outStream.close();
	    } catch(Exception exception) {
	    	exception.printStackTrace();
	    }
	}
	
	private final String getApplicationURL(HttpServletRequest request){
		//return request.getScheme()+"://"+request.getServerName()+":"+request.getLocalPort()+request.getServletContext().getContextPath();
		logger.info("coach mark loaction url" + serverURL);
		
		return serverURL;
	 }
	
	private String jsonResponse(String responseCode, String responseDetails, boolean error) throws Exception{
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("ResponseCode", responseCode);
		
		if(error) {
			map.put("error", responseDetails);
		} else {
			map.put("ResponseDetails", responseDetails);
		}
		
		final Gson gson = new Gson();
		
		String jsonErrorMessage = gson.toJson(map);
		
		final JSONParser parser = new JSONParser();

		Object obj = parser.parse(jsonErrorMessage);
		
		JSONObject jsonObject = (JSONObject) obj;
		
		return jsonObject.toString();
	    }
	
	private final String actualPath(String relativePath){
	    final StringBuilder actualPath = new StringBuilder();
	   
	    actualPath.append(directoryUtil.getCoachMarkDirectoryLocation());
	    actualPath.append("/"+relativePath);
	    
	    return actualPath.toString();
	}
}
