package com.axisbankcmspaypro.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "app_menu_report")
public class PayConnectUserReportField implements Serializable{
	

	private static final long serialVersionUID = 330237412429920830L;
	
	private int id;
	private AppReportField appReportField;
	private PayConnectUser payConnectUser;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="app_report_field_id")
	public AppReportField getAppReportField() {
		return appReportField;
	}

	public void setAppReportField(AppReportField appReportField) {
		this.appReportField = appReportField;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PayConnectUserReportField other = (PayConnectUserReportField) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}

	@ManyToOne
	@JoinColumn(name="pay_connectUser_id")
	public PayConnectUser getPayConnectUser() {
		return payConnectUser;
	}

	public void setPayConnectUser(PayConnectUser payConnectUser) {
		this.payConnectUser = payConnectUser;
	}

}
