package com.axisbankcmspaypro.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.axisbankcmspaypro.enumeration.TicketStatus;

@Entity
@Table(name = "application_ticket")
public class Ticket extends BaseEntity {

	@Transient
	private static final long serialVersionUID = -4234316926260009810L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "ticket_number")
	private String ticketNumber;
	
	@Column(name = "raise_date")
	private Date ticketDate;
	
	@Column(name = "reason")
	private String reason;
	
	@ManyToOne
	@JoinColumn(name = "ticket_category_id")
	private TicketCategory ticketCategory;
	
	@ManyToOne
	@JoinColumn(name = "application_id")
	private Application application;
	
	@ManyToOne
	@JoinColumn(name = "app_user_id")
	private PayConnectUser payConnectUser;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status")
	private TicketStatus ticketStatus;
	
	@Column(name = "TICKET_TITLE")
	private String ticketTitle;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public TicketCategory getTicketCategory() {
		return ticketCategory;
	}

	public void setTicketCategory(TicketCategory ticketCategory) {
		this.ticketCategory = ticketCategory;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public PayConnectUser getPayConnectUser() {
		return payConnectUser;
	}

	public void setPayConnectUser(PayConnectUser payConnectUser) {
		this.payConnectUser = payConnectUser;
	}
	
	/**
	 * @return the ticketTitle
	 */
	public String getTicketTitle() {
		return ticketTitle;
	}

	/**
	 * @param ticketTitle the ticketTitle to set
	 */
	public void setTicketTitle(String ticketTitle) {
		this.ticketTitle = ticketTitle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ticket other = (Ticket) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
