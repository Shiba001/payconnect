package com.axisbankcmspaypro.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="FEATURE_MANAGEMENT" )
public class Authorization implements Serializable{
	private static final long serialVersionUID = -723583058586873479L;
		
	private int featureID;
	private String authrization;
	private String viewBalance;
	
	private Application application;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	public int getFeatureID() {
		return featureID;
	}

	public void setFeatureID(int featureID) {
		this.featureID = featureID;
	}
    
	@Column(name="AUTHORIZATION")
	public String getAuthrization() {
		return authrization;
	}

	public void setAuthrization(String authrization) {
		this.authrization = authrization;
	}
    
	@Column(name="VIEW_BAL")
	public String getViewBalance() {
		return viewBalance;
	}

	public void setViewBalance(String viewBalance) {
		this.viewBalance = viewBalance;
	}

	@JoinColumn(name = "application_id")
	@OneToOne
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
}
