package com.axisbankcmspaypro.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "app_report_field")
public class AppReportField implements Serializable{

	private static final long serialVersionUID = 5078864529088254075L;
	
	private int id;
	private String reportField;
	private AppMenu appMenu;
	private String fieldLabel;
	
	private List<PayConnectUserReportField> payConnectUserReportFields;


	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "report_field")
	public String getReportField() {
		return reportField;
	}

	public void setReportField(String reportField) {
		this.reportField = reportField;
	}

	@ManyToOne
	@JoinColumn(name="app_menu_id")
	public AppMenu getAppMenu() {
		return appMenu;
	}

	public void setAppMenu(AppMenu appMenu) {
		this.appMenu = appMenu;
	}
	
	@OneToMany(mappedBy="appReportField",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<PayConnectUserReportField> getPayConnectUserReportFields() {
		return payConnectUserReportFields;
	}

	public void setPayConnectUserReportFields(
			List<PayConnectUserReportField> payConnectUserReportFields) {
		this.payConnectUserReportFields = payConnectUserReportFields;
	}

	@Column(name = "field_label")
	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppReportField other = (AppReportField) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}

}
