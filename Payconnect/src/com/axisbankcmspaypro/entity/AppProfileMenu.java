
/**
 * @author Supratim Sarkar
 */
package com.axisbankcmspaypro.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "app_profile_menu")
@Entity
public class AppProfileMenu extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6381500649444578421L;
	private int id;
	private AppProfile appProfile;
	private AppMenu appMenu;

	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="app_profile_id")
	public AppProfile getAppProfile() {
		return appProfile;
	}
	public void setAppProfile(AppProfile appProfile) {
		this.appProfile = appProfile;
	}

	@ManyToOne
	@JoinColumn(name="app_menu_id")
	public AppMenu getAppMenu() {
		return appMenu;
	}

	public void setAppMenu(AppMenu appMenu) {
		this.appMenu = appMenu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppProfileMenu other = (AppProfileMenu) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return Integer.toString(id);
	}

}
