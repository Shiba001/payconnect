package com.axisbankcmspaypro.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.axisbankcmspaypro.enumeration.Status;

@Entity
@Table(name = "application")
public class Application extends BaseEntity {
	
	@Transient
	private static final long serialVersionUID = 3464040907267039537L;

	private int id;
	
	private String applicationName;
	
	private String system;
	
	private Status status;
	
	private List<DownTime> downTimes; // one to many
	
	private List<Ticket> tickets; // one to many
	
	private List<CMSError> cmsErrors;
	
	private List<AppMenu> appMenus; //one to many
	
	private List<AppProfile> appProfiles;
	
	private List<PayConnectUser> payConnectUsers;//one to many
	
	private List<CMSPushNotification> cmsPushNotifications;
	
	private List<CMSFaq> cmsFaqs;
	
	private Authorization authorization;
	
	private PhoneMatchSetting phoneMatchSetting;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getId() {
		return id;//comment
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "application_name")
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	@Column(name = "system")
	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status")
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<DownTime> getDownTimes() {
		return downTimes;
	}

	public void setDownTimes(List<DownTime> downTimes) {
		this.downTimes = downTimes;
	}

	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}
	
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<CMSError> getCmsErrors() {
		return cmsErrors;
	}

	public void setCmsErrors(List<CMSError> cmsErrors) {
		this.cmsErrors = cmsErrors;
	}
	
	/**
	 * @author Supratim Sarkar
	 * @return
	 */
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<AppMenu> getAppMenus() {
		return appMenus;
	}

	public void setAppMenus(List<AppMenu> appMenus) {
		this.appMenus = appMenus;
	}
	
	@OneToMany(mappedBy="application",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<AppProfile> getAppProfiles() {
		return appProfiles;
	}

	public void setAppProfiles(List<AppProfile> appProfiles) {
		this.appProfiles = appProfiles;
	}
	
	@OneToMany(mappedBy="application",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<PayConnectUser> getPayConnectUsers() {
		return payConnectUsers;
	}

	public void setPayConnectUsers(List<PayConnectUser> payConnectUsers) {
		this.payConnectUsers = payConnectUsers;
	}
	
	@OneToMany(mappedBy="application",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<CMSPushNotification> getCmsPushNotifications() {
		return cmsPushNotifications;
	}

	public void setCmsPushNotifications(List<CMSPushNotification> cmsPushNotifications) {
		this.cmsPushNotifications = cmsPushNotifications;
	}
	
	@OneToMany(mappedBy="application",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<CMSFaq> getCmsFaqs() {
		return cmsFaqs;
	}
	public void setCmsFaqs(List<CMSFaq> cmsFaqs) {
		this.cmsFaqs = cmsFaqs;
	}
	
	/**
	 * @return the authorization
	 */
	
	@OneToOne(mappedBy = "application", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Authorization getAuthorization() {
		return authorization;
	}

	/**
	 * @param authorization the authorization to set
	 */
	public void setAuthorization(Authorization authorization) {
		this.authorization = authorization;
	}

	/**
	 * @return the phoneMatchSetting
	 */
	@OneToOne(mappedBy = "application", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public PhoneMatchSetting getPhoneMatchSetting() {
		return phoneMatchSetting;
	}

	/**
	 * @param phoneMatchSetting the phoneMatchSetting to set
	 */
	public void setPhoneMatchSetting(PhoneMatchSetting phoneMatchSetting) {
		this.phoneMatchSetting = phoneMatchSetting;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return Integer.toString(id);
	}

	
}
