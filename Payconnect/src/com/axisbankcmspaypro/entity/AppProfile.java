
/**
 * @author Supratim Sarkar
 */
package com.axisbankcmspaypro.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.axisbankcmspaypro.enumeration.Status;


@Table(name="app_profile")
@Entity
public class AppProfile extends BaseEntity{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 5114972099735963790L;
	
	private int id;
	private String profileName;
	private Status status;
	private Application application;
	private List<AppProfileMenu> appProfileMenus;
	private List<PayConnectUser> payConnectUsers;
	
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name = "profile_name")
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	
	@ManyToOne
	@JoinColumn(name="application_id")
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	
	@OneToMany(mappedBy="appProfile",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<AppProfileMenu> getAppProfileMenus() {
		return appProfileMenus;
	}
	public void setAppProfileMenus(List<AppProfileMenu> appProfileMenus) {
		this.appProfileMenus = appProfileMenus;
	}
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status")
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	@OneToMany(mappedBy="appProfile",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<PayConnectUser> getPayConnectUsers() {
		return payConnectUsers;
	}
	public void setPayConnectUsers(List<PayConnectUser> payConnectUsers) {
		this.payConnectUsers = payConnectUsers;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppProfile other = (AppProfile) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return Integer.toString(id);
	}

}
