package com.axisbankcmspaypro.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rm_information")
public class RmInformation{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7780311023969064899L;
	
	private int id;
	private String supportDesk;
	private String rmName;
	private String email;
	private String phoneNo;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="support_desk")
	public String getSupportDesk() {
		return supportDesk;
	}
	public void setSupportDesk(String supportDesk) {
		this.supportDesk = supportDesk;
	}
	@Column(name="rm_name")
	public String getRmName() {
		return rmName;
	}
	public void setRmName(String rmName) {
		this.rmName = rmName;
	}
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name="phone_no")
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RmInformation other = (RmInformation) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return Integer.toString(id);
	}
	
	
	
	
	

}
