package com.axisbankcmspaypro.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="cms_configuration")
public class CMSConfiguration {

	@Id
	@SequenceGenerator(name = "configuration")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "login_attempt_counter")
	private int loginAttemptCounter;
	@Column(name = "capcha_attempt_counter")
	private int capchaAttemptCounter;
	@Column(name = "mpin_attempt_counter")
	private int mpinAttemptCounter;
	@Column(name = "reset_password_attempt_counter")
	private int resetPasswordCounter;// newly added
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLoginAttemptCounter() {
		return loginAttemptCounter;
	}

	public void setLoginAttemptCounter(int loginAttemptCounter) {
		this.loginAttemptCounter = loginAttemptCounter;
	}

	public int getCapchaAttemptCounter() {
		return capchaAttemptCounter;
	}

	public void setCapchaAttemptCounter(int capchaAttemptCounter) {
		this.capchaAttemptCounter = capchaAttemptCounter;
	}

	public int getMpinAttemptCounter() {
		return mpinAttemptCounter;
	}

	public void setMpinAttemptCounter(int mpinAttemptCounter) {
		this.mpinAttemptCounter = mpinAttemptCounter;
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	

	public int getResetPasswordCounter() {
		return resetPasswordCounter;
	}

	public void setResetPasswordCounter(int resetPasswordCounter) {
		this.resetPasswordCounter = resetPasswordCounter;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CMSConfiguration other = (CMSConfiguration) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}

}
