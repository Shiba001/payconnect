package com.axisbankcmspaypro.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Avishek Seal
 *
 */
@Entity
@Table(name = "application_activity")
public class ApplicationActivity implements Serializable{

	private static final long serialVersionUID = -2247964939398232933L;
	
	private Integer id;
	
	private PayConnectUser payConnectUser;
	
	private Application application;
	
	private AppMenu appMenu;

	private Date entry;
	
	private Date exit;
	
	private Date activityDate;
	
	private Long duration;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JoinColumn(name = "user_activity_id")
	@ManyToOne
	public PayConnectUser getPayConnectUser() {
		return payConnectUser;
	}

	public void setPayConnectUser(PayConnectUser payConnectUser) {
		this.payConnectUser = payConnectUser;
	}

	@JoinColumn(name = "user_application_id")
	@ManyToOne
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@JoinColumn(name = "user_menu_id")
	@ManyToOne
	public AppMenu getAppMenu() {
		return appMenu;
	}

	public void setAppMenu(AppMenu appMenu) {
		this.appMenu = appMenu;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "entry_time")
	public Date getEntry() {
		return entry;
	}

	public void setEntry(Date entry) {
		this.entry = entry;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "exit_time")
	public Date getExit() {
		return exit;
	}

	
	public void setExit(Date exit) {
		this.exit = exit;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_date")
	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}
	
	@Column(name = "duration")
	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long calculateDuration(){
	    return (exit.getTime() - entry.getTime());
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationActivity other = (ApplicationActivity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}
}
