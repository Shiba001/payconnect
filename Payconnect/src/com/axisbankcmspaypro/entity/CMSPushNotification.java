package com.axisbankcmspaypro.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.axisbankcmspaypro.comon.DeliveryStatus;
import com.axisbankcmspaypro.enumeration.PushNotificationStatus;



@Table(name="cms_push_notification")
@Entity
public class CMSPushNotification extends BaseEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5391173504215437202L;
	
	private int id;
	private String message;
	private Application application;
	private Date sendingDate;
	private Date expiryeDate;
	private DeliveryStatus deliveryStatus;
	private PushNotificationStatus notificationStatus;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@ManyToOne
	@JoinColumn(name = "app_id")
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sending_date")
	public Date getSendingDate() {
		return sendingDate;
	}
	public void setSendingDate(Date sendingDate) {
		this.sendingDate = sendingDate;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expirye_date")
	public Date getExpiryeDate() {
		return expiryeDate;
	}
	public void setExpiryeDate(Date expiryeDate) {
		this.expiryeDate = expiryeDate;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "delivery_status")	
	public DeliveryStatus getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "notification_status")
	public PushNotificationStatus getNotificationStatus() {
		return notificationStatus;
	}
	public void setNotificationStatus(PushNotificationStatus notificationStatus) {
		this.notificationStatus = notificationStatus;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CMSPushNotification other = (CMSPushNotification) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return Integer.toString(id);
	}
	
	

}
