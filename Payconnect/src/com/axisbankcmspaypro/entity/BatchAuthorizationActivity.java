package com.axisbankcmspaypro.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.axisbankcmspaypro.enumeration.BatchStatus;

@Entity
@Table(name = "authorization_activity")
public class BatchAuthorizationActivity implements Serializable{
	
	private static final long serialVersionUID = -307408148144079882L;
	
	private int id;
	
	private String batch;
	
	private BatchStatus batchStatus;
	
	private PayConnectUser payConnectUser;
	
	private Application application;
	
	private Date actionDate;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "batch")
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "batch_status")
	public BatchStatus getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(BatchStatus batchStatus) {
		this.batchStatus = batchStatus;
	}

	@JoinColumn(name = "batch_user_id")
	@ManyToOne
	public PayConnectUser getPayConnectUser() {
		return payConnectUser;
	}

	public void setPayConnectUser(PayConnectUser payConnectUser) {
		this.payConnectUser = payConnectUser;
	}

	@Column(name = "action_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	@JoinColumn(name = "application_id")
	@ManyToOne
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
}
