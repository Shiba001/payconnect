package com.axisbankcmspaypro.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Avishek Seal
 *
 */
@Table(name = "pay_connect_user")
@Entity
public class PayConnectUser extends BaseEntity {

	private static final long serialVersionUID = -8483496063123500405L;
	
	private Integer id;
	
	private String userID;
	
	private Application application;
	
	private String deviceID;
	
	private String emailID;
	
	private String mobileNumber;
	
	private String lastPasswordChangedDate;
	
	private String passwordChangeInWeb;
	
	private String userStatus;
	
	private AppProfile appProfile;
	
	/* for mpin */
	
	private String mpin;
	
	private String deviceToken;
	
	private String deviceName;
	
	private String latitude;
	
	private String longitude;
	
	private String deviceType;
	
	private String deviceUnlockCode;
	
	/* for mpin */
	
	private String userName;
	
	private String corporateID;
	
	private String passwordExpiryDate;
	
	private String userType;
	
	private String clientIp;
	
	private List<PayConnectUserReportField> payConnectUserReportFields;
	
	private Integer fingerOn;
	
	
   /* colummn added for lastLogonDate*/
	
	private String lastLogonDate;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "user_id")
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	@JoinColumn(name = "app_user_id")
	@ManyToOne
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@Column(name = "device_id")
	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	@Column(name = "email_id")
	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	@Column(name = "mobile_no")
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name = "last_password_change_date")
	public String getLastPasswordChangedDate() {
		return lastPasswordChangedDate;
	}

	public void setLastPasswordChangedDate(String lastPasswordChangedDate) {
		this.lastPasswordChangedDate = lastPasswordChangedDate;
	}

	@Column(name = "password_change_in_web")
	public String getPasswordChangeInWeb() {
		return passwordChangeInWeb;
	}

	public void setPasswordChangeInWeb(String passwordChangeInWeb) {
		this.passwordChangeInWeb = passwordChangeInWeb;
	}

	@Column(name = "user_status")
	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	
	@JoinColumn(name = "user_profile_id")
	@ManyToOne
	public AppProfile getAppProfile() {
		return appProfile;
	}

	public void setAppProfile(AppProfile appProfile) {
		this.appProfile = appProfile;
	}
	
	
	@OneToMany(mappedBy="payConnectUser",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<PayConnectUserReportField> getPayConnectUserReportFields() {
		return payConnectUserReportFields;
	}

	public void setPayConnectUserReportFields(List<PayConnectUserReportField> payConnectUserReportFields) {
		this.payConnectUserReportFields = payConnectUserReportFields;
	}

	@Column(name = "mpin")
	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	@Column(name = "device_token")
	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	

	/**
	 * Avishek Seal
	 * @return the deviceName
	 */
	@Column(name = "DEVICE_NAME")
	public String getDeviceName() {
	    return deviceName;
	}

	/**
	 * Avishek Seal
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
	    this.deviceName = deviceName;
	}

	/**
	 * Avishek Seal
	 * @return the latitude
	 */
	@Column(name = "LATITUDE")
	public String getLatitude() {
	    return latitude;
	}

	/**
	 * Avishek Seal
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
	    this.latitude = latitude;
	}

	/**
	 * Avishek Seal
	 * @return the longitude
	 */
	@Column(name = "LONGITUDE")
	public String getLongitude() {
	    return longitude;
	}

	/**
	 * Avishek Seal
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
	    this.longitude = longitude;
	}

	/**
	 * Avishek Seal
	 * @return the corporateID
	 */
	@Column(name = "CORPORATE_ID")
	public String getCorporateID() {
	    return corporateID;
	}

	/**
	 * Avishek Seal
	 * @param corporateID the corporateID to set
	 */
	public void setCorporateID(String corporateID) {
	    this.corporateID = corporateID;
	}


	/**
	 * Avishek Seal
	 * @return the passwordExpiryDate
	 */
	@Column(name = "PASSWORD_EXPIRY_DATE")
	public String getPasswordExpiryDate() {
	    return passwordExpiryDate;
	}

	/**
	 * Avishek Seal
	 * @param passwordExpiryDate the passwordExpiryDate to set
	 */
	public void setPasswordExpiryDate(String passwordExpiryDate) {
	    this.passwordExpiryDate = passwordExpiryDate;
	}

	
	/**
	 * Avishek Seal
	 * @return the userName
	 */
	@Column(name = "user_name")
	public String getUserName() {
	    return userName;
	}
	

	/**
	 * Avishek Seal
	 * @return the deviceType
	 */
	@Column(name = "device_type")
	public String getDeviceType() {
	    return deviceType;
	}

	/**
	 * Avishek Seal
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
	    this.deviceType = deviceType;
	}
	
	
	@Column(name = "device_unlock_code")
	public String getDeviceUnlockCode() {
		return deviceUnlockCode;
	}

	public void setDeviceUnlockCode(String deviceUnlockCode) {
		this.deviceUnlockCode = deviceUnlockCode;
	}

	/**
	 * Avishek Seal
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
	    this.userName = userName;
	}

	/**
	 * Avishek Seal
	 * @return the userType
	 */
	@Column(name = "user_type")
	public String getUserType() {
	    return userType;
	}

	/**
	 * Avishek Seal
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
	    this.userType = userType;
	}
	
	
    @Column(name="client_ip")
	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	
	@Column(name = "finger_on")
	public Integer getFingerOn() {
		return fingerOn;
	}

	public void setFingerOn(Integer fingerOn) {
		this.fingerOn = fingerOn;
	}
	
	
	
	@Column(name = "last_Logondate")
	public String getLastLogonDate() {
		return lastLogonDate;
	}

	public void setLastLogonDate(String lastLogonDate) {
		this.lastLogonDate = lastLogonDate;
	}
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PayConnectUser other = (PayConnectUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}
}
