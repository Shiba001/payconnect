package com.axisbankcmspaypro.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author Mokhyada Mohanty
 *
 */

@Entity
@Table(name = "application_role")
public class ApplicationRole implements Serializable {
	
	private static final long serialVersionUID = 4841803062728196246L;
	
	private Integer id;
	private AppProfile appProfile;
	private Integer roleUniqueKey;
	private String roleName;
	private Application application;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JoinColumn(name = "appProfile_id")
	@OneToOne
	public AppProfile getAppProfile() {
		return appProfile;
	}
	
	public void setAppProfile(AppProfile appProfile) {
		this.appProfile = appProfile;
	}
	
	@Column(name = "role_uniquekey",unique=true)
	public Integer getRoleUniqueKey() {
		return roleUniqueKey;
	}
	
	public void setRoleUniqueKey(Integer roleUniqueKey) {
		this.roleUniqueKey = roleUniqueKey;
	}
	
	
	@Column(name = "role_name")
	public String getRoleName() {
		return roleName;
	}
	
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	@JoinColumn(name = "application_id")
	@OneToOne
	public Application getApplication() {
		return application;
	}
	
	public void setApplication(Application application) {
		this.application = application;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationRole other = (ApplicationRole) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return Integer.toString(id);
	}

}
