
/**
 * @author Supratim Sarkar
 */
package com.axisbankcmspaypro.entity;


import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.axisbankcmspaypro.enumeration.Status;


@Table(name = "app_menu")
@Entity
public class AppMenu extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4345461533263142503L;
	private int id;
	private String menuName;
	private String uniqueMenuId;
	private Status status;
	private Application application; //many to one
	private List<AppProfileMenu> appProfileMenus;
	private Integer orderID;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "menu_name")
	public String getManuName() {
		return menuName;
	}

	public void setManuName(String manuName) {
		this.menuName = manuName;
	}

	@Column(name = "unique_menu_id")
	public String getUniqueMenuId() {
		return uniqueMenuId;
	}

	public void setUniqueMenuId(String uniqueMenuId) {
		this.uniqueMenuId = uniqueMenuId;
	}

	@ManyToOne
	@JoinColumn(name="application_id")
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="status")
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	@OneToMany(mappedBy="appMenu",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	public List<AppProfileMenu> getAppProfileMenus() {
		return appProfileMenus;
	}
	public void setAppProfileMenus(List<AppProfileMenu> appProfileMenus) {
		this.appProfileMenus = appProfileMenus;
	}
	
	/**
	 * @return the orderID
	 */
	@Column(name = "order_id")
	public Integer getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID the orderID to set
	 */
	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppMenu other = (AppMenu) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}

}
