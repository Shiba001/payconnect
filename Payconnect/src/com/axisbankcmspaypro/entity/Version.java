package com.axisbankcmspaypro.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.axisbankcmspaypro.enumeration.DeviceType;
import com.axisbankcmspaypro.enumeration.Status;
import com.axisbankcmspaypro.enumeration.VersionUpdateStatus;

@Entity
@Table(name = "version")
public class Version extends BaseEntity{
	
	@Transient
	private static final long serialVersionUID = -9220970013633905526L;

	private int id;
	
	private String warVersion;
	
	private String apiVersion;
	
	private byte[] releaseNote;
	
	private VersionUpdateStatus versionUpdateStatus;
	
	private Date releaseDate;
	
	private DeviceType deviceType;
	
	private Status status;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "war_version")
	public String getWarVersion() {
		return warVersion;
	}

	public void setWarVersion(String warVersion) {
		this.warVersion = warVersion;
	}

	@Column(name = "api_version")
	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Column(name = "release_note")
	public byte[] getReleaseNote() {
		return releaseNote;
	}

	public void setReleaseNote(byte[] releaseNote) {
		this.releaseNote = releaseNote;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "device_type")
	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name = "version_update_status")
	@Enumerated(EnumType.ORDINAL)
	public VersionUpdateStatus getVersionUpdateStatus() {
		return versionUpdateStatus;
	}

	public void setVersionUpdateStatus(VersionUpdateStatus versionUpdateStatus) {
		this.versionUpdateStatus = versionUpdateStatus;
	}

	@Column(name = "release_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status")
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Version other = (Version) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return Integer.toString(id);
	}
}
