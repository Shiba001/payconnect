package com.axisbankcmspaypro.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.poweraccess.dao.GenericDAO;
import com.axisbankcmspaypro.entity.AppReportField;
import com.axisbankcmspaypro.entity.PayConnectUserReportField;

@Repository
public class AppReportFieldDAO extends GenericDAO<AppReportField, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	Logger logger = Logger.getLogger(AppReportField.class);

	public AppReportFieldDAO() {

		super(AppReportField.class);

	}

	/**
	 * 
	 * @param userId
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public List<AppReportField> getFieldByReportId(int reportId) {

		System.out.println("AppReportField getFieldByReportId dao method >>>>>>>>>>> ");

		if (logger.isInfoEnabled()) {
			logger.info("AppReportField getFieldByReportId start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "appReportField");

		criteria.createAlias("appReportField.appMenu", "appMenu_alias").add(Restrictions.eq("appMenu_alias.id", reportId));
		
		

		if (logger.isInfoEnabled()) {
			logger.info("AppReportField getFieldByReportId end");

		}
		return criteria.list();
	}
	

}
