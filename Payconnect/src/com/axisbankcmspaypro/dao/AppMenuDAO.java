package com.axisbankcmspaypro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.AppMenu;
import com.poweraccess.dao.GenericDAO;


@Repository
public class AppMenuDAO extends GenericDAO<AppMenu, Integer>{
	private static final long serialVersionUID = -4606335779342384403L;

	public AppMenuDAO() {
	    super(AppMenu.class);
	}

	public AppMenu getAppMenuByUniqueID(String uniqueID){
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
	    
	    criteria.add(Restrictions.eq("uniqueMenuId", uniqueID));
	    
	    @SuppressWarnings("unchecked")
	    List<AppMenu> appMenus = criteria.list();
	    
	    return CollectionUtils.isEmpty(appMenus) ? null : appMenus.get(0);
	}
}