/**
 * 
 */
package com.axisbankcmspaypro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.AppProfile;
import com.axisbankcmspaypro.entity.ApplicationRole;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class ApplicationRoleDAO extends GenericDAO<ApplicationRole, Integer> {
    
    private static final long serialVersionUID = 8945337019142295608L;

    /**
     * @param entityClass
     */
    public ApplicationRoleDAO() {
    	super(ApplicationRole.class);
    }

    /**
     * this method is used to get default profile for a specific application and for a specific user type (unique key)
     * @since 25-Feb-2016
     * @author Avishek Seal
     * @param system
     * @param uniqueKey
     * @return
     */
    public AppProfile getDefaultApplicationProfile(String system, String uniqueKey){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "role");
	criteria.createAlias("role.application", "application");
	criteria.add(Restrictions.eq("roleUniqueKey", Integer.parseInt(uniqueKey)));//unique key is integer
	criteria.add(Restrictions.eq("application.system", system));
	
	@SuppressWarnings("unchecked")
	final List<ApplicationRole> applicationRoles = criteria.list();
	
	System.out.println("list size"+applicationRoles.get(0));
	
	final ApplicationRole applicationRole = applicationRoles.get(0);
	
	if(applicationRole != null) {
	    return applicationRole.getAppProfile();
	}
	
	return null;
    }
}
