/**
 * 
 */
package com.axisbankcmspaypro.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.CMSCoachMark;
import com.axisbankcmspaypro.enumeration.Status;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class CMSCoachMarkDAO extends GenericDAO<CMSCoachMark, Integer> {

    private static final long serialVersionUID = 4656525289318886915L;

    /**
     * @param entityClass
     */
    public CMSCoachMarkDAO() {
	super(CMSCoachMark.class);
	// TODO Auto-generated constructor stub
    }

    /**
     * this method is used to get coach mark for a specific appllication on current date if present
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @return
     */
    @SuppressWarnings("deprecation")
    public CMSCoachMark getTodaysCMSCoachMark(String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "coach_mark");
	
	criteria.createAlias("coach_mark.application", "application");
	
	Date TODAY = new Date();
	TODAY.setHours(0);
	TODAY.setMinutes(0);
	TODAY.setSeconds(0);
	
	Conjunction conjunction = Restrictions.conjunction();
	
	conjunction.add(Restrictions.le("startDate", TODAY));
	conjunction.add(Restrictions.ge("endDate", TODAY));
	conjunction.add(Restrictions.eq("status", Status.ACTIVE));
	
	criteria.add(conjunction);
	
	criteria.add(Restrictions.eq("application.system", system));
	
	@SuppressWarnings("unchecked")
	List<CMSCoachMark> cmsCoachMarks = criteria.list();
	
	return CollectionUtils.isEmpty(cmsCoachMarks) ? null : cmsCoachMarks.get(0);
    }
}
