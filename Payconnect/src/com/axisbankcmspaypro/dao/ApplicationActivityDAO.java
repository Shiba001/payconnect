/**
 * 
 */
package com.axisbankcmspaypro.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.ApplicationActivity;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */

@Repository
public class ApplicationActivityDAO extends GenericDAO<ApplicationActivity, Integer> {

    private static final long serialVersionUID = -3664078060715194265L;
    
    /**
     * @param entityClass
     */
    public ApplicationActivityDAO() {
	super(ApplicationActivity.class);
    }
    
    
    @SuppressWarnings("deprecation")
    public ApplicationActivity getUserTodaysLastActivity(String userID, String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "application_activity");
	criteria.createAlias("application_activity.payConnectUser", "payConnectUser");
	criteria.createAlias("application_activity.application", "application");
	
	Date today = new Date();
	
	today.setHours(0);
	today.setMinutes(0);
	today.setSeconds(0);
	
	criteria.add(Restrictions.eq("payConnectUser.userID", userID));
	criteria.add(Restrictions.eq("application.system", system));
	criteria.add(Restrictions.eq("activityDate", today));
	criteria.addOrder(Order.desc("id"));
	
	@SuppressWarnings("unchecked")
	List<ApplicationActivity> activities = criteria.list();
	
	return CollectionUtils.isEmpty(activities) ? null : activities.get(0);
    }
}
