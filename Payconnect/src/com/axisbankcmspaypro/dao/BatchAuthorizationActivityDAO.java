package com.axisbankcmspaypro.dao;

import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.BatchAuthorizationActivity;
import com.poweraccess.dao.GenericDAO;

@Repository
public class BatchAuthorizationActivityDAO extends GenericDAO<BatchAuthorizationActivity, Integer>{

	private static final long serialVersionUID = -414942832198905514L;

	public BatchAuthorizationActivityDAO() {
		super(BatchAuthorizationActivity.class);
	}

}
