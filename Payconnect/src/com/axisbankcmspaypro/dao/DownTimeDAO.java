/**
 * 
 */
package com.axisbankcmspaypro.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.DownTime;
import com.axisbankcmspaypro.enumeration.Status;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class DownTimeDAO extends GenericDAO<DownTime, Integer> {

    private static final long serialVersionUID = 1547272479400101274L;
    
    private static final Logger logger=Logger.getLogger(DownTimeDAO.class);

    /**
     * @param entityClass
     */
    public DownTimeDAO() {
	super(DownTime.class);
    }

    /**
     * this method is used to get list of upcoming down times for a specific application
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     */
/*    @SuppressWarnings("unchecked")
    public List<DownTime> getListOfUpcomingDownTime(String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "downtime");
	criteria.createAlias("downtime.application", "application");
	Disjunction disjunction = Restrictions.disjunction();
	
	final Date TODAY = new Date();
	
	disjunction.add(Restrictions.ge("startTimestamp", TODAY));
	disjunction.add(Restrictions.ge("endTimestamp", TODAY));
	criteria.add(disjunction);
	criteria.add(Restrictions.eq("application.system", system));
	criteria.add(Restrictions.eq("status", Status.ACTIVE));
	criteria.addOrder(Order.asc("startTimestamp"));
	
	return criteria.list();
    }*/
    
    
    
    
    @SuppressWarnings("unchecked")
	public List<DownTime> getUpcomingDownTimes(){
    	try{
    		
    		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
    		//Disjunction disjunction = Restrictions.disjunction();
    		
    		criteria.add(Restrictions.le("startTimestamp", new Date()));
    		//disjunction.add(Restrictions.ge("endTimestamp", new Date()));
    		//criteria.add(disjunction);
    		criteria.add(Restrictions.eq("status", Status.ACTIVE));
    		criteria.addOrder(Order.asc("startTimestamp"));
    		
    		return criteria.list();
    		
    	}catch(Exception e){
    		logger.error("getUpcomingDownTimes error "+e);
    		e.printStackTrace();
    		return null;
    	}
	
	}
    
    
    /**
     * this method is used to check whether a specific application is within down time or not
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     */
    public boolean isDownTimeNow(String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "downtime");
	criteria.createAlias("downtime.application", "application");
	Disjunction disjunction = Restrictions.disjunction();
	
	final Date TODAY = new Date();
	disjunction.add(Restrictions.ge("startTimestamp", TODAY));
	disjunction.add(Restrictions.ge("endTimestamp", TODAY));
	criteria.add(Restrictions.eq("application.system", system));
	criteria.add(Restrictions.eq("status", Status.ACTIVE));
	criteria.addOrder(Order.asc("startTimestamp"));
	
	@SuppressWarnings("unchecked")
	final List<DownTime> downTimes = criteria.list();
	
	if(!CollectionUtils.isEmpty(downTimes)){
	    final DownTime downTime = downTimes.get(0);
	    return downTime.getStartTimestamp().getTime() <= TODAY.getTime() && downTime.getEndTimestamp().getTime() >= TODAY.getTime();
	} else {
	    return false;
	}
	
	
    }
    
    
    
    /**
     * New Query
     * @return
     */
    
    @SuppressWarnings("unchecked")
	public List<DownTime> isDownTimeLater(){
    	try{
    		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "downtime");
        	Disjunction disjunction = Restrictions.disjunction();
        	final Date TODAY = new Date();
        	disjunction.add(Restrictions.ge("startTimestamp", TODAY));
        	criteria.add(Restrictions.eq("status", Status.ACTIVE));
        	criteria.addOrder(Order.asc("startTimestamp"));
            final List<DownTime> downTimes = criteria.list();
        	return downTimes;
        	
    		}
    	 catch(Exception e){
    		logger.error("isDownTimeLater error "+e);
    		e.printStackTrace();
    		return null;
    	}
    	
        }
    
    
    
    
    /**
     * this method is used to check whether a specific application is within down time or not
     * @since 19-Feb-2016
     * @author Arup paul
     * @param system
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean isDownTimeNow(){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "downtime");
	//criteria.createAlias("downtime.application", "application");
	Disjunction disjunction = Restrictions.disjunction();
	
	final Date TODAY = new Date();
	disjunction.add(Restrictions.ge("startTimestamp", TODAY));
	disjunction.add(Restrictions.ge("endTimestamp", TODAY));
	//criteria.add(Restrictions.eq("application.system", system));
	criteria.add(Restrictions.eq("status", Status.ACTIVE));
	criteria.addOrder(Order.asc("startTimestamp"));
	final List<DownTime> downTimes = criteria.list();
	
	if(!CollectionUtils.isEmpty(downTimes)){
		
	    final DownTime downTime = downTimes.get(0);
	   	return downTime.getStartTimestamp().getTime() <= TODAY.getTime() && downTime.getEndTimestamp().getTime() >= TODAY.getTime();
	} else {
	    return false;
	}
	
    }
}
