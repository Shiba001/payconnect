package com.axisbankcmspaypro.dao;

import java.util.List;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.AppDeviceBlock;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.poweraccess.dao.GenericDAO;

@Repository
public class DeviceDAO extends GenericDAO<AppDeviceBlock, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5870559111806080927L;
	private Logger logger = Logger.getLogger(DeviceDAO.class);

	public DeviceDAO() {
		super(AppDeviceBlock.class);
		// TODO Auto-generated constructor stub
	}
	
	
	

	/**
	 * This method is use to get device id details
	 * @param device id
	 * @return
	 */
	
	public AppDeviceBlock getDeviceId(String deviceId){
		
		
		
		if(logger.isInfoEnabled()){
			logger.info("getDeviceId start");
		}
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		
		criteria.add(Restrictions.eq("deviceId", deviceId));
		
		if(logger.isInfoEnabled()){
			logger.info("getDeviceId End");
		}
		return (AppDeviceBlock)criteria.uniqueResult();
	}
	
	
	
	
public AppDeviceBlock getDeviceByUnlockCode(String deviceId){
		
		
		
		if(logger.isInfoEnabled()){
			logger.info("getDeviceId start");
		}
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		
		criteria.add(Restrictions.eq("deviceId", deviceId));
		
		//criteria.add(Restrictions.eq("unlockCode", unlockCode));
		
		criteria.add(Restrictions.eq("deviceStatus", "D"));
		
		if(logger.isInfoEnabled()){
			logger.info("getDeviceId End");
		}
		return (AppDeviceBlock)criteria.uniqueResult();
	}


public void saveOrUpdate(AppDeviceBlock appDeviceBlock) {
	sessionFactory.getCurrentSession().saveOrUpdate(appDeviceBlock);
}


/**
 * 
 * @param deviceId
 * @return
 */
/*public boolean isUnlockCodeGenerate(String deviceId){
	
	if(logger.isInfoEnabled()){
		logger.info("getDeviceId start");
	}
	
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
	
	criteria.add(Restrictions.eq("deviceId", deviceId));
	criteria.add(Restrictions.eq("deviceStatus", "D"));
	
	
	
	@SuppressWarnings("unchecked")
	List<AppDeviceBlock>appDeviceBlocks= criteria.list();
	
	if(!CollectionUtils.isEmpty(appDeviceBlocks)){
		
		AppDeviceBlock appDeviceBlock=appDeviceBlocks.get(0);
		
		return StringUtils.isNotBlank(appDeviceBlock.getUnlockCode()) && StringUtils.equals(appDeviceBlock.getDeviceStatus(), "D");
		
	}
	
	if(logger.isInfoEnabled()){
		logger.info("getDeviceId start");
	}
	return false;
	
	
}*/






public AppDeviceBlock isUnlockCodeGenerate(String deviceId){
	
	if(logger.isInfoEnabled()){
		logger.info("isUnlockCodeGenerate start");
	}
	
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
	
	criteria.add(Restrictions.eq("deviceId", deviceId));
	criteria.add(Restrictions.eq("deviceStatus", "D"));
	
	if(logger.isInfoEnabled()){
		logger.info("isUnlockCodeGenerate End");
	}
	
		
		return (AppDeviceBlock) criteria.uniqueResult();
		
	
	
	
}
	
	
	
	
}
