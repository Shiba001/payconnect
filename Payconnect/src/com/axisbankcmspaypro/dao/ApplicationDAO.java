/**
 * 
 */
package com.axisbankcmspaypro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.Application;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class ApplicationDAO extends GenericDAO<Application, Integer> {
    
    private static final long serialVersionUID = 3581663278602312136L;

    public ApplicationDAO() {
	super(Application.class);
    }
    
    public Application getApplicationBySystem(String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
	criteria.add(Restrictions.eq("system", system));
	
	
	@SuppressWarnings("rawtypes")
	List<Application> list = criteria.list();
	
	return CollectionUtils.isEmpty(list) ? null : list.get(0);
    }
    
    
    
    
  

}
