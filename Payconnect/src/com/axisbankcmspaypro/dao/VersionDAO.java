package com.axisbankcmspaypro.dao;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;





import com.axisbankcmspaypro.entity.Version;
import com.axisbankcmspaypro.enumeration.DeviceType;
import com.axisbankcmspaypro.enumeration.Status;
import com.poweraccess.dao.GenericDAO;

@Repository
public class VersionDAO extends GenericDAO<Version, Integer>{
	
	private static final long serialVersionUID = -8395214913256593526L;

	public VersionDAO() {
		super(Version.class);
	}
	
	/**
	 * this method is used to get current version of application with respect to device type
	 * @param deviceType
	 * @return
	 */
	public Version getCurrentVersion(DeviceType deviceType){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		
		criteria.add(Restrictions.eq("deviceType", deviceType));
		criteria.add(Restrictions.eq("status", Status.ACTIVE));
		
		@SuppressWarnings("unchecked")
		List<Version> versions = criteria.list();
		
		return CollectionUtils.isNotEmpty(versions) ? versions.get(0) : null;
	}
}
