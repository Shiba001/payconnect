package com.axisbankcmspaypro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.CMSError;
import com.axisbankcmspaypro.enumeration.Status;
import com.poweraccess.dao.GenericDAO;

@Repository
public class CMSErrorDAO extends GenericDAO<CMSError, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1247104517117725260L;

	public CMSErrorDAO() {
		super(CMSError.class);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * 
	 * @param system
	 * @return
	 */
	
	@SuppressWarnings("unchecked")
	public List<CMSError> getCmsErrors(String system){
		
		
         Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		
		criteria.add(Restrictions.eq("status", Status.ACTIVE));
		
		criteria.createAlias("application", "application_alias");
		
		criteria.add(Restrictions.eq("application_alias.system", system));
		
		return criteria.list();
		
		
		
	}

}
