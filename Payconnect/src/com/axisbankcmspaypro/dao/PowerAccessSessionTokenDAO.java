package com.axisbankcmspaypro.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.poweraccess.dao.GenericDAO;
import com.poweraccess.model.SessionToken;

@Repository
public class PowerAccessSessionTokenDAO extends
		GenericDAO<SessionToken, Integer> {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -8048633476719903299L;

	public PowerAccessSessionTokenDAO() {
		super(SessionToken.class);
		// TODO Auto-generated constructor stub
	}

	private Logger logger = Logger.getLogger(PowerAccessSessionTokenDAO.class);

	/**
	 * This method is use to get session token details
	 * 
	 * @param userId
	 * @return
	 */
	public SessionToken getPowerAccessSessionToken(String userId) {

		System.out
				.println("inside power accessSessionToken dao method >>>>>>>>>>> ");

		if (logger.isInfoEnabled()) {
			logger.info("getPayPowerSessionToken start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass());

		criteria.add(Restrictions.eq("userID", userId));

		if (logger.isInfoEnabled()) {
			logger.info("getPayProSessionToken End");
		}
		return (SessionToken) criteria.uniqueResult();
	}

	/**
	 * This method is checking for session token and user id
	 * 
	 * @param userId
	 * @param sessionToken
	 * @return
	 */
	public boolean isPowerAccessSessionToken(String userId, String sessionToken) {

		if (logger.isInfoEnabled()) {
			logger.info("isPowerAccessSessionToken start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass());
		criteria.add(Restrictions.eq("userID", userId));
		criteria.add(Restrictions.eq("sessionToken", sessionToken));

		if (logger.isInfoEnabled()) {
			logger.info("isPowerAccessSessionToken End");
		}
		return criteria.list().size() > 0;
	}
	
	/**
	 * this method is used to fetch session information if exists
	 * @since 04-Mar-2016
	 * @author Avishek Seal
	 * @param userId
	 * @param sessionToken
	 * @return
	 */
	public SessionToken getPowerAccessSessionToken(String userId, String sessionToken) {
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());

	    criteria.add(Restrictions.eq("userID", userId));
	    criteria.add(Restrictions.eq("sessionToken", sessionToken));

	    @SuppressWarnings("unchecked")
	    List<SessionToken> sessionTokens = criteria.list();
	    
	    return CollectionUtils.isEmpty(sessionTokens) ? null : sessionTokens.get(0);
	}

}
