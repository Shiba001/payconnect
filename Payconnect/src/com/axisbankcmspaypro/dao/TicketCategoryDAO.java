package com.axisbankcmspaypro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.TicketCategory;
import com.poweraccess.dao.GenericDAO;
/**
 * 
 * @author Avishek Seal
 *
 */
@Repository
public class TicketCategoryDAO extends GenericDAO<TicketCategory, Integer> {

	private static final long serialVersionUID = 680789822885526251L;

	public TicketCategoryDAO() {
		super(TicketCategory.class);
	}

	/**
	 * this method is used to check whether a category is present or not
	 * @author Avishek Seal
	 * @since 13-Jan-2016
	 * @param categoryName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean isCategoryPresent(String categoryName) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass());
		criteria.add(Restrictions.eq("ticketCategoryName", categoryName));
		List<TicketCategory> ticketCategories = criteria.list();

		return (ticketCategories != null && !ticketCategories.isEmpty());
	}
	
	/**
	 * this method is used to get ticket category by name
	 * @author Avishek Seal
	 * @since 13-Jan-2016
	 * @param categoryName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TicketCategory getTicketCategoryByName(String categoryName){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass());
		criteria.add(Restrictions.eq("ticketCategoryName", categoryName));
		List<TicketCategory> ticketCategories = criteria.list();
		
		if(ticketCategories != null && !ticketCategories.isEmpty()){
			return ticketCategories.get(0);
		} else {
			return null;
		}
		
	}
}
