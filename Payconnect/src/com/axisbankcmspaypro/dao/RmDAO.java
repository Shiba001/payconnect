package com.axisbankcmspaypro.dao;

import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.RmInformation;
import com.poweraccess.dao.GenericDAO;

@Repository
public class RmDAO extends GenericDAO<RmInformation, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8286740369436883367L;


	public RmDAO() {
		super(RmInformation.class);
		
	}
	
	
	
	
	
	
}
