package com.axisbankcmspaypro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.PayConnectUserReportField;
import com.poweraccess.dao.GenericDAO;

@Repository
public class PayConnectReportFieldDAO extends GenericDAO<PayConnectUserReportField, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4543432753522798657L;


	public PayConnectReportFieldDAO() {
		super(PayConnectUserReportField.class);
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PayConnectUserReportField> getListOfPermittedField(Integer appUserID, Integer menuID){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "userReportField");
		
		criteria.createAlias("userReportField.payConnectUser", "payConnectUser");
		criteria.createAlias("userReportField.appReportField", "appReportField");
		criteria.createAlias("appReportField.appMenu", "appMenu");
		
		criteria.add(Restrictions.eq("payConnectUser.id", appUserID));
		criteria.add(Restrictions.eq("appMenu.id", menuID));
		
		return criteria.list();
	}

	/**
	 * this method is used to get list of report field for a particular report (menu ID) an user (user ID) can access
	 * @since 18-Feb-2016
	 * @author Avishek Seal
	 * @param userID
	 * @param menuID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PayConnectUserReportField> getListOfPermittedField(String userID, Integer menuID){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "userReportField");
		
		criteria.createAlias("userReportField.payConnectUser", "payConnectUser");
		criteria.createAlias("userReportField.appReportField", "appReportField");
		criteria.createAlias("appReportField.appMenu", "appMenu");
		
		criteria.add(Restrictions.eq("payConnectUser.userID", userID));
		criteria.add(Restrictions.eq("appMenu.id", menuID));
		
		return criteria.list();
	}
	
	/**
	 * this method is used to get list of report field for a particular report (report unique key) an user (user ID) can access
	 * @since 18-Feb-2016
	 * @author Avishek Seal
	 * @param userID
	 * @param reportUniqueKey
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PayConnectUserReportField> getListOfPermittedField(String userID, String reportUniqueKey, String system){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "userReportField");
		
		criteria.createAlias("userReportField.payConnectUser", "payConnectUser");
		criteria.createAlias("payConnectUser.application", "application");
		criteria.createAlias("userReportField.appReportField", "appReportField");
		criteria.createAlias("appReportField.appMenu", "appMenu");
		
		criteria.add(Restrictions.eq("application.system", system));
		criteria.add(Restrictions.eq("payConnectUser.userID", userID));
		criteria.add(Restrictions.eq("appMenu.uniqueMenuId", reportUniqueKey));
		
		return criteria.list();
	}

}
