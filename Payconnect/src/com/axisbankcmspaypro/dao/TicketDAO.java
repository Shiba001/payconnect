/**
 * 
 */
package com.axisbankcmspaypro.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.Ticket;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class TicketDAO extends GenericDAO<Ticket, Integer> {

    private static final long serialVersionUID = 4145292663443459260L;

    /**
     * @param entityClass
     */
    public TicketDAO() {
	super(Ticket.class);
    }

    public Serializable save(Ticket ticket) {
	return sessionFactory.getCurrentSession().save(ticket);
    }
    
    /**
     * this method is used to get list of tickets by a specific application user
     * @author Avishek Seal
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param userID
     * @param system
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Ticket> getUserTickets(String userID, String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "ticket");
	criteria.createAlias("ticket.application", "application");
	criteria.createAlias("ticket.payConnectUser", "payConnectUser");
	
	criteria.add(Restrictions.eq("application.system", system));
	criteria.add(Restrictions.eq("payConnectUser.userID", userID));
	criteria.addOrder(Order.desc("ticketDate"));
	return criteria.list();
    }
}
