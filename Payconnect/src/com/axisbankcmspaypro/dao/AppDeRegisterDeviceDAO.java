package com.axisbankcmspaypro.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.poweraccess.dao.GenericDAO;


@Repository
public class AppDeRegisterDeviceDAO extends GenericDAO<PayConnectUser, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 235831065101862008L;
	private Logger logger = Logger.getLogger(AppDeRegisterDeviceDAO.class);

	public AppDeRegisterDeviceDAO() {
		super(PayConnectUser.class);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * This method is use to get payy connect user details
	 * @param deviceId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PayConnectUser> getPayConnectUserDeatils(String deviceId){
		
		if(logger.isInfoEnabled()){
			logger.info("getPayConnectUserDeatils start");
		}
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		criteria.add(Restrictions.eq("deviceID", deviceId));
		List<PayConnectUser> connectUsers = criteria.list();
		if(logger.isInfoEnabled()){
			logger.info("getPayConnectUserDeatils End");
		}
		return connectUsers;
	}
	
	/**
	 * This method is use to get update using deviceId
	 * @param deviceId
	 */
	public void updateByDeviceID(String deviceId){
		
		String hql = "update PayConnectUser pu where pu.deviceID=:deviceID";
		
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("deviceID", deviceId);
			query.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	
}
