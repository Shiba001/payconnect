/**
 * 
 */
package com.axisbankcmspaypro.dao;

import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.AppProfile;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class AppProfileDAO extends GenericDAO<AppProfile, Integer> {

    private static final long serialVersionUID = 5003537405671654336L;
    
    /**
     * @param entityClass
     */
    public AppProfileDAO() {
	super(AppProfile.class);
    }

    /*public AppProfile getDefaultApplicationProfile(String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "profile");
	criteria.createAlias("profile.application", "application_alias");
	criteria.add(Restrictions.eq("application_alias.system", system));
	criteria.add(Restrictions.eq("profileType", ProfileType.Default));
	
	@SuppressWarnings("unchecked")
	List<AppProfile> list = criteria.list();
	
	return CollectionUtils.isEmpty(list) ? null : list.get(0);
    }*/
}
