package com.axisbankcmspaypro.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.PayConnectUser;
import com.payconnect.model.Logout;
import com.payconnect.model.ResetPasswordModel;
import com.poweraccess.dao.GenericDAO;

@Repository
public class PayConnectUserDAO extends GenericDAO<PayConnectUser, Integer> {

	private static final long serialVersionUID = -8199762439457269719L;
	private Logger logger = Logger.getLogger(PayConnectUserDAO.class);

	public PayConnectUserDAO() {
		super(PayConnectUser.class);
	}

	/**
	 * This method is use to get all menu details by user
	 * 
	 * @param userId
	 * @param appId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public PayConnectUser getByUserIdAndApplication(String userId, String system,String deviceId) {

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass(), "app_user");
		criteria.add(Restrictions.eq("userID", userId));
		criteria.add(Restrictions.eq("deviceID", deviceId));
		criteria.createAlias("app_user.application", "application_alias");
		criteria.add(Restrictions.eq("application_alias.system", system));

		List<PayConnectUser> list = criteria.list();

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication End");
		}
		return org.springframework.util.CollectionUtils.isEmpty(list) ? null
				: (PayConnectUser) list.get(0);
	}
	
	/**
	 * this method is used to get those users who uses same device
	 * @param deviceId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PayConnectUser> getUsersOfDevice(String deviceId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		criteria.add(Restrictions.eq("deviceID", deviceId));

		return criteria.list();
	}
	
	/**
	 * 
	 * @param userId
	 * @param system
	 * @return
	 */
	public PayConnectUser getByUserIdAndApplication(String userId, String system) {

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass(), "app_user");
		criteria.add(Restrictions.eq("userID", userId));
		criteria.createAlias("app_user.application", "application_alias");
		criteria.add(Restrictions.eq("application_alias.system", system));

		@SuppressWarnings("unchecked")
		List<PayConnectUser> list = criteria.list();

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication End");
		}
		return org.springframework.util.CollectionUtils.isEmpty(list) ? null
				: (PayConnectUser) list.get(0);
	}
	
	
	


	public boolean isUserPresent(String userId) {

		if (logger.isInfoEnabled()) {
			logger.info("isUserPresent start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass());
		criteria.add(Restrictions.eq("userID", userId));

		if (logger.isInfoEnabled()) {
			logger.info("isUserPresent End");
		}
		return criteria.list().size() > 0;
	}

	/**
	 * 
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userID
	 * @param system
	 * @return
	 */
	public boolean isUserBlocked(String userID, String system) {
		if (logger.isInfoEnabled()) {
			logger.info("isUserBlocked start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass(), "app_user");
		criteria.add(Restrictions.eq("userID", userID));
		criteria.createAlias("app_user.application", "application_alias");
		criteria.add(Restrictions.eq("application_alias.system", system));

		@SuppressWarnings("unchecked")
		List<PayConnectUser> list = criteria.list();

		if (!CollectionUtils.isEmpty(list)) {
			PayConnectUser connectUser = list.get(0);
			return StringUtils.isNotBlank(connectUser.getUserStatus())
					&& StringUtils.equals(connectUser.getUserStatus(), "D");
		}

		if (logger.isInfoEnabled()) {
			logger.info("isUserBlocked End");
		}

		return false;

	}

	/**
	 * this method is used to get Payconnect user object by user id and system
	 * value
	 * 
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param userID
	 * @param system
	 * @return
	 */
	public PayConnectUser getConnectUserByUserID(String userID, String system) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass(), "app_user");
		criteria.add(Restrictions.eq("userID", userID));
		criteria.createAlias("app_user.application", "application_alias");
		criteria.add(Restrictions.eq("application_alias.system", system));

		@SuppressWarnings("unchecked")
		List<PayConnectUser> list = criteria.list();

		return CollectionUtils.isEmpty(list) ? null : list.get(0);
	}

	/**
	 * this method is used to save or update a payconnect user
	 * 
	 * @since 08-Jan-2016
	 * @author Avishek Seal
	 * @param payConnectUser
	 */
	public void saveOrUpdate(PayConnectUser payConnectUser) {
		sessionFactory.getCurrentSession().saveOrUpdate(payConnectUser);
	}
	
	/**
	 * this method is used to check whether the user is present or not before reset password
	 * @since 15-Jan-2016
	 * @author Avishek Seal
	 * @param resetPasswordModel
	 * @return
	 */
	public boolean isUserPresent(ResetPasswordModel resetPasswordModel){
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "app_user");
	    
	    criteria.createAlias("app_user.application", "application_alias");
	    criteria.add(Restrictions.eq("application_alias.system", resetPasswordModel.getSystemValueFromApi()));
	    criteria.add(Restrictions.eq("userID", resetPasswordModel.getUserName()));
	    
	   return !CollectionUtils.isEmpty(criteria.list());
	}
	
	/**
	 * this method is used to get pay connect user details for a valid device id and mpin value
	 * @since 25-Feb-2016
	 * @author Avishek Seal
	 * @param deviceID
	 * @param MPIN
	 * @return
	 */
	public PayConnectUser getPayConnectUser(String deviceID, String MPIN){
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
	    
	    criteria.add(Restrictions.eq("deviceID", deviceID));
	    criteria.add(Restrictions.eq("mpin", MPIN));
		//criteria.add(Restrictions.eq("fingerOn", 0));
	    
	    @SuppressWarnings("unchecked")
	    final List<PayConnectUser> connectUsers = criteria.list();
	    
	    return CollectionUtils.isEmpty(connectUsers) ? null : connectUsers.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<PayConnectUser> getConnectUsersHavingSameDevice(String deviceId){
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
	    
	    criteria.add(Restrictions.eq("deviceID", deviceId));
	    
	    return criteria.list();
	}
	
	/**
	 * this method is used to fetch pay cconnect user during logout
	 * @since 04-Mar-2016
	 * @author Avishek Seal
	 * @param logout
	 * @return
	 */
	public PayConnectUser getPayConnectUser(Logout logout) {
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "app_user");
	
	    criteria.createAlias("app_user.application", "application_alias");
	    
	    Conjunction mpin = Restrictions.conjunction();
	    
	    Conjunction user = Restrictions.conjunction();
	    
	    Disjunction disjunction = Restrictions.disjunction();
	    
	    mpin.add(Restrictions.eq("mpin", logout.getMpin()));
	    mpin.add(Restrictions.eq("deviceID", logout.getDeviceId()));
	    
	    user.add(Restrictions.eq("userID", logout.getUserId()));
	    user.add(Restrictions.eq("application_alias.system", logout.getSystem()));
	    
	    disjunction.add(user);
	    disjunction.add(mpin);
	    
	    criteria.add(disjunction);

	    @SuppressWarnings("unchecked")
	    List<PayConnectUser> list = criteria.list();

	    return org.springframework.util.CollectionUtils.isEmpty(list) ? null : list.get(0);
	}
	
	
	public PayConnectUser getConnectUserByDevice(String deviceId){
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
	    
	    criteria.add(Restrictions.eq("deviceID", deviceId));
	    
	    return (PayConnectUser) criteria.uniqueResult();
	}
	
	
	@SuppressWarnings("unchecked")
	public PayConnectUser getByUserIdAndApplicationAndDeviceId(String userId, String deviceId,String system) {

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass(), "app_user");
		criteria.add(Restrictions.eq("userID", userId));
		criteria.add(Restrictions.eq("deviceID", deviceId));
		criteria.createAlias("app_user.application", "application_alias");
		criteria.add(Restrictions.eq("application_alias.system", system));

		List<PayConnectUser> list = criteria.list();

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication End");
		}
		return org.springframework.util.CollectionUtils.isEmpty(list) ? null
				: (PayConnectUser) list.get(0);
	}
	
	public PayConnectUser getByUserIdAndApplicationAndDeviceId(String deviceId, String system) {

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication start");
		}

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "app_user");
		criteria.add(Restrictions.eq("fingerOn", 1));
		criteria.add(Restrictions.eq("deviceID", deviceId));
		criteria.createAlias("app_user.application", "application_alias");
		criteria.add(Restrictions.eq("application_alias.system", system));

		List<PayConnectUser> list = criteria.list();

		if (logger.isInfoEnabled()) {
			logger.info("getByUserIdAndApplication End");
		}
		return org.springframework.util.CollectionUtils.isEmpty(list) ? null
				: (PayConnectUser) list.get(0);
	}
	
	
	
	
}