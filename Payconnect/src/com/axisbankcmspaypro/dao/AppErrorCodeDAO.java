package com.axisbankcmspaypro.dao;

import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.CMSError;
import com.poweraccess.dao.GenericDAO;

@Repository
public class AppErrorCodeDAO extends GenericDAO<CMSError, Integer> {

	public AppErrorCodeDAO() {
		super(CMSError.class);
		// TODO Auto-generated constructor stub
	}

}