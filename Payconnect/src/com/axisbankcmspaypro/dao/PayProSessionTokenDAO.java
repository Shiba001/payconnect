package com.axisbankcmspaypro.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.poweraccess.dao.GenericDAO;


@Repository
public class PayProSessionTokenDAO extends GenericDAO<PayProSessionToken, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6923812277894737222L;
	private Logger logger = Logger.getLogger(PayProSessionTokenDAO.class);

	public PayProSessionTokenDAO() {
		super(PayProSessionToken.class);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * This method is use to get session token details
	 * @param userId
	 * @return
	 */
	
	public PayProSessionToken getPayProSessionToken(String userId){
		
		//System.out.println("inside getPayProSessionToken dao method >>>>>>>>>>> ");
		
		if(logger.isInfoEnabled()){
			logger.info("getPayProSessionToken start");
		}
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());
		
		criteria.add(Restrictions.eq("userId", userId));
		
		if(logger.isInfoEnabled()){
			logger.info("getPayProSessionToken End");
		}
		return (PayProSessionToken)criteria.uniqueResult();
	}
	/**
	 * This method is use to check that session token and user id are present or not in db
	 * @param sessionToken
	 * @param userId
	 * @return
	 */
	public boolean isPayProSessionTokenPresent(String sessionToken,String userId){
		
		if(logger.isInfoEnabled()){
			logger.info("isPayProSessionTokenPresent start");
		}
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				getEntityClass());
		criteria.add(Restrictions.eq("userId", userId));
		criteria.add(Restrictions.eq("sessionToken", sessionToken));
		
		if(logger.isInfoEnabled()){
			logger.info("isPayProSessionTokenPresent End");
		}
		
		//System.out.println(sessionToken + " " + userId);
		
		@SuppressWarnings("unchecked")
		List<PayProSessionToken> list = criteria.list();
		
		//System.out.println("List ## " + list);
		
		return !CollectionUtils.isEmpty(list);
	}

	/**
	 * this method iss used to fecth session info if exists
	 * @since 04-Mar-2016
	 * @author Avishek Seal
	 * @param sessionToken
	 * @param userId
	 * @return
	 */
	public PayProSessionToken getPayProSessionTokenPresent(String sessionToken,String userId){
	    Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass());

	    criteria.add(Restrictions.eq("userId", userId));
	    criteria.add(Restrictions.eq("sessionToken", sessionToken));

	    if(logger.isInfoEnabled()){
		logger.info("isPayProSessionTokenPresent End");
	    }
		
	    @SuppressWarnings("unchecked")
	    List<PayProSessionToken> list = criteria.list();
		
	    return CollectionUtils.isEmpty(list) ? null : list.get(0);
	}
}
