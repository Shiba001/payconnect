/**
 * 
 */
package com.axisbankcmspaypro.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.CMSFaq;
import com.axisbankcmspaypro.enumeration.Status;
import com.poweraccess.dao.GenericDAO;

/**
 * @author Avishek Seal
 *
 */
@Repository
public class CMSFaqDAO extends GenericDAO<CMSFaq, Integer> {

    private static final long serialVersionUID = 366055118532136803L;

    /**
     * @param entityClass
     */
    public CMSFaqDAO() {
	super(CMSFaq.class);
    }
    
    /**
     * this method is used to get list of ACTIVE FAQ by application
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<CMSFaq> getActiveFAQsByApplication(String system){
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(getEntityClass(), "faq");
	
	criteria.createAlias("faq.application", "application");
	criteria.add(Restrictions.eq("application.system", system));
	criteria.add(Restrictions.eq("status", Status.ACTIVE));
	
	return criteria.list();
    }
}
