package com.axisbankcmspaypro.dao;

import org.springframework.stereotype.Repository;

import com.axisbankcmspaypro.entity.CMSConfiguration;
import com.poweraccess.dao.GenericDAO;

@Repository
public class CmsConfigurationDAO extends GenericDAO<CMSConfiguration, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4547513604190442640L;

	public CmsConfigurationDAO() {
		super(CMSConfiguration.class);
		// TODO Auto-generated constructor stub
	}
	
	
	

}
