package com.axisbankcmspaypro.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.entity.Ticket;

@Component
public class TicketConverter implements CopyConverter<Ticket, com.axisbankcmspaypro.model.Ticket>{

	private static final long serialVersionUID = 6430739081850626441L;
	
	@Override
	public com.axisbankcmspaypro.model.Ticket entityToModel(Ticket e) {
		final com.axisbankcmspaypro.model.Ticket ticket = new com.axisbankcmspaypro.model.Ticket();
		
		if(e != null){
			ticket.setTicketID(e.getId());
			ticket.setReason(e.getReason());
			ticket.setTicketStatus(e.getTicketStatus());
			ticket.setTicketDate(e.getTicketDate());
			ticket.setApplicationUserID(e.getPayConnectUser().getUserID());
			ticket.setApplicationName(e.getApplication().getApplicationName());
			ticket.setTicketNumber(e.getTicketNumber());
			ticket.setTicketTitle(e.getTicketTitle());
			
			if (e.getTicketCategory() != null){
				ticket.setTicketCategory(e.getTicketCategory().getTicketCategoryName());
			}
			
			ticket.setTicketTitle(ticket.getTicketTitle());
		}
		return ticket;
	}
	
	@Override
	public List<com.axisbankcmspaypro.model.Ticket> entityToModel(List<Ticket> es) {
		final List<com.axisbankcmspaypro.model.Ticket> tickets = new ArrayList<>();
		
		if(!CollectionUtils.isEmpty(es)){
			for(Ticket ticket : es){
				tickets.add(entityToModel(ticket));
			}			
		}
		return tickets;
	}
	
	@Override
	public List<Ticket> modelToEntity(List<com.axisbankcmspaypro.model.Ticket> ms) {
		return null;
	}
	
	@Override
	public Ticket modelToEntity(com.axisbankcmspaypro.model.Ticket m) {
		final Ticket ticket = new Ticket();
		
		if( m != null) {
			
			ticket.setReason(m.getReason());
			ticket.setTicketDate(m.getTicketDate());
			ticket.setTicketTitle(m.getTicketTitle());
		}
		
		return ticket;
	}
	
}
