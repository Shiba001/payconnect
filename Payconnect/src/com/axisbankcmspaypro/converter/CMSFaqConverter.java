package com.axisbankcmspaypro.converter;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.entity.CMSFaq;
import com.axisbankcmspaypro.enumeration.Status;
import com.axisbankcmspaypro.model.Faq;


@Component
public class CMSFaqConverter implements CopyConverter<CMSFaq, Faq>{

	@Autowired
	private ApplicationDAO applicationDAO;

	/**
	 * 
	 */
	private static final long serialVersionUID = -7284934780318129163L;

	
	@Override
	public CMSFaq modelToEntity(Faq faqModel) {
		
		CMSFaq cmsFaq = new CMSFaq();
		
		cmsFaq.setQuestion(faqModel.getQuestion());
		cmsFaq.setAnswer(faqModel.getAnswer().getBytes(Charset.forName("UTF-8")));
		cmsFaq.setStatus(Status.ACTIVE);
		cmsFaq.setCreateTimeStamp(new Date());
		cmsFaq.setLastUpdateTimeStamp(new Date());
		cmsFaq.setVersion(0L);
		return cmsFaq;
	}

	@Override
	public Faq entityToModel(CMSFaq e) {
		// TODO Auto-generated method stub
		
		Faq faq = new Faq();
		faq.setId(e.getId());
		faq.setQuestion(e.getQuestion());
		if(e.getAnswer() != null){
			faq.setAnswer(new String(e.getAnswer()));
		}
		
		return faq;
	}

	@Override
	public List<Faq> entityToModel(List<CMSFaq> es) {
		// TODO Auto-generated method stub
		
		List<Faq> faqs = new ArrayList<>();
		if(es!=null){
			for(CMSFaq cmsFaq:es){
				
				faqs.add(entityToModel(cmsFaq));
			}
		}
		
		return faqs;
	}

	@Override
	public List<CMSFaq> modelToEntity(List<Faq> ms) {
		// TODO Auto-generated method stub
		return null;
	}

}
