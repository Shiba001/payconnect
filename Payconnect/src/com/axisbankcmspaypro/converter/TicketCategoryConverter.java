package com.axisbankcmspaypro.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.axisbankcmspaypro.entity.TicketCategory;

@Component
public class TicketCategoryConverter implements CopyConverter<TicketCategory, com.axisbankcmspaypro.model.TicketCategory> {

	private static final long serialVersionUID = 1888688878790125189L;
	
	@Override
	public List<com.axisbankcmspaypro.model.TicketCategory> entityToModel(List<TicketCategory> es) {
		final List<com.axisbankcmspaypro.model.TicketCategory> ticketCategories = new ArrayList<>();
		
		if(es != null && !es.isEmpty()){
			for(TicketCategory category : es){
				ticketCategories.add(entityToModel(category));
			}
		}
		
		return ticketCategories;
	}
	
	@Override
	public com.axisbankcmspaypro.model.TicketCategory entityToModel(TicketCategory e) {
		final com.axisbankcmspaypro.model.TicketCategory ticketCategory = new com.axisbankcmspaypro.model.TicketCategory();
		
		if(e != null){
			ticketCategory.setCategoryID(e.getId());
			ticketCategory.setCategoryName(e.getTicketCategoryName());
		}
		
		return ticketCategory;
	}
	
	@Override
	public List<TicketCategory> modelToEntity(List<com.axisbankcmspaypro.model.TicketCategory> ms) {
		
		return null;
	}
	@Override
	public TicketCategory modelToEntity(com.axisbankcmspaypro.model.TicketCategory m) {
		
		return null;
	}
	
}
