package com.axisbankcmspaypro.enumeration;

public enum TicketStatus {
	OPEN, SOLVED;
}
