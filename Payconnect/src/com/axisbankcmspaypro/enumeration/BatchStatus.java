package com.axisbankcmspaypro.enumeration;

public enum BatchStatus {
	Approved, Rejected;
}
