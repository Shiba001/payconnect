package com.axisbankcmspaypro.enumeration;

public enum VersionUpdateStatus {
	Mandatory, Optional;
}
