/**
 * 
 */
package com.axisbankcmspaypro.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.catalina.util.Base64;

import com.common.Common;
import com.payconnect.model.AbstractResponseModel;
import com.payconnect.model.MapToField;
/**
 * @author Avishek Seal
 *
 */
public class CoachMarkModel extends AbstractResponseModel<CoachMarkModel> implements Serializable, Detector{

    private static final long serialVersionUID = -4790508882624248290L;
    
    @MapToField(name = "response.code", encrypt = false)
    private String responseCode;
    
    @MapToField(name = "response.details", encrypt = false)
    private String responseDetails;
    
    @MapToField(name = "coach.mark.url")
    private String URL;
    
    @MapToField(name = "coach.mark.message")
    private String messageText;
    
    public CoachMarkModel() {
		super(CoachMarkModel.class);
	}

    /**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the responseDetails
	 */
	public String getResponseDetails() {
		return responseDetails;
	}

	/**
	 * @param responseDetails the responseDetails to set
	 */
	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}

	/**
     * Avishek Seal
     * @return the uRL
     */
    public String getURL() {
        return URL;
    }

    /**
     * Avishek Seal
     * @param uRL the uRL to set
     */
    public void setURL(String uRL) {
        URL = uRL;
    }

    /**
     * Avishek Seal
     * @return the messageText
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Avishek Seal
     * @param messageText the messageText to set
     */
    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
    
    public String getImageString(){
	String data = "data:#content;base64,";
	
	try {
	    if(StringUtils.isNotBlank(URL)){
		final File img = new File(URL);
		data = data.replace("#content", TIKA.detect(img));
		return data+Base64.encode(FileUtils.readFileToByteArray(img));
	    } else {
		return null;
	    }
	} catch (IOException e) {
		return null;
	}
    }
    
    @Override
    public String toJsonString() throws Exception {
    	Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.encrypt()) {
						jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
					} else {
						jsonObject.put(bundle.getString(field2.name()), field.get(this));
					}

				} else {
					if (field2.nullAllowed()) {
						jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
					}
				}
			}
		}

		return jsonObject.toString();
    }
}
