package com.axisbankcmspaypro.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.ResourceBundle;

import org.json.JSONArray;

import com.common.Common;
import com.payconnect.model.AbstractResponseModel;
import com.payconnect.model.MapToField;

public class TicketCategoryModel extends AbstractResponseModel<TicketCategoryModel>{

	private static final long serialVersionUID = 1084584137598096778L;

	@MapToField(name = "response.code", encrypt = false)
	private String responseCode;
	
	@MapToField(name = "response.details", encrypt = false)
	private String responseDetails;
	
	@MapToField(name = "ticket.category.list", associateList = true, nullAllowed = true, encrypt = false)
	private List<TicketCategory> ticketCategories;
	
	public TicketCategoryModel() {
		super(TicketCategoryModel.class);
	}
	
	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}



	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}



	/**
	 * @return the responseDetails
	 */
	public String getResponseDetails() {
		return responseDetails;
	}



	/**
	 * @param responseDetails the responseDetails to set
	 */
	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}



	/**
	 * @return the ticketCategories
	 */
	public List<TicketCategory> getTicketCategories() {
		return ticketCategories;
	}



	/**
	 * @param ticketCategories the ticketCategories to set
	 */
	public void setTicketCategories(List<TicketCategory> ticketCategories) {
		this.ticketCategories = ticketCategories;
	}



	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.associateList()) {
						JSONArray array = new JSONArray(field.get(this)
								.getClass().cast(field.get(this)).toString());
						jsonObject.put(bundle.getString(field2.name()), array);
					} else {
						if (field2.encrypt()) {
							jsonObject.put(bundle.getString(field2.name()),
									Common.encode(Common.PRIVATE_KEY, field
											.get(this).toString()));
						} else {

							jsonObject.put(bundle.getString(field2.name()),
									field.get(this).toString());
						}
					}

				} else {
					if (field2.nullAllowed()) {
						if(field2.associateList()) {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, "[]"));
						} else {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
						}
					}
				}
			}
		}

		return jsonObject.toString();
	}
}
