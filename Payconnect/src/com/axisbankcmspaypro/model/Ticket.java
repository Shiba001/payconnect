package com.axisbankcmspaypro.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.json.JSONArray;
import org.json.JSONObject;

import com.axisbankcmspaypro.comon.AxisbankDateFormat;
import com.axisbankcmspaypro.enumeration.TicketStatus;
import com.common.Common;
import com.payconnect.model.AbstractResponseModel;
import com.payconnect.model.MapToField;

public class Ticket extends AbstractResponseModel<Ticket> implements Serializable, AxisbankDateFormat{
	
	private static final long serialVersionUID = 3227309903794069994L;

	private int ticketID;
	
	@MapToField(name = "ticket.number")
	private String ticketNumber;
	
	@MapToField(name = "ticket.date")
	private Date ticketDate;
	
	@MapToField(name = "ticket.reason")
	private String reason;
	
	@MapToField(name = "ticket.files", associateList = true, nullAllowed = true, encrypt = false)
	private List<TicketFile> ticketFiles;
	
	@MapToField(name = "ticket.category")
	private String ticketCategory;
	
	private String applicationName;
	
	private String applicationUserID;
	
	@MapToField(name = "ticket.status")
	private TicketStatus ticketStatus;
	
	@MapToField(name = "ticket.title")
	private String ticketTitle;

	public Ticket() {
		super(Ticket.class);
	}

	public int getTicketID() {
		return ticketID;
	}

	public void setTicketID(int ticketID) {
		this.ticketID = ticketID;
	}
	
	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getTicketDateString(){
		return DATE_FORMAT.format(ticketDate);
	}
	
	public Date getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(Date ticketDate) {
		this.ticketDate = ticketDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<TicketFile> getFiles() {
		return ticketFiles;
	}

	public void setFiles(List<TicketFile> ticketFiles) {
		this.ticketFiles = ticketFiles;
	}

	public String getTicketCategory() {
		return ticketCategory;
	}

	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationUserID() {
		return applicationUserID;
	}

	public void setApplicationUserID(String applicationUserID) {
		this.applicationUserID = applicationUserID;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	/**
	 * @return the ticketTitle
	 */
	public String getTicketTitle() {
		return ticketTitle;
	}

	/**
	 * @param ticketTitle the ticketTitle to set
	 */
	public void setTicketTitle(String ticketTitle) {
		this.ticketTitle = ticketTitle;
	}

	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = this.getClass().getDeclaredFields();
	   	
	   	ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

	   	JSONObject jsonObject = new JSONObject();
	   	
	   	for (Field field : filelds) {
	   		Annotation annotation = field.getAnnotation(MapToField.class);
	   		MapToField field2 = MapToField.class.cast(annotation);

	   		if (field2 != null) {
	   			if (field.get(this) != null) {
	   				if (field2.associateList()) {
						JSONArray array = new JSONArray(field.get(this).getClass().cast(field.get(this)).toString());
						jsonObject.put(bundle.getString(field2.name()), array);
					} else {
						if (field2.encrypt()) {
		   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
		   				} else {
		   					jsonObject.put(bundle.getString(field2.name()), field.get(this));
		   				}
					}

	   			} else {
	   				if (field2.nullAllowed()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
	   				}
	   			}
	   		}
	   	}
	 
	   	return jsonObject.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
		    return toJsonString();
		} catch (Exception e) {
		    return "{}";
		}
	}
}
