package com.axisbankcmspaypro.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.ResourceBundle;

import com.axisbankcmspaypro.enumeration.DeviceType;
import com.axisbankcmspaypro.enumeration.VersionUpdateStatus;
import com.common.Common;
import com.payconnect.model.AbstractResponseModel;
import com.payconnect.model.MapToField;

/**
 * 
 * @author Avishek Seal
 *
 */
public class VersionModel extends AbstractResponseModel<VersionModel> implements Serializable{

	private static final long serialVersionUID = 1323322504403064890L;
	
	@MapToField(name = "response.code", encrypt = false)
	private String responseCode;
	
	@MapToField(name = "response.details", encrypt = false)
	private String responseDetials;
	
	@MapToField(name = "version.war")
	private String warVersion;
	
	@MapToField(name = "version.api")
	private String apiVersion;
	
	@MapToField(name = "version.update")
	private VersionUpdateStatus versionUpdateStatus;
	
	@MapToField(name = "version.release")
	private Date releaseDate;
	
	private DeviceType deviceType;

	
	public VersionModel() {
		super(VersionModel.class);
	}
	
	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}


	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}


	/**
	 * @return the responseDetials
	 */
	public String getResponseDetials() {
		return responseDetials;
	}


	/**
	 * @param responseDetials the responseDetials to set
	 */
	public void setResponseDetials(String responseDetials) {
		this.responseDetials = responseDetials;
	}


	/**
	 * @return the warVersion
	 */
	public String getWarVersion() {
		return warVersion;
	}

	/**
	 * @param warVersion the warVersion to set
	 */
	public void setWarVersion(String warVersion) {
		this.warVersion = warVersion;
	}

	/**
	 * @return the apiVersion
	 */
	public String getApiVersion() {
		return apiVersion;
	}

	/**
	 * @param apiVersion the apiVersion to set
	 */
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	/**
	 * @return the versionUpdateStatus
	 */
	public VersionUpdateStatus getVersionUpdateStatus() {
		return versionUpdateStatus;
	}

	/**
	 * @param versionUpdateStatus the versionUpdateStatus to set
	 */
	public void setVersionUpdateStatus(VersionUpdateStatus versionUpdateStatus) {
		this.versionUpdateStatus = versionUpdateStatus;
	}

	/**
	 * @return the releaseDate
	 */
	public Date getReleaseDate() {
		return releaseDate;
	}

	/**
	 * @param releaseDate the releaseDate to set
	 */
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the deviceType
	 */
	public DeviceType getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
	
	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.encrypt()) {
						jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
					} else {
						jsonObject.put(bundle.getString(field2.name()), field.get(this).toString());
					}

				} else {
					if (field2.nullAllowed()) {
						jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
					}
				}
			}
		}

		return jsonObject.toString();
	}

}
