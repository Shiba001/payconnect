/**
 * 
 */
package com.axisbankcmspaypro.model;




/**
 * @author Avishek Seal
 *
 */

public class DowntimeModel {

   
    
    private String startDate;
    
    private String endDate;
    
    private String reason;
    
    private String responseCode;
    
    private String responseDetails;
    

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDetails() {
		return responseDetails;
	}

	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}
    


    
    

    
  

    

}
