package com.axisbankcmspaypro.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.ResourceBundle;

import org.json.JSONArray;

import com.common.Common;
import com.payconnect.model.AbstractResponseModel;
import com.payconnect.model.MapToField;

public class TicketResponseModel extends AbstractResponseModel<TicketResponseModel>{

	private static final long serialVersionUID = 1945493605106405895L;
	
	@MapToField(name = "response.code", encrypt = false)
	private String responseCode;
	
	@MapToField(name = "response.details", encrypt = false)
	private String responseDetails;
	
	@MapToField(name = "ticket.list", associateList = true, nullAllowed = true, encrypt = false)
	private List<Ticket> tickets;
	
	public TicketResponseModel() {
		super(TicketResponseModel.class);
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}


	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}


	/**
	 * @return the responseDetails
	 */
	public String getResponseDetails() {
		return responseDetails;
	}


	/**
	 * @param responseDetails the responseDetails to set
	 */
	public void setResponseDetails(String responseDetails) {
		this.responseDetails = responseDetails;
	}


	/**
	 * @return the tickets
	 */
	public List<Ticket> getTickets() {
		return tickets;
	}


	/**
	 * @param tickets the tickets to set
	 */
	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}


	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = thisClass.getDeclaredFields();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

		for (Field field : filelds) {
			Annotation annotation = field.getAnnotation(MapToField.class);
			MapToField field2 = MapToField.class.cast(annotation);

			if (field2 != null) {
				if (field.get(this) != null) {
					if (field2.associateList()) {
						JSONArray array = new JSONArray(field.get(this)
								.getClass().cast(field.get(this)).toString());
						jsonObject.put(bundle.getString(field2.name()), array);
					} else {
						if (field2.encrypt()) {
							jsonObject.put(bundle.getString(field2.name()),Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
						} else {

							jsonObject.put(bundle.getString(field2.name()),
									field.get(this).toString());
						}
					}

				} else {
					if (field2.nullAllowed()) {
						if(field2.associateList()) {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, "[]"));
						} else {
							jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
						}
					}
				}
			}
		}

		return jsonObject.toString();
	}
}
