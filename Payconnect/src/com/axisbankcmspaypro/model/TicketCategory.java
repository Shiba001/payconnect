package com.axisbankcmspaypro.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.json.JSONObject;

import com.axisbankcmspaypro.comon.AxisbankDateFormat;
import com.common.Common;
import com.payconnect.model.AbstractResponseModel;
import com.payconnect.model.MapToField;

public class TicketCategory extends AbstractResponseModel<TicketCategory> implements Serializable, AxisbankDateFormat {

	private static final long serialVersionUID = -4814179600815128858L;
	
	@MapToField(name = "ticket.category.id")
	private int categoryID;
	
	@MapToField(name = "ticket.category.name")
	private String categoryName;
	
	public TicketCategory() {
		super(TicketCategory.class);
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = this.getClass().getDeclaredFields();
	   	
	   	ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

	   	JSONObject jsonObject = new JSONObject();
	   	
	   	for (Field field : filelds) {
	   		Annotation annotation = field.getAnnotation(MapToField.class);
	   		MapToField field2 = MapToField.class.cast(annotation);

	   		if (field2 != null) {
	   			if (field.get(this) != null) {
	   				if (field2.encrypt()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
	   				} else {
	   					jsonObject.put(bundle.getString(field2.name()), field.get(this));
	   				}
	   			} else {
	   				if (field2.nullAllowed()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
	   				}
	   			}
	   		}
	   	}
	 
	   	return jsonObject.toString();
	}
	
	@Override
    public String toString() {
        try {
		    return toJsonString();
		} catch (Exception e) {
		    return "{}";
		}
    }
}
