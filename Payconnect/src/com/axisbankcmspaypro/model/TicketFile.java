package com.axisbankcmspaypro.model;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ResourceBundle;

import org.json.JSONObject;

import com.common.Common;
import com.payconnect.model.AbstractResponseModel;
import com.payconnect.model.MapToField;

public class TicketFile extends AbstractResponseModel<TicketFile> implements Serializable {
	
	private static final long serialVersionUID = 5735598015328068819L;
	
	private byte[] fileContent;
	
	@MapToField(name = "ticket.file.type")
	private String fileType;
	
	@MapToField(name = "ticket.file.name")
	private String fileName;
	
	@MapToField(name = "ticket.file.link")
	private String fileLink;
	
	@MapToField(name = "ticket.file.thumbnail")
	private String fileThumbnail;
	
	public TicketFile() {
		super(TicketFile.class);
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the fileLink
	 */
	public String getFileLink() {
		return fileLink;
	}

	/**
	 * @param fileLink the fileLink to set
	 */
	public void setFileLink(String fileLink) {
		this.fileLink = fileLink;
	}
	
	/**
	 * @return the fileThumbnail
	 */
	public String getFileThumbnail() {
		return fileThumbnail;
	}

	/**
	 * @param fileThumbnail the fileThumbnail to set
	 */
	public void setFileThumbnail(String fileThumbnail) {
		this.fileThumbnail = fileThumbnail;
	}

	/**
	 * 
	 */
	@Override
	public String toJsonString() throws Exception {
		Field[] filelds = this.getClass().getDeclaredFields();
	   	
	   	ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

	   	JSONObject jsonObject = new JSONObject();
	   	
	   	for (Field field : filelds) {
	   		Annotation annotation = field.getAnnotation(MapToField.class);
	   		MapToField field2 = MapToField.class.cast(annotation);

	   		if (field2 != null) {
	   			if (field.get(this) != null) {
	   				if (field2.encrypt()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, field.get(this).toString()));
	   				} else {
	   					jsonObject.put(bundle.getString(field2.name()), field.get(this));
	   				}
	   			} else {
	   				if (field2.nullAllowed()) {
	   					jsonObject.put(bundle.getString(field2.name()), Common.encode(Common.PRIVATE_KEY, ""));
	   				}
	   			}
	   		}
	   	}
	 
	   	return jsonObject.toString();
	}
	
	@Override
    public String toString() {
        try {
		    return toJsonString();
		} catch (Exception e) {
		    return "{}";
		}
    }
}
