/**
 * 
 */
package com.axisbankcmspaypro.exception;

/**
 * @author Avishek Seal
 *
 */
public class InvalidInputException extends Exception {

    private static final long serialVersionUID = -588016534571886169L;
    
    private String countLeft;
    /**
     * 
     */
    
    public InvalidInputException(String data) {
    	super(data+" is blank ");
    }
    
    public InvalidInputException(String data, String countLeft) {
    	super(data+" is blank ");
    	
    	this.countLeft = countLeft;
    }
	/**
	 * @return the countLeft
	 */
	public String getCountLeft() {
		return countLeft;
	}
}