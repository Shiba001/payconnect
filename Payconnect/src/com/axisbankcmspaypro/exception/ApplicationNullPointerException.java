package com.axisbankcmspaypro.exception;

public class ApplicationNullPointerException extends Exception {

	private static final long serialVersionUID = 3465668905522679212L;

	private String name;
	
	public ApplicationNullPointerException(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
