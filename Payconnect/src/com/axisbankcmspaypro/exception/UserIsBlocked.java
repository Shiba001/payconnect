/**
 * 
 */
package com.axisbankcmspaypro.exception;

/**
 * @author Avishek Seal
 *
 */
public class UserIsBlocked extends Exception {

    private static final long serialVersionUID = 5589687889923874589L;
    
    public UserIsBlocked() {
	super("User is blocked");
    }

}
