package com.axisbankcmspaypro.exception;

public class InvalidDateException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7245404802918065241L;

	public InvalidDateException(String data) {
		super("Invalid "+data+". it should be dd-mm-yyyy pattern");
	}

}
