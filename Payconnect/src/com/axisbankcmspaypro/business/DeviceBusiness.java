package com.axisbankcmspaypro.business;

import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.DeviceDAO;
import com.axisbankcmspaypro.entity.AppDeviceBlock;
import com.payconnect.model.DeviceModel;
import com.payconnect.model.LoginRequestModel;
import com.payconnect.model.UnlockDeviceModel;

@Service
@Transactional
public class DeviceBusiness {
	
	@Autowired
	private DeviceDAO deviceDAO;
	
	
	/**
	 * This method is use to save or update device id details
	 * @param deviceModel
	 * @return
	 */
	public void saveOrUpdateDeviceId(DeviceModel deviceModel){
		
		System.out.println("inside save and update block");
		
		AppDeviceBlock appDeviceBlock=deviceDAO.getDeviceId(deviceModel.getDeviceId());
		
		if(appDeviceBlock == null){
			
			appDeviceBlock = new AppDeviceBlock();
			
			appDeviceBlock.setDeviceId(deviceModel.getDeviceId().trim());
			appDeviceBlock.setCreateDate(new Date());
			appDeviceBlock.setUpdateDate(new Date());
			appDeviceBlock.setDeviceStatus("A");
		
			deviceDAO.create(appDeviceBlock);
		}else {
			appDeviceBlock.setDeviceId(deviceModel.getDeviceId());
			appDeviceBlock.setCreateDate(new Date());
			appDeviceBlock.setUpdateDate(new Date());
			appDeviceBlock.setDeviceStatus("A");
			
			deviceDAO.update(appDeviceBlock);
		}
	}
	
	
	
	  /**
     * this method is used to block payconnect user from CMS end
     * @since 08-Jan-2016
     * @author Avishek Seal
     * @param loginRequestModel
     */
    public void blockDevice(LoginRequestModel loginRequestModel){
    	
    	System.out.println("inside block device");
    	
    	AppDeviceBlock appDeviceBlock =deviceDAO.getDeviceId(loginRequestModel.getDeviceID());
	
	if(appDeviceBlock != null){
		appDeviceBlock.setDeviceStatus("D");
		deviceDAO.saveOrUpdate(appDeviceBlock);
	}
    } 
    
    
    
    
    
    
    

    /**
     * this method is used to delete device against device id
     
     */

      public void deleteDeviceId(String deviceId){
    	  
    	  final AppDeviceBlock appDeviceBlock = deviceDAO.getDeviceId(deviceId);
    	  
    	  if(appDeviceBlock!=null){
    		  
    		  deviceDAO.delete(appDeviceBlock);
    	  }
    	
    	
      }
      
      /**
       * 
       * @param loginRequestModel
       * @return
       */
     /* public boolean isUnlockCodeGenerated(LoginRequestModel loginRequestModel){
    		return deviceDAO.isUnlockCodeGenerate(loginRequestModel.getDeviceID());
    	    }*/
    	    
      
      
      
      public UnlockDeviceModel isUnlockCodeGenerated(LoginRequestModel loginRequestModel){
    	  
    	 AppDeviceBlock appDeviceBlock= deviceDAO.isUnlockCodeGenerate(loginRequestModel.getDeviceID());
    	 
    	final UnlockDeviceModel unlockDeviceModel=new UnlockDeviceModel();
    	
    	if (Objects.isNull(appDeviceBlock)) {

			unlockDeviceModel.setUnlockCode("");
			unlockDeviceModel.setDeviceStatus("A");
		}

		else if (Objects.nonNull(appDeviceBlock)) {
			
			
			unlockDeviceModel.setDeviceStatus(appDeviceBlock.getDeviceStatus());
			unlockDeviceModel.setUnlockCode(appDeviceBlock.getUnlockCode());
		}
    	 
    	
		 return unlockDeviceModel;
  		
  	    }
      
      
     
      
   

}
