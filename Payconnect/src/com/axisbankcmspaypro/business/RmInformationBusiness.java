package com.axisbankcmspaypro.business;


import java.util.List;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.RmDAO;
import com.axisbankcmspaypro.entity.RmInformation;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.payconnect.model.RMInformationModel;

@Service
@Transactional
public class RmInformationBusiness {
	
	
	@Autowired
	private RmDAO rmDAO;

	/**
	 * this method is used to retrieve Rm Information
	 * 
	 * @since 02-MaY-2016
	 * @param RMInformationModel
	 * @throws Exception
	
	 */
	public RMInformationModel getRmInformation()
			throws Exception {
		

		 
		List<RmInformation>informations=rmDAO.findAll();
		
		final  RMInformationModel rmInformationModel=new RMInformationModel();
		
				
		 if(!CollectionUtils.isEmpty(informations)){
			 
		    RmInformation rmInformation=informations.get(0);
		  
			rmInformationModel.setSupportDesk(rmInformation.getSupportDesk());
			
			rmInformationModel.setRmName(rmInformation.getRmName());
			
			rmInformationModel.setEmail(rmInformation.getEmail());
			
			rmInformationModel.setPhoneNo(rmInformation.getPhoneNo());
		
		}
		
		 return rmInformationModel;
		
	

	}

}
