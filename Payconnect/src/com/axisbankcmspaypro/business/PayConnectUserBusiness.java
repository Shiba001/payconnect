/**
 * 
 */
package com.axisbankcmspaypro.business;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbank.converter.RequestToPayConnectConverter;
import com.axisbankcmspaypro.dao.AppProfileDAO;
import com.axisbankcmspaypro.dao.ApplicationRoleDAO;
import com.axisbankcmspaypro.dao.CmsConfigurationDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.payconnect.model.LoginRequestModel;
import com.payconnect.model.LoginResponseModel;

/**
 * @author Avishek Seal
 *
 */


@Service
@Transactional
public class PayConnectUserBusiness{
    
    @Autowired
    private PayConnectUserDAO connectUserDAO;
    
    @Autowired
    private RequestToPayConnectConverter requestToPayConnectConverter;
    
    @Autowired
    private AppProfileDAO appProfileDAO;
    
    @Autowired
    private ApplicationRoleDAO applicationRoleDAO;
    
    @Autowired
    private CmsConfigurationDAO cmsConfigurationDAO;
    
    /**
     * this user is used to check whether the user in CMS is blocked or not
     * @since 08-Jan-2016
     * @author Avishek Seal
     * @param loginRequestModel
     * @return
     */
    public boolean isUserblocked(LoginRequestModel loginRequestModel){
	return connectUserDAO.isUserBlocked(loginRequestModel.getUserName(), loginRequestModel.getSystemValueFromApi());
    }
    
    /**
     * this method is used to save or update payconnect user
     * @since 08-Jan-2016
     * @author Avishek Seal
     * @param loginRequestModel
     * @param loginResponseModel
     */
    public void saveOrUpdateUser(LoginRequestModel loginRequestModel, LoginResponseModel loginResponseModel){
	PayConnectUser payConnectUser = connectUserDAO.getConnectUserByUserID(loginResponseModel.getUserID(), loginRequestModel.getSystemValueFromApi());//requestToPayConnectConverter.entityToModel(loginRequestModel)
	
	if(payConnectUser == null){
	    payConnectUser = requestToPayConnectConverter.toUser(loginRequestModel, loginResponseModel);
	}
	
	if(StringUtils.isNotBlank(loginResponseModel.getEmailID())){
	    payConnectUser.setEmailID(loginResponseModel.getEmailID());
	}
	
	if(StringUtils.isNotBlank(loginResponseModel.getMobileNumber())){
	    payConnectUser.setMobileNumber(loginResponseModel.getMobileNumber());
	}
	
	if(StringUtils.isNotBlank(loginResponseModel.getLastChangePaswordDate())){
	    payConnectUser.setLastPasswordChangedDate(loginResponseModel.getLastChangePaswordDate());
	}
	
	if(StringUtils.isNotBlank(loginResponseModel.getUserStatus())){
	    payConnectUser.setUserStatus(loginResponseModel.getUserStatus());
	}
	
	if(StringUtils.isNotBlank(loginRequestModel.getDeviceID())) {
		payConnectUser.setDeviceID(loginRequestModel.getDeviceID());
	}
	
	//payConnectUser.setPasswordChangeInWeb(loginResponseModel.getPasswordChangeinWeb());
	
	//change to password change in web set to zero
	payConnectUser.setPasswordChangeInWeb("0");
	
	
	if(payConnectUser.getAppProfile() == null){
	    payConnectUser.setAppProfile(applicationRoleDAO.getDefaultApplicationProfile(loginRequestModel.getSystemValueFromApi(), loginResponseModel.getUserType()));
	}
	
	payConnectUser.setUserType(loginResponseModel.getUserType());
	payConnectUser.setLastUpdateTimeStamp(new Date());
	payConnectUser.setCorporateID(loginResponseModel.getCorpID());
	payConnectUser.setPasswordExpiryDate(loginResponseModel.getPasswordExpiryDate());
	payConnectUser.setUserName(loginResponseModel.getUserName());
	payConnectUser.setLatitude(loginRequestModel.getLatitude());
	payConnectUser.setLongitude(loginRequestModel.getLongitude());
	payConnectUser.setClientIp(loginRequestModel.getClientIp());
	payConnectUser.setFingerOn(0);
	
	//newly added
	payConnectUser.setLastLogonDate(loginResponseModel.getLastLogin());
	
	if(payConnectUser.getId() == null){
	    payConnectUser.setCreateTimeStamp(new Date());
	    connectUserDAO.create(payConnectUser);
	} else {
	    connectUserDAO.update(payConnectUser);
	}
    }
    
    /**
     * this method is used to block payconnect user from CMS end
     * @since 08-Jan-2016
     * @author Avishek Seal
     * @param loginRequestModel
     */
    public void blockPayConnectUser(LoginRequestModel loginRequestModel){
	PayConnectUser payConnectUser = connectUserDAO.getConnectUserByUserID(loginRequestModel.getUserName(), loginRequestModel.getSystemValueFromApi());
	
	if(payConnectUser != null){
	    payConnectUser.setUserStatus("D");
	    connectUserDAO.saveOrUpdate(payConnectUser);
	}
    } 

    /**
     * 
     * @param userId
     * @param system
     * @param deviceId
     * @return
     */
    public PayConnectUser getByUserIdAndApplication(String userId, String system,String deviceId) {
    	return connectUserDAO.getByUserIdAndApplication(userId, system, deviceId);
    }
}
