package com.axisbankcmspaypro.business;


import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidDateException;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.common.Common;
import com.payconnect.model.PasswordExpiryDateModel;

@Service
@Transactional
public class PasswordExpiryDateBusiness {
	
	
	 @Autowired
	    private PayConnectUserDAO payConnectUserDAO;
	    
	    /**
	     * this method is used to update password expiry date for a specific user and system
	     * @since 09-Mar-2016
	     * @param passwordChangeInWebModel
	     * @throws InvalidInputException
	     * @throws DataNotFound
	     * @throws InvalidDateException 
	     */
	    public void updatePasswordExpiry(PasswordExpiryDateModel passwordExpiryDateModel) throws InvalidInputException, DataNotFound, InvalidDateException{
		if(StringUtils.isBlank(passwordExpiryDateModel.getUserId())){
		    throw new InvalidInputException("user");
		}
		
		if(StringUtils.isBlank(passwordExpiryDateModel.getSystem())) {
		    throw new InvalidInputException("system");
		}
		
		if(StringUtils.isBlank(passwordExpiryDateModel.getPwdExpiryDate())) {
		    throw new InvalidInputException("Password expiry date");
		}
		
		if(!Common.isValidDate(passwordExpiryDateModel.getPwdExpiryDate())){
			
			throw new InvalidDateException("Password expiry date");
		}
		
		final PayConnectUser connectUser = payConnectUserDAO.getByUserIdAndApplication(passwordExpiryDateModel.getUserId(), passwordExpiryDateModel.getSystem());
		
		if(connectUser == null) {
		    throw new DataNotFound("Invalid User id and system");
		}
		
	 
		
		connectUser.setPasswordExpiryDate(passwordExpiryDateModel.getPwdExpiryDate());
		
		payConnectUserDAO.update(connectUser);
	    }

}
