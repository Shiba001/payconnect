package com.axisbankcmspaypro.business;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.dao.PayProSessionTokenDAO;
import com.axisbankcmspaypro.dao.PowerAccessSessionTokenDAO;
import com.axisbankcmspaypro.entity.Application;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.common.MessageUtil;
import com.payconnect.model.Logout;
import com.poweraccess.model.SessionToken;


@Transactional
@Service
public class AppLogOutBusiness {
	
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private ApplicationDAO applicationDAO;
	
	@Autowired
	private PowerAccessSessionTokenDAO accessSessionTokenDAO;
	
	@Autowired
	private PayConnectUserDAO connectUserDAO;
	
	@Autowired
	private PayProSessionTokenDAO payProSessionTokenDAO;
	
	@Autowired
	private PowerAccessSessionTokenDAO powerAccessSessionTokenDAO;
	
	/**
	 * This method is use to get logout business
	 * @param logout
	 * @param httpServletRequest
	 * @return
	 */
	/*@SuppressWarnings({ "unchecked", "unused", "unused" })
	public JSONObject logoutBusiness(Logout logout,HttpServletRequest httpServletRequest){
		
		final JSONObject jsonObject = new JSONObject();
		System.out.println(logout.getDeviceId() + " == " + logout.getSessionToken() + " == " + logout.getSystem() + " == " + logout.getUserId());
		if(!StringUtils.isNotEmpty(logout.getSessionToken())){
			//{“error”:”Session Token is missing.”}
			jsonObject.put("ResponseCode", messageUtil.getBundle("error.sessiontoken.missing.code"));
			jsonObject.put("error", messageUtil.getBundle("error.sessiontoken.message"));
			return jsonObject;
		}
		
		if(!StringUtils.isNotEmpty(logout.getUserId())){
			//{“error”:”Something went wrong. Please try again later.”}
			jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
			return jsonObject;
		}
		
		Application application = applicationDAO.getApplicationBySystem(logout.getSystem());
		
		if(application == null){
			
			jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
			return jsonObject;
		}
		
		PayConnectUser payConnectUser = connectUserDAO.getByUserIdAndApplication(logout.getUserId(), logout.getSystem(),logout.getDeviceId()); 
		if(payConnectUser == null){
			//{“error”:”Something went wrong. Please try again later.”}
			jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
			return jsonObject;
		}
		
		//paypro logout fire
			
		if(!payProSessionTokenDAO.isPayProSessionTokenPresent(logout.getSessionToken(), logout.getUserId())){
			
			jsonObject.put("ResponseCode", messageUtil.getBundle("error.invalid.sessiontoken.code"));
			 jsonObject.put("error", messageUtil.getBundle("invalid.sessiontoken.message"));
			return jsonObject;
		}
		
		HttpSession httpSession = httpServletRequest.getSession(false);
		httpSession.removeAttribute("paypro_session_token");
		httpSession.invalidate();
		
		jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("success.code"));
		jsonObject.put(messageUtil.getBundle("response.details"), messageUtil.getBundle("success.message"));
		
		return jsonObject;
	}*/
	
	/**
	 * this method is used to logged out from the Application
	 * @since 04-Mar-2016
	 * @author Avishek Seal
	 * @param logout
	 * @param httpServletRequest
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public JSONObject logoutBusiness(Logout logout){
		
		final JSONObject jsonObject = new JSONObject();
		
		if(StringUtils.isBlank(logout.getSessionToken())){
			
			//System.out.println("inside missing token !!!!!!!!");
		    jsonObject.put("ResponseCode", messageUtil.getBundle("error.sessiontoken.missing.code"));
		    jsonObject.put("error", messageUtil.getBundle("error.sessiontoken.message"));
			
		    return jsonObject;
		}
		
		final PayConnectUser payConnectUser = connectUserDAO.getPayConnectUser(logout);
		
		if(payConnectUser == null) {
		    jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
		    jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));

		    return jsonObject;
		}
		
		if(StringUtils.equals(payConnectUser.getApplication().getSystem(), "2")) {
		    final PayProSessionToken payProSessionToken = payProSessionTokenDAO.getPayProSessionTokenPresent(logout.getSessionToken(), payConnectUser.getUserID());
		    
		    if(payProSessionToken == null) {
			jsonObject.put("ResponseCode", messageUtil.getBundle("error.invalid.sessiontoken.code"));
			jsonObject.put("error", messageUtil.getBundle("invalid.sessiontoken.message"));
				
			return jsonObject;
		    } else {
			payProSessionTokenDAO.delete(payProSessionToken);
		    }
		} else if(StringUtils.equals(payConnectUser.getApplication().getSystem(), "1")) {
		    final SessionToken sessionToken = powerAccessSessionTokenDAO.getPowerAccessSessionToken(payConnectUser.getUserID(), logout.getSessionToken());
		    
		    if(sessionToken == null) {
			jsonObject.put("ResponseCode", messageUtil.getBundle("error.invalid.sessiontoken.code"));
			jsonObject.put("error", messageUtil.getBundle("invalid.sessiontoken.message"));
				
			return jsonObject;
		    } else {
			powerAccessSessionTokenDAO.delete(sessionToken);
		    }
		} else {
		    jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
		    jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));

		    return jsonObject;
		}
		
		jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("success.code"));
		jsonObject.put(messageUtil.getBundle("response.details"), messageUtil.getBundle("success.message"));
		
		return jsonObject;
	}
}
