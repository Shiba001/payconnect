package com.axisbankcmspaypro.business;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.DataNotUpdateException;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.payconnect.model.SetBiomatricModel;

@Service
@Transactional
public class SetBiomatricBusiness {
	
	@Autowired
	private PayConnectUserDAO payConnectUserDAO;
	
	
	public SetBiomatricModel biomatricIndenfication(SetBiomatricModel setBiomatricModel)
			throws Exception {

		if (StringUtils.isBlank(setBiomatricModel.getUserId())) {
			throw new InvalidInputException("user id");
		}

		if (StringUtils.isBlank(setBiomatricModel.getSystem())) {
			throw new InvalidInputException("system");
		}
		
		final SetBiomatricModel biomatricModel=new SetBiomatricModel();

		PayConnectUser payConnectUser=payConnectUserDAO.getByUserIdAndApplication(setBiomatricModel.getUserId(), setBiomatricModel.getSystem());
		
		

		if (Objects.isNull(payConnectUser)) {

			throw new DataNotFound("Invalid user id or system");
		}
		
		else if(Objects.nonNull(payConnectUser)){
			
			
			
			biomatricModel.setUserId(payConnectUser.getUserID());
			
			biomatricModel.setSystem(payConnectUser.getApplication().getSystem());
			
		}
		
		return biomatricModel;
		
		
	}
	
	
	public void updatPayconnectUserFingerOn(SetBiomatricModel setBiomatricModel) throws DataNotUpdateException{
		
	  PayConnectUser payConnectUser=payConnectUserDAO.getByUserIdAndApplication(setBiomatricModel.getUserId(), setBiomatricModel.getSystem());
	  
	  if(Objects.nonNull(payConnectUser)){
		  
		  payConnectUser.setFingerOn(1);
		  
		  payConnectUserDAO.update(payConnectUser);
	  }
	  
	  else{
		  
		  throw new DataNotUpdateException();
	  }
		
		
	}
	
	
	
	

}
