package com.axisbankcmspaypro.business;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.DeviceDAO;
import com.axisbankcmspaypro.entity.AppDeviceBlock;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.payconnect.model.UnlockDeviceModel;
import com.payconnect.model.VerifyDeviceModel;

@Service
@Transactional
public class VerifyDeviceBusiness {

	@Autowired
	private DeviceDAO deviceDAO;

	/**
	 * this method is used to check device is bolock or not against device id
	 * 
	 * @since 09-Mar-2016
	 * @param VerifyDeviceModel
	 * @throws Exception
	 * @throws Invalid
	 *             Device Id
	 */
	public UnlockDeviceModel getVerifyDevice(VerifyDeviceModel verifyDeviceModel)
			throws Exception {
		

		if (StringUtils.isBlank(verifyDeviceModel.getDeviceId())) {
			throw new InvalidInputException("device id");
		}
		
		

		AppDeviceBlock appDeviceBlock = deviceDAO.getDeviceId(verifyDeviceModel
				.getDeviceId());

		final UnlockDeviceModel unlockDeviceModel = new UnlockDeviceModel();

		if (Objects.isNull(appDeviceBlock)) {

			unlockDeviceModel.setDeviceStatus("A");
		}

		else if (Objects.nonNull(appDeviceBlock)) {
			
			
			unlockDeviceModel.setDeviceStatus(appDeviceBlock.getDeviceStatus());
			unlockDeviceModel.setUnlockCode(appDeviceBlock.getUnlockCode());
		}
		
		return unlockDeviceModel;

	}
	
	
	

	public UnlockDeviceModel UnlockDevice(String deviceId,String unlockCode)
			throws Exception {

		if (StringUtils.isBlank(deviceId)) {
			throw new InvalidInputException("device Id");
		}

		if (StringUtils.isBlank(unlockCode)) {
			throw new InvalidInputException("Unlock code");
		}
		
		final UnlockDeviceModel unlockDeviceModel=new UnlockDeviceModel();

		 AppDeviceBlock appDeviceBlock = deviceDAO.getDeviceByUnlockCode(deviceId);
		
		

		if (Objects.isNull(appDeviceBlock)) {

			throw new DataNotFound("Invalid device id");
		}
		
		
		
		else if(Objects.nonNull(appDeviceBlock)){
			
			
			unlockDeviceModel.setUnlockCode(appDeviceBlock.getUnlockCode());
			
			unlockDeviceModel.setDeviceId(appDeviceBlock.getDeviceId());
			
		}

		return unlockDeviceModel;

	}
	
	
	 /**
     * this method is used to delete device against device id
     
     */
    public void activeDevice(String deviceId){
    	
    	
    	
    	AppDeviceBlock appDeviceBlock =deviceDAO.getDeviceId(deviceId);
	
	if(appDeviceBlock != null){
		appDeviceBlock.setDeviceStatus("A");
		appDeviceBlock.setUnlockCode("");
		deviceDAO.saveOrUpdate(appDeviceBlock);
	}
    } 
    
	
	
	

}
