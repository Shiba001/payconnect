package com.axisbankcmspaypro.business;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.dao.AppMenuDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.dao.PayProSessionTokenDAO;
import com.axisbankcmspaypro.entity.AppProfileMenu;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.common.Common;
import com.common.MessageUtil;



@Service
@Transactional
public class AppMenuBusiness {
	
	private Logger logger = Logger.getLogger(AppMenuBusiness.class);

	@Autowired
	private AppMenuDAO appMenuDAO;
	
	@Autowired
	private PayConnectUserDAO userDAO;
	
	@Autowired
	private MessageUtil messageUtil;
	
	@Autowired
	private PayProSessionTokenDAO payProSessionTokenDAO;
	
	@SuppressWarnings("unchecked")
	public JSONObject getAllMenuByUser(String userId,String system) throws Exception{
		
		if(logger.isInfoEnabled()){
			logger.info("getAllMenuByUser start");
		}
		
		final PayConnectUser payConnectUser = userDAO.getByUserIdAndApplication(userId, system);
		
		final List<AppProfileMenu> appProfileMenus = payConnectUser.getAppProfile().getAppProfileMenus();
		
		final JSONObject jsonObject = new JSONObject();
		
		final JSONArray array = new JSONArray();
		
		if(!CollectionUtils.isEmpty(appProfileMenus)) {
			for(AppProfileMenu appProfileMenu : appProfileMenus) {
				JSONObject menuObject = new JSONObject();
				
				menuObject.put(messageUtil.getBundle("menu.id"), appProfileMenu.getAppMenu().getUniqueMenuId());
				menuObject.put(messageUtil.getBundle("menu.orderid"), appProfileMenu.getAppMenu().getOrderID());
				menuObject.put(messageUtil.getBundle("menu.name"), appProfileMenu.getAppMenu().getManuName());
				
				Common.load(menuObject);
				
				array.add(menuObject);
			}
		}
		
		jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("success.code"));
		jsonObject.put(messageUtil.getBundle("response.details"), messageUtil.getBundle("success.message"));
		jsonObject.put(messageUtil.getBundle("menu.menulist"), array);
		
		return jsonObject;
	}
	
	/**
	 * 
	 * @param sessionToken
	 * @param userId
	 * @return
	 */
	public boolean isPayProSessionTokenPresent(String sessionToken,String userId){
		return payProSessionTokenDAO.isPayProSessionTokenPresent(sessionToken, userId);
	}
	
}
