/**
 * 
 */
package com.axisbankcmspaypro.business;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.dao.DownTimeDAO;
import com.axisbankcmspaypro.entity.DownTime;
import com.axisbankcmspaypro.model.DowntimeModel;
import com.common.Common;

/**
 * @author Avishek Seal
 *
 */
@Service
@Transactional
public class DowntimeBusiness {

    @Autowired
    private DownTimeDAO downTimeDAO;
    
    private final Logger logger=Logger.getLogger(DowntimeBusiness.class);
    
    
    /**
     * this method is used to get list of upcoming downtimes
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     * @throws ParseException 
     */
    public DowntimeModel getUpcomingDowntimes() throws ParseException{
    	
	final List<DownTime> downTimes = downTimeDAO.findAll();
	
	//System.out.println("size of downtime "+downTimes.size());
	
	DowntimeModel downtimeModel = new DowntimeModel();
	final Date TODAY = new Date();
	
	if(!CollectionUtils.isEmpty(downTimes)) {
		
	 for(DownTime downTime : downTimes) {
	    	
		 downtimeModel = new DowntimeModel();
		
		//System.out.println("start date from db "+downTime.getStartTimestamp());		
		//System.out.println("end date from db "+downTime.getEndTimestamp());		
		//System.out.println(TODAY);
		//System.out.println("current time with long "+TODAY.getTime());	
		//System.out.println("current time with long "+downTime.getStartTimestamp().getTime());		
		//System.out.println("current time with long "+downTime.getEndTimestamp().getTime());
		
		//Condition Satisfied within Time
		if(downTime.getStartTimestamp().getTime() <= TODAY.getTime() && downTime.getEndTimestamp().getTime() >= TODAY.getTime()){
			
			//System.out.println("inside polu when time");
			
			 logger.info("inside downtime time when between time");
			
			downtimeModel.setStartDate(Common.formatDateForDownTime(downTime.getStartTimestamp()));
			downtimeModel.setEndDate(Common.formatDateForDownTime(downTime.getEndTimestamp()));
			downtimeModel.setReason(downTime.getReason());
			
			return downtimeModel;
		}
		
		
	    }
	    // After for loop 
	    //System.err.println("When not in between time");
	 
	     logger.info("inside downtime When not in between time");
	    
	    downtimeModel = new DowntimeModel();
		
		final List<DownTime> downTimeslater= downTimeDAO.isDownTimeLater();			
		downtimeModel.setStartDate(Common.formatDateForDownTime(downTimeslater.get(0).getStartTimestamp()));		
		downtimeModel.setEndDate(Common.formatDateForDownTime(downTimeslater.get(0).getEndTimestamp()));			
		downtimeModel.setReason(downTimeslater.get(0).getReason());
	    
	   
	}
	
		return downtimeModel;
	

    }
    
    
    
    
    
    /**
     * this method is used to check whether now server down time or not
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     */
    public boolean isNowDowntime(){
	return downTimeDAO.isDownTimeNow();
    }
    
    
    
    
    
    
    
    /**
     * this method is used to get list of upcoming downtimes
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     * @throws ParseException 
     */
    public DowntimeModel getStaggingUpcomingDowntimes() throws ParseException{
    	
	final List<DownTime> downTimes = downTimeDAO.getUpcomingDownTimes();
	
	
	//final List<DowntimeModel> downtimeModels = new ArrayList<>();
	
	final DowntimeModel downtimeModel=new DowntimeModel();
	
	if(!CollectionUtils.isEmpty(downTimes)) {
		
		System.out.println("start time"+downTimes.get(0).getStartTimestamp());
		
		downtimeModel.setStartDate(Common.formatDateForDownTime(downTimes.get(0).getStartTimestamp()));
		
		downtimeModel.setEndDate(Common.formatDateForDownTime(downTimes.get(0).getEndTimestamp()));
		
		downtimeModel.setReason(downTimes.get(0).getReason());
		
		
	    }
	
	
	return downtimeModel;
	
    }
    
    
    
    
    
    
    /**
     * this method is used to get list of upcoming downtimes
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     * @throws ParseException 
     */
    public DowntimeModel UpcomingDowntimesRespectToCurrentTime() throws ParseException{
    	
	final List<DownTime> downTimes = downTimeDAO.getUpcomingDownTimes();
	
	
	//final List<DowntimeModel> downtimeModels = new ArrayList<>();
	
	final DowntimeModel downtimeModel=new DowntimeModel();
	
	if(!CollectionUtils.isEmpty(downTimes)) {
		
		downtimeModel.setStartDate(Common.formatDateForDownTime(downTimes.get(0).getStartTimestamp()));
		
		downtimeModel.setEndDate(Common.formatDateForDownTime(downTimes.get(0).getEndTimestamp()));
		
		downtimeModel.setReason(downTimes.get(0).getReason());
		
		
	    }
	
	
	return downtimeModel;
	
    }
    
    
    
    
}
