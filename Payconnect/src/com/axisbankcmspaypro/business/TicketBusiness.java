/**
 * 
 */
package com.axisbankcmspaypro.business;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.axisbankcmspaypro.business.data.validation.TicketBusinessDataValidation;
import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.axisbankcmspaypro.comon.FileHandler;
import com.axisbankcmspaypro.converter.TicketCategoryConverter;
import com.axisbankcmspaypro.converter.TicketConverter;
import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.dao.TicketCategoryDAO;
import com.axisbankcmspaypro.dao.TicketDAO;
import com.axisbankcmspaypro.entity.Application;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.enumeration.TicketStatus;
import com.axisbankcmspaypro.exception.ApplicationNullPointerException;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.axisbankcmspaypro.model.Ticket;
import com.axisbankcmspaypro.model.TicketFile;

/**
 * @author Avishek Seal
 *
 */
@Service
@Transactional
public class TicketBusiness {
	private final static Logger logger=Logger.getLogger(TicketBusiness.class);
    
    @Autowired
    private TicketBusinessDataValidation ticketBusinessDataValidation;
    
    @Autowired
    private TicketConverter ticketConverter;
    
    @Autowired
    private PayConnectUserDAO payConnectUserDAO;
    
    @Autowired
    private ApplicationDAO applicationDAO;
    
    @Autowired
    private DirectoryUtil directoryUtil;
    
    @Autowired
    private FileHandler fileHandler;
    
    @Autowired
    private TicketCategoryDAO ticketCategoryDAO;
    
    @Autowired
    private TicketDAO ticketDAO;
    
    @Autowired
    private TicketCategoryConverter ticketCategoryConverter;
    
    
    public void createTicket(Ticket ticket, String system) throws ApplicationNullPointerException, DataNotFound, IOException{
	ticketBusinessDataValidation.ticketValidate(ticket);
	
	final Application application = applicationDAO.getApplicationBySystem(system);
	
	if(application == null) {
	    throw new ApplicationNullPointerException("Applicaton is not present for the provided system value");
	}
	
	final PayConnectUser payConnectUser = payConnectUserDAO.getByUserIdAndApplication(ticket.getApplicationUserID(), system);
	
	if(payConnectUser == null) {
	    throw new DataNotFound("User does not exists in system");
	}
	
	if(!ticketCategoryDAO.isCategoryPresent(ticket.getTicketCategory())){
	    throw new DataNotFound("Invalid Ticket Category");
	}
	
	final com.axisbankcmspaypro.entity.Ticket ticket2 = ticketConverter.modelToEntity(ticket);
	
	ticket2.setApplication(application);
	ticket2.setTicketCategory(ticketCategoryDAO.getTicketCategoryByName(ticket.getTicketCategory()));
	ticket2.setPayConnectUser(payConnectUser);
	ticket2.setTicketNumber(prepareTicketNumber(system));
	ticket2.setTicketStatus(TicketStatus.OPEN);
	
	final Integer ticketID = Integer.class.cast(ticketDAO.save(ticket2));
	
	for(TicketFile ticketFile : ticket.getFiles()) {
		
	    fileHandler.saveFile(ticketFile.getFileContent(), ticketFileLocation(ticketID), ticketFile.getFileName(), ticketFile.getFileType());
	}
	
    }
    
    /**
     * this method is used to get list of tickets for a specific user id and system value
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param userID
     * @param system
     * @return
     * @throws InvalidInputException 
     */
    public List<Ticket> getUserTickets(String userID, String system, String serverAddress) throws InvalidInputException{
	if(StringUtils.isBlank(userID)) {
	    throw new InvalidInputException("User ID");
	}
	
	if(StringUtils.isBlank(system)){
	    throw new InvalidInputException("System");
	}
	
	final List<com.axisbankcmspaypro.entity.Ticket> tickets = ticketDAO.getUserTickets(userID, system);
	
	final List<Ticket> tickets2 = ticketConverter.entityToModel(tickets);
	
	if(!CollectionUtils.isEmpty(tickets2)){
	    for(Ticket ticket : tickets2) {
	    	ticket.setFiles(fileHandler.retrieveFiles(ticketFileLocation(ticket.getTicketID()), String.valueOf(ticket.getTicketID()),serverAddress));
	    }
	}
	
	return tickets2;
    }
    
    /**
     * this method is used to get list of ticket category
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @return
     */
    public List<com.axisbankcmspaypro.model.TicketCategory> getAvailableListOfTicketCategory(){
	return ticketCategoryConverter.entityToModel(ticketCategoryDAO.findAll());
    }
    
    //this method is used to prepare unique ticket number
    private final String prepareTicketNumber(String system){
	final StringBuilder builder = new StringBuilder();
	if(StringUtils.equals(system, "1")) {
	    builder.append("PA");
	} else if(StringUtils.equals(system, "2")){
	    builder.append("PP");
	}
	builder.append(String.valueOf(System.currentTimeMillis()));
	
	return builder.toString();
    }
    
    //this method is used to get the ticket file location
    private final String ticketFileLocation(Integer ticketID){
	StringBuilder builder = new StringBuilder();
	builder.append(directoryUtil.getTicketDirectoryLocation());
	builder.append(File.separator + ticketID.toString());
	System.out.println("ticket file location path is "+builder.toString());
	logger.info("ticket file location path in  is "+builder.toString());
	
	return builder.toString();
    }
}
