package com.axisbankcmspaypro.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.PayProSessionTokenDAO;
import com.axisbankcmspaypro.dao.PowerAccessSessionTokenDAO;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.payconnect.model.PayProSessionTokenModel;
import com.payconnect.model.PowerAccessSessionTokenModel;
import com.poweraccess.model.SessionToken;

@Service
@Transactional
public class PowerAccessTokenBusiness {
	
	

	@Autowired
	private PowerAccessSessionTokenDAO powerAccessSessionTokenDAO;
	
	
	/**
	 * This method is use to save or update session token details
	 * @param payProSessionTokenModel
	 * @return
	 */
	public void saveOrUpdatePowerAccessSessionToken(PowerAccessSessionTokenModel powerAccessSessionTokenModel){
		
		SessionToken sessionToken =powerAccessSessionTokenDAO.getPowerAccessSessionToken(powerAccessSessionTokenModel.getUserID());
		
		if(sessionToken == null){
			
			sessionToken = new SessionToken();
			sessionToken.setUserID(powerAccessSessionTokenModel.getUserID().trim());
			sessionToken.setSessionToken(powerAccessSessionTokenModel.getSessionToken().trim());
			powerAccessSessionTokenDAO.create(sessionToken);
		}
		
		
		else {
			sessionToken.setSessionToken(powerAccessSessionTokenModel.getSessionToken());
			powerAccessSessionTokenDAO.update(sessionToken);
		}
	}
	
	

  
  public  SessionToken getPowerAccessSessionToken(String userid){
	  return powerAccessSessionTokenDAO.getPowerAccessSessionToken(userid);
	 }
	
	

}
