/**
 * 
 */
package com.axisbankcmspaypro.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.comon.DirectoryUtil;
import com.axisbankcmspaypro.comon.FileHandler;
import com.axisbankcmspaypro.dao.CMSCoachMarkDAO;
import com.axisbankcmspaypro.entity.CMSCoachMark;
import com.axisbankcmspaypro.model.CoachMarkModel;

/**
 * @author Avishek Seal
 *
 */
@Service
@Transactional
public class CoachMarkBusiness {

    @Autowired
    private CMSCoachMarkDAO cmsCoachMarkDAO;
    
    @Autowired
    private DirectoryUtil directoryUtil;
    
    @Autowired
    private FileHandler fileHandler;
    
    /**
     * this method is used to fetch coach mark data on today's date for a particular system
     * @since 19-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     */
    public CoachMarkModel getTodaysCoachMarkModel(String system, String serverAddress){
    	
	final CMSCoachMark cmsCoachMark = cmsCoachMarkDAO.getTodaysCMSCoachMark(system);
	
	if(cmsCoachMark != null) {
	    final CoachMarkModel coachMarkModel = new CoachMarkModel();
	    
	    coachMarkModel.setMessageText(cmsCoachMark.getTextName());
	    
	    coachMarkModel.setURL(fileHandler.retrieveCoachMarkImageFile(getCoachMarkLocation(cmsCoachMark.getId()), String.valueOf(cmsCoachMark.getId()),serverAddress));
	    
	    return coachMarkModel;
	}
	
	return null;
    }
    
    //this method is used to get the coach mark image file for a particular coach mark id
    private final String getCoachMarkLocation(Integer ID){
	StringBuilder builder = new StringBuilder();
	builder.append(directoryUtil.getCoachMarkDirectoryLocation());
	builder.append("/" + ID.toString());
	return builder.toString();
    }
}
