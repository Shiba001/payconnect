package com.axisbankcmspaypro.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.PayProSessionTokenDAO;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.payconnect.model.PayProSessionTokenModel;
import com.poweraccess.model.SessionToken;

@Transactional
@Service
public class PayProSessionTokenBusiness {
	
	
	@Autowired
	private PayProSessionTokenDAO payProSessionTokenDAO;
	
	
	/**
	 * This method is use to save or update session token details
	 * @param payProSessionTokenModel
	 * @return
	 */
	public void saveOrUpdatePayProSessionToken(PayProSessionTokenModel payProSessionTokenModel){
		
		PayProSessionToken proSessionToken=payProSessionTokenDAO.getPayProSessionToken(payProSessionTokenModel.getUserID());
		
		if(proSessionToken == null){
			proSessionToken = new PayProSessionToken();
			proSessionToken.setUserId(payProSessionTokenModel.getUserID().trim());
			proSessionToken.setSessionToken(payProSessionTokenModel.getSessionToken().trim());
			payProSessionTokenDAO.create(proSessionToken);
		}else {
			proSessionToken.setSessionToken(payProSessionTokenModel.getSessionToken());
			payProSessionTokenDAO.update(proSessionToken);
		}
	}
	
	
	

	 public  PayProSessionToken getPayproSessionTokenByUser(String userid){
		
		  return payProSessionTokenDAO.getPayProSessionToken(userid);
		  
		 }
	
	
	
}
