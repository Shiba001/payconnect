package com.axisbankcmspaypro.business.data.validation;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axisbankcmspaypro.comon.Util;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.model.Ticket;
import com.common.MessageUtil;

@Component
public class TicketBusinessDataValidation implements Serializable{

	private static final long serialVersionUID = 6015883660154789107L;
	
	@Autowired
	private MessageUtil massageUtil;
	
	public void ticketValidate(Ticket ticketModel) throws DataNotFound{

		if(Util.isEmpty(ticketModel.getApplicationUserID())){
			throw new DataNotFound(massageUtil.getBundle("ticket.application.user.id.blank"));
		}
		if(Util.isEmpty(ticketModel.getReason())){
			throw new DataNotFound(massageUtil.getBundle("ticket.reason.not.found"));
		}
		if(Util.isEmpty(ticketModel.getTicketCategory())){
			throw new DataNotFound(massageUtil.getBundle("ticket.category.not.selected"));
		}
	}
}
