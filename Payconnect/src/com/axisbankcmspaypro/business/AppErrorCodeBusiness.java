package com.axisbankcmspaypro.business;


import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.dao.CMSErrorDAO;
import com.axisbankcmspaypro.entity.CMSError;
import com.common.Common;
import com.common.MessageUtil;


@Service
@Transactional
public class AppErrorCodeBusiness {
	
	private Logger logger = Logger.getLogger(AppErrorCodeBusiness.class);
	@Autowired
	private MessageUtil messageUtil;
	@Autowired
	private ApplicationDAO applicationDAO;
	
	@Autowired
	private CMSErrorDAO cmsErrorDAO;
	
	
	@SuppressWarnings("unchecked")
	public JSONObject getAllErrorCode(String system) throws Exception{
		
		if(logger.isInfoEnabled()){
			logger.info("getAllErrorCode Start");
		}
		
		JSONObject jsonObject = new JSONObject();
		
		if(!StringUtils.isNotBlank(system)){
			
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			 jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
			 
		
			return jsonObject;
		}
		
		
		
		//Application application = applicationDAO.getApplicationBySystem(system);
		
		//System.out.println("id is >>>>>>>>>>"+application.getId());
		
		//List<CMSError> cmsErrors = application.getCmsErrors();
		
		List<CMSError> cmsErrors=cmsErrorDAO.getCmsErrors(system);
		
		
		
		if(cmsErrors!=null){
			
			JSONArray jsonArray = new JSONArray();
			for(CMSError cmsError:cmsErrors){
				JSONObject resultJsonObject = new JSONObject();
				
				if(cmsError.getErrorCode() > 0){
					
					resultJsonObject.put(messageUtil.getBundle("error.api.code"),cmsError.getErrorCode());
				}else{
					resultJsonObject.put(messageUtil.getBundle("error.api.code"), "");
				}
				if(StringUtils.isNotEmpty(cmsError.getErrorMessage())){
					resultJsonObject.put(messageUtil.getBundle("error.api.message"), cmsError.getErrorMessage());
				}else{
					resultJsonObject.put(messageUtil.getBundle("error.api.message"), "");
				}
				
				
				
				Common.load(resultJsonObject);
				
				jsonArray.add(resultJsonObject);
			}

			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("success.code"));
			 jsonObject.put(messageUtil.getBundle("response.details"), messageUtil.getBundle("success.message"));
			 jsonObject.put(messageUtil.getBundle("error.code.messages"), jsonArray);//messageUtil.getBundle("error.code.messages")
			
		} else {
			
			 
			 
			 jsonObject = new JSONObject();
			 JSONArray jsonArray = new JSONArray();
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("success.code"));
			 jsonObject.put(messageUtil.getBundle("response.details"), messageUtil.getBundle("success.message"));
			 jsonObject.put(messageUtil.getBundle("error.code.messages"), jsonArray);
		}
		
		if(logger.isInfoEnabled()){
			logger.info("getAllErrorCode End");
		}
		 return jsonObject;
	}

}
