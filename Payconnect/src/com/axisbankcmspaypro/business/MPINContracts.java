package com.axisbankcmspaypro.business;

import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.axisbankcmspaypro.exception.UserIsBlocked;
import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.MPINRequestModel;

@Service
public interface MPINContracts {
	
	Map<String, Integer> MPIN_LIMIT_COUNT_POOL = new ConcurrentHashMap<>();
	
	LoginResponseModel validateMPIN(String deviceID, String mpin, String latitude, String longitude, String flag, String system) throws InvalidInputException,ParseException;
	
	void createMPIN(MPINRequestModel mpinRequestModel) throws DataNotFound, UserIsBlocked;
	
	void incrementMPINCount(String deviceID);
	
	boolean isMPINCountExceeded(String deviceID);
	
	void clearMPINCounter(String deviceID);
	
	String mpinAttemptLeft(String deviceID);
}
