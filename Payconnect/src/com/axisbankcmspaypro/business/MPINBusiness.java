package com.axisbankcmspaypro.business;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.comon.AxisbankDateFormat;
import com.axisbankcmspaypro.dao.CmsConfigurationDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.entity.CMSConfiguration;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.axisbankcmspaypro.exception.UserIsBlocked;
import com.common.Common;
import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.MPINRequestModel;

@Service
@Transactional
public class MPINBusiness implements MPINContracts{
	
	@Autowired
    private PayConnectUserDAO connectUserDAO;
	
	@Autowired
	private CmsConfigurationDAO cmsConfigurationDAO;
	
	@Autowired
	private DeviceBusiness deviceBusiness;
	
	private static Logger logger=Logger.getLogger(MPINBusiness.class);
	
	@Override
	public void createMPIN(MPINRequestModel mpinRequestModel) throws DataNotFound, UserIsBlocked {
		final PayConnectUser connectUser = connectUserDAO.getByUserIdAndApplication(mpinRequestModel.getUserID(), mpinRequestModel.getSystemValueFromApi());
		
		if(connectUser == null) {
		    throw new DataNotFound("Invalid User id and System value");
		}
		
		if(!StringUtils.equals(connectUser.getUserStatus(), "A")){
		    throw new UserIsBlocked();
		}
		
		if(StringUtils.isNotBlank(mpinRequestModel.getDeviceType())) {
		    connectUser.setDeviceType(mpinRequestModel.getDeviceType());
		}
		
		
		final List<PayConnectUser> connectUsers = connectUserDAO.getConnectUsersHavingSameDevice(mpinRequestModel.getDeviiceID());
		
		for(PayConnectUser connectUser2 : connectUsers) {
		    connectUser2.setDeviceID("");
		    connectUser2.setDeviceName("");
		    connectUser2.setDeviceToken("");
		    connectUser2.setDeviceType("");
		    connectUser2.setMpin("");
		    
		    connectUserDAO.update(connectUser2);
		}
		
		connectUser.setDeviceToken(mpinRequestModel.getDeviceToken());
		connectUser.setDeviceID(mpinRequestModel.getDeviiceID());
		connectUser.setDeviceName(mpinRequestModel.getDeviceName());
		connectUser.setLatitude(mpinRequestModel.getLatitude());
		connectUser.setLongitude(mpinRequestModel.getLongitude());
		connectUser.setMpin(mpinRequestModel.getmPin());
		connectUser.setDeviceType(mpinRequestModel.getDeviceType());
		
		connectUserDAO.update(connectUser);
	}
	
	@Override
	public LoginResponseModel validateMPIN(String deviceID, String mpin, String latitude, String longitude, String flag, String system) throws InvalidInputException, ParseException {
		final LoginResponseModel loginResponseModel = new LoginResponseModel();
		
		if(StringUtils.isBlank(flag)){
			
			 throw new InvalidInputException("Finger on is blank");
		}
		
		if(isMPINCountExceeded(deviceID)){
			loginResponseModel.setResponseCode("202");
			loginResponseModel.setResponseDetails("reached max attempt");
			
			final List<PayConnectUser> connectUsers = connectUserDAO.getUsersOfDevice(deviceID);
			
			for(PayConnectUser connectUser : connectUsers) {
				 connectUser.setDeviceID("");
			     connectUser.setDeviceToken("");
			     connectUser.setLatitude("");
			     connectUser.setLongitude("");
			     connectUser.setDeviceType("");
			     connectUser.setDeviceName("");
			     
			     connectUserDAO.update(connectUser);
			}
			
			//device table device removal code
			
			deviceBusiness.deleteDeviceId(deviceID);
			
			clearMPINCounter(deviceID);
		} else {
			PayConnectUser connectUser = null;
			
			if(StringUtils.equals(flag,"1")) {
				if(StringUtils.isBlank(deviceID)){
				    throw new InvalidInputException("Device ID is blank");
				}
				
				connectUser = connectUserDAO.getByUserIdAndApplicationAndDeviceId(deviceID, system);
			} else {
				if(StringUtils.isBlank(deviceID) && StringUtils.isBlank(mpin)){
				    throw new InvalidInputException("Device ID and MPIN is blank");
				}
				
				connectUser = connectUserDAO.getPayConnectUser(deviceID, mpin);
			}
			
			if(connectUser == null) {
				incrementMPINCount(deviceID);
				
			    throw new InvalidInputException("MPIN is Invalid", mpinAttemptLeft(deviceID));
			} else {
				clearMPINCounter(deviceID);
				
				/*if(!StringUtils.equals(connectUser.getPasswordChangeInWeb(), "1")) {
					throw new InvalidInputException("");
				}*/
				
				// for password change in web
			}
			
			loginResponseModel.setCorpID(connectUser.getCorporateID());
			
			if(connectUser.getLastPasswordChangedDate() != null) {
				loginResponseModel.setLastChangePaswordDate(connectUser.getLastPasswordChangedDate());
			} else {
				loginResponseModel.setLastChangePaswordDate("");
			}
			
			if(connectUser.getPasswordChangeInWeb() != null) {
				loginResponseModel.setPasswordChangeinWeb(connectUser.getPasswordChangeInWeb());
			} else {
				loginResponseModel.setPasswordChangeinWeb("");
			}
			
			if(connectUser.getPasswordExpiryDate() != null){
				loginResponseModel.setPasswordExpiryDate(connectUser.getPasswordExpiryDate());
			} else {
				loginResponseModel.setPasswordExpiryDate("");
			}
			
			String username=connectUser.getUserName();
			
			String[]usernames=username.split(" ");
			
			String firstName=usernames[0];
			
		    String lastName=usernames[1];
			
			loginResponseModel.setUserType(connectUser.getUserType());
			loginResponseModel.setUserID(connectUser.getUserID());
			loginResponseModel.setUserName(username);
			loginResponseModel.setFirstName(firstName);
			loginResponseModel.setLastName(lastName);
			loginResponseModel.setUserStatus(connectUser.getUserStatus());
			
			loginResponseModel.setCurrentDateTime(AxisbankDateFormat.DATE_TIME_FORMAT.format(new Date()));
			
			/*if(connectUser.getLastUpdateTimeStamp() != null) {
				loginResponseModel.setLastLogin(AxisbankDateFormat.DATE_TIME_FORMAT.format(connectUser.getLastUpdateTimeStamp()));
			}*/
			
			System.out.println("last upadted time stamp is "+connectUser.getLastUpdateTimeStamp());
			
			if(StringUtils.isNotBlank(connectUser.getLastLogonDate())){
				
				System.out.println("last login date from mpin "+connectUser.getLastLogonDate());
				
				logger.info("last login date from mpin "+ connectUser.getLastLogonDate());
				
				//loginResponseModel.setLastLogin(AxisbankDateFormat.DATE_TIME_FORMAT.format(connectUser.getLastUpdateTimeStamp()));
				
				loginResponseModel.setLastLogin(Common.formatDateForLastLogin(connectUser.getLastLogonDate()));
				
				logger.info("format last login date "+loginResponseModel.getLastLogin());
				
				System.out.println("format last login date "+loginResponseModel.getLastLogin());
				
				
			}
			
			loginResponseModel.setSystem(connectUser.getApplication().getSystem());
			
			connectUser.setLatitude(latitude);
			connectUser.setLongitude(longitude);
			
			connectUserDAO.update(connectUser);
			
			loginResponseModel.setResponseCode("200");
			loginResponseModel.setResponseDetails("Success");
		}
		
	
		
		return loginResponseModel;
	}
	
	@Override
	public void incrementMPINCount(String deviceID) {
		Integer count = MPIN_LIMIT_COUNT_POOL.get(deviceID);
		
		if(count == null){
			count = 0;
		}
		
		count++;
		
		MPIN_LIMIT_COUNT_POOL.put(deviceID, count);
	}
	
	@Override
	public boolean isMPINCountExceeded(String deviceID) {
		Integer count = MPIN_LIMIT_COUNT_POOL.get(deviceID);
		
		if(count == null){
			count = 0;
		}
		
		List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		Integer mpinCount = 1;
		
		if(CollectionUtils.isNotEmpty(cmsConfigurations)) {
			if(cmsConfigurations.get(0) != null){
				mpinCount = cmsConfigurations.get(0).getMpinAttemptCounter();
			}
		}
		
		return mpinCount == count;
	}
	
	@Override
	public String mpinAttemptLeft(String deviceID) {
		Integer count = MPIN_LIMIT_COUNT_POOL.get(deviceID);
		
		if(count == null){
			count = 0;
		}
		
		List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		Integer mpinCount = 1;
		
		if(CollectionUtils.isNotEmpty(cmsConfigurations)) {
			if(cmsConfigurations.get(0) != null){
				mpinCount = cmsConfigurations.get(0).getMpinAttemptCounter();
			}
		}
		
		return String.valueOf((mpinCount - count));
	}
	
	@Override
	public void clearMPINCounter(String deviceID) {
		MPIN_LIMIT_COUNT_POOL.remove(deviceID);
	}
	
}
