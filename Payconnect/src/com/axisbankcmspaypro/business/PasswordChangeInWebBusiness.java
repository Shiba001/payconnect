/**
 * 
 */
package com.axisbankcmspaypro.business;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.exception.DataNotFound;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.payconnect.model.PasswordChangeInWebModel;

/**
 * @author Avishek Seal
 *
 */
@Service
@Transactional
public class PasswordChangeInWebBusiness {

    @Autowired
    private PayConnectUserDAO payConnectUserDAO;
    
    /**
     * this method is used to update password change in web for a specific user and system
     * @since 09-Mar-2016
     * @author Avishek Seal
     * @param passwordChangeInWebModel
     * @throws InvalidInputException
     * @throws DataNotFound
     */
    public void updatePasswordChangeInWeb(PasswordChangeInWebModel passwordChangeInWebModel) throws InvalidInputException, DataNotFound{
	if(StringUtils.isBlank(passwordChangeInWebModel.getUserID())){
	    throw new InvalidInputException("user");
	}
	
	if(StringUtils.isBlank(passwordChangeInWebModel.getSystem())) {
	    throw new InvalidInputException("system");
	}
	
	if(StringUtils.isBlank(passwordChangeInWebModel.getValue())) {
	    throw new InvalidInputException("value");
	}
	
	final PayConnectUser connectUser = payConnectUserDAO.getByUserIdAndApplication(passwordChangeInWebModel.getUserID(), passwordChangeInWebModel.getSystem());
	
	if(connectUser == null) {
	    throw new DataNotFound("Invalid User id and system");
	}
	
	connectUser.setPasswordChangeInWeb(passwordChangeInWebModel.getValue());
	
	payConnectUserDAO.update(connectUser);
    }
    
}