package com.axisbankcmspaypro.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.entity.Application;
import com.axisbankcmspaypro.entity.PhoneMatchSetting;

@Service
@Transactional
public class CheckSmsConfirmationBusiness {

	@Autowired
	private ApplicationDAO applicationDAO;

	public PhoneMatchSetting getPhoneMatchSettingsByApplication(String system) {

		Application application = applicationDAO.getApplicationBySystem(system);

		PhoneMatchSetting phoneMatchSetting = application
				.getPhoneMatchSetting();

		return phoneMatchSetting;

	}

}
