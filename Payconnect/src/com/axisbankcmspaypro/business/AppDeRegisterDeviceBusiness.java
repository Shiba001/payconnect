package com.axisbankcmspaypro.business;


import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.AppDeRegisterDeviceDAO;
import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.dao.PayProSessionTokenDAO;
import com.axisbankcmspaypro.dao.PowerAccessSessionTokenDAO;
import com.axisbankcmspaypro.entity.Application;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.common.MessageUtil;
import com.payconnect.model.DeRegisterDevice;

@Service
@Transactional
public class AppDeRegisterDeviceBusiness {
	
	@Autowired
	private AppDeRegisterDeviceDAO appDeRegisterDeviceDAO;
	@Autowired
	private PayProSessionTokenDAO payProSessionTokenDAO;
	@Autowired
	private ApplicationDAO applicationDAO;
	@Autowired
	public MessageUtil messageUtil;
	@Autowired
	private PowerAccessSessionTokenDAO accessSessionTokenDAO;
	@Autowired
	private PayConnectUserDAO connectUserDAO;
	
	@Autowired
	private DeviceBusiness deviceBusiness;
	
	@SuppressWarnings("unchecked")
	public JSONObject deRegisterDevice(DeRegisterDevice deRegisterDevice){
		
		final JSONObject jsonObject = new JSONObject();
		
		if(!StringUtils.isNotEmpty(deRegisterDevice.getSessionToken())){
			
			jsonObject.put("ResponseCode", messageUtil.getBundle("error.sessiontoken.missing.code"));
			jsonObject.put("error", messageUtil.getBundle("error.sessiontoken.message"));
			return jsonObject;
		}
		
		if(!StringUtils.isNotEmpty(deRegisterDevice.getSystem())){
			
			
			jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
			return jsonObject;
		}
		
		if(!StringUtils.isNotEmpty(deRegisterDevice.getUserId())){
			
			
			jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
			return jsonObject;
		}
		
		Application application = applicationDAO.getApplicationBySystem(deRegisterDevice.getSystem());
		
		if(application == null){
			
		
			jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			
			jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
			
			return jsonObject;
		}
		
		
	
		
		
		if(deRegisterDevice.getSystem().equalsIgnoreCase("1")){
			
			
			
			if(!accessSessionTokenDAO.isPowerAccessSessionToken(deRegisterDevice.getUserId(), deRegisterDevice.getSessionToken())){
				
				jsonObject.put("ResponseCode", messageUtil.getBundle("error.invalid.sessiontoken.code"));
				
				 jsonObject.put("error", messageUtil.getBundle("invalid.sessiontoken.message"));
				 
				return jsonObject;
			}
			
		}else{
			
		
			
			if(!payProSessionTokenDAO.isPayProSessionTokenPresent(deRegisterDevice.getSessionToken(), deRegisterDevice.getUserId())){
				
				
				jsonObject.put("ResponseCode", messageUtil.getBundle("error.invalid.sessiontoken.code"));
				
				 jsonObject.put("error", messageUtil.getBundle("invalid.sessiontoken.message"));
				 
				return jsonObject;
			}
		}
		
		
		PayConnectUser payConnectUser= connectUserDAO.getByUserIdAndApplication(deRegisterDevice.getUserId(),deRegisterDevice.getSystem(),deRegisterDevice.getDeviceId());
		
		 if(payConnectUser==null){
			
			
			 
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("wrong.error.code"));
			 jsonObject.put(messageUtil.getBundle("error"), messageUtil.getBundle("wrong.message"));
		 }
		 
		 if(payConnectUser!=null){
		 
			 
				 
				 payConnectUser.setDeviceID("");
				 payConnectUser.setDeviceToken("");
				 payConnectUser.setLatitude("");
				 payConnectUser.setLongitude("");
				 payConnectUser.setDeviceType("");
				 payConnectUser.setDeviceName("");
				 connectUserDAO.update(payConnectUser);
				 
				 
				 deviceBusiness.deleteDeviceId(deRegisterDevice.getDeviceId());//delete device id from app device block table 
				
			 
			 
			 jsonObject.put(messageUtil.getBundle("response.code"), messageUtil.getBundle("success.code"));
			 jsonObject.put(messageUtil.getBundle("response.details"), messageUtil.getBundle("success.message"));
		 }
		return jsonObject;
		
		
	}

}
