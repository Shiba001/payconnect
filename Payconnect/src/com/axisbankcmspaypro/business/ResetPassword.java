package com.axisbankcmspaypro.business;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.ResetPasswordModel;

@Service
public interface ResetPassword {
	
	Map<String, Integer> PAYPRO_COUNT = new ConcurrentHashMap<String, Integer>();
	
	Map<String, Integer> POWERACCESS_COUNT = new ConcurrentHashMap<String, Integer>();
	
	LoginResponseModel resetPasswordForPaypro(ResetPasswordModel resetPasswordModel) throws Exception;
	
	LoginResponseModel resetPasswordForPowerAccess(ResetPasswordModel resetPasswordModel) throws Exception;
	
	void increasePayproCounter(String userID);
	
	void increasePowerAcessCounter(String userID);
	
	void clearPayproCounter(String userID);
	
	void clearPowerAccessCounter(String userID);
	
	boolean isPayproCounterLimitExceeded(String userID);
	
	boolean isPowerAccessCounterLimitExceeded(String userID);
	
	String powerAccessResetPasswordAttemptAvailable(String userName);
	
	String payproResetPasswordAttemptAvailable(String userName);
}
