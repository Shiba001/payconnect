package com.axisbankcmspaypro.business;


import java.util.List;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbank.converter.ResetPasswordConverter;
import com.axisbank.converter.ResponseConverter;
import com.axisbank.dao.PayProResetPasswordDAO;
import com.axisbank.dao.PowerAccessResetPasswordDAO;
import com.axisbank.entity.Response;
import com.axisbankcmspaypro.dao.CmsConfigurationDAO;
import com.axisbankcmspaypro.dao.DeviceDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.dao.PayProSessionTokenDAO;
import com.axisbankcmspaypro.dao.PowerAccessSessionTokenDAO;
import com.axisbankcmspaypro.entity.AppDeviceBlock;
import com.axisbankcmspaypro.entity.CMSConfiguration;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.payconnect.model.LoginResponseModel;
import com.payconnect.model.ResetPasswordModel;

@Transactional
@Service
public class ResetPasswordBusiness implements ResetPassword{
	
	@Autowired
	private ResetPasswordConverter resetPasswordConverter;
	
	@Autowired
	private PayProResetPasswordDAO payProResetPasswordDAO;
	
	@Autowired
	private PowerAccessResetPasswordDAO powerAccessResetPasswordDAO;
	
	@Autowired
	private PayConnectUserDAO payConnectUserDAO;
	
	@Autowired
	private PayProSessionTokenDAO payProSessionTokenDAO;
	
	@Autowired
	private PowerAccessSessionTokenDAO powerAccessSessionTokenDAO;
	
	@Autowired
	private ResponseConverter responseConverter;
	
	@Autowired
    private PayConnectUserDAO connectUserDAO;
	
	@Autowired
	private CmsConfigurationDAO cmsConfigurationDAO;
	
	@Autowired
	private DeviceDAO deviceDAO;

	/**
	 * this method is used to reset password for paypro
	 * @since 15-Jan-2016
	 * @author Avishek Seal
	 * @param resetPasswordModel
	 * @return
	 * @throws Exception
	 */
	@Override
	public LoginResponseModel resetPasswordForPaypro(ResetPasswordModel resetPasswordModel) throws Exception {
	    if(resetPasswordModel.isCoroporateCodeBlank() || resetPasswordModel.isOldPasswordBlank() || resetPasswordModel.isPasswordBlank() || resetPasswordModel.isUserNameBlank()|| resetPasswordModel.isDeviceIDBlank()) {
	    	throw new Exception("Something went wrong");
	    } else {
	    	if(isPayproCounterLimitExceeded(resetPasswordModel.getUserName())) {
	    		clearPayproCounter(resetPasswordModel.getUserName());
	    		
	    		deregisterDevice(resetPasswordModel.getUserName(), resetPasswordModel.getSystemValueFromApiInteger().toString());
	    		
	    		deleteDeviceFromAppDeviceBlock(resetPasswordModel.getDeviceID());//delete device id from App device block table
	    		
	    		throw new Exception("Password changing attempt limit exceeded");
	    	} else {
	    		if(payConnectUserDAO.isUserPresent(resetPasswordModel)){
	    		    if(resetPasswordModel.isSessionTokenBlank()){
	    			final LoginResponseModel loginResponseModel = new LoginResponseModel();
	    			loginResponseModel.setError("Session Token is missing.");
	    			loginResponseModel.setResponseDetails("Failure");
	    			loginResponseModel.setResponseCode("403");
	    			
	    			return loginResponseModel;
	    		    } else {
		    			if(payProSessionTokenDAO.isPayProSessionTokenPresent(resetPasswordModel.getSessionToken(), resetPasswordModel.getUserName())){
		    					Response response = payProResetPasswordDAO.getResetPasswordResponse(resetPasswordConverter.modelToEntity(resetPasswordModel));
		    					final LoginResponseModel loginResponseModel = responseConverter.entityToModel(response);
		    					
		    					if(loginResponseModel.isSuccess()) {
		    						clearPayproCounter(resetPasswordModel.getUserName());
		    					} else {
		    						increasePayproCounter(resetPasswordModel.getUserName());
		    						loginResponseModel.setResetPasswordAttemptRemaining(payproResetPasswordAttemptAvailable(resetPasswordModel.getUserName()));
		    						loginResponseModel.setResponseDetails("Failure");
		    					}
	
		    					return loginResponseModel;
		    			    } else {
			    				final LoginResponseModel loginResponseModel = new LoginResponseModel();
			    				loginResponseModel.setResponseCode("498");
			    				loginResponseModel.setResponseDetails("Failure");
			    				loginResponseModel.setResponseDetails("Invalid Session Token.");
			    				
			    				return loginResponseModel;
		    			    }
	    		    }
	    		} else {
	    		    throw new Exception("Something went wrong");
	    		}
	    	}
	    }
	    
	}
	
	
	
	
	
	
	/**
	 * this method is used to reset password for power access
	 * @since 15-Jan-2016
	 * @author Avishek Seal
	 * @param resetPasswordModel
	 * @return
	 * @throws Exception
	 */
	@Override
	public LoginResponseModel resetPasswordForPowerAccess(ResetPasswordModel resetPasswordModel) throws Exception {
	    if(resetPasswordModel.isCoroporateCodeBlank() || resetPasswordModel.isOldPasswordBlank() || resetPasswordModel.isPasswordBlank() || resetPasswordModel.isUserNameBlank()|| resetPasswordModel.isDeviceIDBlank()) {
	    	throw new Exception("Something went wrong");
	    } else {
		if(payConnectUserDAO.isUserPresent(resetPasswordModel)){
			if(isPowerAccessCounterLimitExceeded(resetPasswordModel.getUserName())) {
				clearPowerAccessCounter(resetPasswordModel.getUserName());
				
				deregisterDevice(resetPasswordModel.getUserName(), resetPasswordModel.getSystemValueFromApiInteger().toString());//deregister device
				
				deleteDeviceFromAppDeviceBlock(resetPasswordModel.getDeviceID());//delete device id from App device block table
				
				throw new Exception("Password changing attempt limit exceeded");
			} else {
				if(resetPasswordModel.isSessionTokenBlank()){
					final LoginResponseModel loginResponseModel = new LoginResponseModel();
					loginResponseModel.setError("Session Token is missing.");
					loginResponseModel.setResponseDetails("Failure");
					loginResponseModel.setResponseCode("403");
					
					return loginResponseModel;
				    } else {
					if(powerAccessSessionTokenDAO.isPowerAccessSessionToken(resetPasswordModel.getUserName(),resetPasswordModel.getSessionToken())){
						Response response = powerAccessResetPasswordDAO.getResetPasswordResponse(resetPasswordModel);
	
						final LoginResponseModel loginResponseModel = responseConverter.entityToModel(response);
						
						loginResponseModel.setUserStatus(response.getUserStatus());
						loginResponseModel.setResponseCode(response.getResponseCode());
						
						if(loginResponseModel.isSuccess()) {
							clearPowerAccessCounter(resetPasswordModel.getUserName());
						} else {
							
							increasePowerAcessCounter(resetPasswordModel.getUserName());
							//loginResponseModel.setLoginAttemptRemaining(loginAttemptAvailable(loginRequestModel.getUserName()));
							loginResponseModel.setResetPasswordAttemptRemaining(powerAccessResetPasswordAttemptAvailable(resetPasswordModel.getUserName()));
						}

	
						return loginResponseModel;
		
					} else {
						final LoginResponseModel loginResponseModel = new LoginResponseModel();
						loginResponseModel.setResponseCode("498");
						loginResponseModel.setResponseDetails("Failure");
						loginResponseModel.setError("Invalid Session Token.");
						
						return loginResponseModel;
					    }
				    }
			}
		} else {
		    throw new Exception("Something went wrong");
		}
	    }
	    
	}
	
	@Override
	public void clearPayproCounter(String userID) {
		PAYPRO_COUNT.remove(userID);
	}
	@Override
	public void increasePayproCounter(String userID) {
		Integer count = PAYPRO_COUNT.get(userID);
		
		if(count == null) {
			count = 0;
		}
		
		count++;
		
		PAYPRO_COUNT.put(userID, count);
	}
	
	@Override
	public boolean isPayproCounterLimitExceeded(String userID) {
		Integer count = PAYPRO_COUNT.get(userID);
		
		
         List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)){
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
        int credentialLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			credentialLimitFromDb = 1;
		} else {
			credentialLimitFromDb = cmsConfiguration.getResetPasswordCounter();
		}
		
		if(count == null) {
			count = 0;
		}
		
		return count == credentialLimitFromDb;
	}
	
	@Override
	public void clearPowerAccessCounter(String userID) {
		POWERACCESS_COUNT.remove(userID);
	}
	
	@Override
	public void increasePowerAcessCounter(String userID) {
		Integer count = POWERACCESS_COUNT.get(userID);
		
		if(count == null) {
			count = 0;
		}
		
		count++;
		
		POWERACCESS_COUNT.put(userID, count);
	}
	
	@Override
	public boolean isPowerAccessCounterLimitExceeded(String userID) {
		Integer count = POWERACCESS_COUNT.get(userID);
		

        List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)){
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
       int credentialLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			credentialLimitFromDb = 1;
		} else {
			credentialLimitFromDb = cmsConfiguration.getResetPasswordCounter();
		}
		
		if(count == null) {
			count = 0;
		}
		
		return count == credentialLimitFromDb;
	}
	
	
	private void deregisterDevice(String userId, String system){
		final PayConnectUser connectUser = connectUserDAO.getByUserIdAndApplication(userId, system);

		connectUser.setDeviceID("");
		connectUser.setDeviceToken("");
		connectUser.setLatitude("");
		connectUser.setLongitude("");
		connectUser.setDeviceType("");
		connectUser.setDeviceName("");
		     
		connectUserDAO.update(connectUser);
	}
	
	
	/**
	 * 
	 * @param deviceId
	 */
	private void deleteDeviceFromAppDeviceBlock(String deviceId){
		
		final AppDeviceBlock appDeviceBlock=deviceDAO.getDeviceId(deviceId);
		
		if(Objects.nonNull(appDeviceBlock)){
			
			deviceDAO.delete(appDeviceBlock);
		}
	}






	@Override
	public String powerAccessResetPasswordAttemptAvailable(String userName) {
		
   Integer powerAccessResetPasswordFailCount = POWERACCESS_COUNT.get(userName);
		
		if(powerAccessResetPasswordFailCount == null) {
			powerAccessResetPasswordFailCount = 0;
		}
		
		final List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)){
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
        int credentialLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			credentialLimitFromDb = 1;
		} else {
			credentialLimitFromDb = cmsConfiguration.getResetPasswordCounter();
			
		}
		
		int remaining = credentialLimitFromDb - powerAccessResetPasswordFailCount;
		
		return String.valueOf(remaining);
	}






	@Override
	public String payproResetPasswordAttemptAvailable(String userName) {
		
		Integer payproResetPasswordFailCount = PAYPRO_COUNT.get(userName);
		
		if(payproResetPasswordFailCount == null) {
			payproResetPasswordFailCount = 0;
		}
		
		final List<CMSConfiguration> cmsConfigurations = cmsConfigurationDAO.findAll();
		
		CMSConfiguration cmsConfiguration = null;
		
		if(!CollectionUtils.isEmpty(cmsConfigurations)){
			cmsConfiguration = cmsConfigurations.get(0);
		}
		
        int credentialLimitFromDb = 0;
		
		if(cmsConfiguration == null) {
			credentialLimitFromDb = 1;
		} else {
			credentialLimitFromDb = cmsConfiguration.getResetPasswordCounter();
			
		}
		
		int remaining = credentialLimitFromDb - payproResetPasswordFailCount;
		
		return String.valueOf(remaining);
	}
	
}
