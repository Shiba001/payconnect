package com.axisbankcmspaypro.business;

import java.sql.Timestamp;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbank.dao.PowerAccessResponseDAO;
import com.axisbankcmspaypro.entity.PayProSessionToken;
import com.axisbankpayconnect.dao.PayconnectOtacDAO;
import com.common.Common;
import com.payconnect.model.PayconnectOtacModel;
import com.poweraccess.model.OtacModel;
import com.sun.jersey.api.client.ClientResponse;

@Service
@Transactional
public class PayconnectOtacBusiness {

	private Logger logger = Logger.getLogger(PowerAccessResponseDAO.class);

	@Value("${error.400}")
	private String error400;

	@Value("${error.401}")
	private String error401;

	@Value("${error.402}")
	private String error402;

	@Value("${error.403}")
	private String error403;

	@Value("${error.404}")
	private String error404;

	@Value("${error.405}")
	private String error405;

	@Value("${error.408}")
	private String error408;

	@Value("${error.500}")
	private String error500;

	@Value("${error.502}")
	private String error502;

	@Value("${error.503}")
	private String error503;

	@Value("${error.504}")
	private String error504;

	@Value("${error.other}")
	private String errorOther;

	@Value("${oatc.sms.api.path}")
	private String otacSmsApiPath;

	@Value("${oatc.sms.api.username}")
	private String otacSmsApiUsername;

	@Value("${oatc.sms.api.password}")
	private String otacSmsApiPassword;

	@Value("${oatc.sms.api.ctype}")
	private String otacSmsApiCtype;

	@Value("${oatc.sms.api.alert}")
	private String otacSmsApiAlert;

	@Value("${oatc.sms.api.msgtype}")
	private String otacSmsApiMsgType;

	@Value("${oatc.sms.api.priority}")
	private String otacSmsApiPriority;

	@Value("${oatc.sms.api.sender}")
	private String otacSmsApiSender;

	@Value("${oatc.sms.api.msg}")
	private String otacSmsApiMsg;

	@Value("${oatc.sms.api.decode}")
	private String otacSmsApiDecode;

	@Value("${otac.sms.gateway.server}")
	private String smsServer;

	@Autowired
	private PayconnectOtacDAO PayconnectOtacDAO;
	
	
	
   /**
    * 
    * @param payconnectOtacModel
    */
	public void insertOrUpdateOctacModel(PayconnectOtacModel payconnectOtacModel) {

		OtacModel otacModel = PayconnectOtacDAO
				.getOtacByUserId(payconnectOtacModel.getUserId());

		if (otacModel == null) {

			System.out.println("inside otac insert part !!!!!!!!!!!!!!!!!");

			otacModel = new OtacModel();

			otacModel.setOtac(payconnectOtacModel.getOtac());

			otacModel.setUserID(payconnectOtacModel.getUserId());

			otacModel.setRegisteredPhone(payconnectOtacModel
					.getRegisteredPhone());

			otacModel.setStatus("0");

			otacModel.setCreateDate(new java.sql.Timestamp(payconnectOtacModel
					.getCreateDate().getTime()));

			otacModel.setIsMatch("0");

			otacModel.setIsValidated("0");

			otacModel.setSystem(payconnectOtacModel.getSystem());

			otacModel.setDeviceId(payconnectOtacModel.getDeviceId());

			PayconnectOtacDAO.create(otacModel);

		}

		else {

			System.out.println("inside otac update part !!!!!!!!!!!!!!!!!");

			otacModel.setOtac(payconnectOtacModel.getOtac());

			otacModel.setUserID(payconnectOtacModel.getUserId());

			otacModel.setRegisteredPhone(payconnectOtacModel
					.getRegisteredPhone());

			otacModel.setStatus("0");

			otacModel.setCreateDate(new java.sql.Timestamp(payconnectOtacModel
					.getCreateDate().getTime()));

			otacModel.setIsMatch("0");

			otacModel.setIsValidated("0");

			otacModel.setSystem(payconnectOtacModel.getSystem());

			otacModel.setDeviceId(payconnectOtacModel.getDeviceId());

			PayconnectOtacDAO.update(otacModel);

		}

	}
	
	
	
	
	
	
	public void UpdateOctacModelForMatchRecipient(PayconnectOtacModel payconnectOtacModel) {

		OtacModel otacModel = PayconnectOtacDAO.getOtacByUserIdAndSystemValue(payconnectOtacModel.getUserId(), payconnectOtacModel.getSystem());

		
		if(otacModel!=null) {
			
	
			//System.out.println("inside otac update part !!!!!!!!!!!!!!!!!");

			
             otacModel.setUserID(payconnectOtacModel.getUserId());

			otacModel.setStatus(payconnectOtacModel.getStatus());

			otacModel.setIsMatch(payconnectOtacModel.getIsMatch());

			otacModel.setSmsReceiveDate(new java.sql.Timestamp(payconnectOtacModel.getSmsReceiveDate().getTime()));

			otacModel.setEncStr(payconnectOtacModel.getEncriptStr());
			
			otacModel.setMobileNo(payconnectOtacModel.getMobileNo());
			
			otacModel.setDecStr(payconnectOtacModel.getDecriptStr());

			otacModel.setDeviceId(payconnectOtacModel.getDeviceId());
			
			otacModel.setSystem(payconnectOtacModel.getSystem());
			

			PayconnectOtacDAO.update(otacModel);

		}
		
		

	}
	
	
	
	
	public void UpdateOctacModelForCheckSmsConfirmation(PayconnectOtacModel payconnectOtacModel) {

		OtacModel otacModel = PayconnectOtacDAO.getOtacByUserIdAndSystemValue(payconnectOtacModel.getUserId(), payconnectOtacModel.getSystem());

		
		if(otacModel!=null) {
			
	
			System.out.println("inside checksms confirmation  otac update part !!!!!!!!!!!!!!!!!");

			

			otacModel.setVerifyDate(new java.sql.Timestamp(payconnectOtacModel.getVerifyDate().getTime()));

			

			PayconnectOtacDAO.update(otacModel);

		}
		
		

	}


	/**
	 * 
	 * @param userId
	 * @param systemValue
	 * @return
	 */
	public OtacModel getOtacByUserAndSystemValue(String userId, String systemValue) {

		return PayconnectOtacDAO.getOtacByUserIdAndSystemValue(userId, systemValue);

	}
	
	
	
	/**
	 * 
	 * @param payconnectOtacModel
	 */
	public void updateOtac(OtacModel otacModel){
		
		PayconnectOtacDAO.update(otacModel);
		
	}
	
	
	
	
	/**
	 * 
	 * @param mobile
	 * @param otac
	 * @return
	 */

	public String getResponseFromSmsApi(String mobile, String otac) {

		String sms_url = "";

		String successMessage = "OTP SEND SUCCESSFULLY";

		if (smsServer.equals("axis")) {

			sms_url = otacSmsApiPath + "?dcode=" + otacSmsApiDecode
					+ "&userid=" + otacSmsApiUsername + "&pwd="
					+ otacSmsApiPassword + "&ctype=" + otacSmsApiCtype
					+ "&alert=" + otacSmsApiAlert + "&msgtype="
					+ otacSmsApiMsgType + "&priority=" + otacSmsApiPriority
					+ "&sender=" + otacSmsApiSender + "&pno=" + mobile
					+ "&msgtxt=" + otac + otacSmsApiMsg;
		} else {

			sms_url = "http://alerts.solutionsinfini.com/api/v3/index.php?method=sms"
					+ "&api_key=A6f8b45980b586dd9a2fc8281e9dc5b34&to="
					+ mobile
					+ "&sender=MYRTAX"
					+ "&message=Hello,%20Welcome%20to%20MakeYourTax.com.%20The%20OTP%20for%20registration%20is%20"
					+ otac + "&format=json&custom=1,2&flash=0";
		}

		ClientResponse sms_response = Common.payconnectApiAcess(sms_url, "", "get");

		String sms_str = sms_response.getEntity(String.class);

		logger.info("SMS URL Login" + sms_url);

		String smsResponseStatus = String.valueOf(sms_response.getStatus());

		logger.info("SMS URL Login" + smsResponseStatus);

		if (!smsResponseStatus.equals("200")) {

			String errorMsg = this.getServerErrorMsg(smsResponseStatus);

			logger.info("axis sms gateway error "+errorMsg);

			return errorMsg;

		}

		return successMessage;

	}

	// To generate server error msg

	/**
	 * 
	 * @param errorCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getServerErrorMsg(String errorCode) {

		HashMap errorMap = new HashMap();

		errorMap.put("400", error400);
		errorMap.put("401", error401);
		errorMap.put("402", error402);
		errorMap.put("403", error403);
		errorMap.put("404", error404);
		errorMap.put("405", error405);
		errorMap.put("408", error408);
		errorMap.put("500", error500);
		errorMap.put("502", error502);
		errorMap.put("503", error503);
		errorMap.put("504", error504);

		if (errorMap.get(errorCode) != null) {
			return (String) errorMap.get(errorCode);
		} else {
			return errorOther;
		}

	}

}
