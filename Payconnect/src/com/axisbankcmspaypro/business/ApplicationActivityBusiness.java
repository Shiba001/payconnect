/**
 * 
 */
package com.axisbankcmspaypro.business;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.AppMenuDAO;
import com.axisbankcmspaypro.dao.ApplicationActivityDAO;
import com.axisbankcmspaypro.dao.ApplicationDAO;
import com.axisbankcmspaypro.dao.BatchAuthorizationActivityDAO;
import com.axisbankcmspaypro.dao.PayConnectUserDAO;
import com.axisbankcmspaypro.entity.AppMenu;
import com.axisbankcmspaypro.entity.Application;
import com.axisbankcmspaypro.entity.ApplicationActivity;
import com.axisbankcmspaypro.entity.BatchAuthorizationActivity;
import com.axisbankcmspaypro.entity.PayConnectUser;
import com.axisbankcmspaypro.enumeration.BatchStatus;
import com.axisbankcmspaypro.exception.ApplicationNullPointerException;
import com.axisbankcmspaypro.exception.DataNotFound;

/**
 * @author Avishek Seal
 *
 */
@Service
@Transactional
public class ApplicationActivityBusiness {

    @Autowired
    private ApplicationActivityDAO applicationActivityDAO;
    
    @Autowired
    private BatchAuthorizationActivityDAO batchAuthorizationActivityDAO;
    
    @Autowired
    private ApplicationDAO applicationDAO;
    
    @Autowired
    private PayConnectUserDAO payConnectUserDAO;
    
    @Autowired
    private AppMenuDAO appMenuDAO; 
    
    /**
     * this method is used before transition of each activity for a specific user and specific appllication
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param userID
     * @param system
     * @param menuUniqueValue
     * @throws DataNotFound 
     * @throws ApplicationNullPointerException 
     */
    public void executeBeforeActivityTransition(String userID, String system, String menuUniqueValue) throws DataNotFound, ApplicationNullPointerException{
	ApplicationActivity activity = applicationActivityDAO.getUserTodaysLastActivity(userID, system);
	
	final Application application = applicationDAO.getApplicationBySystem(system);
	
	if(application == null) {
	    throw new ApplicationNullPointerException("Application is not valid");
	}
	final AppMenu appMenu = appMenuDAO.getAppMenuByUniqueID(menuUniqueValue);
	
	if(appMenu == null) {
	    throw new DataNotFound("Application Menu is invalid");
	}
	
	final PayConnectUser payConnectUser = payConnectUserDAO.getByUserIdAndApplication(userID, system);
	final Date TODAY = new Date();
	
	if(activity == null) {
	    activity = new ApplicationActivity();
	    activity.setActivityDate(TODAY);
	    activity.setApplication(application);
	    activity.setAppMenu(appMenu);
	    activity.setPayConnectUser(payConnectUser);
	    activity.setEntry(TODAY);
	    activity.setDuration(0L);
	    
	    applicationActivityDAO.create(activity);
	} else {
	    activity.setExit(TODAY);
	    activity.setDuration(activity.calculateDuration());
	    
	    final ApplicationActivity activity2 = new ApplicationActivity();
	    
	    activity2.setActivityDate(TODAY);
	    activity2.setApplication(application);
	    activity2.setAppMenu(appMenu);
	    activity2.setPayConnectUser(payConnectUser);
	    activity2.setEntry(TODAY);
	    activity2.setDuration(0L);
	    
	    applicationActivityDAO.update(activity);
	    applicationActivityDAO.create(activity2);
	}
    }
    
    /**
     * this method is used to execute before logout of application for a specific user
     * @since 18-Feb-2016
     * @author Avishek Seal
     * @param userID
     * @param system
     * @param menuUniqueValue
     * @throws ApplicationNullPointerException 
     * @throws DataNotFound 
     */
    public void executeBeforeLogout(String userID, String system, String menuUniqueValue) throws ApplicationNullPointerException, DataNotFound{
	final Application application = applicationDAO.getApplicationBySystem(system);
	
	if(application == null) {
	    throw new ApplicationNullPointerException("Application is not valid");
	}
	
	final AppMenu appMenu = appMenuDAO.getAppMenuByUniqueID(menuUniqueValue);
	
	if(appMenu == null) {
	    throw new DataNotFound("Application Menu is invalid");
	}
	
	final PayConnectUser payConnectUser = payConnectUserDAO.getByUserIdAndApplication(userID, system);
	final Date TODAY = new Date();
	
	final ApplicationActivity activity = applicationActivityDAO.getUserTodaysLastActivity(userID, system);
	
	if(activity != null) {
	    activity.setExit(TODAY);
	    activity.setDuration(activity.calculateDuration());
	}
	
	final ApplicationActivity activity2 = new ApplicationActivity();
	    
	activity2.setActivityDate(TODAY);
	activity2.setApplication(application);
	activity2.setAppMenu(appMenu);
	activity2.setPayConnectUser(payConnectUser);
	activity2.setEntry(TODAY);
	activity2.setDuration(0L);
	activity2.setExit(TODAY);
	
	applicationActivityDAO.update(activity);
	applicationActivityDAO.create(activity2);
    }
    
    /**
     * this method is used to save the status of batch authorization
     * @author Avishek Seal
     * @param userID
     * @param system
     * @param batchNumber
     * @param status
     * @throws ApplicationNullPointerException
     */
    public void saveStatusOfBatchAuthorization(String userID, String system, String batchNumber, String status) throws ApplicationNullPointerException{
    	final Application application = applicationDAO.getApplicationBySystem(system);
    	
    	if(application == null) {
    	    throw new ApplicationNullPointerException("Application is not valid");
    	}
    	
    	final PayConnectUser payConnectUser = payConnectUserDAO.getByUserIdAndApplication(userID, system);
    	
    	final BatchAuthorizationActivity authorizationActivity = new BatchAuthorizationActivity();
    	
    	authorizationActivity.setActionDate(new Date());
    	authorizationActivity.setApplication(application);
    	authorizationActivity.setBatch(batchNumber);
    	
    	if(StringUtils.equals(BatchStatus.Approved.toString(), batchNumber)) {
    		authorizationActivity.setBatchStatus(BatchStatus.Approved);
    	} else {
    		authorizationActivity.setBatchStatus(BatchStatus.Rejected);
    	}
    	
    	authorizationActivity.setPayConnectUser(payConnectUser);
    	
    	batchAuthorizationActivityDAO.create(authorizationActivity);
    }
}
