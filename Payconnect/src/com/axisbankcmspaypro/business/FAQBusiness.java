/**
 * 
 */
package com.axisbankcmspaypro.business;

import java.util.List;



import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.converter.CMSFaqConverter;
import com.axisbankcmspaypro.dao.CMSFaqDAO;
import com.axisbankcmspaypro.exception.InvalidInputException;
import com.axisbankcmspaypro.model.Faq;

/**
 * @author Avishek Seal
 *
 */
@Service
@Transactional
public class FAQBusiness {
    
    @Autowired
    private CMSFaqDAO cmsFaqDAO;
    
    @Autowired
    private CMSFaqConverter cmsFaqConverter;
    
    /**
     * this method is used to get list of active FAQ for a specific application
     * @since 16-Feb-2016
     * @author Avishek Seal
     * @param system
     * @return
     * @throws InvalidInputException
     */
    public List<Faq> getActiveFAQs(String system) throws InvalidInputException {
	if(StringUtils.isBlank(system)){
	    throw new InvalidInputException("System");
	}
	
	return cmsFaqConverter.entityToModel(cmsFaqDAO.getActiveFAQsByApplication(system));
    }
}
