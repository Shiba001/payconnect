package com.axisbankcmspaypro.business;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axisbankcmspaypro.dao.VersionDAO;
import com.axisbankcmspaypro.entity.Version;
import com.axisbankcmspaypro.enumeration.DeviceType;
import com.axisbankcmspaypro.model.VersionModel;

@Service
@Transactional
public class VersionBusiness {
	
	@Autowired
	private VersionDAO versionDAO;

	/**
	 * this method is used to get current application version with respecet to device type
	 * @author Avishek Seal
	 * @param deviceType
	 * @return
	 */
	public VersionModel getCurrentVersion(String deviceType){
		if(StringUtils.isNotBlank(deviceType)) {
			if(StringUtils.equals(deviceType, DeviceType.android.name())) {
				return getVersionModel(versionDAO.getCurrentVersion(DeviceType.android));
			} else if(StringUtils.equals(deviceType, DeviceType.ios.name())) {
				return getVersionModel(versionDAO.getCurrentVersion(DeviceType.ios));
			}
		}
		
		return null;
	}
	
	//this method is used to convert version to version model
	private final VersionModel getVersionModel(Version version) {
		final VersionModel versionModel = new VersionModel();
		
		versionModel.setApiVersion(version.getApiVersion());
		versionModel.setDeviceType(version.getDeviceType());
		versionModel.setReleaseDate(version.getReleaseDate());
		versionModel.setVersionUpdateStatus(version.getVersionUpdateStatus());
		versionModel.setWarVersion(version.getWarVersion());
		
		return versionModel;
	}
}
